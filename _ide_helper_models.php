<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Accounting
 *
 * @property int $id
 * @property int $company_id
 * @property float $monthly_revenue
 * @property int $monthly_vouchers
 * @property int $staff_payroll
 * @property int $currency_id
 * @property float $first_rate
 * @property float $normal_rate
 * @property string $state
 * @property string $started_at
 * @property string $terminated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereFirstRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereMonthlyRevenue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereMonthlyVouchers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereNormalRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereStaffPayroll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereTerminatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property int $payment_document_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Book[] $books
 * @property-read \App\Company $company
 * @property-read \App\Currency $currency
 * @property-read \App\PaymentDocument $paymentDocument
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting wherePaymentDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereUserId($value)
 */
	class Accounting extends \Eloquent {}
}

namespace App{
/**
 * App\BankAccount
 *
 * @property int $id
 * @property string $titular
 * @property string $account_number
 * @property string $cci
 * @property string $bank
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereCci($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereTitular($value)
 * @mixin \Eloquent
 * @property int $currency_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereCurrencyId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 * @property-read \App\Currency $currency
 */
	class BankAccount extends \Eloquent {}
}

namespace App{
/**
 * App\Book
 *
 * @property int $id
 * @property int $folios
 * @property string|null $notary
 * @property string|null $legalization
 * @property string|null $picture
 * @property int $book_type_id
 * @property int $accounting_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereAccountingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereFolios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereLegalization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereNotary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Accounting $accounting
 * @property-read \App\BookType $bookType
 */
	class Book extends \Eloquent {}
}

namespace App{
/**
 * App\BookType
 *
 * @property int $id
 * @property string $name libro de compras, libro de ventas, libro de actas
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Book[] $books
 */
	class BookType extends \Eloquent {}
}

namespace App{
/**
 * App\CashFlow
 *
 * @property int $id
 * @property string $description
 * @property string $operation_type
 * @property string|null $state
 * @property float $amount_movement
 * @property string $done_at
 * @property int|null $voucher_id
 * @property int $user_did_id
 * @property int $user_registered_id
 * @property int|null $user_transfer_id
 * @property int|null $bank_account_id
 * @property int $payment_way_id
 * @property string|null $number_voucher_associated Ingresar número de voucher de EGRESO
 * @property string|null $picture_voucher Foto del voucher de EGRESO
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereAmountMovement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereBankAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereDoneAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereNumberVoucherAssociated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereOperationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow wherePaymentWayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow wherePictureVoucher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereUserDidId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereUserRegisteredId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereUserTransferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereVoucherId($value)
 * @mixin \Eloquent
 * @property int $currency_id
 * @property-read \App\BankAccount|null $bankAccount
 * @property-read \App\Currency $currency
 * @property-read \App\PaymentWay $paymentWay
 * @property-read \App\User $userRegistered
 * @property-read \App\User|null $userTransfer
 * @property-read \App\Voucher|null $voucher
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashFlow whereCurrencyId($value)
 */
	class CashFlow extends \Eloquent {}
}

namespace App{
/**
 * App\CashRegister
 *
 * @property int $id
 * @property string $operation_type
 * @property int $user_registered_id
 * @property int|null $payment_method_id
 * @property string $state
 * @property int|null $voucher_id
 * @property int|null $customer_id
 * @property string|null $establishment
 * @property int|null $user_transferred_id
 * @property string $done_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereDoneAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereEstablishment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereOperationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserRegisteredId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserTransferredId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereVoucherId($value)
 * @mixin \Eloquent
 * @property float $initial_cash
 * @property string $last_cash_count último arqueo de caja
 * @property int|null $user_owner_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereInitialCash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereLastCashCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserOwnerId($value)
 * @property int $user_id Dueño de la caja
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserId($value)
 */
	class CashRegister extends \Eloquent {}
}

namespace App{
/**
 * App\Company
 *
 * @property int $id
 * @property string $name
 * @property string $tax_number
 * @property string $manager_name
 * @property string $manager_last_name
 * @property string $contact_name
 * @property string $contact_last_name
 * @property string $contact_email
 * @property int $tax_regime_id
 * @property int $company_turn_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCompanyTurnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereManagerLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereManagerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTaxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTaxRegimeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $customer_id
 * @property string|null $registered_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereRegisteredAt($value)
 * @property int $office_id
 * @property int $user_id
 * @property string $slug
 * @property string $address
 * @property string $manager_dni
 * @property string $contact_phone
 * @property-read \App\Accounting $accounting
 * @property-read \App\CompanyTurn $companyTurn
 * @property-read \App\Constitution $constitution
 * @property-read \App\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \App\Office $office
 * @property-read \App\TaxRegime $taxRegime
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereManagerDni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereUserId($value)
 */
	class Company extends \Eloquent {}
}

namespace App{
/**
 * App\CompanyTurn
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 */
	class CompanyTurn extends \Eloquent {}
}

namespace App{
/**
 * App\Constitution
 *
 * @property int $id
 * @property int $customer_id
 * @property int|null $company_id
 * @property float $capital_amount
 * @property string|null $reservation_number
 * @property int $reservation_service
 * @property int|null $partners_number
 * @property int|null $additional_managers
 * @property string|null $registration_charge_picture
 * @property string $impression_state
 * @property float|null $lawyer_fee
 * @property float|null $notary_fee
 * @property string|null $public_record
 * @property string|null $notarial_letter
 * @property string|null $directory
 * @property string|null $others
 * @property float|null $devolution
 * @property string|null $picture_1
 * @property string|null $picture_2
 * @property string|null $picture_3
 * @property string|null $picture_4
 * @property string|null $done_at
 * @property string|null $signed_at
 * @property string|null $reservation_expiration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereAdditionalManagers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCapitalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereDevolution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereDirectory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereDoneAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereImpressionState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereLawyerFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereNotarialLetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereNotaryFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereOthers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePartnersNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePublicRecord($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereRegistrationChargePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereReservationExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereReservationNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereReservationService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereSignedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property int $currency_id
 * @property float $charged_amount
 * @property-read \App\Company $company
 * @property-read \App\Currency $currency
 * @property-read \App\Customer $customer
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereChargedAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereUserId($value)
 */
	class Constitution extends \Eloquent {}
}

namespace App{
/**
 * App\Currency
 *
 * @property int $id
 * @property string $name
 * @property string $symbol
 * @property string $currency_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereSymbol($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Accounting[] $accountings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankAccount[] $bankAccounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Constitution[] $constitutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 */
	class Currency extends \Eloquent {}
}

namespace App{
/**
 * App\Customer
 *
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string|null $birthday
 * @property string|null $email
 * @property int $identity_document_id
 * @property string $document_number
 * @property int $office_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereIdentityDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_registered_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUserRegisteredId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Constitution[] $constitutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \App\IdentityDocument $identityDocument
 * @property-read \App\Office $office
 * @property-read \App\User $user
 * @property string $slug
 * @property string $cellphone
 * @property int $user_id usuario que registró
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCellphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUserId($value)
 */
	class Customer extends \Eloquent {}
}

namespace App{
/**
 * App\Extra
 *
 * @property int $id
 * @property int $customer_id
 * @property int|null $company_id
 * @property string $name
 * @property string $description
 * @property int $folios
 * @property string $done_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereDoneAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereFolios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $service_type_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereServiceTypeId($value)
 * @property int $user_id
 * @property int $currency_id
 * @property float $charged_amount
 * @property-read \App\Company|null $company
 * @property-read \App\Currency $currency
 * @property-read \App\Customer|null $customer
 * @property-read \App\ServiceType $serviceType
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereChargedAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereUserId($value)
 */
	class Extra extends \Eloquent {}
}

namespace App{
/**
 * App\IdentityDocument
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 */
	class IdentityDocument extends \Eloquent {}
}

namespace App{
/**
 * App\Office
 *
 * @property int $id
 * @property string $name nombre oficina del usuario
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 */
	class Office extends \Eloquent {}
}

namespace App{
/**
 * App\PaymentDocument
 *
 * @property int $id
 * @property string $name Recibo por honorarios, recibo de caja, factura
 * @property string $titular Mónica, Villamares
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument whereTitular($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Accounting[] $accountings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 */
	class PaymentDocument extends \Eloquent {}
}

namespace App{
/**
 * App\PaymentWay
 *
 * @property int $id
 * @property string $name Depósito, efectivo en caja, Visanet
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 */
	class PaymentWay extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property int $id
 * @property string $name nombre del rol del usuario
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\ServiceType
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 */
	class ServiceType extends \Eloquent {}
}

namespace App{
/**
 * App\TaxRegime
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 */
	class TaxRegime extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property int $role_id
 * @property int $office_id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $picture
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlowDid
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlowRegistered
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlowTransfer
 * @property-read \App\CashRegister $cashRegister
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 * @property-read \App\Office $office
 * @property-read \App\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $voucherEmitted
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $voucherRegistered
 * @property string $slug
 * @property string $state
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Accounting[] $accountings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Constitution[] $constitutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereState($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Voucher
 *
 * @property int $id
 * @property int $payment_method_id
 * @property int $user_registered_id
 * @property int|null $user_cancelled_id
 * @property float $amount_payed
 * @property float $amount_total
 * @property int $service_type_id
 * @property int $constitution_id
 * @property int $accounting_id
 * @property int $extra_id
 * @property string $emitted_at
 * @property string|null $cancelled_at
 * @property string|null $expired_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereAccountingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereAmountPayed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereAmountTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereCancelledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereConstitutionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereEmittedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereExtraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereServiceTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUserCancelledId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUserRegisteredId($value)
 * @mixin \Eloquent
 * @property string $number
 * @property string|null $description
 * @property int $payment_document_id
 * @property int|null $user_emitted_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher wherePaymentDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUserEmittedId($value)
 * @property int $currency_id
 * @property-read \App\Accounting|null $accounting
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 * @property-read \App\Constitution|null $constitution
 * @property-read \App\Currency $currency
 * @property-read \App\Extra|null $extra
 * @property-read \App\PaymentDocument $paymentDocument
 * @property-read \App\ServiceType $serviceType
 * @property-read \App\User $userEmitted
 * @property-read \App\User $userRegistered
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereCurrencyId($value)
 */
	class Voucher extends \Eloquent {}
}

