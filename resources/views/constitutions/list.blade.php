@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Constituciones"),
        'icon' => "clipboard"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.constitutions.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">RUC</th>
                    <th scope="col">Oficina</th>
                    <th scope="col">Contacto</th>
                    <th scope="col">Brindado</th>
                    <th scope="col">Impresión</th>
                    <th scope="col">Captó</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($constitutions as $constitution)
                    <tr>
                        <td>{{ $constitution->getKey('') }}</td>
                        <td>
                            {{ $constitution->company->name }}
                        </td>
                        <td>
                            @isset($constitution->company->tax_number)
                                {{ $constitution->company->tax_number }}
                            @endisset
                        </td>
                        <td>{{ $constitution->company->customer->office->name }}</td>
                        <td>{{ $constitution->company->contact_name }}
                            {{ $constitution->company->contact_last_name }}</td>
                        <td>{{ $constitution->done_at }}</td>
                        <td>
                            @switch($constitution->impression_state)
                                @case(\App\Constitution::NOT_PRINTED)
                                <span>No impreso</span>
                                @break
                                @case(\App\Constitution::PRINTED_FOR_ONE)
                                <span>Para uno</span>
                                @break
                                @case(\App\Constitution::PRINTED_FOR_TWO)
                                <span>Para dos</span>
                                @break
                            @endswitch
                        </td>
                        <td>{{ $constitution->userGot->name." ".$constitution->userGot->last_name  }}</td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('constitutions.admin',
                                ['id'=>$constitution->id]) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                @can('update', \App\Constitution::class)
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('constitutions.edit', $constitution->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay servicios de constitución disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {{ $constitutions->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
