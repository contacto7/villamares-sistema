@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Constituciones"),
        'icon' => "clipboard"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $constitution->id ? route('constitutions.store'): route('constitutions.update', ['id'=>$constitution->id]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($constitution->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group">
                <label for="user_got_id">Usuario que captó*</label>
                <select
                    class="form-control {{ $errors->has('user_got_id') ? 'is-invalid': '' }}"
                    name="user_got_id"
                    id="user_got_id"
                    required
                >
                    @foreach(\App\User::get() as $user)
                        <option
                            {{ (int) old('user_got_id') === $user->id || $constitution->user_got_id === $user->id ? 'selected' : '' }}
                            value="{{ $user->id }}"
                        >{{ $user->name." ".$user->last_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label>Empresa*</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modalCompany"
                    href="#modalCompany"
                >
                    Seleccionar empresa
                </a>
            </div>
            <div class="form-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_id') ? 'is-invalid': '' }}"
                    name="company_id"
                    id="company_id"
                    placeholder="Servicio contable"
                    value="{{ old('company_id') ?: $constitution->company_id }}"
                    required
                >
            </div>
            <div class="form-group">
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_id') ? 'is-invalid': '' }}"
                    name="company_name"
                    id="company_name"
                    placeholder="Empresa"
                    value="@if(old('company_name')){{ old('company_name')  }}
                    @elseif($constitution->company_id)@php
                        echo $constitution->company->name;
                    @endphp
                    @endif"
                    readonly
                >
                @if($errors->has('company_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('company_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="currency_id">Moneda*</label>
                <select
                    class="form-control {{ $errors->has('currency_id') ? 'is-invalid': '' }}"
                    name="currency_id"
                    id="currency_id"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_id') === $id || $constitution->currency_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="charged_amount">Monto de servicio*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('charged_amount') ? 'is-invalid': '' }}"
                    name="charged_amount"
                    id="charged_amount"
                    placeholder=""
                    value="{{ old('charged_amount') ?: $constitution->charged_amount }}"
                    required
                    @cannot('changeChargedAmount', [ \App\Constitution::class, $constitution ])
                        readonly
                    @endcan
                >
                @if($errors->has('charged_amount'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('charged_amount') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="capital_amount">Capital*</label>
                <input
                    type="number"
                    class="form-control amount_changer {{ $errors->has('capital_amount') ? 'is-invalid': '' }}"
                    name="capital_amount"
                    id="capital_amount"
                    placeholder=""
                    value="{{ old('capital_amount') ?: $constitution->capital_amount }}"
                    required
                >
                @if($errors->has('capital_amount'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('capital_amount') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="reservation_number">Número de reserva</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('reservation_number') ? 'is-invalid': '' }}"
                    name="reservation_number"
                    id="reservation_number"
                    placeholder=""
                    value="{{ old('reservation_number') ?: $constitution->reservation_number }}"
                    required
                >
                @if($errors->has('reservation_number'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('reservation_number') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="reservation_service">Servicio de reserva</label>
                <select
                    class="form-control amount_changer {{ $errors->has('reservation_service') ? 'is-invalid': '' }}"
                    name="reservation_service"
                    id="reservation_service"
                    required
                >
                    <option
                        {{
                            (int) old('reservation_service') === \App\Constitution::NOT_RESERVATION_SERVICE
                            ||
                            (int) $constitution->reservation_service === \App\Constitution::NOT_RESERVATION_SERVICE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::NOT_RESERVATION_SERVICE }}"
                    >No</option>
                    <option
                        {{
                            (int) old('reservation_service') === \App\Constitution::FIRST_RESERVATION
                            ||
                            (int) $constitution->reservation_service === \App\Constitution::FIRST_RESERVATION
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::FIRST_RESERVATION }}"
                    >1era reserva</option>
                    <option
                        {{
                            (int) old('reservation_service') === \App\Constitution::SECOND_RESERVATION
                            ||
                            (int) $constitution->reservation_service === \App\Constitution::SECOND_RESERVATION
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::SECOND_RESERVATION }}"
                    >2da reserva</option>
                    <option
                        {{
                            (int) old('reservation_service') === \App\Constitution::THIRD_RESERVATION
                            ||
                            (int) $constitution->reservation_service === \App\Constitution::THIRD_RESERVATION
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::THIRD_RESERVATION }}"
                    >3era reserva</option>
                </select>
            </div>
            <div class="form-group">
                <label for="reservation_expiration">Vencimiento de reserva <span id="mensaje-fecha-reserva" class="badge badge-danger"></span></label>
                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input
                        type="text"
                        name="reservation_expiration"
                        id="reservation_expiration"
                        class="form-control datetimepicker-input {{ $errors->has('reservation_expiration') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker1"
                        value="{{ old('reservation_expiration') ?: $constitution->reservation_expiration }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('reservation_expiration'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('reservation_expiration') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="partners_number">Número de socios</label>
                <input
                    type="number"
                    class="form-control amount_changer {{ $errors->has('partners_number') ? 'is-invalid': '' }}"
                    name="partners_number"
                    id="partners_number"
                    placeholder=""
                    value="{{ old('partners_number') ?: $constitution->partners_number }}"
                    required
                >
                @if($errors->has('partners_number'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('partners_number') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="additional_managers">Número de gerentes adicionales</label>
                <input
                    type="number"
                    class="form-control amount_changer {{ $errors->has('additional_managers') ? 'is-invalid': '' }}"
                    name="additional_managers"
                    id="additional_managers"
                    placeholder=""
                    value="{{ old('additional_managers') ?: $constitution->additional_managers }}"
                    required
                >
                @if($errors->has('additional_managers'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('additional_managers') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="impression_state">Estado de impresión de pase</label>
                <select
                    class="form-control {{ $errors->has('reservation_service') ? 'is-invalid': '' }}"
                    name="impression_state"
                    id="impression_state"
                    required
                >
                    <option
                        {{
                            (int) old('impression_state') === \App\Constitution::NOT_PRINTED
                            ||
                            (int) $constitution->impression_state === \App\Constitution::NOT_PRINTED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::NOT_PRINTED }}"
                    >No impreso</option>
                    <option
                        {{
                            (int) old('impression_state') === \App\Constitution::PRINTED_FOR_ONE
                            ||
                            (int) $constitution->impression_state === \App\Constitution::PRINTED_FOR_ONE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::PRINTED_FOR_ONE }}"
                    >Impreso para uno</option>
                    <option
                        {{
                            (int) old('impression_state') === \App\Constitution::PRINTED_FOR_TWO
                            ||
                            (int) $constitution->impression_state === \App\Constitution::PRINTED_FOR_TWO
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::PRINTED_FOR_TWO }}"
                    >Impreso para dos</option>
                </select>
            </div>
            <div class="form-group">
                <label for="lawyer_fee">Comisión de abogado</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('lawyer_fee') ? 'is-invalid': '' }}"
                    name="lawyer_fee"
                    id="lawyer_fee"
                    placeholder=""
                    value="{{ old('lawyer_fee') ?: $constitution->lawyer_fee }}"
                    required
                >
                @if($errors->has('lawyer_fee'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('lawyer_fee') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="notary_fee">Comisión de notaría</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('notary_fee') ? 'is-invalid': '' }}"
                    name="notary_fee"
                    id="notary_fee"
                    placeholder=""
                    value="{{ old('notary_fee') ?: $constitution->notary_fee }}"
                    required
                >
                @if($errors->has('notary_fee'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('notary_fee') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="public_record">Registros públicos</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('public_record') ? 'is-invalid': '' }}"
                    name="public_record"
                    id="public_record"
                    placeholder=""
                    value="{{ old('public_record') ?: $constitution->public_record }}"
                    required
                >
                @if($errors->has('public_record'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('public_record') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="notarial_letter">Carta notarial</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('notarial_letter') ? 'is-invalid': '' }}"
                    name="notarial_letter"
                    id="notarial_letter"
                    placeholder=""
                    value="{{ old('notarial_letter') ?: $constitution->notarial_letter }}"
                    required
                >
                @if($errors->has('notarial_letter'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('notarial_letter') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="directory">Directorio</label>
                <input
                    type="number"
                    class="form-control amount_changer {{ $errors->has('directory') ? 'is-invalid': '' }}"
                    name="directory"
                    id="directory"
                    placeholder=""
                    value="{{ old('directory') ?: $constitution->directory }}"
                    required
                >
                @if($errors->has('directory'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('directory') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="vehicles">Vehículos</label>
                <input
                    type="number"
                    class="form-control amount_changer {{ $errors->has('vehicles') ? 'is-invalid': '' }}"
                    name="vehicles"
                    id="vehicles"
                    placeholder=""
                    value="{{ old('vehicles') ?: $constitution->vehicles }}"
                    required
                >
                @if($errors->has('vehicles'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('vehicles') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="social_object">Objeto social</label>
                <select
                    class="form-control amount_changer {{ $errors->has('social_object') ? 'is-invalid': '' }}"
                    name="social_object"
                    id="social_object"
                    required
                >
                    <option value="">Seleccionar</option>
                    <option
                        {{
                            (int) old('social_object') === \App\Constitution::NORMAL_SOCIAL_OBJECT
                            ||
                            (int) $constitution->social_object === \App\Constitution::NORMAL_SOCIAL_OBJECT
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::NORMAL_SOCIAL_OBJECT }}"
                    >Normal</option>
                    <option
                        {{
                            (int) old('social_object') === \App\Constitution::TWO_PAGES_SOCIAL_OBJECT
                            ||
                            (int) $constitution->social_object === \App\Constitution::TWO_PAGES_SOCIAL_OBJECT
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Constitution::TWO_PAGES_SOCIAL_OBJECT }}"
                    >Más de 1 hoja</option>
                </select>
                @if($errors->has('social_object'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('social_object') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="others">Adicionales</label>
                <textarea
                    class="form-control {{ $errors->has('others') ? 'is-invalid': '' }}"
                    name="others"
                    id="others"
                    placeholder=""
                >{{ old('others') ?: $constitution->others }}</textarea>
                @if($errors->has('others'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('others') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="devolution">Devolución</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('devolution') ? 'is-invalid': '' }}"
                    name="devolution"
                    id="devolution"
                    placeholder=""
                    value="{{ old('devolution') ?: $constitution->devolution }}"
                    required
                >
                @if($errors->has('devolution'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('devolution') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="done_at">Fecha de solicitud</label>
                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                    <input
                        type="text"
                        name="done_at"
                        id="done_at"
                        class="form-control datetimepicker-input {{ $errors->has('done_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker2"
                        value="{{ old('done_at') ?: $constitution->done_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('done_at'))
                        <span class="invalid-feedback">
                    <strong>{{ $errors->first('done_at') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="signed_at">Fecha de firma de constitución</label>
                <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                    <input
                        type="text"
                        name="signed_at"
                        id="signed_at"
                        class="form-control datetimepicker-input {{ $errors->has('signed_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker3"
                        value="{{ old('signed_at') ?: $constitution->signed_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('signed_at'))
                        <span class="invalid-feedback">
                    <strong>{{ $errors->first('signed_at') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="picked_at">Fecha de recojo de testimonio</label>
                <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                    <input
                        type="text"
                        name="picked_at"
                        id="picked_at"
                        class="form-control datetimepicker-input {{ $errors->has('picked_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker4"
                        value="{{ old('picked_at') ?: $constitution->picked_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('picked_at'))
                        <span class="invalid-feedback">
                    <strong>{{ $errors->first('picked_at') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="delivered_at">Fecha de entrega al cliente</label>
                <div class="input-group date" id="datetimepicker5" data-target-input="nearest">
                    <input
                        type="text"
                        name="delivered_at"
                        id="delivered_at"
                        class="form-control datetimepicker-input {{ $errors->has('delivered_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker5"
                        value="{{ old('delivered_at') ?: $constitution->delivered_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker5" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('delivered_at'))
                        <span class="invalid-feedback">
                    <strong>{{ $errors->first('delivered_at') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="delivered_person">Nombre completo de persona que recogió</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('delivered_person') ? 'is-invalid': '' }}"
                    name="delivered_person"
                    id="delivered_person"
                    placeholder=""
                    value="{{ old('delivered_person') ?: $constitution->delivered_person }}"
                    required
                >
                @if($errors->has('delivered_person'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('delivered_person') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('registration_charge_picture') ? 'is-invalid': '' }}"
                        name="registration_charge_picture"
                        id="registration_charge_picture"
                        placeholder=""
                        value="{{ old('registration_charge_picture') ?: $constitution->registration_charge_picture }}"
                    >
                    <label  class="custom-file-label" for="registration_charge_picture">
                        Imagen de cargo de inscripción en RRPP
                    </label>
                    @if($errors->has('registration_charge_picture'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('registration_charge_picture') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('picture_1') ? 'is-invalid': '' }}"
                        name="picture_1"
                        id="picture_1"
                        placeholder=""
                        value="{{ old('picture_1') ?: $constitution->picture_1 }}"
                    >
                    <label  class="custom-file-label" for="picture_1">
                        Pagos notariales
                    </label>
                    @if($errors->has('picture_1'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('picture_1') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('picture_2') ? 'is-invalid': '' }}"
                        name="picture_2"
                        id="picture_2"
                        placeholder=""
                        value="{{ old('picture_2') ?: $constitution->picture_2 }}"
                    >
                    <label  class="custom-file-label" for="picture_2">
                        Pagos registrales
                    </label>
                    @if($errors->has('picture_2'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('picture_2') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('picture_3') ? 'is-invalid': '' }}"
                        name="picture_3"
                        id="picture_3"
                        placeholder=""
                        value="{{ old('picture_3') ?: $constitution->picture_3 }}"
                    >
                    <label  class="custom-file-label" for="picture_3">
                        Cargo de entrega al cliente
                    </label>
                    @if($errors->has('picture_3'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('picture_3') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('picture_4') ? 'is-invalid': '' }}"
                        name="picture_4"
                        id="picture_4"
                        placeholder=""
                        value="{{ old('picture_4') ?: $constitution->picture_4 }}"
                    >
                    <label  class="custom-file-label" for="picture_4">
                        Devolución de efectivo
                    </label>
                    @if($errors->has('picture_4'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('picture_4') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
    <!-- Modal para la empresa -->
    @include('partials.constitutions.companyModal')

@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
            $('#datetimepicker3').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
            $('#datetimepicker4').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
            $('#datetimepicker5').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
        });
        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.company-search-btn', function(){

            let company_search;
            company_search = $("#company-search").val();

            let company_name = "";
            let without_service = 2;

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('companies.rucSearch') }}',
                data: {
                    company_search: company_search,
                    without_service: without_service
                },
                success: function (data) {
                    let company_id = null;

                    //console.log(data);

                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data['id']) != "undefined"
                        &&
                        data['name'] !== null){

                        company_id = data['id'];
                        company_name = data['name'];

                    }else{
                        console.log("No se encontró compañia");
                        return;
                    }

                    /*
                    * Si es servicio es contablidad o constitución,
                    * el id del servicio y en nombre de la empresa
                    * se guarda en un input escondido y en un input
                    * no editable, dentro del modal
                    */

                    $("#company-filtered").val(company_name);
                    $("#id-filtered").val(company_id);

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para añadir el id del servicio y el nombre del cliente
        * al input del formulario
        */
        $(document).on('click', '.company-add-btn', function(){
            //Obtenemos el tipo del servicio del selector "Tipo de servicio" en el formulario
            let company_id = $("#id-filtered").val();
            let company_name = $("#company-filtered").val();

            $("#company_id").val(company_id);
            $("#company_name").val(company_name);

            $("#company-search").val("");
            $("#company-filtered").val("");
            $("#id-filtered").val("");
        });
        /*
        * Función para añadir el id del servicio y el nombre del cliente
        * al input del formulario
        */
        $(document).on('change input', '#reservation_expiration', function(){
            let reservation_expiration = $(this).val();
            reservation_expiration = new Date(reservation_expiration);
            console.log("Fecha elegida:"+reservation_expiration);

            var limit_reservation = new Date();
            console.log("Hoy:"+limit_reservation);
            var diasOffest = 15;
            limit_reservation.setDate(limit_reservation.getDate() + diasOffest);
            console.log("En 15 días:"+limit_reservation);

            if(reservation_expiration >= limit_reservation){
                $("#mensaje-fecha-reserva").html("");
                console.log("Fecha correcta");
            }else{
                $("#mensaje-fecha-reserva").html("vence en menos de 15 días");
                console.log("Fecha incorrecta");
            }

        });
        /*
        * Función para saber el monto a cobrar
        */
        $(document).on('change input', '.amount_changer', function(){

            let capitalAmount = changeForCapitalAmount();
            let reservationService = changeForReservationService();
            let partnersNumber = changeForPartnersNumber();
            let additionalManagers = changeForAdditionalManagers();
            let directory = changeForDirectory();
            let vehicles = changeForVehicles();
            let additionalObjects = changeForAdditionalObjects();

            let amountCharged = capitalAmount + reservationService + partnersNumber + additionalManagers + directory + vehicles + additionalObjects;

            $("#charged_amount").val(amountCharged);
            $("#charged_amount").attr("value",amountCharged);
        });

        function changeForCapitalAmount(){
            if(!$("#capital_amount").val()){ return 600; }
            let change_attr = parseInt($("#capital_amount").val());
            let amount = 0;
            let base = 10000;

            if (change_attr < base) { amount = 600; } else
            if (change_attr < base*2) { amount = 650; } else
            if (change_attr < base*3) { amount = 700; } else
            if (change_attr < base*4) { amount = 800; } else
            if (change_attr < base*5) { amount = 850; } else
            if (change_attr < base*6) { amount = 950; } else
            if (change_attr < base*7) { amount = 1000; } else
            if (change_attr < base*8) { amount = 1050; } else
            if (change_attr < base*9) { amount = 1100; } else
            if (change_attr < base*10) { amount = 1200; } else
            if (change_attr < base*15) { amount = 1350; } else
            if (change_attr < base*20) { amount = 1750; } else
            if (change_attr < base*25) { amount = 1950; } else
            if (change_attr < base*30) { amount = 2250; } else
            if (change_attr < base*35) { amount = 2450; } else
            if (change_attr < base*40) { amount = 2700; } else
            if (change_attr < base*45) { amount = 2950; } else
            if (change_attr < base*50) { amount = 3250; } else
            if (change_attr < base*60) { amount = 3600; } else
            if (change_attr < base*70) { amount = 4000; }
            else{ amount = 600; }

            return amount;
        }

        function changeForReservationService(){
            if(!$("#reservation_service").val()){ return 0; }
            let change_attr = parseInt($("#reservation_service").val());
            let amount = 0;
            let base = 20;

            amount = base*(change_attr - 1);

            return amount;
        }

        function changeForPartnersNumber(){
            if(!$("#partners_number").val()){ return 0; }
            let change_attr = parseInt($("#partners_number").val());
            let amount = 0;
            let base = 25;

            if(change_attr >= 5){ amount = base*(change_attr - 4); }

            return amount;
        }

        function changeForAdditionalManagers(){
            if(!$("#additional_managers").val()){ return 0; }
            let change_attr = parseInt($("#additional_managers").val());
            let amount = 0;
            let base = 50;

            amount = base*(change_attr);

            return amount;
        }

        function changeForDirectory(){
            if(!$("#directory").val()){ return 0; }
            let change_attr = parseInt($("#directory").val());
            let amount = 0;
            let base = 250;

            if(change_attr > 0){ amount = base; }

            return amount;
        }

        function changeForVehicles(){
            if(!$("#vehicles").val()){ return 0; }
            let change_attr = parseInt($("#vehicles").val());
            let amount = 0;
            let base = 250;

            amount = base*(change_attr);

            return amount;
        }

        function changeForAdditionalObjects(){
            if(!$("#social_object").val()){ return 0; }
            let change_attr = parseInt($("#social_object").val());
            let amount = 0;
            let base = 30;

            if(change_attr === 2 ){ amount = base; }

            return amount;
        }
        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);
            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });


    </script>
@endpush
