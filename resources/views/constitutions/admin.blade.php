@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Constitución"),
        'icon' => "clipboard"
    ])
@endsection

@section('content')
    @foreach($constitutions as $constitution)
        <div class="container container-accounting-admin">
            <div class="row text-center flex-column">
                <h2 class="flex-row">{{ $constitution->company->name }}</h2>
                <h3 class="flex-row">
                    @isset($constitution->company->tax_number)
                        {{ $constitution->company->tax_number }}
                    @endisset
                </h3>
            </div>
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                    <div class="info-row">
                        <span><b>Fecha de solicitud:</b></span>
                        <span>{{ $constitution->done_at }}</span>
                    </div>
                    @isset($constitution->signed_at)
                    <div class="info-row">
                        <span><b>Fecha de firma de constitución:</b></span>
                        <span>{{ $constitution->signed_at }}</span>
                    </div>
                    @endisset
                    @isset($constitution->picked_at)
                    <div class="info-row">
                        <span><b>Fecha de recojo de testimonio:</b></span>
                        <span>{{ $constitution->picked_at }}</span>
                    </div>
                    @endisset
                    @isset($constitution->delivered_at)
                    <div class="info-row">
                        <span><b>Fecha de entrega al cliente:</b></span>
                        <span>{{ $constitution->delivered_at }}</span>
                    </div>
                    @endisset
                    @isset($constitution->delivered_person)
                    <div class="info-row">
                        <span><b>Nombre de persona que recogió:</b></span>
                        <span>{{ $constitution->delivered_person }}</span>
                    </div>
                    @endisset
                    <div class="info-row">
                        <span><b>Oficina:</b></span>
                        <span>{{ $constitution->company->customer->office->name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Usuario que registró:</b></span>
                        <span>
                        {{ $constitution->user->name }}
                            {{ $constitution->user->last_name }}
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Usuario que captó:</b></span>
                        <span>
                        {{ $constitution->userGot->name }}
                            {{ $constitution->userGot->last_name }}
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Impresión:</b></span>
                        <span>
                            @switch($constitution->impression_state)
                                @case(\App\Constitution::NOT_PRINTED)
                                <span>No impreso</span>
                                @break
                                @case(\App\Constitution::PRINTED_FOR_ONE)
                                <span>Para uno</span>
                                @break
                                @case(\App\Constitution::PRINTED_FOR_TWO)
                                <span>Para dos</span>
                                @break
                            @endswitch
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Monto de servicio:</b></span>
                        <span>
                            S/. {{ number_format($constitution->charged_amount , 2, '.', "'") }}
                        </span>
                    </div>
                </div>
            </div>
            <ul class="nav nav-tabs" id="constitutionTab" role="tablist">
                <li class="nav-item">
                    <a
                        class="nav-link active"
                        id="contact-tab"
                        data-toggle="tab"
                        href="#contact"
                        role="tab"
                        aria-controls="contact"
                        aria-selected="true"
                    >Contacto</a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link"
                        id="customer-tab"
                        data-toggle="tab"
                        href="#customer"
                        role="tab"
                        aria-controls="customer"
                        aria-selected="true"
                    >Cliente</a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link"
                        id="company-tab"
                        data-toggle="tab"
                        href="#company"
                        role="tab"
                        aria-controls="company"
                        aria-selected="true"
                    >Empresa</a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link"
                        id="constitution-tab"
                        data-toggle="tab"
                        href="#constitution"
                        role="tab"
                        aria-controls="constitution"
                        aria-selected="true"
                    >Constitución</a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link"
                        id="vouchers-tab"
                        data-toggle="tab"
                        href="#vouchers"
                        role="tab"
                        aria-controls="vouchers"
                        aria-selected="true"
                    >Comprobantes de pago</a>
                </li>
            </ul>

            <div class="tab-content" id="constitutionTabContent">
                <div class="tab-pane fade show active"
                     id="contact"
                     role="tabpanel"
                     aria-labelledby="contact-tab">
                    <div class="info-wrapper">
                        @include('partials.constitutions.contact')
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="customer"
                     role="tabpanel"
                     aria-labelledby="customer-tab">
                    <div class="info-wrapper">
                        @include('partials.constitutions.customer')
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="company"
                     role="tabpanel"
                     aria-labelledby="company-tab">
                    <div class="info-wrapper">
                        @include('partials.constitutions.company')
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="constitution"
                     role="tabpanel"
                     aria-labelledby="constitution-tab">
                    <div class="info-wrapper">
                        @include('partials.constitutions.constitutions')
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="vouchers"
                     role="tabpanel"
                     aria-labelledby="vouchers-tab">
                    <div class="row">
                        @include('partials.constitutions.vouchers')
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
