
@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Flujo de dinero"),
        'icon' => "money"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.cashflows.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Método</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Comprobante</th>
                    <th scope="col">Realizado</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($cashflows as $cashflow)
                    <tr>
                        <td>{{ $cashflow->getKey('') }}</td>
                        <td>
                            @switch($cashflow->operation_type)
                                @case(\App\CashFlow::INFLOW)
                                <span>Ingreso</span>
                                @break
                                @case(\App\CashFlow::OUTFLOW)
                                <span>Egreso</span>
                                @break
                                @case(\App\CashFlow::INTERN_TRANSFER_IN)
                                <span>
                                        Transferido
                                        de
                                        {{ $cashflow->userTransfer->name }}
                                    </span>
                                @break
                                @case(\App\CashFlow::INTERN_TRANSFER_OUT)
                                <span>
                                        Tranfirió
                                        a
                                        {{ $cashflow->userTransfer->name }}
                                    </span>
                                @break
                                @case(\App\CashFlow::BANK_TO_CASH)
                                <span>
                                    Banco a caja
                                </span>
                                @break
                                @case(\App\CashFlow::CASH_TO_BANK)
                                <span>
                                    Caja a banco
                                </span>
                                @break
                                @default
                                <span>Algo salió mal</span>
                            @endswitch
                        </td>
                        <td>{{ $cashflow->description }}</td>
                        <td>{{ $cashflow->paymentWay->name }}</td>
                        <td>
                            @switch($cashflow->state)
                                @case(\App\CashFlow::WAITING)
                                    @can('validateMovement', [\App\CashRegister::class, $cashflow])
                                        @include('partials.cashregister.updateState')
                                    @else
                                        <span>Esperando</span>
                                    @endcan
                                @break
                                @case(\App\CashFlow::REJECTED)
                                    <span>Rechazada</span>
                                @break
                                @case(\App\CashFlow::ACCEPTED)
                                    <span>Aceptada</span>
                                @break
                                @default
                            @endswitch
                        </td>
                        <td>
                            @if($cashflow->voucher)
                                <a
                                    class=""
                                    href="{{ route('vouchers.admin', $cashflow->voucher->id) }}"
                                    target="_blank"
                                >
                                    <i class="fa fa-list-alt"></i>
                                    {{ $cashflow->voucher->number }}
                                </a>
                            @elseif($cashflow->number_voucher_associated)
                                {{ $cashflow->number_voucher_associated }}
                            @endif
                        </td>
                        <td>{{ $cashflow->done_at }}</td>
                        <td>
                            <div>
                                {{ $cashflow->currency->symbol }}{{ number_format($cashflow->amount_movement, 2, '.', "'") }}
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('cashFlows.admin', $cashflow->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                @can('update', [\App\CashFlow::class])
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('cashFlows.edit', $cashflow->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay movimientos bancarios disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {{ $cashflows->appends(request()->except('page'))->links() }}
        </div>

    </div>
@endsection
