@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Flujo de dinero"),
        'icon' => "money"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $cashFlow->id ? route('cashFlows.store'): route('cashFlows.update', ['id'=>$cashFlow->id]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($cashFlow->id)
                @method('PUT')
            @endif

            @csrf
            <div class="form-group">
                <label for="description">Descripción*</label>
                <textarea
                    class="form-control {{ $errors->has('description') ? 'is-invalid': '' }}"
                    name="description"
                    id="description"
                    placeholder=""
                    required
                >{{ old('description') ?: $cashFlow->description }}</textarea>
                @if($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="operation_type">Tipo de operación*</label>
                <select
                    class="form-control {{ $errors->has('operation_type') ? 'is-invalid': '' }}"
                    name="operation_type"
                    id="operation_type"
                    required
                >
                    <option
                        {{ (int) old('operation_type') === 1 || (int) $cashFlow->operation_type === 1 ? 'selected' : '' }}
                        value="1"
                    >Ingreso</option>
                    <option
                        {{ (int) old('operation_type') === 2 || (int) $cashFlow->operation_type === 2 ? 'selected' : '' }}
                        value="2"
                    >Egreso</option>
                    <option
                        {{ (int) old('operation_type') === 4 || (int) $cashFlow->operation_type === 4 ? 'selected' : '' }}
                        value="4"
                    >Traspaso a otro usuario</option>
                    <option
                        {{ (int) old('operation_type') === 5 || (int) $cashFlow->operation_type === 5 ? 'selected' : '' }}
                        value="5"
                    >Banco a caja</option>
                    <option
                        {{ (int) old('operation_type') === 6 || (int) $cashFlow->operation_type === 6 ? 'selected' : '' }}
                        value="6"
                    >Caja a banco</option>
                </select>
            </div>
            <div class="form-group" style="display: {{ (int) old('operation_type') === 4 || (int) $cashFlow->operation_type === 4 ? '' : 'none' }}" id="user-transfer">
                <label for="user_transfer_id">Usuario al que se transfirió el dinero</label>
                <select
                    class="form-control {{ $errors->has('user_transfer_id') ? 'is-invalid': '' }}"
                    name="user_transfer_id"
                    id="user_transfer_id"
                    required
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\User::get() as $user)
                        <option
                            {{ (int) old('user_transfer_id') === $user->id || $cashFlow->user_transfer_id === $user->id ? 'selected' : '' }}
                            value="{{ $user->id }}"
                        >{{ $user->name." ".$user->last_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="currency_id">Moneda*</label>
                <select
                    class="form-control {{ $errors->has('currency_id') ? 'is-invalid': '' }}"
                    name="currency_id"
                    id="currency_id"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_id') === $id || $cashFlow->currency_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="amount_movement">Monto de operación*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('amount_movement') ? 'is-invalid': '' }}"
                    name="amount_movement"
                    id="amount_movement"
                    placeholder=""
                    value="{{ old('amount_movement') ?: $cashFlow->amount_movement }}"
                    required
                >
                @if($errors->has('amount_movement'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('amount_movement') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="bank_account_id">Banco</label>
                <select
                    class="form-control {{ $errors->has('bank_account_id') ? 'is-invalid': '' }}"
                    name="bank_account_id"
                    id="bank_account_id"
                    required
                >
                    <option value="">No se utilizó banco</option>
                    @foreach(\App\BankAccount::with('currency')->get() as $bankAccounts)
                        <option
                            {{ (int) old('bank_account_id') === $bankAccounts->id || $cashFlow->bank_account_id === $bankAccounts->id ? 'selected' : '' }}
                            value="{{ $bankAccounts->id }}"
                        >{{ $bankAccounts->titular." - ".$bankAccounts->bank." en ".$bankAccounts->currency->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="payment_way_id">Método de pago*</label>
                <select
                    class="form-control {{ $errors->has('payment_way_id') ? 'is-invalid': '' }}"
                    name="payment_way_id"
                    id="payment_way_id"
                    required
                >
                    @foreach(\App\PaymentWay::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('payment_way_id') === $id || $cashFlow->payment_way_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="done_at">Realizado en*</label>
                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input
                        type="text"
                        name="done_at"
                        id="done_at"
                        class="form-control datetimepicker-input {{ $errors->has('done_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker1"
                        value="{{ old('done_at') ?: $cashFlow->done_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('done_at'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('done_at') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group"  style="display: {{ (int) old('payment_way_id') === 4 || (int) $cashFlow->payment_way_id === 4 ? '' : 'none' }}" id="deposited-at">
                <label for="deposited_at">Depositado en</label>
                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                    <input
                        type="text"
                        name="deposited_at"
                        id="deposited_at"
                        class="form-control datetimepicker-input {{ $errors->has('deposited_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker2"
                        value="{{ old('deposited_at') ?: $cashFlow->deposited_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('deposited_at'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('deposited_at') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group service-group">
                <label for="name">Voucher interno emitido (opcional)</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#voucherModal"
                    href="#voucherModal"
                >
                    Seleccionar voucher
                </a>
            </div>
            <div class="form-group" style="display: none;">
                <input
                    type="number"
                    class="form-control {{ $errors->has('voucher_id') ? 'is-invalid': '' }}"
                    name="voucher_id"
                    id="voucher_id"
                    placeholder="Id de voucher"
                    value="{{ old('voucher_id') ?: $cashFlow->voucher_id }}"
                    required
                    autofocus
                >
            </div>
            <div class="form-group">
                <input
                    type="text"
                    class="form-control {{ $errors->has('voucher_id') ? 'is-invalid': '' }}"
                    name="voucher_number"
                    id="voucher_number"
                    placeholder="Voucher emitido"
                    value="@if(old('voucher_number')){{ old('voucher_number')  }}
                            @elseif($cashFlow->voucher_id)@php
                                echo $cashFlow->voucher->number." - ";
                                echo $cashFlow->voucher->serviceType->name.", ";
                                switch ($cashFlow->voucher->service_type_id) {
                                    case 1:
                                        echo $cashFlow->voucher->accounting->company->name;
                                        break;
                                    case 2:
                                        echo $cashFlow->voucher->constitution->company->name;
                                        break;
                                    default:
                                        if ($cashFlow->voucher->extra->company){
                                            echo $cashFlow->voucher->extra->company->name;
                                        }else{
                                            echo $cashFlow->voucher->extra->customer->name." ";
                                            echo $cashFlow->voucher->extra->customer->last_name;
                                        }
                                        break;
    }
                            @endphp
                        @endif"
                    readonly
                >
                @if($errors->has('voucher_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('voucher_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="number_voucher_associated">Número de voucher externo asociado (opcional)</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('number_voucher_associated') ? 'is-invalid': '' }}"
                    name="number_voucher_associated"
                    id="number_voucher_associated"
                    placeholder=""
                    value="{{ old('number_voucher_associated') ?: $cashFlow->number_voucher_associated }}"
                >
                @if($errors->has('number_voucher_associated'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('number_voucher_associated') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('picture_voucher') ? 'is-invalid': '' }}"
                        name="picture_voucher"
                        id="picture_voucher"
                        placeholder=""
                        value="{{ old('picture_voucher') ?: $cashFlow->picture_voucher }}"
                    >
                    <label  class="custom-file-label" for="picture_voucher">
                        Imagen de voucher externo asociado (opcional)
                    </label>
                    @if($errors->has('picture_voucher'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('picture_voucher') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
    <!-- Modal para el extra -->
    @include('partials.cashflows.voucherModal')

@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
        });
        $(function () {
            $('#datetimepicker2').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
        });
        /*
        * Función para obtener el id del voucher y su número,
        * según el RUC brindado
        */
        $(document).on('click', '.voucher-search-btn', function(){

            let voucher_search = $("#voucher-search").val();

            console.log(voucher_search);
            //Enviamos una solicitud con el tipo del servicio y el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('vouchers.numberSearch') }}',
                data: {
                    voucher_search: voucher_search
                },
                success: function (data) {
                    let voucher_id = null;
                    let service_type = null;
                    let service_name = "";
                    let customer_name = "";

                    console.log(data);
                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data.id) != "undefined"
                        &&
                        data.id !== null){

                        voucher_id = data.id;
                        service_type = data.service_type_id;
                        console.log("Voucher: "+voucher_id);
                        console.log("Servicio:  "+service_type);
                    }else{
                        console.log("No existe");
                        return;
                    }

                    /*
                    * Definimos el nombre del tipo de servicio,
                    * que luego se usará para obtener los dato
                    * de servicio del objeto regresado
                    */
                    customer_name = data.number+" - ";
                    customer_name += data.service_type.name+", ";

                    switch (service_type) {
                        case 1:
                            customer_name += data.accounting.company.name;
                            break;
                        case 2:
                            customer_name = data.constitution.company.name;
                            break;
                        default:
                            if (
                                typeof(data.extra.company) != "undefined"
                                &&
                                data.extra.company !== null){
                                customer_name += data.extra.company.name;
                            }else{
                                customer_name += data.extra.customer.name;
                                customer_name += " ";
                                customer_name += data.extra.customer.last_name;
                            }
                            break;
                    }

                    /*
                    * Si es servicio es contablidad o constitución,
                    * el id del servicio y en nombre de la empresa
                    * se guarda en un input escondido y en un input
                    * no editable, dentro del modal
                    */

                    $("#voucher-filtered").val(customer_name);
                    $("#id-filtered").val(voucher_id);

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para añadir el id del servicio y el nombre del cliente
        * al input del formulario
        */
        $(document).on('click', '.voucher-add-btn', function(){
            //Obtenemos el tipo del servicio del selector "Tipo de servicio" en el formulario
            let voucher_id = $("#id-filtered").val();
            let voucher_name = $("#voucher-filtered").val();

            //console.log(service_type);
            //console.log(service_name);
            $("#voucher_id").val(voucher_id);
            $("#voucher_number").val(voucher_name);

            $("#voucher-search").val("");
            $("#voucher-filtered").val("");
            $("#id-filtered").val("");
        });
        /*
        * Función para cambiar el botón del modal, según el servicio,
        * cuando se cambio el selector de Tipo de servicio
        */
        $(document).on('change', '#operation_type', function(){
            let operation_type = $(this).val();

            if(operation_type === "4"){
                $("#user-transfer").show();
            }else{
                $("#user-transfer").hide();
                $("#user_transfer_id").val($("#user-transfer option:first").val());
            }
        });
        /*
        * Función para cambiar el botón del modal, según el servicio,
        * cuando se cambio el selector de Tipo de servicio
        */
        $(document).on('change', '#payment_way_id', function(){
            let payment_way = $(this).val();

            if(payment_way === "1"){

                $("#deposited-at").hide();
                $("#deposited_at").val("");
                $("#bank_account_id").val($("#bank_account_id option:first").val());
            }else{
                $("#deposited-at").show();
            }
        });
        /*
        * Función para cambiar el botón del modal, según el servicio,
        * cuando se cambio el selector de Tipo de servicio
        */
        $(document).on('change', '#bank_account_id', function(){
            let bank_account = $(this).val();
            let payment_way = $("#payment_way_id").val();

            if(bank_account !== undefined || bank_account !== null){
                if(payment_way === "1"){
                    $("#payment_way_id").val(2);
                }

            }
        });
        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);
            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });


    </script>
@endpush
