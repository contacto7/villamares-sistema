@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Flujo"),
        'icon' => "money"
    ])
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <div class="row text-center flex-column">
            <h2 class="flex-row">

                @switch($cashflow->operation_type)
                    @case(\App\CashFlow::INFLOW)
                    <span>Ingreso</span>
                    @break
                    @case(\App\CashFlow::OUTFLOW)
                    <span>Egreso</span>
                    @break
                    @case(\App\CashFlow::INTERN_TRANSFER_IN)
                    <span>
                        Transferido
                        de
                        {{ $cashflow->userTransfer->name }}
                    </span>
                    @break
                    @case(\App\CashFlow::INTERN_TRANSFER_OUT)
                    <span>
                        Tranfirió
                        a
                        {{ $cashflow->userTransfer->name }}
                    </span>
                    @break
                    @case(\App\CashFlow::BANK_TO_CASH)
                    <span>
                                    Banco a caja
                                </span>
                    @break
                    @case(\App\CashFlow::CASH_TO_BANK)
                    <span>
                                    Caja a banco
                                </span>
                    @break
                    @default
                    <span>Algo salió mal</span>
                @endswitch

            </h2>
            <h3 class="flex-row">
                @switch($cashflow->state)
                    @case(\App\CashFlow::WAITING)
                    <span>Esperando</span>
                    @break
                    @case(\App\CashFlow::REJECTED)
                    <span>Rechazada</span>
                    @break
                    @case(\App\CashFlow::ACCEPTED)
                    <span>Aceptada</span>
                    @break
                    @default
                    <span></span>
                @endswitch
            </h3>
            <h3 class="flex-row">
                <span>
                    {{ $cashflow->currency->symbol }}{{ number_format($cashflow->amount_movement, 2, '.', "'") }}
                </span>
            </h3>
        </div>
        <div class="row mt-3 mb-3">
            <div class="info-wrapper">
                <div class="info-row">
                    <span><b>Descripción:</b></span>
                    <span>{{ $cashflow->description }}</span>
                </div>
                <div class="info-row">
                    <span><b>Método:</b></span>
                    <span>{{ $cashflow->paymentWay->name }}</span>
                </div>
                @if($cashflow->bankAccount)
                    <div class="info-row">
                        <span><b>Banco:</b></span>
                        <span>{{ $cashflow->bankAccount->titular." - ".
                    $cashflow->bankAccount->bank." en ".
                    $cashflow->bankAccount->currency->name }}</span>
                    </div>
                @endif
                <div class="info-row">
                    <span><b>Comprobante:</b></span>
                    <span>
                        @if($cashflow->voucher)
                            <a
                                class=""
                                href="{{ route('vouchers.admin', $cashflow->voucher->id) }}"
                                target="_blank"
                            >
                                    <i class="fa fa-list-alt"></i>
                                    {{ $cashflow->voucher->number }}
                                </a>
                        @elseif($cashflow->number_voucher_associated)
                            {{ $cashflow->number_voucher_associated }}
                        @endif

                    </span>
                </div>
                <div class="info-row">
                    <span><b>Realizado:</b></span>
                    <span>{{ $cashflow->done_at }}</span>
                </div>
                @if($cashflow->deposited_at)
                    <div class="info-row">
                        <span><b>Depositado:</b></span>
                        <span>{{ $cashflow->deposited_at }}</span>
                    </div>
                @endif
                <div class="info-row">
                    <span><b>Usuario que registró la operación:</b></span>
                    <span>
                        {{ $cashflow->userRegistered->name." ".$cashflow->userRegistered->last_name }}
                    </span>
                </div>
                @if($cashflow->picture_voucher)
                <div class="info-row">
                    <span><b>Descargar imagen asociada:</b></span>
                    <span>
                        <a href="{{ route('cashFlows.download', $cashflow->picture_voucher) }}">{{ $cashflow->picture_voucher }}</a>
                    </span>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
