@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Libros faltantes"),
        'icon' => "book"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.books.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Libro</th>
                </tr>
                </thead>
                <tbody>
                @forelse($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td>
                            <a href="{{ route('companies.admin',
                            ['id' =>$company->id]) }}">
                                <i class="fa fa-building"></i>
                                {{ $company->name }}
                            </a>
                        </td>
                        <td>
                            {{ $actualBook->name }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("Todos los libros han sido entregados")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $companies->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
