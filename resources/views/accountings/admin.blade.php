@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Servicio contable"),
        'icon' => "book"
    ])
@endsection

@section('content')
    @foreach($accountings as $accounting)
    <div class="container container-accounting-admin">
        <div class="row text-center flex-column">
            <h2 class="flex-row">{{ $accounting->company->name }}</h2>
            <h3 class="flex-row">{{ $accounting->company->tax_number }}</h3>
        </div>

        @php
            if($errors->any()) {
        @endphp
            <div class="alert alert-danger alert-dismissible fade show mt-2 mb-2" role="alert">
                <strong>Hubo un error agregando el libro,
                 por favor verifique que está colocando los datos requeridos</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @php
            }
        @endphp
        <div class="row mt-3 mb-3">
            <div class="info-wrapper">
                <div class="info-row">
                    <span><b>Estado:</b></span>
                    <span>
                        @switch($accounting->state)
                            @case(1)
                            <span>Cuenta con servicio</span>
                            @break
                            @case(2)
                            <span>Servicio terminado en {{ $accounting->terminated_at }}</span>
                            @break
                        @endswitch</span>
                </div>
                <div class="info-row">
                    <span><b>Oficina:</b></span>
                    <span>{{ $accounting->company->customer->office->name }}</span>
                </div>
                <div class="info-row">
                    <span><b>Inicio de servicio:</b></span>
                    <span>{{ $accounting->started_at }}</span>
                </div>
                <div class="info-row">
                    <span><b>Usuario que registró:</b></span>
                    <span>
                        {{ $accounting->user->name }}
                        {{ $accounting->user->last_name }}
                    </span>
                </div>
                <div class="info-row">
                    <span><b>Trabajador asignado:</b></span>
                    <span>
                        {{ $accounting->worker->name }}
                        {{ $accounting->worker->last_name }}
                    </span>
                </div>
                <div class="info-row">
                    <span><b>Coordinador:</b></span>
                    <span>
                        {{ $accounting->supervisor->name }}
                        {{ $accounting->supervisor->last_name }}
                    </span>
                </div>
                <div class="info-row">
                    <a
                        class="btn btn-outline-info btn-book"
                        data-toggle="modal"
                        data-target="#modalCompanyBook"
                        href="#modalCompanyBook"
                        data-id="0"
                    >
                        Agregar libro
                    </a>
                </div>
            </div>
        </div>
        <ul class="nav nav-tabs" id="accountingTab" role="tablist">
            <li class="nav-item">
                <a
                    class="nav-link active"
                    id="contact-tab"
                    data-toggle="tab"
                    href="#contact"
                    role="tab"
                    aria-controls="contact"
                    aria-selected="true"
                >Contacto</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    id="customer-tab"
                    data-toggle="tab"
                    href="#customer"
                    role="tab"
                    aria-controls="customer"
                    aria-selected="true"
                >Cliente</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    id="company-tab"
                    data-toggle="tab"
                    href="#company"
                    role="tab"
                    aria-controls="company"
                    aria-selected="true"
                >Empresa</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    id="contact-tab"
                    data-toggle="tab"
                    href="#accounting"
                    role="tab"
                    aria-controls="accounting"
                    aria-selected="true"
                >Contabilidad</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    id="books-tab"
                    data-toggle="tab"
                    href="#books"
                    role="tab"
                    aria-controls="books"
                    aria-selected="true"
                >Libros</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    id="vouchers-tab"
                    data-toggle="tab"
                    href="#vouchers"
                    role="tab"
                    aria-controls="vouchers"
                    aria-selected="true"
                >Comprobantes de pago</a>
            </li>
        </ul>

        <div class="tab-content" id="accountingTabContent">
            <div class="tab-pane fade show active"
                 id="contact"
                 role="tabpanel"
                 aria-labelledby="contact-tab">
                <div class="info-wrapper">
                    @include('partials.accountings.contact')
                </div>
            </div>
            <div class="tab-pane fade"
                 id="customer"
                 role="tabpanel"
                 aria-labelledby="customer-tab">
                <div class="info-wrapper">
                    @include('partials.accountings.customer')
                </div>
            </div>
            <div class="tab-pane fade"
                 id="company"
                 role="tabpanel"
                 aria-labelledby="company-tab">
                <div class="info-wrapper">
                    @include('partials.accountings.company')
                </div>
            </div>
            <div class="tab-pane fade"
                 id="accounting"
                 role="tabpanel"
                 aria-labelledby="accounting-tab">
                <div class="info-wrapper">
                    @include('partials.accountings.accountings')
                </div>
            </div>

            <div class="tab-pane fade"
                 id="books"
                 role="tabpanel"
                 aria-labelledby="books-tab">
                <div class="row">

                @forelse($accounting->books as $book)
                    <div class="col-md-6 col-lg-4">
                        @include('partials.accountings.books')
                    </div>
                @empty
                    <div>
                        La empresa no cuenta con libros contables
                    </div>
                @endforelse
                </div>
            </div>

            <div class="tab-pane fade"
                 id="vouchers"
                 role="tabpanel"
                 aria-labelledby="vouchers-tab">
                <div class="row">
                    <div class="p-2">Se muestran los ultimos 12 vouchers</div>
                    @include('partials.accountings.vouchers')
                    <a
                        href="{{ route('vouchers.accounting',['id'=>$accounting->id])  }}"
                        target="_blank"
                    >
                        Ver todos</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal para la empresa -->
    <!-- The Modal -->
    <div class="modal" id="modalCompanyBook" data-type="1">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Agregar libro</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body modal-ajax-content"></div>

            </div>
        </div>
    </div>

    @endforeach
@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.btn-book', function(){
            $(".modal-ajax-content").html('Cargando los datos...');

            let book_id = $(this).attr('data-id');
            let accounting_id = '{{ $accounting->id }}';

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('books.modalForm') }}',
                data: {
                    book_id: book_id,
                    accounting_id: accounting_id
                },
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        $(".modal-ajax-content").html(data);
                        $('#datetimepicker1').datetimepicker({
                            format: 'YYYY-MM-DD HH:mm:ss',
                            locale: 'es'
                        });
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });
    </script>
@endpush
