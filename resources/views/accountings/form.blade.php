@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Servicio contable"),
        'icon' => "book"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $accounting->id ? route('accountings.store'): route('accountings.update', ['id'=>$accounting->id]) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($accounting->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group">
                <label>Empresa*</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modalCompany"
                    href="#modalCompany"
                >
                    Seleccionar empresa
                </a>
            </div>
            <div class="form-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_id') ? 'is-invalid': '' }}"
                    name="company_id"
                    id="company_id"
                    placeholder="Servicio contable"
                    value="{{ old('company_id') ?: $accounting->company_id }}"
                    required
                >
            </div>
            <div class="form-group">
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_id') ? 'is-invalid': '' }}"
                    name="company_name"
                    id="company_name"
                    placeholder="Empresa"
                    value="@if(old('company_name')){{ old('company_name')  }}
                    @elseif($accounting->company_id)@php
                        echo $accounting->company->name;
                    @endphp
                    @endif"
                    readonly
                >
                @if($errors->has('company_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('company_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="monthly_revenue">Ingreso mensual*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('monthly_revenue') ? 'is-invalid': '' }}"
                    name="monthly_revenue"
                    id="monthly_revenue"
                    placeholder=""
                    value="{{ old('monthly_revenue') ?: $accounting->monthly_revenue }}"
                    required
                >
                @if($errors->has('monthly_revenue'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('monthly_revenue') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="monthly_vouchers">Comprobantes mensuales*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('monthly_vouchers') ? 'is-invalid': '' }}"
                    name="monthly_vouchers"
                    id="monthly_vouchers"
                    placeholder=""
                    value="{{ old('monthly_vouchers') ?: $accounting->monthly_vouchers }}"
                    required
                >
                @if($errors->has('monthly_vouchers'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('monthly_vouchers') }}</strong>
            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="staff_payroll">Personal en planilla*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('staff_payroll') ? 'is-invalid': '' }}"
                    name="staff_payroll"
                    id="staff_payroll"
                    placeholder=""
                    value="{{ old('staff_payroll') ?: $accounting->staff_payroll }}"
                    required
                >
                @if($errors->has('staff_payroll'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('staff_payroll') }}</strong>
            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="currency_id">Moneda*</label>
                <select
                    class="form-control {{ $errors->has('currency_id') ? 'is-invalid': '' }}"
                    name="currency_id"
                    id="currency_id"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_id') === $id || $accounting->currency_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="payment_document_id">Tipo de comprobante*</label>
                <select
                    class="form-control {{ $errors->has('payment_document_id') ? 'is-invalid': '' }}"
                    name="payment_document_id"
                    id="payment_document_id"
                    required
                >
                    @foreach(\App\PaymentDocument::get() as $paymentDocs)
                        <option
                            {{ (int) old('payment_document_id') === $paymentDocs->id || $accounting->payment_document_id === $paymentDocs->id ? 'selected' : '' }}
                            value="{{ $paymentDocs->id }}"
                        >{{ $paymentDocs->name }} - {{ $paymentDocs->titular }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="first_rate">Primera tarifa*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('first_rate') ? 'is-invalid': '' }}"
                    name="first_rate"
                    id="first_rate"
                    placeholder=""
                    value="{{ old('first_rate') ?: $accounting->first_rate }}"
                    required
                >
                @if($errors->has('first_rate'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('first_rate') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="normal_rate">Tarifa normal*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('normal_rate') ? 'is-invalid': '' }}"
                    name="normal_rate"
                    id="normal_rate"
                    placeholder=""
                    value="{{ old('normal_rate') ?: $accounting->normal_rate }}"
                    required
                >
                @if($errors->has('normal_rate'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('normal_rate') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="state">Estado*</label>
                <select
                    class="form-control {{ $errors->has('state') ? 'is-invalid': '' }}"
                    name="state"
                    id="state"
                    required
                >
                    <option
                        {{
                            (int) old('state') === \App\Accounting::RUNNING
                            ||
                            (int) $accounting->state === \App\Accounting::RUNNING
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Accounting::RUNNING }}"
                    >Brindando</option>
                    <option
                        {{
                            (int) old('state') === \App\Accounting::TERMINATED
                            ||
                            (int) $accounting->state === \App\Accounting::TERMINATED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Accounting::TERMINATED }}"
                    >Terminado</option>
                </select>
            </div>
            <div class="form-group">
                <label for="electronic_books">Libros electrónicos*</label>
                <select
                    class="form-control {{ $errors->has('electronic_books') ? 'is-invalid': '' }}"
                    name="electronic_books"
                    id="electronic_books"
                    required
                >
                    <option
                        {{
                            (int) old('electronic_books') === \App\Accounting::WITHOUT_ELECTRONIC_BOOKS
                            ||
                            (int) $accounting->electronic_books === \App\Accounting::WITHOUT_ELECTRONIC_BOOKS
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Accounting::WITHOUT_ELECTRONIC_BOOKS }}"
                    >Sin libros electrónicos</option>
                    <option
                        {{
                            (int) old('electronic_books') === \App\Accounting::WITH_ELECTRONIC_BOOKS
                            ||
                            (int) $accounting->electronic_books === \App\Accounting::WITH_ELECTRONIC_BOOKS
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Accounting::WITH_ELECTRONIC_BOOKS }}"
                    >Con libros electrónicos</option>
                </select>
            </div>
            <div class="form-group">
                <label for="electronic_books_date">Contabilidad electrónica iniciada en</label>
                <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                    <input
                        type="text"
                        name="electronic_books_date"
                        id="electronic_books_date"
                        class="form-control datetimepicker-input {{ $errors->has('electronic_books_date') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker3"
                        value="{{ old('electronic_books_date') ?: $accounting->electronic_books_date }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('electronic_books_date'))
                        <span class="invalid-feedback">
                    <strong>{{ $errors->first('electronic_books_date') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="electronic_books_details">Detalles de contabilidad electrónica</label>
                <textarea
                    class="form-control {{ $errors->has('electronic_books_details') ? 'is-invalid': '' }}"
                    name="electronic_books_details"
                    id="electronic_books_details"
                    placeholder=""
                    required
                >{{ old('electronic_books_details') ?: $accounting->electronic_books_details }}</textarea>
                @if($errors->has('electronic_books_details'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('electronic_books_details') }}</strong>
            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="started_at">Empezado en*</label>
                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input
                        type="text"
                        name="started_at"
                        id="started_at"
                        class="form-control datetimepicker-input {{ $errors->has('started_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker1"
                        value="{{ old('started_at') ?: $accounting->started_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('started_at'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('started_at') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="terminated_at">Terminado en</label>
                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                    <input
                        type="text"
                        name="terminated_at"
                        id="terminated_at"
                        class="form-control datetimepicker-input {{ $errors->has('terminated_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker2"
                        value="{{ old('terminated_at') ?: $accounting->terminated_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('terminated_at'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('terminated_at') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="worker_user_id">Trabajador asignado*</label>
                <select
                    class="form-control {{ $errors->has('worker_user_id') ? 'is-invalid': '' }}"
                    name="worker_user_id"
                    id="worker_user_id"
                    required
                >
                    @foreach(\App\User::get() as $user)
                        <option
                            {{ (int) old('worker_user_id') === $user->id || $accounting->worker_user_id === $user->id ? 'selected' : '' }}
                            value="{{ $user->id }}"
                        >{{ $user->name." ".$user->last_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="supervisor_user_id">Coordinador*</label>
                <select
                    class="form-control {{ $errors->has('supervisor_user_id') ? 'is-invalid': '' }}"
                    name="supervisor_user_id"
                    id="supervisor_user_id"
                    required
                >
                    @foreach(\App\User::get() as $user)
                        <option
                            {{ (int) old('supervisor_user_id') === $user->id || $accounting->supervisor_user_id === $user->id ? 'selected' : '' }}
                            value="{{ $user->id }}"
                        >{{ $user->name." ".$user->last_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control {{ $errors->has('delivery_charge') ? 'is-invalid': '' }}"
                        name="delivery_charge"
                        id="delivery_charge"
                        placeholder=""
                        value="{{ old('delivery_charge') ?: $accounting->delivery_charge }}"
                    >
                    <label  class="custom-file-label" for="delivery_charge">
                        Cargo de entrega
                    </label>
                    @if($errors->has('delivery_charge'))
                        <span class="invalid-feedback">
                    <strong>{{ $errors->first('delivery_charge') }}</strong>
                </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
    <!-- Modal para la empresa -->
    @include('partials.accountings.companyModal')

@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es'
            });
            $('#datetimepicker3').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es'
            });
        });
        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.company-search-btn', function(){

            let company_search;
            company_search = $("#company-search").val();

            let company_name = "";
            let without_service = 1;

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('companies.rucSearch') }}',
                data: {
                    company_search: company_search,
                    without_service: without_service
                },
                success: function (data) {
                    let company_id = null;

                    //console.log(data);

                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data['id']) != "undefined"
                        &&
                        data['name'] !== null){

                        company_id = data['id'];
                        company_name = data['name'];

                    }else{
                        console.log("No se encontró compañia");
                        return;
                    }

                    /*
                    * Si es servicio es contablidad o constitución,
                    * el id del servicio y en nombre de la empresa
                    * se guarda en un input escondido y en un input
                    * no editable, dentro del modal
                    */

                    $("#company-filtered").val(company_name);
                    $("#id-filtered").val(company_id);

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para añadir el id del servicio y el nombre del cliente
        * al input del formulario
        */
        $(document).on('click', '.company-add-btn', function(){
            //Obtenemos el tipo del servicio del selector "Tipo de servicio" en el formulario
            let company_id = $("#id-filtered").val();
            let company_name = $("#company-filtered").val();

            $("#company_id").val(company_id);
            $("#company_name").val(company_name);

            $("#company-search").val("");
            $("#company-filtered").val("");
            $("#id-filtered").val("");
        });


    </script>
@endpush
