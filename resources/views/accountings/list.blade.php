@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Servicio contable"),
        'icon' => "book"
    ])
@endsection

@section('content')
    <div class="container">
        @can('seeResume', \App\Accounting::class)
        <div class="row justify-content-center">
            @include('partials.accountings.resume')
        </div>
        @endcan
        <div class="row">
            @include('partials.accountings.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">RUC</th>
                    <th scope="col">Comprobante</th>
                    <th scope="col">Oficina</th>
                    <th scope="col">Trabajador</th>
                    <th scope="col">Coordinador</th>
                    <th scope="col">Inicio</th>
                    <th scope="col">Tarifa</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($accountings as $accounting)
                    <tr>
                        <td>{{ $accounting->getKey('') }}</td>
                        <td>{{ $accounting->company->name }}</td>
                        <td>{{ $accounting->company->tax_number }}</td>
                        <td>{{ $accounting->paymentDocument->name }}</td>
                        <td>{{ $accounting->company->customer->office->name }}</td>
                        <td>{{ $accounting->worker->name }}
                            {{ $accounting->worker->last_name }}</td>
                        <td>{{ $accounting->supervisor->name }}
                            {{ $accounting->supervisor->last_name }}</td>
                        <td>{{ $accounting->started_at }}</td>
                        <td>
                            {{ $accounting->currency->symbol }}{{ number_format($accounting->normal_rate, 2, '.', "'") }}
                        </td>
                        <td>
                            @switch($accounting->state)
                                @case(1)
                                <span>Brindando</span>
                                @break
                                @case(2)
                                <span>Terminado</span>
                                @break
                            @endswitch
                        </td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('accountings.admin',
                                ['id'=>$accounting->id]) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                @can('update', \App\Accounting::class)
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('accountings.edit', $accounting->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay servicios contables disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {{ $accountings->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
