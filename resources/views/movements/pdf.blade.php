<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<style>
    body{
        font-size: 14px;
    }
    td:first-child{
        font-weight: bold;
    }
    .title{
        font-size: 16px;
        text-align: center;
        text-transform: uppercase;
    }
    .subtitle{
        font-size: 16px;
        text-align: center;
        padding-bottom: 30px;
        text-transform: uppercase;
    }
    .firma-row{
        padding: 10px 0;
    }
    @page {

    }
    td{
        vertical-align: top;
    }
    tr th:nth-child(1){
        width: 30px;
    }
    tr th:nth-child(2){
        width: 50px;
    }
    tr th:nth-child(3){
        width: 270px;
    }
    tr th:nth-child(4){
        width: 80px;
    }
    tr th:nth-child(5){
        width: 70px;
    }
    tr th:nth-child(6){
        width: 70px;
    }
    table{
        border-collapse: collapse;
        max-width: 100%;
    }
    table, th, td {
        border: 1px solid black;
        padding: 5px;
    }

</style>
<div>
    <div class="title">
        Consolidado de Banco
    </div>
    @if($bankInfo)
    <div class="title">
        {{ $bankInfo->titular }} - {{ $bankInfo->bank }}
    </div>
    <div class="title">
        {{ $monthName }} DEL {{ $year }}
    </div>
    <div class="title">
        Ingresos del mes: {{ $bankInfo->currency->symbol." ".$ingresos_mes_formateado }}
    </div>
    <div class="title">
        Egresos del mes: {{ $bankInfo->currency->symbol." ".$egresos_mes_formateado }}
    </div>
    <div class="title">
        Balance del mes: {{ $bankInfo->currency->symbol." ".$balance_mes_formateado }}
    </div>
    <div class="subtitle">
        Balance General: {{ $bankInfo->currency->symbol." ".$balance_general }}
    </div>
    @endif()
    <table>
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Moneda</th>
            <th scope="col">Concepto</th>
            <th scope="col">Realizado</th>
            <th scope="col">Depósito</th>
            <th scope="col">Retiro</th>
        </tr>
        </thead>
        <tbody>
        @forelse($movements as $movement)
            <tr>
                <td>{{ $movement->id }}</td>
                <td>{{ $movement->bankAccount->currency->name }}</td>
                <td>{{ $movement->description }}</td>
                <td>{{ $movement->done_at }}</td>
                <td>
                    @if($movement->operation_type == 1 || $movement->operation_type == 6)
                        {{ $movement->currency->symbol }}{{ number_format($movement->amount_movement, 2, '.', "'") }}
                    @endif
                </td>
                <td>
                    @if($movement->operation_type == 2 || $movement->operation_type == 5)
                        -{{ $movement->currency->symbol }}{{ number_format($movement->amount_movement, 2, '.', "'") }}
                    @endif
                </td>
            </tr>
        @empty
            <tr>
                <td>{{ __("No hay movimientos bancarios disponibles")}}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>


</body>
</html>
