@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Movimientos bancarios"),
        'icon' => "credit-card"
    ])
@endsection

@section('content')
    <div class="container">
        @if(request()->has('bank'))
            @include('partials.movements.resume')
        @endif
        <div class="row">
            @include('partials.movements.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Titular</th>
                    <th scope="col">Banco</th>
                    <th scope="col">Moneda</th>
                    <th scope="col">Concepto</th>
                    <th scope="col">Realizado</th>
                    <th scope="col">Depósito</th>
                    <th scope="col">Retiro</th>
                </tr>
                </thead>
                <tbody>
                @forelse($movements as $movement)
                    <tr>
                        <td>{{ $movement->id }}</td>
                        <td>{{ $movement->bankAccount->titular }}</td>
                        <td>{{ $movement->bankAccount->bank }}</td>
                        <td>{{ $movement->bankAccount->currency->name }}</td>
                        <td>{{ $movement->description }}</td>
                        <td>{{ $movement->done_at }}</td>
                        <td>
                            @if($movement->operation_type == 1 || $movement->operation_type == 6)
                                {{ $movement->currency->symbol }}
                                {{ number_format($movement->amount_movement, 2, '.', "'") }}
                            @endif
                        </td>
                        <td>
                            @if($movement->operation_type == 2 || $movement->operation_type == 5)
                                {{ $movement->currency->symbol }}
                                -{{ number_format($movement->amount_movement, 2, '.', "'") }}
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay movimientos bancarios disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para el consolidado -->
        @include('partials.movements.consolidateModal')

        <div class="row justify-content-center">
            {{ $movements->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')

    <script>
        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.consolidate-search-btn', function(){
            let bank;
            let year;
            let month;

            bank = $("#bank-search").val();
            year = $("#year-search").val();
            month = $("#month-search").val();

            var url = "consolidate?bank="+bank+"&year="+year+"&month="+month;
            window.open(url,'_blank')
/*
            //date = year+"-"+month+"-"+"01";

            console.log(bank);
            console.log(year);
            console.log(month);
            consolidateByDateAjax(bank, year, month);
*/
        });
        function consolidateByDateAjax(bank, year, month) {
            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('movements.consolidate') }}',
                data: {
                    bank: bank,
                    year: year,
                    month: month
                },
                success: function (data) {
                    console.log(data);
                    return data;
                    window.open('http://stackoverflow.com/', '_blank');


                },error:function(){
                    console.log(data);
                }
            });
        }
    </script>
@endpush
