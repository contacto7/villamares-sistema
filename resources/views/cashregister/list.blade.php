@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Registro de caja"),
        'icon' => "calculator"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.cashregister.search')
        </div>
        <div class="row mb-2 pl-2 pr-2">
            <div class="card card-cash-register">
                <div class="card-body">
                    <b>ÚLTIMO ARQUEO: </b>
                    <span> {{ \Carbon\Carbon::parse($last_count->last_cash_count)->format('d/m/Y h:i:s') }}</span>
                    <br>
                    <b>TOTAL EFECTIVO SOLES: </b>
                    <span> S/. {{ number_format($total_cash_soles, 2, '.', "'") }}</span>
                    <br>
                    <b>TOTAL EFECTIVO DOLARES: </b>
                    <span> $ {{ number_format($total_cash_dolares, 2, '.', "'") }}</span>
                    <br>
                    @isset($cashflows[0])
                    <b>USUARIO: </b>
                    <span> {{ $cashflows[0]->userRegistered->name }}  {{ $cashflows[0]->userRegistered->last_name }} </span>
                    @endisset
                    <br>
                    <div class="btn-group btn-group-block">
                        <a
                            class="form-control btn btn-villamares"
                            href="{{ route('cashFlows.create') }}"
                            target="_blank"
                        >
                            Añadir flujo
                        </a>
                        @can('balanceCash', \App\CashRegister::class)
                            @isset($cashflows[0])
                                <form
                                    method="POST"
                                    action="{{ route('cashFlows.arqueo') }}"
                                    novalidate
                                >
                                    @method('PUT')
                                    @csrf
                                    <input type="hidden" value="{{ auth()->user()->id }}" name="id">
                                    <input type="hidden" value="{{ auth()->user()->role_id  }}" name="role">
                                    <button type="submit" class="btn btn btn-villamares">Arquear</button>
                                </form>
                            @endisset
                        @endcan()
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Método</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Comprobante</th>
                    <th scope="col">Realizado</th>
                    <th scope="col">Monto</th>
                </tr>
                </thead>
                <tbody>
                @forelse($cashflows as $cashflow)
                    <tr>
                        <td>{{ $cashflow->getKey('') }}</td>
                        <td>
                            @switch($cashflow->operation_type)
                                @case(\App\CashFlow::INFLOW)
                                    <span>Ingreso</span>
                                @break
                                @case(\App\CashFlow::OUTFLOW)
                                    <span>Egreso</span>
                                @break
                                @case(\App\CashFlow::INTERN_TRANSFER_IN)
                                    <span>
                                        Transferido
                                        de
                                        {{ $cashflow->userTransfer->name }}
                                    </span>
                                @break
                                @case(\App\CashFlow::INTERN_TRANSFER_OUT)
                                    <span>
                                        Tranfirió
                                        a
                                        {{ $cashflow->userTransfer->name }}
                                    </span>
                                @break
                                @case(\App\CashFlow::BANK_TO_CASH)
                                <span>
                                    Banco a caja
                                </span>
                                @break
                                @case(\App\CashFlow::CASH_TO_BANK)
                                <span>
                                    Caja a banco
                                </span>
                                @break
                                @default
                                    <span>Algo salió mal</span>
                            @endswitch
                        </td>
                        <td>{{ $cashflow->description }}</td>
                        <td>{{ $cashflow->paymentWay->name }}</td>
                        <td>
                            @switch($cashflow->state)
                                @case(\App\CashFlow::WAITING)
                                    @can('validateMovement', [\App\CashRegister::class, $cashflow])
                                        @include('partials.cashregister.updateState')
                                    @else
                                        <span>Esperando</span>
                                    @endcan
                                @break
                                @case(\App\CashFlow::REJECTED)
                                <span>Rechazada</span>
                                @break
                                @case(\App\CashFlow::ACCEPTED)
                                <span>Aceptada</span>
                                @break
                                @default
                            @endswitch
                        </td>
                        <td>
                            @if($cashflow->voucher)
                                <a
                                    class=""
                                    href="{{ route('vouchers.admin', $cashflow->voucher->id) }}"
                                    target="_blank"
                                >
                                    <i class="fa fa-list-alt"></i>
                                    {{ $cashflow->voucher->number }}
                                </a>
                            @elseif($cashflow->number_voucher_associated)
                                {{ $cashflow->number_voucher_associated }}
                            @endif
                        </td>
                        <td>{{ $cashflow->done_at }}</td>
                        <td>
                            <div>
                                {{ $cashflow->currency->symbol }}{{ number_format($cashflow->amount_movement, 2, '.', "'") }}
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay movimientos bancarios disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

    </div>
@endsection
