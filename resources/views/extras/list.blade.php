@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Extras"),
        'icon' => "briefcase"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.extras.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Servicio</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Brindado</th>
                    <th scope="col">Captó</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($extras as $extra)
                    <tr>
                        <td>{{ $extra->getKey('') }}</td>
                        <td>{{ $extra->serviceType->name }}</td>
                        <td>
                            @isset($extra->customer->id)
                                {{ $extra->customer->name }}
                                {{ $extra->customer->last_name }}<br>
                                {{ $extra->customer->document_number }}
                            @endisset
                        </td>
                        <td>
                            @isset($extra->company->id)
                                {{ $extra->company->name }}<br>
                                {{ $extra->company->tax_number }}
                            @endisset
                        </td>
                        <td>{{ $extra->done_at }}</td>
                        <td>{{ $extra->userGot->name." ".$extra->userGot->last_name  }}</td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('extras.admin',
                                ['id'=>$extra->id]) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                @can('update', \App\Extra::class)
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('extras.edit', $extra->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay otros servicios disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row justify-content-center">
            {{ $extras->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
