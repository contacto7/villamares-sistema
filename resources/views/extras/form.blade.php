@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Extra"),
        'icon' => "briefcase"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $extra->id ? route('extras.store'): route('extras.update', ['id'=>$extra->id]) }}"
            novalidate
        >
            @if($extra->id)
                @method('PUT')
            @endif

            @csrf
            <div class="form-group">
                <label for="customer_type">Servicio a*</label>
                <select
                    class="form-control {{ $errors->has('customer_type') ? 'is-invalid': '' }}"
                    name="customer_type"
                    id="customer_type"
                    required
                >
                    <option
                        {{ (int) old('customer_type') === 1 || $extra->customer  ? 'selected' : '' }}
                        value="1"
                    >Cliente</option>
                    <option
                        {{ (int) old('customer_type') === 2 || $extra->company ? 'selected' : '' }}
                        value="2"
                    >Empresa</option>
                </select>
            </div>
            <div class="form-group customer-group">
                <label>Cliente*</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modalCustomer"
                    href="#modalCustomer"
                >
                    Seleccionar Cliente
                </a>
            </div>
            <div class="form-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('customer_id') ? 'is-invalid': '' }}"
                    name="customer_id"
                    id="customer_id"
                    placeholder="Id de cliente"
                    value="{{ old('customer_id') ?: $extra->customer_id }}"
                    required
                >
            </div>
            <div class="form-group customer-group">
                <input
                    type="text"
                    class="form-control {{ $errors->has('customer_id') ? 'is-invalid': '' }}"
                    name="customer_name"
                    id="customer_name"
                    placeholder="Cliente"
                    value="@if(old('customer_name')){{ old('customer_name')  }}
                    @elseif($extra->customer_id)@php
                        echo $extra->customer->name;
                    @endphp
                    @endif"
                    readonly
                >
                @if($errors->has('customer_id'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('customer_id') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group company-group" style="display: none">
                <label>Empresa*</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modalCompany"
                    href="#modalCompany"
                >
                    Seleccionar empresa
                </a>
            </div>
            <div class="form-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_id') ? 'is-invalid': '' }}"
                    name="company_id"
                    id="company_id"
                    placeholder="Id de compañía"
                    value="{{ old('company_id') ?: $extra->company_id }}"
                    required
                >
            </div>
            <div class="form-group company-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_id') ? 'is-invalid': '' }}"
                    name="company_name"
                    id="company_name"
                    placeholder="Empresa"
                    value="@if(old('company_name')){{ old('company_name')  }}
                    @elseif($extra->company_id)@php
                        echo $extra->company->name;
                    @endphp
                    @endif"
                    readonly
                >
                @if($errors->has('company_id'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('company_id') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="service_type_id">Tipo de servicio*</label>
                <select
                    class="form-control {{ $errors->has('service_type_id') ? 'is-invalid': '' }}"
                    name="service_type_id"
                    id="service_type_id"
                    required
                >
                    @foreach(\App\ServiceType::where('id','>=',3)->pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('service_type_id') === $id || (int) $extra->service_type_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
                @if($errors->has('service_type_id'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('service_type_id') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="currency_id">Moneda*</label>
                <select
                    class="form-control {{ $errors->has('currency_id') ? 'is-invalid': '' }}"
                    name="currency_id"
                    id="currency_id"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_id') === $id || $extra->currency_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="charged_amount">Monto cobrado*</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('charged_amount') ? 'is-invalid': '' }}"
                    name="charged_amount"
                    id="charged_amount"
                    placeholder=""
                    value="{{ old('charged_amount') ?: $extra->charged_amount }}"
                    required
                >
                @if($errors->has('charged_amount'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('charged_amount') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="description">Descripción*</label>
                <textarea
                    class="form-control {{ $errors->has('description') ? 'is-invalid': '' }}"
                    name="description"
                    id="description"
                    placeholder=""
                >{{ old('description') ?: $extra->description }}</textarea>
                @if($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="folios">Folios</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('folios') ? 'is-invalid': '' }}"
                    name="folios"
                    id="folios"
                    placeholder=""
                    value="{{ old('folios') ?: $extra->folios }}"
                >
                @if($errors->has('folios'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('folios') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="done_at">Servicio realizado en*</label>
                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input
                        type="text"
                        name="done_at"
                        id="done_at"
                        class="form-control datetimepicker-input {{ $errors->has('done_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker1"
                        value="{{ old('done_at') ?: $extra->done_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('done_at'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('done_at') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="user_got_id">Usuario que captó*</label>
                <select
                    class="form-control {{ $errors->has('user_got_id') ? 'is-invalid': '' }}"
                    name="user_got_id"
                    id="user_got_id"
                    required
                >
                    @foreach(\App\User::get() as $user)
                        <option
                            {{ (int) old('user_got_id') === $user->id || $extra->user_got_id === $user->id ? 'selected' : '' }}
                            value="{{ $user->id }}"
                        >{{ $user->name." ".$user->last_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
    <!-- Modal para la empresa -->
    @include('partials.extras.companyModal')
    <!-- Modal para la empresa -->
    @include('partials.extras.customerModal')

@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
        });
        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.company-search-btn', function(){
            let customer_type = $(this).data('type');
            console.log(customer_type);

            let company_search;

            if(customer_type === 1){
                console.log("Cliente");
                company_search = $("#modalCustomer .company-search").val();
                customerDniAjax(company_search);
            }else if(customer_type === 2){
                console.log("Empresa");
                company_search = $("#modalCompany .company-search").val();
                companyRucAjax(company_search);
            }

        });
        /*
        * Función para añadir el id del servicio y el nombre del cliente
        * al input del formulario
        */
        $(document).on('click', '.company-add-btn', function(){
            //Obtenemos el tipo del servicio del selector "Tipo de servicio" en el formulario

            let customer_type = $(this).data('type');
            let company_id;
            let company_name;

            if(customer_type === 1){
                //Limpiamos los datos de companía
                $("#company_id").val("");
                $("#company_name").val("");

                company_id = $("#modalCustomer .id-filtered").val();
                company_name = $("#modalCustomer .company-filtered").val();

                $("#customer_id").val(company_id);
                $("#customer_name").val(company_name);
            }else if(customer_type === 2){
                //Limpiamos los datos de cliente
                $("#customer_id").val("");
                $("#customer_name").val("");

                company_id = $("#modalCompany .id-filtered").val();
                company_name = $("#modalCompany .company-filtered").val();

                $("#company_id").val(company_id);
                $("#company_name").val(company_name);

            }

            $(".company-search").val("");
            $(".company-filtered").val("");
            $(".id-filtered").val("");
        });

        function customerDniAjax(customer_search) {
            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('customers.dniSearch') }}',
                data: {
                    customer_search: customer_search
                },
                success: function (data) {
                    let company_name = "";
                    let company_id = null;

                    //console.log(data);

                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data['id']) != "undefined"
                        &&
                        data['name'] !== null){

                        company_id = data['id'];
                        company_name = data['name'];

                    }else{
                        console.log("No se encontró compañia");
                        return;
                    }

                    /*
                    * Si es servicio es contablidad o constitución,
                    * el id del servicio y en nombre de la empresa
                    * se guarda en un input escondido y en un input
                    * no editable, dentro del modal
                    */

                    $("#modalCustomer .company-filtered").val(company_name);
                    $("#modalCustomer .id-filtered").val(company_id);

                },error:function(){
                    console.log(data);
                }
            });
        }

        function companyRucAjax(company_search) {
            console.log("Copañia");
            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('companies.rucSearch') }}',
                data: {
                    company_search: company_search
                },
                success: function (data) {
                    let company_name = "";
                    let company_id = null;

                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data['id']) != "undefined"
                        &&
                        data['name'] !== null){

                        company_id = data['id'];
                        company_name = data['name'];

                    }else{
                        console.log("No se encontró compañia");
                        return;
                    }

                    /*
                    * Si es servicio es contablidad o constitución,
                    * el id del servicio y en nombre de la empresa
                    * se guarda en un input escondido y en un input
                    * no editable, dentro del modal
                    */

                    $("#modalCompany .company-filtered").val(company_name);
                    $("#modalCompany .id-filtered").val(company_id);
                    company-group
                },error:function(){
                    console.log(data);
                }
            });
        }
        /*
        * Función para cambiar el botón de empresa o cliente
         * cuando se cambio el selector de tipo de cliente
        */
        $(document).on('change', '#customer_type', function(){
            console.log("cambio");
            let customer_type = $(this).val();

            changeCustomerSelected(customer_type);
        });
        /*
        * Función para cambiar el botón de empresa o cliente
         * cuando se cambio el selector de tipo de cliente
        */
        function changeCustomerSelected(customer_type){

            if(customer_type === "1"){
                console.log("cliente");
                $("#company_id").val("");
                $("#company_name").val("");

                $(".company-group").hide();
                $(".customer-group").show();
            }else if(customer_type === "2"){
                console.log("empresa");
                $("#customer_id").val("");
                $("#customer_name").val("");

                $(".customer-group").hide();
                $(".company-group").show();
            }
        }
    </script>
@endpush
