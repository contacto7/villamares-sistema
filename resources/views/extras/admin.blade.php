@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Extras"),
        'icon' => "briefcase"
    ])
@endsection

@section('content')
    @foreach($extras as $extra)
        <div class="container container-accounting-admin">
            @isset($extra->company->id)
                <div class="row text-center flex-column">
                    <h2 class="flex-row">{{ $extra->company->name }}</h2>
                    <h3 class="flex-row">
                        {{ $extra->company->tax_number }}
                    </h3>
                </div>
            @endisset
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                    <div class="info-row">
                        <span><b>Tipo de servicio:</b></span>
                        <span>{{ $extra->serviceType->name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Servicio brindado:</b></span>
                        <span>{{ $extra->done_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Oficina:</b></span>
                        <span>
                            @if($extra->company)
                                {{ $extra->company->office->name }}
                            @elseif($extra->customer)
                                {{ $extra->customer->office->name }}
                            @endif
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Usuario que registró:</b></span>
                        <span>
                        {{ $extra->user->name }}
                            {{ $extra->user->last_name }}
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Usuario que captó:</b></span>
                        <span>
                        {{ $extra->userGot->name }}
                            {{ $extra->userGot->last_name }}
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Monto cobrado:</b></span>
                        <span>
                            S/. {{ number_format($extra->charged_amount , 2, '.', "'") }}
                        </span>
                    </div>
                </div>
            </div>
            <ul class="nav nav-tabs" id="constitutionTab" role="tablist">

                @isset($extra->company->id)
                <li class="nav-item">
                    <a
                        class="nav-link active"
                        id="contact-tab"
                        data-toggle="tab"
                        href="#contact"
                        role="tab"
                        aria-controls="contact"
                        aria-selected="true"
                    >Contacto</a>
                </li>
                @endisset
                <li class="nav-item">
                    <a
                        class="nav-link {{ $extra->company ?: 'active' }}"
                        id="customer-tab"
                        data-toggle="tab"
                        href="#customer"
                        role="tab"
                        aria-controls="customer"
                        aria-selected="true"
                    >Cliente</a>
                </li>
                @isset($extra->company->id)
                <li class="nav-item">
                    <a
                        class="nav-link"
                        id="company-tab"
                        data-toggle="tab"
                        href="#company"
                        role="tab"
                        aria-controls="company"
                        aria-selected="true"
                    >Empresa</a>
                </li>
                @endisset
                <li class="nav-item">
                    <a
                        class="nav-link"
                        id="constitution-tab"
                        data-toggle="tab"
                        href="#constitution"
                        role="tab"
                        aria-controls="constitution"
                        aria-selected="true"
                    >Servicio</a>
                </li>
                <li class="nav-item">
                    <a
                        class="nav-link"
                        id="vouchers-tab"
                        data-toggle="tab"
                        href="#vouchers"
                        role="tab"
                        aria-controls="vouchers"
                        aria-selected="true"
                    >Comprobantes de pago</a>
                </li>
            </ul>

            <div class="tab-content" id="constitutionTabContent">
                @isset($extra->company->id)
                <div class="tab-pane fade show active"
                     id="contact"
                     role="tabpanel"
                     aria-labelledby="contact-tab">
                    <div class="info-wrapper">
                        @include('partials.extras.contact')
                    </div>
                </div>
                @endisset
                <div class="tab-pane fade {{ $extra->company ?: 'show active' }}"
                     id="customer"
                     role="tabpanel"
                     aria-labelledby="customer-tab">
                    <div class="info-wrapper">
                        @include('partials.extras.customer', ['customer'=> $extra->company ? $extra->company->customer:$extra->customer])
                    </div>
                </div>
                @isset($extra->company->id)
                <div class="tab-pane fade"
                     id="company"
                     role="tabpanel"
                     aria-labelledby="company-tab">
                    <div class="info-wrapper">
                        @include('partials.extras.company')
                    </div>
                </div>
                @endisset
                <div class="tab-pane fade"
                     id="constitution"
                     role="tabpanel"
                     aria-labelledby="constitution-tab">
                    <div class="info-wrapper">
                        @include('partials.extras.extra')
                    </div>
                </div>
                <div class="tab-pane fade"
                     id="vouchers"
                     role="tabpanel"
                     aria-labelledby="vouchers-tab">
                    <div class="row">
                        @include('partials.extras.vouchers')
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
