<form
    method="POST"
    action="{{ ! $book->id ? route('books.store'): route('books.update', ['id'=>$book->id]) }}"
    novalidate
    enctype="multipart/form-data"
>
    @if($book->id)
        @method('PUT')
    @endif

    @csrf
    <div class="form-group">
        <label for="folios">Folios*</label>
        <input
            type="number"
            class="form-control {{ $errors->has('folios') ? 'is-invalid': '' }}"
            name="folios"
            id="folios"
            placeholder=""
            value="{{ old('folios') ?: $book->folios }}"
            required
        >
        @if($errors->has('folios'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('folios') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <label for="notary">Notaría*</label>
        <input
            type="text"
            class="form-control {{ $errors->has('notary') ? 'is-invalid': '' }}"
            name="notary"
            id="notary"
            placeholder=""
            value="{{ old('notary') ?: $book->notary }}"
            required
        >
        @if($errors->has('number'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('notary') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <label for="legalization">Fecha de legalización*</label>
        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
            <input
                type="text"
                name="legalization"
                id="legalization"
                class="form-control datetimepicker-input {{ $errors->has('legalization') ? 'is-invalid': '' }}"
                data-target="#datetimepicker1"
                value="{{ old('legalization') ?: $book->legalization }}"
            />
            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
            @if($errors->has('legalization'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('legalization') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label>Seleccionar imagen*</label>
        <div class="col-md-12">
            <input
                type="file"
                class="custom-file-input form-control {{ $errors->has('picture') ? 'is-invalid': '' }}"
                name="picture"
                id="picture"
                placeholder=""
                value="{{ old('picture') ?: $book->picture }}"
            >
            <label  class="custom-file-label" for="picture">
                Imagen de libro
            </label>
            @if($errors->has('picture'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('picture') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="book_type_id">Libro*</label>
        <select
            class="form-control {{ $errors->has('book_type_id') ? 'is-invalid': '' }}"
            name="book_type_id"
            id="book_type_id"
            required
        >
            @foreach(\App\BookType::pluck('name', 'id') as $id => $name)
                <option
                    {{ (int) old('book_type_id') === $id || $book->book_type_id === $id ? 'selected' : '' }}
                    value="{{ $id }}"
                >{{ $name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group" style="display: none">
        <label for="accounting_id">Servicio contable</label>
        <input
            type="number"
            class="form-control {{ $errors->has('folios') ? 'is-invalid': '' }}"
            name="accounting_id"
            id="accounting_id"
            placeholder=""
            value="{{ $accounting_id }}"
            required
        >
        @if($errors->has('accounting_id'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('accounting_id') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group">
        <button type="submit" class="btn btn-danger">
            {{ __($btnText) }}
        </button>
    </div>

</form>
