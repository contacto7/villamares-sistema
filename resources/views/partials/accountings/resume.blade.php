<table class="table table-bordered table-transparent">
    <thead>
    <tr>
        <th scope="col">Comprobante</th>
        <th scope="col">Deuda soles</th>
        <th scope="col">Deuda dólares</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Factura</td>
        <td>
            S/{{ number_format($rate_comprobantes[0], 2, '.', "'") }}
        </td>
        <td>
            ${{ number_format($rate_comprobantes[1], 2, '.', "'") }}
        </td>
    </tr>
    <tr>
        <td>Recibo de caja</td>
        <td>
            S/{{ number_format($rate_comprobantes[2], 2, '.', "'") }}
        </td>
        <td>
            ${{ number_format($rate_comprobantes[3], 2, '.', "'") }}
        </td>
    </tr>
    </tbody>
</table>
