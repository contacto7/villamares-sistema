<div class="info-row">
    <span><b>Nombres:</b></span>
    <span>{{ $accounting->company->customer->name }}</span>
</div>
<div class="info-row">
    <span><b>Apellidos:</b></span>
    <span>{{ $accounting->company->customer->last_name }}</span>
</div>
<div class="info-row">
    <span><b>Correo:</b></span>
    <span>{{ $accounting->company->customer->email }}</span>
</div>
<div class="info-row">
    <span><b>Teléfono:</b></span>
    <span>{{ $accounting->company->customer->cellphone }}</span>
</div>
<div class="info-row">
    <span><b>Documento de identidad:</b></span>
    <span>{{ $accounting->company->customer->identityDocument->name }}</span>
</div>
<div class="info-row">
    <span><b>Número de documento:</b></span>
    <span>{{ $accounting->company->customer->document_number }}</span>
</div>
