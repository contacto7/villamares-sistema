<div class="info-row">
    <span><b>Total ingresos mensuales:</b></span>
    <span>{{ number_format($accounting->monthly_revenue , 2, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Cantidad de comprobantes mensuales:</b></span>
    <span>{{ number_format($accounting->monthly_vouchers , 0, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Número de personal en planilla:</b></span>
    <span>{{ number_format($accounting->staff_payroll , 0, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Comprobantes a emitir:</b></span>
    <span>{{ $accounting->paymentDocument->name }}</span>
</div>
<div class="info-row">
    <span><b>Moneda de pago:</b></span>
    <span>{{ $accounting->currency->name }}</span>
</div>
<div class="info-row">
    <span><b>Primera tarifa:</b></span>
    <span>{{ number_format($accounting->first_rate , 2, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Tarifa normal:</b></span>
    <span>{{ number_format($accounting->normal_rate , 2, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Libros electrónicos:</b></span>
    <span>
        @switch($accounting->electronic_books)
            @case(\App\Accounting::WITHOUT_ELECTRONIC_BOOKS)
            <span>Sin libros electrónicos</span>
            @break
            @case(\App\Accounting::WITH_ELECTRONIC_BOOKS)
            <span>Con libros electrónicos</span>
            @break
        @endswitch
    </span>
</div>
@if($accounting->electronic_books_date)
    <div class="info-row">
        <span><b>Contabilidad electrónica iniciada en:</b></span>
        <span>{{ $accounting->electronic_books_date }}</span>
    </div>
@endif
@if($accounting->electronic_books_details)
    <div class="info-row">
        <span><b>Detalles de contabilidad electrónica:</b></span>
        <span>{{ $accounting->electronic_books_details }}</span>
    </div>
@endif
@if($accounting->delivery_charge)
    <div class="info-row">
        <span><b>Descargar cargo de entrega:</b></span>
        <span>
            <a href="{{ route('accountings.download', $accounting->delivery_charge) }}">{{ $accounting->delivery_charge }}</a>
        </span>
    </div>
@endif
