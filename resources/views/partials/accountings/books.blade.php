<div class="card mb-3 h-100">
    <img
        class="card-img-top"
        src="{{ $book->picture ? $book->pathAttachment():url('image/book.png') }}"
        alt="{{ $book->bookType->name }}"
    />
    <div class="card-body">
        <h4 class="card-title">{{ $book->bookType->name }}</h4>
        <hr>
        <p class="card-text">
        <div class="info-row">
            <span><b>Folios:</b></span>
            <span>{{ $book->folios }}</span>
        </div>
        <div class="info-row">
            <span><b>Notaría:</b></span>
            <span>{{ $book->notary }}</span>
        </div>
        <div class="info-row">
            <span><b>Legalización:</b></span>
            <span>{{ $book->legalization }}</span>
        </div>
        </p>
    </div>
    <div class="card-footer">
        <a
            class="btn btn-outline-info btn-block btn-book"
            data-toggle="modal"
            data-target="#modalCompanyBook"
            href="#modalCompanyBook"
            data-id="{{ $book->id }}"
        >
            {{ __("Editar") }}
        </a>
    </div>
</div>
