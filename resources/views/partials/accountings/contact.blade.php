<div class="info-row">
    <span><b>Nombres:</b></span>
    <span>{{ $accounting->company->contact_name }}</span>
</div>
<div class="info-row">
    <span><b>Apellidos:</b></span>
    <span>{{ $accounting->company->contact_last_name }}</span>
</div>
<div class="info-row">
    <span><b>Correo:</b></span>
    <span>{{ $accounting->company->contact_email }}</span>
</div>
<div class="info-row">
    <span><b>Teléfono:</b></span>
    <span>{{ $accounting->company->contact_phone }}</span>
</div>
