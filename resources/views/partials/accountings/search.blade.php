<form action="{{ route('accountings.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            Empresa
        </label>
        <div class="col-sm-10">
            <input
                class="form-control"
                name="name"
                id="name"
                type="text"
                placeholder="{{ __("Ingrese la razón social") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            RUC
        </label>
        <div class="col-sm-10">
            <input
                class="form-control"
                name="tax-number"
                id="tax-number"
                type="text"
                placeholder="{{ __("Ingrese el RUC") }}"
            >
        </div>
    </div>
    @can('filterByWorker', \App\Accounting::class)
    <div class="form-group row">
        <label
            for="worker_user_id"
            class="col-sm-2 col-form-label"
        >
            Trabajador
        </label>
        <div class="col-sm-10">
            <select
                class="form-control"
                name="worker_user_id"
                id="worker_user_id"
            >
                <option value="">Seleccionar</option>
                @foreach(\App\User::get() as $user)
                    <option
                        value="{{ $user->id }}"
                    >{{ $user->name." ".$user->last_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endcan
    @can('filterByCoordinator', \App\Accounting::class)
    <div class="form-group row">
        <label
            for="supervisor_user_id"
            class="col-sm-2 col-form-label"
        >
            Coordinador
        </label>
        <div class="col-sm-10">
            <select
                class="form-control"
                name="supervisor_user_id"
                id="supervisor_user_id"
            >
                <option value="">Seleccionar</option>
                @foreach(\App\User::get() as $user)
                    <option
                        value="{{ $user->id }}"
                    >{{ $user->name." ".$user->last_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endcan
    @can('filterByState', \App\Accounting::class)
    <div class="form-group row">
        <label
            for="state"
            class="col-sm-2 col-form-label"
        >
            Estado
        </label>
        <div class="col-sm-10">
            <select
                class="form-control"
                name="state"
                id="state"
            >
                <option value="">Seleccionar</option>
                <option value="1">Brindando</option>
                <option value="2">Terminado</option>
            </select>
        </div>
    </div>
    @endcan
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-2 btn-group">
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >
            @can('create', \App\Accounting::class)
            <a
                class="form-control btn btn-villamares"
                href="{{ route('accountings.create') }}"
            >
                Añadir
            </a>
            @endcan
        </div>
    </div>
</form>
