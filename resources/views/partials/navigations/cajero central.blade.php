<li><a class="nav-link" href="{{ route('cashRegister.list') }}">{{ __("Caja")  }}</a></li>
<li class="nav-item dropdown">
    <a
        class="nav-link dropdown-toggle"
        href="#"
        id="navbarDropdownMenuLink"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
    >
        {{ __("Ver") }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="{{ route('vouchers.debts') }}">
            {{ __("Cuentas por cobrar") }}
        </a>
        <a class="dropdown-item" href="{{ route('customers.list') }}">
            {{ __("Clientes") }}
        </a>
        <a class="dropdown-item" href="{{ route('customers.nearBirthdays') }}">
            {{ __("Cumpleaños") }}
        </a>
        <a class="dropdown-item" href="{{ route('companies.list') }}">
            {{ __("Empresas") }}
        </a>
        <a class="dropdown-item" href="{{ route('vouchers.list') }}">
            {{ __("Comprobantes") }}
        </a>
        <a class="dropdown-item" href="{{ route('books.missing') }}">
            {{ __("Libros faltantes") }}
        </a>
        <a class="dropdown-item" href="{{ route('cashFlows.list') }}">
            {{ __("Flujo") }}
        </a>
    </div>
</li>
<li class="nav-item dropdown">
    <a
        class="nav-link dropdown-toggle"
        href="#"
        id="navbarDropdownMenuLink"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
    >
        {{ __("Servicios") }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="{{ route('accountings.list') }}">
            {{ __("Contabilidad") }}
        </a>
        <a class="dropdown-item" href="{{ route('constitutions.list') }}">
            {{ __("Constituciones") }}
        </a>
        <a class="dropdown-item" href="{{ route('extras.list') }}">
            {{ __("Otros") }}
        </a>
    </div>
</li>
@include('partials.navigations.logged')
