<li><a class="nav-link" href="{{ route('cashRegister.list') }}">{{ __("Caja")  }}</a></li>
<li class="nav-item dropdown">
    <a
        class="nav-link dropdown-toggle"
        href="#"
        id="navbarDropdownMenuLink"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
    >
        {{ __("Ver") }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="#">
            {{ __("Cuentas por cobrar") }}
        </a>
        <a class="dropdown-item" href="#">
            {{ __("Clientes") }}
        </a>
        <a class="dropdown-item" href="#">
            {{ __("Comprobantes") }}
        </a>
    </div>
</li>
<li class="nav-item dropdown">
    <a
        class="nav-link dropdown-toggle"
        href="#"
        id="navbarDropdownMenuLink"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
    >
        {{ __("Servicios") }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="#">
            {{ __("Contabilidad") }}
        </a>
        <a class="dropdown-item" href="#">
            {{ __("Constituciones") }}
        </a>
        <a class="dropdown-item" href="#">
            {{ __("Otros") }}
        </a>
    </div>
</li>

@include('partials.navigations.logged')
