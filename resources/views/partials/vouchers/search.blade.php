<form action="{{ route('vouchers.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            Empresa
        </label>
        <div class="col-sm-10">
            <input
                class="form-control"
                name="tax-number"
                id="tax-number"
                type="text"
                placeholder="{{ __("RUC") }}"
                value="{{ request()->input('tax-number') }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            Cliente
        </label>
        <div class="col-sm-10">
            <input
                class="form-control"
                name="document-number"
                id="document-number"
                type="text"
                placeholder="{{ __("Número de documento de identidad") }}"
                value="{{ request()->input('document-number') }}"
            >
        </div>
    </div>
    @can('filterByVoucher', \App\Voucher::class)
    <div class="form-group row">
        <label
            for="payment_document_id"
            class="col-sm-2 col-form-label"
        >
            Comprobante
        </label>
        <div class="col-sm-10">
            <select
                class="form-control"
                name="payment_document_id"
                id="payment_document_id"
            >
                <option value="" >Seleccionar</option>
                @foreach(\App\PaymentDocument::get() as $paymentDocs)
                    <option
                        {{ (int) request()->input('payment_document_id') === $paymentDocs->id ? 'selected' : '' }}
                        value="{{ $paymentDocs->id }}"
                    >{{ $paymentDocs->name }} - {{ $paymentDocs->titular }}</option>
                @endforeach
            </select>
         </div>
    </div>
    @endcan
    @can('filterByServiceType', \App\Voucher::class)
    <div class="form-group row">
        <label
            for="service_type_id"
            class="col-sm-2 col-form-label"
        >
            Tipo de servicio
        </label>
        <div class="col-sm-10">
            <select
                class="form-control"
                name="service_type_id"
                id="service_type_id"
            >
                <option value="" >Seleccionar</option>
                @foreach(\App\ServiceType::pluck('name', 'id') as $id => $name)
                    <option
                        {{ (int) request()->input('service_type_id') === $id ? 'selected' : '' }}
                        value="{{ $id }}"
                    >{{ $name }}</option>
                @endforeach
            </select>
         </div>
    </div>
    @endcan
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-2 btn-group">
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >
            @can('create', \App\Voucher::class)
                <a
                    class="form-control btn btn-villamares"
                    href="{{ route('vouchers.create') }}"
                >
                    Añadir
                </a>
            @endcan
        </div>
    </div>
</form>
