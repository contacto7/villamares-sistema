<div class="modal" id="modal-3" data-type="3">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Extra</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="company-search">RUC</label>
                    <input
                        type="text"
                        class="form-control company-search"
                        name="company-search"
                        placeholder=""
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info company-search-btn">
                        Buscar
                    </a>
                </div>
                <div class="form-group">
                    <label for="customer-search">Documento de identidad</label>
                    <input
                        type="text"
                        class="form-control customer-search"
                        name="customer-search"
                        id="customer-search"
                        placeholder=""
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info customer-search-btn">
                        Buscar
                    </a>
                </div>
                <div class="form-group" id="extras-list"></div>
                <div class="form-group" style="display: none">
                    <label for="id-filtered">ID Cliente encontrado</label>
                    <input
                        type="text"
                        class="form-control id-filtered"
                        name="id-filtered"
                        id="id-filtered"
                        readonly
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info customer-add-btn" data-dismiss="modal">
                        Agregar
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
