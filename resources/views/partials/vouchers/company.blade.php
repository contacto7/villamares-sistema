<h3>Empresa</h3>
<div class="row mt-3 mb-3">
    <div class="info-wrapper">
        <div class="info-row">
            <span><b>Razón social:</b></span>
            <span>{{ $company->name }}</span>
        </div>
        <div class="info-row">
            <span><b>RUC:</b></span>
            <span>{{ $company->tax_number }}</span>
        </div>
        <div class="info-row">
            <span><b>Nombres de contacto:</b></span>
            <span>{{ $company->contact_name }}</span>
        </div>
        <div class="info-row">
            <span><b>Apellidos de contacto:</b></span>
            <span>{{ $company->contact_last_name }}</span>
        </div>
        <div class="info-row">
            <span><b>Correo de contacto:</b></span>
            <span>{{ $company->contact_email }}</span>
        </div>
        <div class="info-row">
            <span><b>Teléfono de contacto:</b></span>
            <span>{{ $company->contact_phone }}</span>
        </div>
        <div class="info-row">
            <span><b>Dirección:</b></span>
            <span>{{ $company->address }}</span>
        </div>
        <div class="info-row">
            <span><b>Nombres del gerente:</b></span>
            <span>{{ $company->manager_name }}</span>
        </div>
        <div class="info-row">
            <span><b>Apellidos del gerente:</b></span>
            <span>{{ $company->manager_last_name }}</span>
        </div>
        <div class="info-row">
            <span><b>DNI del gerente:</b></span>
            <span>{{ $company->manager_dni }}</span>
        </div>
        <div class="info-row">
            <span><b>Régimen:</b></span>
            <span>{{ $company->taxRegime->description }}</span>
        </div>
        <div class="info-row">
            <span><b>Giro de la empresa:</b></span>
            <span>{{ $company->companyTurn->name }}</span>
        </div>
    </div>
</div>
