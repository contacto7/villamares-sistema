<!-- The Modal -->
<div class="modal" id="modal-2" data-type="2">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Constitución de empresas</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="company-search">RUC</label>
                    <input
                        type="text"
                        class="form-control company-search"
                        name="company-search"
                        placeholder=""
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info company-search-btn">
                        Buscar
                    </a>
                </div>
                <div class="form-group">
                    <label for="customer-filtered">Empresa encontrada con constitución</label>
                    <input
                        type="text"
                        class="form-control customer-filtered"
                        name="customer-filtered"
                        readonly
                    >
                </div>
                <div class="form-group" style="display: none">
                    <label for="id-filtered">ID Cliente encontrado</label>
                    <input
                        type="text"
                        class="form-control id-filtered"
                        name="id-filtered"
                        readonly
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info customer-add-btn" data-dismiss="modal">
                        Agregar
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
