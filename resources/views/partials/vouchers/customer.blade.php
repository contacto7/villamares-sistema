<div class="info-row">
    <span><b>Nombres:</b></span>
    <span>{{ $customer->name }}</span>
</div>
<div class="info-row">
    <span><b>Apellidos:</b></span>
    <span>{{ $customer->last_name }}</span>
</div>
<div class="info-row">
    <span><b>Correo:</b></span>
    <span>{{ $customer->email }}</span>
</div>
<div class="info-row">
    <span><b>Teléfono:</b></span>
    <span>{{ $customer->cellphone }}</span>
</div>
<div class="info-row">
    <span><b>Documento de identidad:</b></span>
    <span>{{ $customer->identityDocument->name }}</span>
</div>
<div class="info-row">
    <span><b>Número de documento:</b></span>
    <span>{{ $customer->document_number }}</span>
</div>
<div class="info-row">
    <span><b>Oficina:</b></span>
    <span>{{ $customer->office->name }}</span>
</div>
