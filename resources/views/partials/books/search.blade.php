<form action="{{ route('books.filter') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="bank"
            class="col-sm-3 col-form-label"
        >
            Libro
        </label>
        <div class="col-sm-9">
            <select
                class="form-control"
                name="book"
                id="book"
                type="text"
            >
                @foreach($books as $book)
                    <option
                        value="{{ $book->id }}"
                    >
                        {{ $book->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label
            class="col-sm-3 col-form-label"
        >

        </label>
        <div class="col-sm-2">
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >
        </div>
    </div>
</form>
