<div class="info-row">
    <span><b>Nombres del gerente:</b></span>
    <span>{{ $constitution->company->manager_name }}</span>
</div>
<div class="info-row">
    <span><b>Apellidos del gerente:</b></span>
    <span>{{ $constitution->company->manager_last_name }}</span>
</div>
<div class="info-row">
    <span><b>DNI del gerente:</b></span>
    <span>{{ $constitution->company->manager_dni }}</span>
</div>
<div class="info-row">
    <span><b>Régimen:</b></span>
    <span>{{ $constitution->company->taxRegime->description }}</span>
</div>
<div class="info-row">
    <span><b>Giro de la empresa:</b></span>
    <span>{{ $constitution->company->companyTurn->name }}</span>
</div>
<div class="info-row">
    <span><b>Dirección:</b></span>
    <span>{{ $constitution->company->address }}</span>
</div>
