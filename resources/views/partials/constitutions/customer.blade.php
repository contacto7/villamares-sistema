<div class="info-row">
    <span><b>Nombres:</b></span>
    <span>{{ $constitution->company->customer->name }}</span>
</div>
<div class="info-row">
    <span><b>Apellidos:</b></span>
    <span>{{ $constitution->company->customer->last_name }}</span>
</div>
<div class="info-row">
    <span><b>Correo:</b></span>
    <span>{{ $constitution->company->customer->email }}</span>
</div>
<div class="info-row">
    <span><b>Teléfono:</b></span>
    <span>{{ $constitution->company->customer->cellphone }}</span>
</div>
<div class="info-row">
    <span><b>Documento de identidad:</b></span>
    <span>{{ $constitution->company->customer->identityDocument->name }}</span>
</div>
<div class="info-row">
    <span><b>Número de documento:</b></span>
    <span>{{ $constitution->company->customer->document_number }}</span>
</div>
