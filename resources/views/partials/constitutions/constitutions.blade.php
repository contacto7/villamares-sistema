<div class="info-row">
    <span><b>Capital:</b></span>
    <span>{{ number_format($constitution->capital_amount , 2, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Reserva número:</b></span>
    <span>{{ number_format($constitution->reservation_number , 0, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Servicio de reserva:</b></span>
    <span>
        @if($constitution->reservation_service === \App\Constitution::FIRST_RESERVATION)
            {{ __("Primera reserva") }}
        @elseif($constitution->reservation_service ===  \App\Constitution::SECOND_RESERVATION)
            {{ __("Segunda reserva") }}
        @elseif($constitution->reservation_service ===  \App\Constitution::THIRD_RESERVATION)
            {{ __("Tercera reserva") }}
        @else
            {{ __("No") }}
        @endif
    </span>
</div>
<div class="info-row">
    <span><b>Expiración de reserva:</b></span>
    <span>{{ $constitution->reservation_expiration }}</span>
</div>
<div class="info-row">
    <span><b>Número de socios:</b></span>
    <span>{{ $constitution->partners_number }}</span>
</div>
<div class="info-row">
    <span><b>Gerentes adicionales:</b></span>
    <span>{{ $constitution->additional_managers }}</span>
</div>
<div class="info-row">
    <span><b>Registros públicos:</b></span>
    <span>{{ $constitution->public_record }}</span>
</div>
<div class="info-row">
    <span><b>Carta notarial:</b></span>
    <span>{{ $constitution->notarial_letter }}</span>
</div>
<div class="info-row">
    <span><b>Directorio:</b></span>
    <span>{{ $constitution->directory }}</span>
</div>
<div class="info-row">
    <span><b>Vehículos:</b></span>
    <span>{{ $constitution->vehicles }}</span>
</div>
<div class="info-row">
    <span><b>Adicionales:</b></span>
    <span>{{ $constitution->others }}</span>
</div>
<div class="info-row">
    <span><b>Comisión abogado:</b></span>
    <span>S/. {{ number_format($constitution->lawyer_fee , 2, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Comisión notaría:</b></span>
    <span>S/. {{ number_format($constitution->notary_fee , 2, '.', "'") }}</span>
</div>
<div class="info-row">
    <span><b>Devolución:</b></span>
    <span>S/. {{ number_format($constitution->devolution , 2, '.', "'") }}</span>
</div>
@isset($constitution->registration_charge_picture)
    <div class="info-row">
        <span><b>Imagen de cargo de inscripción en RRPP:</b></span>
        <br>
        <span>
            <img
                class="img-constitution"
                src="{{ $constitution->pathAttachment($constitution->registration_charge_picture) }}"
                alt="{{ $constitution->registration_charge_picture }}"
            />
        </span>
    </div>
@endisset
@isset($constitution->picture_1)
    <div class="info-row">
        <span><b>Pagos notariales:</b></span>
        <br>
        <span>
            <img
                class="img-constitution"
                src="{{ $constitution->pathAttachment($constitution->picture_1) }}"
                alt="{{ $constitution->picture_1 }}"
            />
        </span>
    </div>
@endisset
@isset($constitution->picture_2)
    <div class="info-row">
        <span><b>Pagos registrales:</b></span>
        <br>
        <span>
            <img
                class="img-constitution"
                src="{{ $constitution->pathAttachment($constitution->picture_2) }}"
                alt="{{ $constitution->picture_2 }}"
            />
        </span>
    </div>
@endisset
@isset($constitution->picture_3)
    <div class="info-row">
        <span><b>Cargo de entrega al cliente:</b></span>
        <br>
        <span>
            <img
                class="img-constitution"
                src="{{ $constitution->pathAttachment($constitution->picture_3) }}"
                alt="{{ $constitution->picture_3 }}"
            />
        </span>
    </div>
@endisset
@isset($constitution->picture_4)
    <div class="info-row">
        <span><b>Devolución de efectivo:</b></span>
        <br>
        <span>
            <img
                class="img-constitution"
                src="{{ $constitution->pathAttachment($constitution->picture_4) }}"
                alt="{{ $constitution->picture_4 }}"
            />
        </span>
    </div>
@endisset
