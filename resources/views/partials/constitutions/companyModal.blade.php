<!-- The Modal -->
<div class="modal" id="modalCompany" data-type="1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Empresa</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="company-search">RUC</label>
                    <input
                        type="text"
                        class="form-control company-search"
                        id="company-search"
                        name="company-search"
                        placeholder=""
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info company-search-btn">
                        Buscar
                    </a>
                </div>
                <div class="form-group">
                    <label for="company-filtered">Empresa sin contabilidad</label>
                    <input
                        type="text"
                        class="form-control"
                        id="company-filtered"
                        name="company-filtered"
                        readonly
                    >
                </div>
                <div class="form-group" style="display: none">
                    <label for="id-filtered">ID empresa</label>
                    <input
                        type="text"
                        class="form-control id-filtered"
                        id="id-filtered"
                        name="id-filtered"
                        readonly
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info company-add-btn" data-dismiss="modal">
                        Agregar
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
