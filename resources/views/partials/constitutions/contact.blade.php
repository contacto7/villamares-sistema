<div class="info-row">
    <span><b>Nombres:</b></span>
    <span>{{ $constitution->company->contact_name }}</span>
</div>
<div class="info-row">
    <span><b>Apellidos:</b></span>
    <span>{{ $constitution->company->contact_last_name }}</span>
</div>
<div class="info-row">
    <span><b>Correo:</b></span>
    <span>{{ $constitution->company->contact_email }}</span>
</div>
<div class="info-row">
    <span><b>Teléfono:</b></span>
    <span>{{ $constitution->company->contact_phone }}</span>
</div>
