<form action="{{ route('constitutions.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            Empresa
        </label>
        <div class="col-sm-10">
            <input
                class="form-control"
                name="name"
                id="name"
                type="text"
                placeholder="{{ __("Ingrese la razón social") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            RUC
        </label>
        <div class="col-sm-10">
            <input
                class="form-control"
                name="tax-number"
                id="tax-number"
                type="text"
                placeholder="{{ __("Ingrese el RUC") }}"
            >
        </div>
    </div>
    @can('filterOffice', \App\Constitution::class)
        <div class="form-group row">
            <label
                for="office_id"
                class="col-sm-2 col-form-label"
            >
                Oficina
            </label>
            <div class="col-sm-10">
                <select
                    class="form-control"
                    name="office_id"
                    id="office_id"
                >
                    <option value="">Seleccionar</option>
                    @foreach(\App\Office::pluck('name', 'id') as $id => $name)
                        <option
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endcan
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-2 btn-group">
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >
            @can('addConstitution', \App\Constitution::class)
                <a
                    class="form-control btn btn-villamares"
                    href="{{ route('constitutions.create') }}"
                >
                    Añadir
                </a>
            @endcan
        </div>
    </div>
</form>
