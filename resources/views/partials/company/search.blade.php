<form action="{{ route('companies.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-3 col-form-label"
        >
            Razón social
        </label>
        <div class="col-sm-9">
            <input
                class="form-control"
                name="name"
                id="name"
                type="text"
                placeholder="{{ __("Ingrese la razón social") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-3 col-form-label"
        >
            RUC
        </label>
        <div class="col-sm-9">
            <input
                class="form-control"
                name="tax-number"
                id="tax-number"
                type="text"
                placeholder="{{ __("Ingrese el RUC") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-3 btn-group">
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >

            @can('create', \App\Company::class)
            <a
                class="form-control btn btn-villamares"
                href="{{ route('companies.create') }}"
            >
                Añadir
            </a>
            @endcan
        </div>
    </div>
</form>
