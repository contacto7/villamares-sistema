<!-- The Modal -->
<div class="modal" id="voucherModal" data-type="1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Servicio contable</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="company-search">Número de voucher</label>
                    <input
                        type="text"
                        class="form-control"
                        name="voucher-search"
                        id="voucher-search"
                        placeholder=""
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info voucher-search-btn">
                        Buscar
                    </a>
                </div>
                <div class="form-group">
                    <label for="voucher-filtered">Voucher encontrado</label>
                    <input
                        type="text"
                        class="form-control"
                        id="voucher-filtered"
                        name="voucher-filtered"
                        readonly
                    >
                </div>
                <div class="form-group" style="display: none">
                    <label for="id-filtered">ID voucher encontrado</label>
                    <input
                        type="text"
                        class="form-control"
                        id="id-filtered"
                        name="id-filtered"
                        readonly
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info voucher-add-btn" data-dismiss="modal">
                        Agregar
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
