<form action="{{ route('cashFlows.search') }}" method="get" class="col-sm-12 form-search-villamares">
    @can('filterByUser', \App\CashFlow::class)
    <div class="form-group row">
        <label
            for="user_registered_id"
            class="col-sm-3 col-form-label"
        >
            Realizó
        </label>
        <div class="col-sm-9">
            <select
                class="form-control"
                name="user_registered_id"
                id="user_registered_id"
            >
                <option value="">Seleccionar</option>
                @foreach(\App\User::get() as $user)
                    <option
                        {{ (int) request()->input('user_registered_id') === $user->id ? 'selected' : '' }}
                        value="{{ $user->id }}"
                    >{{ $user->name." ".$user->last_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endcan
    @can('filterByOperationType', \App\CashFlow::class)
    <div class="form-group row">
        <label
            for="user_registered_id"
            class="col-sm-3 col-form-label"
        >
            Operación
        </label>
        <div class="col-sm-9">
            <select
                class="form-control"
                name="operation_type"
                id="operation_type"
            >
                <option value="">Seleccionar</option>
                <option
                    {{ (int) request()->input('operation_type') === 1 ? 'selected' : '' }}
                    value="1"
                >Ingreso</option>
                <option
                    {{ (int) request()->input('operation_type') === 2 ? 'selected' : '' }}
                    value="2"
                >Egreso</option>
                <option
                    {{ (int) request()->input('operation_type') === 3 ? 'selected' : '' }}
                    value="3"
                >Traspaso de otro usuario</option>
                <option
                    {{ (int) request()->input('operation_type') === 4 ? 'selected' : '' }}
                    value="4"
                >Traspasó a otro usuario</option>
                <option
                    {{ (int) request()->input('operation_type') === 5 ? 'selected' : '' }}
                    value="5"
                >Banco a caja</option>
                <option
                    {{ (int) request()->input('operation_type') === 6 ? 'selected' : '' }}
                    value="6"
                >Caja a banco</option>
            </select>
        </div>
    </div>
    @endcan
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-3 btn-group">
            @canany(['filterByUser', 'filterByOperationType'], \App\CashFlow::class)
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >
            @endcanany
            <a
                class="form-control btn btn-villamares"
                href="{{ route('cashFlows.create') }}"
            >
                Añadir
            </a>
        </div>
    </div>
</form>
