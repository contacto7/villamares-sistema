@can('filterMovement', \App\CashRegister::class)
    <form action="{{ route('cashRegister.search') }}" method="get" class="col-sm-12 form-search-villamares">
        <div class="form-group row">
            <label
                for="user_id"
                class="col-sm-3 col-form-label"
            >
                Cajero
            </label>
            <div class="col-sm-9">
                <select
                    class="form-control"
                    name="user_id"
                    id="user_id"
                    required
                >
                    @foreach(\App\User::get() as $user)
                        <option
                            {{ (int) request()->input('user_id') === $user->id ? 'selected' : '' }}
                            value="{{ $user->id }}"
                        >{{ $user->name." ".$user->last_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3 offset-sm-3 btn-group">
                <input
                    class="form-control btn btn-villamares"
                    name="filter"
                    type="submit"
                    value="buscar"
                >
            </div>
        </div>
    </form>
@endcan
