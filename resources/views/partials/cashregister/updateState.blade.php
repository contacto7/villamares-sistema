<form
    method="POST"
    action="{{ route('cashFlows.updateState', ['id'=>$cashflow->id]) }}"
    novalidate
>
    @if($cashflow->id)
        @method('PUT')
    @endif

    @csrf
    <input type="hidden" value="{{ $cashflow->id }}" name="id">
    <input type="hidden" value="{{ $cashflow->cash_flow_id }}" name="cash_flow_id">
    <input type="hidden" value="{{ \App\CashFlow::ACCEPTED }}" name="state">
    <button type="submit" class="btn btn-info btn-block">Aceptar</button>
</form>
<form
    method="POST"
    action="{{ route('cashFlows.updateState', ['id'=>$cashflow->id]) }}"
    novalidate
>
    @if($cashflow->id)
        @method('PUT')
    @endif

    @csrf
    <input type="hidden" value="{{ $cashflow->id }}" name="id">
    <input type="hidden" value="{{ $cashflow->cash_flow_id }}" name="cash_flow_id">
    <input type="hidden" value="{{ \App\CashFlow::REJECTED }}" name="state">
    <button type="submit"  class="btn btn-danger btn-block">Rechazar</button>
</form>
