<form action="{{ route('customers.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            Cliente
        </label>
        <div class="col-sm-5">
            <input
                class="form-control"
                name="name"
                id="name"
                type="text"
                placeholder="{{ __("Nombres") }}"
            >
        </div>
        <div class="col-sm-5">
            <input
                class="form-control"
                name="last-name"
                id="last-name"
                type="text"
                placeholder="{{ __("Apellidos") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-2 col-form-label"
        >
            Documento
        </label>
        <div class="col-sm-10">
            <input
                class="form-control"
                name="document-number"
                id="document-number"
                type="text"
                placeholder="{{ __("Número de documento de identidad") }}"
            >
        </div>
    </div>
    @canany(['filterAccountingProspect', 'filterConstitutionProspect'], \App\Customer::class)
    <div class="form-group row">
        <label
            for="state"
            class="col-sm-2 col-form-label"
        >
            Tipo de prospecto
        </label>
        <div class="col-sm-10">
            <select
                class="form-control"
                name="prospect_type"
                id="prospect_type"
            >
                <option value="">Seleccionar</option>
                @can('filterConstitutionProspect', \App\Customer::class)
                <option value="{{ \App\Customer::CONSTITUTION_PROSPECT }}">Constitucion</option>
                @endcan
                @can('filterAccountingProspect', \App\Customer::class)
                <option value="{{ \App\Customer::ACCOUNTING_PROSPECT }}">Contabilidad</option>
                @endcan
            </select>
        </div>
    </div>
    @endcan
    @can('filterByUser', \App\Customer::class)
    <div class="form-group row">
        <label
            for="user_id"
            class="col-sm-2 col-form-label"
        >
            Registró
        </label>
        <div class="col-sm-10">
            <select
                class="form-control"
                name="user_id"
                id="user_id"
            >
                <option value="">Seleccionar</option>
                @foreach(\App\User::get() as $user)
                    <option
                        {{ (int) request()->input('user_id') === $user->id ? 'selected' : '' }}
                        value="{{ $user->id }}"
                    >{{ $user->name." ".$user->last_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endcan
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-2 btn-group">
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >
            @can('create', \App\Customer::class)
            <a
                class="form-control btn btn-villamares"
                href="{{ route('customers.create') }}"
            >
                Añadir
            </a>
            @endcan
        </div>
    </div>
</form>
