<table class="table table-striped table-light">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Número</th>
        <th scope="col">Pagado</th>
        <th scope="col">Total</th>
        <th scope="col">Emitido</th>
        <th scope="col">Expira</th>
        <th scope="col">Comprobante</th>
        <th scope="col">Emisor</th>
    </tr>
    </thead>
    <tbody>
    @forelse($extra->vouchers as $voucher)
        <tr>
            <td>{{ $voucher->id }}</td>
            <td>{{ $voucher->number }}</td>
            <td>
                {{ $voucher->currency->symbol }}
                {{ number_format($voucher->amount_payed, 2, '.', "'") }}</td>
            <td>
                {{ $voucher->currency->symbol }}
                {{ number_format($voucher->amount_total, 2, '.', "'") }}</td>
            <td>{{ $voucher->emitted_at }}</td>
            <td>{{ $voucher->expired_at }}</td>
            <td>{{ $voucher->paymentDocument->name }}</td>
            <td>{{ $voucher->userEmitted->name }}</td>
        </tr>
    @empty
        <tr>
            <td>{{ __("No hay comprobantes disponibles")}}</td>
        </tr>
    @endforelse
    </tbody>
</table>
