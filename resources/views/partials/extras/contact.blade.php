<div class="info-row">
    <span><b>Nombres:</b></span>
    <span>{{ $extra->company->contact_name }}</span>
</div>
<div class="info-row">
    <span><b>Apellidos:</b></span>
    <span>{{ $extra->company->contact_last_name }}</span>
</div>
<div class="info-row">
    <span><b>Correo:</b></span>
    <span>{{ $extra->company->contact_email }}</span>
</div>
<div class="info-row">
    <span><b>Teléfono:</b></span>
    <span>{{ $extra->company->contact_phone }}</span>
</div>
