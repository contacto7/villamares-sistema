<div class="info-row">
    <span><b>Descripción:</b></span>
    <span>{{ $extra->description }}</span>
</div>
<div class="info-row">
    <span><b>Folios:</b></span>
    <span>{{ $extra->folios }}</span>
</div>
