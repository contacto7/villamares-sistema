<!-- The Modal -->
<div class="modal" id="modalConsolidado" data-type="1">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Empresa</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="bank-search">Banco</label>
                    <select
                        class="form-control"
                        name="bank-search"
                        id="bank-search"
                    >
                        @foreach($banks as $bank)
                            <option
                                value="{{ $bank->id }}"
                            >
                                {{ $bank->titular }} -
                                {{ $bank->bank }} en
                                {{ $bank->currency->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="company-search">Año</label>
                    <input
                        type="number"
                        class="form-control"
                        name="year-search"
                        id="year-search"
                        placeholder=""
                        value="{{  \Carbon\Carbon::now()->year }}"
                    >
                </div>

                <div class="form-group">
                    <label for="month-search">Mes</label>
                    <select
                        class="form-control"
                        name="month-search"
                        id="month-search"
                        required
                    >
                        <option
                            value="1"
                        >Enero</option>
                        <option
                            value="2"
                        >Febrero</option>
                        <option
                            value="3"
                        >Marzo</option>
                        <option
                            value="4"
                        >Abril</option>
                        <option
                            value="5"
                        >Mayo</option>
                        <option
                            value="6"
                        >Junio</option>
                        <option
                            value="7"
                        >Julio</option>
                        <option
                            value="8"
                        >Agosto</option>
                        <option
                            value="9"
                        >Septiembre</option>
                        <option
                            value="10"
                        >Octubre</option>
                        <option
                            value="11"
                        >Noviembre</option>
                        <option
                            value="12"
                        >Diciembre</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-outline-info consolidate-search-btn" data-dismiss="modal">
                        Descargar
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>
