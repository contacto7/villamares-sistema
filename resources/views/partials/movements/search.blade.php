<form action="{{ route('movements.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="type"
            class="col-sm-3 col-form-label"
        >
            Tipo de flujo
        </label>
        <div class="col-sm-9">
            <select
                class="form-control"
                name="type"
                id="type"
                type="text"
                placeholder="{{ __("Tipo") }}"
            >
                <option value="">Ambos</option>
                <option value="1">Ingresos</option>
                <option value="2">Egresos</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label
            for="bank"
            class="col-sm-3 col-form-label"
        >
            Cuenta bancaria
        </label>
        <div class="col-sm-9">
            <select
                class="form-control"
                name="bank"
                id="bank"
                type="text"
                placeholder="{{ __("Banco") }}"
            >
                <option value="">Todos</option>
                @foreach($banks as $bank)
                    <option
                        value="{{ $bank->id }}"
                    >
                        {{ $bank->titular }} -
                        {{ $bank->bank }} en
                        {{ $bank->currency->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-3 btn-group">
            <input
                class="form-control btn btn-villamares"
                name="filter"
                type="submit"
                value="buscar"
            >
            <a
                class="form-control btn btn-villamares"
                data-toggle="modal"
                data-target="#modalConsolidado"
                href="#modalConsolidado"
            >
                Consolidado
            </a>
        </div>
    </div>
</form>
