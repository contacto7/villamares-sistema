@php
    $banksFounded = $banks->where('id', request()->get('bank'));
@endphp
@foreach($banksFounded as $bank)
    <div class="card card-cash-register  mb-4">
        <div class="card-body row p-4">
            <div class="col-md-12">
                <h3>Resumen de cuenta</h3>
            </div>
            <b class="col-md-3">
                Banco:
            </b>
            <div class="col-md-9">
                {{ $bank->titular }} -
                {{ $bank->bank }} en
                {{ $bank->currency->name }}
            </div>
            <b class="col-md-3">
                Ingresos:
            </b>
            <div class="col-md-9">
                {{ $bank->currency->symbol }}
                {{
                    number_format(
                        App\CashFlow::getAccountStatus($bank->id,1 ), 2, '.', "'"
                    )
                }}
            </div>
            <b class="col-md-3">
                Egresos:
            </b>
            <div class="col-md-9">
                {{ $bank->currency->symbol }}
                {{
                    number_format(
                        App\CashFlow::getAccountStatus($bank->id,2 ), 2, '.', "'"
                    )
                }}
            </div>
            <b class="col-md-3">
                Balance:
            </b>
            <div class="col-md-9">
                {{ $bank->currency->symbol }}
                {{
                    number_format(
                        App\CashFlow::getAccountStatus($bank->id,1 )-
                        App\CashFlow::getAccountStatus($bank->id,2 ), 2, '.', "'"
                    )
                }}
            </div>
        </div>
    </div>
@endforeach
