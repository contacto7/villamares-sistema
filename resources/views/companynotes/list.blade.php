<form
    method="POST"
    action="{{ route('companyNotes.store') }}"
    novalidate
    id="form-nota"
    style="display: none"
>
    @csrf
    <input type="hidden" value="{{ $company_id }}" name="company_id" id="company_id">
    <textarea
        class="form-control"
        name="description"
        id="description"
        placeholder="Ingrese la descripción de la nota"
    ></textarea>
    <button type="submit" class="btn btn-villamares btn-block">Agregar</button>
</form>
<button class="btn btn-villamares btn-block agregar-nota">Agregar nota a la empresa</button>
<table class="table table-striped table-light">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Registro</th>
        <th scope="col">Descripción</th>
        <th scope="col">Usuario</th>
    </tr>
    </thead>
    <tbody>
    @forelse($notes as $note)
        <tr>
            <td>{{ $note->getKey('') }}</td>
            <td>{{ $note->created_at }}</td>
            <td>{!! nl2br(e($note->description)) !!}</td>
            <td>
                {{ $note->user->name." ".$note->user->last_name }}
            </td>
        </tr>
    @empty
        <tr>
            <td>{{ __("No hay notas disponibles")}}</td>
        </tr>
    @endforelse
    </tbody>
</table>
<div class="row justify-content-center notes-pagination">{{ $notes->links() }}</div>
