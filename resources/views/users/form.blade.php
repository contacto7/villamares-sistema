@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Usuario"),
        'icon' => "user"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $user->id ? route('users.store'): route('users.update', ['slug'=>$user->slug]) }}"
            novalidate
        >
            @if($user->id)
                @method('PUT')
            @endif

            @csrf
            <div class="form-group">
                <label for="role_id">Rol</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('role_id') ? 'is-invalid': '' }}"
                    name="role_id"
                    id="role_id"
                >
                    @foreach(\App\Role::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('role_id') === $id || $user->role_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="office_id">Oficina</label>
                <select
                    class="form-control {{ $errors->has('office_id') ? 'is-invalid': '' }}"
                    name="office_id"
                    id="office_id"
                >
                    @foreach(\App\Office::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('office_id') === $id || $user->office_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Nombres</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $user->name }}"
                    required
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="last_name">Apellidos</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('last_name') ? 'is-invalid': '' }}"
                    name="last_name"
                    id="last_name"
                    placeholder=""
                    value="{{ old('last_name') ?: $user->last_name }}"
                    required
                >
                @if($errors->has('last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Correo electrónico</label>
                <input
                    type="email"
                    class="form-control {{ $errors->has('email') ? 'is-invalid': '' }}"
                    name="email"
                    id="email"
                    placeholder=""
                    value="{{ old('email') ?: $user->email }}"
                >
                @if($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="text">Contraseña</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('password') ? 'is-invalid': '' }}"
                    name="password"
                    id="password"
                    placeholder=""
                    value=""
                    required
                >
                @if($errors->has('password'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="state">Estado</label>
                <select
                    class="form-control {{ $errors->has('state') ? 'is-invalid': '' }}"
                    name="state"
                    id="state"
                    required
                >
                    <option
                        {{
                            (int) old('state') === \App\User::ACTIVE
                            ||
                            (int) $user->state === \App\User::ACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\User::ACTIVE }}"
                    >Activo</option>
                    <option
                        {{
                            (int) old('state') === \App\User::INACTIVE
                            ||
                            (int) $user->state === \App\User::INACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\User::INACTIVE }}"
                    >Inactivo</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
@endsection
