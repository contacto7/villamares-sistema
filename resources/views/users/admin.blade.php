@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Usuarios"),
        'icon' => "user"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3 mb-3">
                <a
                    class="form-control btn btn-villamares"
                    href="{{ route('users.create') }}"
                >
                    Añadir
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Rol</th>
                    <th scope="col">Oficina</th>
                    <th scope="col">correo</th>
                    <th scope="col">ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td style="text-transform: capitalize">{{ $user->role->name }}</td>
                        <td>{{ $user->office->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('vouchers.list', $user) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Comprobantes emitidos"
                                >
                                    <i class="fa fa-book"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('customers.list', $user) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Clientes registrados"
                                >
                                    <i class="fa fa-users"></i>
                                </a>
                            </div>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('companies.user', $user->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Compañías registradas"
                                >
                                    <i class="fa fa-building"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('users.edit', $user->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar usuario"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay usuarios disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $users->links() }}
        </div>
    </div>
@endsection
