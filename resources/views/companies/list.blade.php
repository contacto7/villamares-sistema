@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Empresas"),
        'icon' => "building"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.company.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Razón Social</th>
                    <th scope="col">RUC</th>
                    <th scope="col">Contacto</th>
                    <th scope="col">Teléfono contacto</th>
                    <th scope="col">Oficina</th>
                    <th scope="col">Registró</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->tax_number }}</td>
                        <td>{{ $company->contact_name." ".$company->contact_lat_name }}</td>
                        <td>{{ $company->customer->cellphone }}</td>
                        <td>{{ $company->office->name }}</td>
                        <td>{{ $company->user->name }}</td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('vouchers.company', $company->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Comprobantes emitidos"
                                >
                                    <i class="fa fa-book"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('companies.admin', $company->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </div>
                            <div class="btn-group">
                                @can('update', \App\Company::class)
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('companies.edit', $company->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                                <a
                                    class="btn btn-outline-info btn-block btn-notes"
                                    data-toggle="modal"
                                    data-target="#modalNotes"
                                    href="#modalNotes"
                                    data-id="{{ $company->id }}"
                                >
                                    <i class="fa fa-tag"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $companies->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        //Variable globar para guardar el id del cliente seleccionad
        let company_id_actual;
        /*
        * Función para ver las notas del cliente
        */
        $(document).on('click', '.btn-notes', function(){
            $(".modal-ajax-content").html('Cargando los datos...');

            let company_id = $(this).attr('data-id');

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('companyNotes.modalSeeForm') }}',
                data: {
                    company_id: company_id
                },
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        company_id_actual = company_id;
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para paginar las notase
        */
        $(document).on('click', '.notes-pagination .pagination a', function(e){
            e.preventDefault();
            console.log("Se hizo click");

            $(".modal-ajax-content").html('Cargando los datos...');

            var url = $(this).attr('href')+"&company_id="+company_id_actual;

            console.log(url);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: url,
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });


        /*
        * Función para ver las notas del cliente
        */
        $(document).on('click', '.agregar-nota', function(){
            $(this).hide();
            $("#form-nota").show();

        });
    </script>
@endpush
