@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Empresa"),
        'icon' => "building"
    ])
@endsection

@section('content')
    @foreach($companies as $company)
        <div class="container container-accounting-admin">
            <div class="row text-center flex-column">
                <h2 class="flex-row">{{ $company->name }}</h2>
                <h3 class="flex-row">
                    {{ $company->tax_number }}
                </h3>
            </div>
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                    <div class="info-row">
                        <span><b>Nombres de contacto:</b></span>
                        <span>{{ $company->contact_name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Apellidos de contacto:</b></span>
                        <span>{{ $company->contact_last_name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Correo de contacto:</b></span>
                        <span>{{ $company->contact_email }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Teléfono de contacto:</b></span>
                        <span>{{ $company->contact_phone }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Dirección:</b></span>
                        <span>{{ $company->address }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Nombres del gerente:</b></span>
                        <span>{{ $company->manager_name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Apellidos del gerente:</b></span>
                        <span>{{ $company->manager_last_name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>DNI del gerente:</b></span>
                        <span>{{ $company->manager_dni }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Régimen:</b></span>
                        <span>{{ $company->taxRegime->description }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Giro de la empresa:</b></span>
                        <span>{{ $company->companyTurn->name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Giro de la empresa (otro):</b></span>
                        <span>{{ $company->company_turn_other }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Fecha de registro:</b></span>
                        <span>{{ $company->created_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Fecha de actualización:</b></span>
                        <span>{{ $company->updated_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Servicios:</b></span>
                        @if($company->accounting)
                        <a
                            href="{{ route('accountings.admin',
                                ['id'=>$company->accounting->id]) }}"
                        >Servicio contable</a><span class="coma-servicios">,</span>
                        @endif
                        @if($company->constitution)
                        <a
                            href="{{ route('constitutions.admin',
                                ['id'=>$company->constitution->id]) }}"
                        >Constitución</a><span class="coma-servicios">,</span>
                        @endif
                        @foreach($company->extras as $extra)
                            <a
                                href="{{ route('extras.admin',
                                ['id'=>$extra->id]) }}"
                            >{{ $extra->serviceType->name }}</a><span class="coma-servicios">,</span>
                        @endforeach
                    </div>
                </div>
            </div>
            <h3>Cliente</h3>
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                    @include('partials.company.customer', ['customer' => $company->customer])
                </div>
            </div>
        </div>
    @endforeach
@endsection
