@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Empresa"),
        'icon' => "building"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $company->id ? route('companies.store'): route('companies.update', ['slug'=>$company->slug]) }}"
            novalidate
        >
            @if($company->id)
                @method('PUT')
            @endif

            @csrf
            <div class="form-group" style="@can('createOnlyFromOffice', \App\Company::class) display:none @endcan">
                <label for="office_id">Oficina*</label>
                <select
                    class="form-control {{ $errors->has('office_id') ? 'is-invalid': '' }}"
                    name="office_id"
                    id="office_id"
                    required
                >
                    @foreach(\App\Office::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('office_id') === $id || $company->office_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Cliente*</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#myModal"
                    href="#myModal"
                >
                    Seleccionar cliente
                </a>
            </div>
            <div class="form-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('customer_id') ? 'is-invalid': '' }}"
                    name="customer_id"
                    id="customer_id"
                    placeholder=""
                    value="{{ old('customer_id') ?: $company->customer_id }}"
                    required
                    autofocus
                >
            </div>
            <div class="form-group">
                <div id="name-customer" class="form-control {{ $errors->has('customer_id') ? 'is-invalid': '' }}" readonly="">
                    {{ $company->customer ? $company->customer->name." ".$company->customer->last_name:" " }}
                </div>
                @if($errors->has('customer_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('customer_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name">Razón social*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $company->name }}"
                    required
                    autofocus
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="tax_number">RUC*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('tax_number') ? 'is-invalid': '' }}"
                    name="tax_number"
                    id="tax_number"
                    placeholder=""
                    value="{{ old('tax_number') ?: $company->tax_number }}"
                    required
                >
                @if($errors->has('tax_number'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('tax_number') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="address">Dirección*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('address') ? 'is-invalid': '' }}"
                    name="address"
                    id="address"
                    placeholder=""
                    value="{{ old('address') ?: $company->address }}"
                    required
                >
                @if($errors->has('address'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="manager_name">Nombres de gerente*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('manager_name') ? 'is-invalid': '' }}"
                    name="manager_name"
                    id="manager_name"
                    placeholder=""
                    value="{{ old('manager_name') ?: $company->manager_name }}"
                    required
                    autofocus
                >
                @if($errors->has('manager_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('manager_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="manager_last_name">Apellidos de gerente*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('manager_last_name') ? 'is-invalid': '' }}"
                    name="manager_last_name"
                    id="manager_last_name"
                    placeholder=""
                    value="{{ old('manager_last_name') ?: $company->manager_last_name }}"
                    required
                >
                @if($errors->has('manager_last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('manager_last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="manager_dni">DNI de manager*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('manager_dni') ? 'is-invalid': '' }}"
                    name="manager_dni"
                    id="manager_dni"
                    placeholder=""
                    value="{{ old('manager_dni') ?: $company->manager_dni }}"
                    required
                >
                @if($errors->has('manager_dni'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('manager_dni') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="contact_name">Nombres de contacto*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('contact_name') ? 'is-invalid': '' }}"
                    name="contact_name"
                    id="contact_name"
                    placeholder=""
                    value="{{ old('contact_name') ?: $company->contact_name }}"
                    required
                    autofocus
                >
                @if($errors->has('contact_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('contact_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="contact_last_name">Apellidos de contacto*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('contact_last_name') ? 'is-invalid': '' }}"
                    name="contact_last_name"
                    id="contact_last_name"
                    placeholder=""
                    value="{{ old('contact_last_name') ?: $company->contact_last_name }}"
                    required
                >
                @if($errors->has('contact_last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('contact_last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="contact_email">Correo electrónico de contacto*</label>
                <input
                    type="email"
                    class="form-control {{ $errors->has('contact_email') ? 'is-invalid': '' }}"
                    name="contact_email"
                    id="contact_email"
                    placeholder=""
                    value="{{ old('contact_email') ?: $company->contact_email }}"
                    required
                >
                @if($errors->has('contact_email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('contact_email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="contact_phone">Teléfono de contacto*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('contact_phone') ? 'is-invalid': '' }}"
                    name="contact_phone"
                    id="contact_phone"
                    placeholder=""
                    value="{{ old('contact_phone') ?: $company->contact_phone }}"
                    required
                >
                @if($errors->has('contact_phone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('contact_phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="tax_regime_id">Régimen triutario*</label>
                <select
                    class="form-control {{ $errors->has('tax_regime_id') ? 'is-invalid': '' }}"
                    name="tax_regime_id"
                    id="tax_regime_id"
                    required
                >
                    @foreach(\App\TaxRegime::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('tax_regime_id') === $id || $company->tax_regime_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="company_turn_id">Giro de la empresa*</label>
                <select
                    class="form-control {{ $errors->has('company_turn_id') ? 'is-invalid': '' }}"
                    name="company_turn_id"
                    id="company_turn_id"
                >
                    @foreach(\App\CompanyTurn::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('company_turn_id') === $id || $company->company_turn_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="company_turn_other">Giro de la empresa (otro)</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_turn_other') ? 'is-invalid': '' }}"
                    name="company_turn_other"
                    id="company_turn_other"
                    placeholder=""
                    value="{{ old('company_turn_other') ?: $company->company_turn_other }}"
                    required
                    autofocus
                >
                @if($errors->has('company_turn_other'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('company_turn_other') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Cliente</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="customer-search">Documento de identidad</label>
                        <input
                            type="text"
                            class="form-control"
                            name="customer-search"
                            id="customer-search"
                            placeholder=""
                        >
                    </div>
                    <div class="form-group">
                        <a class="btn btn-outline-info" id="customer-search-btn">
                            Buscar
                        </a>
                    </div>
                    <div class="form-group">
                        <label for="customer-filtered">Cliente encontrado</label>
                        <input
                            type="text"
                            class="form-control"
                            name="customer-filtered"
                            id="customer-filtered"
                            readonly
                        >
                    </div>
                    <div class="form-group" style="display: none">
                        <label for="id-filtered">ID Cliente encontrado</label>
                        <input
                            type="text"
                            class="form-control"
                            name="id-filtered"
                            id="id-filtered"
                            readonly
                        >
                    </div>
                    <div class="form-group">
                        <a class="btn btn-outline-info" id="customer-add-btn" data-dismiss="modal">
                            Agregar
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).on('click', '#customer-search-btn', function(){
            console.log('se hizo click');
            let customer_search = $("#customer-search").val();

            console.log(customer_search);
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('customers.dniSearch') }}',
                data: {
                    customer_search: customer_search
                },
                success: function (data) {
                    $("#customer-filtered").val(data['name']+" "+data['last_name']);
                    $("#id-filtered").val(data['id']);
                },error:function(){
                    console.log(data);
                }
            });
        });
        $(document).on('click', '#customer-add-btn', function(){
            let customer_id= $("#id-filtered").val();
            let customer_name= $("#customer-filtered").val();
            $("#customer_id").val(customer_id);
            $("#name-customer").html(customer_name);
        });


    </script>
@endpush
