<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<style>
    body{
        font-size: 12px;
    }
    td:first-child{
        font-weight: bold;
    }
    .title{
        text-align: center;
    }
    .subtitle{
        text-align: center;
        padding-bottom: 10px;
    }
    .firma-row{
        padding: 10px 0;
    }
    @page {
        margin:20px;
    }
    td{
        vertical-align: top;
    }

</style>
<div>
    @php /* @endphp

    <div class="title">
        @php

        //CAPITALIZMOS SOLO LAS PALABRAS
            $words = explode(' ', $voucher->paymentDocument->name);
            $words_not_capitalize = ["de","por"];
            foreach ($words as $word ) {

                if (in_array($word, $words_not_capitalize)) {
                    echo $word;
                }else{
                    echo ucfirst ( $word ) ;
                }
                echo " ";
            }
        @endphp
         de
    </div>

    @php */ @endphp
    <div class="title">
        Recibo de Caja de Encargo
    </div>
    <div class="subtitle">
        Villamares Contadores Tributarios
    </div>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <td>
                    Fecha
                </td>
                <td>
                    {{ $voucher->emitted_at }}
                </td>
            </tr>
            <tr>
                <td>
                    Cliente
                </td>
                <td>
                    @switch($voucher->service_type_id)
                        @case(1)
                        @include(
                        'partials.vouchers.customerPDF',
                        ['customer' => $voucher->accounting->company->customer]
                        )

                        @break
                        @case(2)
                        @include(
                        'partials.vouchers.customerPDF',
                        ['customer' => $voucher->constitution->company->customer]
                        )
                        @break
                        @default
                        @include(
                        'partials.vouchers.customerPDF',
                        ['customer' => $voucher->extra->customer]
                        )
                    @endswitch
                </td>
            </tr>
            <tr>
                <td>
                    Empresa
                </td>
                <td>
                    @switch($voucher->service_type_id)
                        @case(1)
                        @include(
                        'partials.vouchers.companyPDF',
                        ['company' => $voucher->accounting->company]
                        )
                        @break
                        @case(2)
                        @include(
                        'partials.vouchers.companyPDF',
                        ['company' => $voucher->constitution->company]
                        )
                        @break
                        @default
                        @isset($voucher->extra->company->id)
                            @include(
                            'partials.vouchers.companyPDF',
                            ['company' => $voucher->extra->company]
                            )
                        @endisset
                    @endswitch
                </td>
            </tr>
            <tr>
                <td>
                    Servicio
                </td>
                <td>
                    {{ $voucher->description }}
                </td>
            </tr>
            <tr>
                <td>
                    Total
                </td>
                <td>
                    {{ $voucher->currency->symbol }}
                    {{ number_format($voucher->amount_total, 2, '.', "'") }}
                </td>
            </tr>
            <tr>
                <td>
                   Pagado
                </td>
                <td>
                    {{ $voucher->currency->symbol }}
                    {{ number_format($voucher->amount_payed, 2, '.', "'") }}
                </td>
            </tr>
            <tr>
                <td>
                   Saldo
                </td>
                <td>
                    {{ $voucher->currency->symbol }}
                    {{ number_format($voucher->amount_total - $voucher->amount_payed, 2, '.', "'") }}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                   <div class="firma-row">Firma:</div>
                </td>
            </tr>
            <tr>
                <td>
                   Cajero:
                </td>
                <td>
                    {{ auth()->user()->name." ". auth()->user()->last_name }}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Gracias. Esperamos verlo pronto.
                </td>
            </tr>
        </tbody>
    </table>
</div>


</body>
</html>
