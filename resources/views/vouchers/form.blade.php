@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Comprobantes"),
        'icon' => "list-alt"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $voucher->id ? route('vouchers.store'): route('vouchers.update', ['id'=>$voucher->id]) }}"
            novalidate
        >
            @if($voucher->id)
                @method('PUT')
            @endif

            @csrf
            <div class="form-group">
                <label for="payment_document_id">Tipo de comprobante</label>
                <select
                    class="form-control {{ $errors->has('payment_document_id') ? 'is-invalid': '' }}"
                    name="payment_document_id"
                    id="payment_document_id"
                    required
                >
                    @foreach($allowedDocuments as $paymentDocs)
                        <option
                            {{ (int) old('payment_document_id') === $paymentDocs->id || $voucher->payment_document_id === $paymentDocs->id ? 'selected' : '' }}
                            value="{{ $paymentDocs->id }}"
                        >{{ $paymentDocs->name }} - {{ $paymentDocs->titular }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" id="voucher-number-row">
                <label for="number">Número de comprobante</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('number') ? 'is-invalid': '' }}"
                    name="number"
                    id="number"
                    placeholder=""
                    value="{{ old('number') ?: $voucher->number }}"
                    required
                >
                @if($errors->has('number'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('number') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="description">Descripción</label>
                <textarea
                    class="form-control {{ $errors->has('description') ? 'is-invalid': '' }}"
                    name="description"
                    id="description"
                    placeholder=""
                    required
                >{{ old('description') ?: $voucher->description }}</textarea>
                @if($errors->has('description'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="associate_voucher">Comprobante asociado</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('associate_voucher') ? 'is-invalid': '' }}"
                    name="associate_voucher"
                    id="associate_voucher"
                    placeholder=""
                    readonly
                    value="{{ old('number') ?: $voucher->associate_voucher }}"
                    required
                >
                @if($errors->has('associate_voucher'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('associate_voucher') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="currency_id">Moneda</label>
                <select
                    class="form-control {{ $errors->has('currency_id') ? 'is-invalid': '' }}"
                    name="currency_id"
                    id="currency_id"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_id') === $id || $voucher->currency_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="amount_total">Monto total</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('amount_total') ? 'is-invalid': '' }}"
                    name="amount_total"
                    id="amount_total"
                    placeholder=""
                    value="{{ old('amount_total') ?: $voucher->amount_total }}"
                    required
                >
                @if($errors->has('amount_total'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('amount_total') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="service_type_id">Tipo de servicio</label>
                <select
                    class="form-control {{ $errors->has('service_type_id') ? 'is-invalid': '' }}"
                    name="service_type_id"
                    id="service_type_id"
                    required
                >
                    @foreach($allowedServices as $serviceType)
                        <option
                            {{ (int) old('service_type_id') === $serviceType->id || $voucher->service_type_id === $serviceType->id ? 'selected' : '' }}
                            value="{{ $serviceType->id }}"
                        >{{ $serviceType->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group service-group" id="service-1">
                <label>Empresa</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modal-1"
                    href="#modal-1"
                >
                    Seleccionar empresa
                </a>
            </div>
            <div class="form-group service-group" id="service-2" style="display: none">
                <label for="name">Empresa</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modal-2"
                    href="#modal-2"
                >
                    Seleccionar empresa
                </a>
            </div>
            <div class="form-group service-group" id="service-3" style="display: none;">
                <label for="name">Cliente</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modal-3"
                    href="#modal-3"
                >
                    Seleccionar cliente
                </a>
            </div>
            <div class="form-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('accounting_id') ? 'is-invalid': '' }} service_type_input"
                    name="accounting_id"
                    id="accounting_id"
                    placeholder="Servicio contable"
                    value="{{ old('accounting_id') ?: $voucher->accounting_id }}"
                    required
                    autofocus
                >
                <input
                    type="text"
                    class="form-control {{ $errors->has('constitution_id') ? 'is-invalid': '' }} service_type_input"
                    name="constitution_id"
                    id="constitution_id"
                    placeholder="Constituciones"
                    value="{{ old('constitution_id') ?: $voucher->constitution_id }}"
                    required
                    autofocus
                >
                <input
                    type="text"
                    class="form-control {{ $errors->has('extra_id') ? 'is-invalid': '' }} service_type_input"
                    name="extra_id"
                    id="extra_id"
                    placeholder="Extras"
                    value="{{ old('extra_id') ?: $voucher->extra_id }}"
                    required
                    autofocus
                >
            </div>
            <div class="form-group">
                <div id="name_customer" class="form-control" readonly="">
                    @switch($voucher->service_type_id)
                        @case(1)
                        <span>
                            @isset($voucher->accounting->company->id)
                                {{
                                    $voucher->accounting->company->name
                                }}
                            @endisset
                        </span>
                        @break
                        @case(2)
                        <span>
                            @isset($voucher->constitution->company->id)
                                {{
                                    $voucher->constitution->company->name
                                }}
                            @endisset
                        </span>
                        @break
                        @default
                        <span>
                            @if($voucher->extra)
                                @if($voucher->extra->company)
                                    {{ $voucher->extra->company->name }}
                                @elseif($voucher->extra->customer)
                                    {{ $voucher->extra->customer->name }} {{ $voucher->extra->customer->last_name }}
                                @endif
                            @endif
                        </span>
                    @endswitch



                </div>
                @if($errors->has('company_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('company_id') }}</strong>
                    </span>
                @endif
                @if($errors->has('customer_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('customer_id') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="emitted_at">Fecha de emisión</label>
                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input
                        type="text"
                        name="emitted_at"
                        id="emitted_at"
                        class="form-control datetimepicker-input {{ $errors->has('emitted_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker1"
                        value="{{ old('emitted_at') ?: $voucher->emitted_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('emitted_at'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('emitted_at') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="expired_at">Fecha de vencimiento</label>
                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                    <input
                        type="text"
                        name="expired_at"
                        id="expired_at"
                        class="form-control datetimepicker-input {{ $errors->has('expired_at') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker2"
                        value="{{ old('expired_at') ?: $voucher->expired_at }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('expired_at'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('expired_at') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
    <!-- Modal para la contabilidad -->
    @include('partials.vouchers.accountingModal')
    <!-- Modal para la constitución -->
    @include('partials.vouchers.constitutionModal')
    <!-- Modal para el extra -->
    @include('partials.vouchers.extraModal')

@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        let voucher_number = '{{ $voucher->number }}';
        let original_voucher_type = parseInt({{ $voucher->payment_document_id }});
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
            let voucher_type = $("#payment_document_id").val();
            if(voucher_type === "4"){
                $("#voucher-number-row").hide();
            }else{
                $("#voucher-number-row").show();
            }

            changeModalButton();
        });
        //CONTABILIDAD, CONSTITUCION Y EXTRA
        /*
        * Función para obtener el id del servicio y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.company-search-btn', function(){
            console.log('se hizo click');
            //Limpiamos los valores del cliente
            $(".customer-search").val("");

            //Obtenemos el tipo del servicio del selector "Tipo de servicio" en el formulario
            let service_type = parseInt($("#service_type_id").val());
            let company_search;

            //Obtenemos el modal de acuerdo al tipo del servicio
            if(service_type>=3){
                company_search = $("#modal-3 .company-search").val();
            }else{
                company_search = $("#modal-"+service_type+" .company-search").val();

            }

            let service_name = "";

            //console.log(company_search);

            //Enviamos una solicitud con el tipo del servicio y el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('companies.rucSearch') }}',
                data: {
                    company_search: company_search,
                    service_type:service_type
                },
                success: function (data) {
                    let service_id = null;

                    //console.log(data);

                    /*
                    * Definimos el nombre del tipo de servicio,
                    * que luego se usará para obtener los dato
                    * de servicio del objeto regresado
                    */
                    switch (service_type) {
                        case 1:
                            service_name="accounting";
                            break;
                        case 2:
                            service_name="constitution";
                            break;
                        default:
                            service_name="extra";
                            break;
                    }

                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data[service_name]) != "undefined"
                        &&
                        data[service_name] !== null){
                        service_id = data[service_name]['id'];
                        console.log("Servicio "+service_id);
                    }else if(
                        typeof(data[0]["extras"]) != "undefined"
                        &&
                        data[0]["extras"] !== null){

                    }else{
                        console.log("Longitud");
                    }

                    if(service_type>=3){
                        /*
                        * Si el servicio es extra (tipo de servicio a partir de 3),
                        * se añaden opciones,
                        * con los valores de los id en sus atributos 'value'
                        * y con los nombres de los servicios en el label
                        */

                        $("#extras-list").html("");

                        console.log(data);
                        $.each(data[0]["extras"], function (i,k) {
                            console.log(k);

                            let extra_option = "";
                            extra_option += '<div class="form-check">';
                            extra_option += '<label class="form-check-label">';
                            extra_option += '<input type="radio" class="form-check-input" name="extraOption" value="';
                            extra_option += k.id;
                            extra_option += '" ';
                            extra_option += 'data-name="';
                            extra_option += data[0].name;
                            extra_option += '">';
                            extra_option += k.service_type.name;
                            extra_option += ", ";
                            extra_option += k.done_at;
                            extra_option += ", ";
                            extra_option += k.currency.symbol;
                            extra_option += k.charged_amount;
                            extra_option += '</label>';
                            extra_option += '</div>';

                            /*
                            * Se añade el html en el elemento con id "extras-list"
                            * dentro del modal
                            */

                            $("#extras-list").append(extra_option);
                        });



                    }else{
                        /*
                        * Si es servicio es contablidad o constitución,
                        * el id del servicio y en nombre de la empresa
                        * se guarda en un input escondido y en un input
                        * no editable, dentro del modal
                        */

                        $("#modal-"+service_type+" .customer-filtered").val(data['name']);
                        $("#modal-"+service_type+" .id-filtered").val(service_id);
                    }
                },error:function(){
                    console.log(data);
                }
            });
        });
        //SERVICIO EXTRA
        /*
        * Función para obtener el id del servicio y el nombre de la persona,
        * según el DNI brindado
        */
        $(document).on('click', '.customer-search-btn', function(){
            console.log('se hizo click');
            //Limpiamos los valores de la empresa
            $(".company-search").val("");

            //Obtenemos el tipo del servicio del selector "Tipo de servicio" en el formulario
            let service_type = parseInt($("#service_type_id").val());
            let customer_search;

            //Obtenemos el modal de acuerdo al tipo del servicio
            if(service_type>=3){
                customer_search = $("#modal-3 .customer-search").val();
            }else{
                customer_search = $("#modal-"+service_type+" .customer-search").val();
            }

            //console.log(service_type);
            //Enviamos una solicitud con el tipo del servicio y el DNI del cliente
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('customers.dniSearch') }}',
                data: {
                    customer_search: customer_search,
                    service_type:service_type
                },
                success: function (data) {
                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data[0]["extras"]) != "undefined"
                        &&
                        data[0]["extras"] !== null){

                        $("#extras-list").html("");

                        console.log(data);
                        $.each(data[0]["extras"], function (i,k) {
                            /*
                            * Se añaden opciones,
                            * con los valores de los id en sus atributos 'value'
                            * y con los nombres de los servicios en el label
                            */

                            //console.log(k);

                            let extra_option = "";
                            extra_option += '<div class="form-check">';
                            extra_option += '<label class="form-check-label">';
                            extra_option += '<input type="radio" class="form-check-input" name="extraOption" value="';
                            extra_option += k.id;
                            extra_option += '" ';
                            extra_option += 'data-name="';
                            extra_option += data[0].name;
                            extra_option += " ";
                            extra_option += data[0].last_name;
                            extra_option += '">';
                            extra_option += k.service_type.name;
                            extra_option += ", ";
                            extra_option += k.done_at;
                            extra_option += ", ";
                            extra_option += k.currency.symbol;
                            extra_option += k.charged_amount;
                            extra_option += '</label>';
                            extra_option += '</div>';




                            $("#extras-list").append(extra_option);
                        });


                    }else{
                        console.log("No hay datos");
                    }

                    /*
                    $("#modal-"+service_type+" .customer-filtered").val(data['name']+" "+data['last_name']);
                    $("#modal-"+service_type+" .id-filtered").val(data['id']);
                    */
                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para añadir el id del servicio y el nombre del cliente
        * al input del formulario
        */
        $(document).on('click', '.customer-add-btn', function(){
            //Obtenemos el tipo del servicio del selector "Tipo de servicio" en el formulario
            let service_type = parseInt($("#service_type_id").val());
            let service_name = "";

            let customer_id;
            let customer_name;

            switch (service_type) {
                case 1:
                    service_name="accounting";
                    customer_id= $("#modal-1 .id-filtered").val();
                    customer_name= $("#modal-1 .customer-filtered").val();
                    break;
                case 2:
                    service_name="constitution";
                    customer_id= $("#modal-2 .id-filtered").val();
                    customer_name= $("#modal-2 .customer-filtered").val();
                    break;
                default:
                    service_name="extra";
                    customer_id= $('input[name=extraOption]:checked').val();
                    customer_name= $('input[name=extraOption]:checked').data('name');
                    break;
            }

            console.log(service_type);
            console.log(service_name);
            $(".service_type_input").val("");
            $("#"+service_name+"_id").val(customer_id);
            $("#name_customer").html(customer_name);

            $(".customer-search").val("");
            $(".company-search").val("");
            $(".customer-filtered").val("");
            $("#extras-list").html("");
        });
        /*
        * Función para cambiar el botón del modal, según el servicio,
        * cuando se cambio el selector de Tipo de servicio
        */
        $(document).on('change', '#service_type_id', function(){
            console.log("cambio");
            changeModalButton();
        });
        /*
        * Función para cambiar el estado del número d comprobante
        * según el tipo de comprobante
        */
        $(document).on('change', '#payment_document_id', function(){
            changeStateVoucherType();
        });

        function changeModalButton() {

            let service_type = $("#service_type_id").val();

            $(".service-group").hide();

            if(service_type === "1"){
                $("#service-1").show();
            }else if(service_type === "2"){
                $("#service-2").show();
            }else{
                $("#service-3").show();
            }
        }

        function changeStateVoucherType() {
            console.log("cambio");
            let voucher_type = $("#payment_document_id").val();

            if(voucher_type === "4"){
                $('#number').attr('value', 1);
                $("#voucher-number-row").hide();
            }else{

                $('#number').attr('value', "");
                $("#voucher-number-row").show();
            }

            if(original_voucher_type == 4){
                console.log("Originalmente RC");
                if(voucher_type !== "4"){
                    $('#associate_voucher').attr('value', voucher_number);
                    $('#associate_voucher').val(voucher_number);
                }
            }

        }


    </script>
@endpush
