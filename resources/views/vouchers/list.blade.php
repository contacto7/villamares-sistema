@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Comprobantes de pago"),
        'icon' => "list-alt"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.vouchers.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Número</th>
                    <th scope="col">Comprobante</th>
                    <th scope="col">Pagado</th>
                    <th scope="col">Total</th>
                    <th scope="col">Emitido</th>
                    <th scope="col">Servicio</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Registró</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($vouchers as $voucher)
                    <tr>
                        <td>{{ $voucher->id }}</td>
                        <td>{{ $voucher->number }}</td>
                        <td>{{ $voucher->paymentDocument->name }}</td>
                        <td>
                            {{ $voucher->currency->symbol }}{{ number_format($voucher->amount_payed, 2, '.', "'") }}
                        </td>
                        <td>
                            {{ $voucher->currency->symbol }}{{ number_format($voucher->amount_total, 2, '.', "'") }}
                        </td>
                        <td>{{ $voucher->emitted_at }}</td>
                        <td>{{ $voucher->serviceType->name }}</td>
                        <td>

                            @switch($voucher->service_type_id)
                                @case(1)
                                <span>
                                    @isset($voucher->accounting->company->customer->id)
                                        {{
                                            $voucher->accounting->company->customer->name
                                        }}
                                    @endisset
                                </span>
                                @break
                                @case(2)
                                <span>
                                    @isset($voucher->constitution->company->customer->id)
                                        {{
                                            $voucher->constitution->company->customer->name
                                        }}
                                    @endisset
                                </span>
                                @break
                                @default
                                <span>
                                    @if($voucher->extra->company)
                                        {{
                                            $voucher->extra->company->customer->name
                                        }}
                                    @else
                                        {{
                                            $voucher->extra->customer->name
                                        }}
                                    @endif
                                </span>
                            @endswitch
                        </td>
                        <td>

                            @switch($voucher->service_type_id)
                                @case(1)
                                <span>
                                    @isset($voucher->accounting->company->id)
                                        {{
                                            $voucher->accounting->company->name
                                        }}
                                    @endisset
                                </span>
                                @break
                                @case(2)
                                <span>
                                    @isset($voucher->constitution->company->id)
                                        {{
                                            $voucher->constitution->company->name
                                        }}
                                    @endisset
                                </span>
                                @break
                                @default
                                <span>
                                    @isset($voucher->extra->company->id)
                                        {{
                                            $voucher->extra->company->name
                                        }}
                                    @endisset
                                </span>
                            @endswitch
                        </td>
                        <td>{{ $voucher->userRegistered->name }}</td>
                        <td class="btn-group-vertical">
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('vouchers.admin', ['id'=>$voucher->id]) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                @can('update', \App\Voucher::class)
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('vouchers.edit', $voucher->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                            </div>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{action('VoucherController@downloadPDF', $voucher->id)}}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Descargar"

                                >
                                    <i class="fa fa-download"></i>
                                </a>

                                @can('update', \App\Voucher::class)
                                <form action="{{ route('vouchers.destroy', $voucher->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button
                                        type="submit"
                                        class="btn btn-outline-danger"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Eliminar"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay comprobantes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $vouchers->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
