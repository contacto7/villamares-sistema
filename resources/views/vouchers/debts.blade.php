@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Cuentas por cobrar"),
        'icon' => "money"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @can('seeResume', [\App\Voucher::class] )
            <table class="table table-bordered table-transparent">
                <thead>
                <tr>
                    <th scope="col">Comprobante</th>
                    <th scope="col">Deuda soles</th>
                    <th scope="col">Deuda dólares</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Factura</td>
                        <td>
                            S/
                            {{
                                number_format($deuda_por_comprobante['factura_debt_soles'] , 2, '.', "'")
                            }}
                        </td>
                        <td>
                            $
                            {{
                                number_format($deuda_por_comprobante['factura_debt_dolares'] , 2, '.', "'")
                            }}
                        </td>
                    </tr>
                    <tr>
                        <td>Recibo de caja</td>
                        <td>
                            S/
                            {{
                                number_format($deuda_por_comprobante['recibo_caja_debt_soles'] , 2, '.', "'")
                            }}
                        </td>
                        <td>
                            $
                            {{
                                number_format($deuda_por_comprobante['recibo_caja_debt_dolares'] , 2, '.', "'")
                            }}
                        </td>
                    </tr>
                </tbody>
            </table>
            @endcan
        </div>
        <div class="row mb-3">
            <div class="col-sm-4 btn-group">
                @can('seeResume', [\App\Voucher::class] )
                    <a
                        class="btn btn-villamares"
                        href="{{ route('vouchers.downloadExcel') }}"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="Descargar"
                    >
                        <i class="fa fa-info-circle"></i>
                        Descargar Reporte
                    </a>
                @endcan
            </div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Servicio</th>
                    <th scope="col">Comprobante</th>
                    <th scope="col">Número</th>
                    <th scope="col">Días</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Emisor</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($vouchers as $voucher)
                    <tr>
                        <td>{{ $voucher->id }}</td>
                        <td>

                            @switch($voucher->service_type_id)
                                @case(1)
                                <span>
                                    @isset($voucher->accounting->company->customer->id)
                                        {{
                                            $voucher->accounting->company->customer->name
                                        }}
                                    @endisset
                                </span>
                                @break
                                @case(2)
                                <span>
                                    @isset($voucher->constitution->company->customer->id)
                                        {{
                                            $voucher->constitution->company->customer->name
                                        }}
                                    @endisset
                                </span>
                                @break
                                @default
                                <span>
                                    @isset($voucher->extra->customer->id)
                                        {{
                                            $voucher->extra->customer->name
                                        }}
                                    @endisset
                                </span>
                            @endswitch
                        </td>
                        <td>{{ $voucher->serviceType->name }}</td>
                        <td>{{ $voucher->paymentDocument->name }}</td>
                        <td>{{ $voucher->number }}</td>
                        <td>
                         <?php

                            $datetime1 = new DateTime($voucher->emitted_at);
                            $datetime2 = new DateTime(now());
                            $interval = $datetime1->diff($datetime2);
                            $days = $interval->format('%a');

                            echo $days;

                        ?>
                        </td>

                        <td>
                            <div>
                                {{ $voucher->currency->symbol }}{{ number_format($voucher->amount_total - $voucher->amount_payed, 2, '.', "'") }}
                            </div>
                        </td>
                        <td>{{ $voucher->userEmitted->name }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('vouchers.admin', ['id'=>$voucher->id]) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Más información"
                            >
                                <i class="fa fa-info-circle"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay comprobantes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $vouchers->links() }}
        </div>
    </div>
@endsection
