@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Comprobante"),
        'icon' => "list-alt"
    ])
@endsection

@section('content')
    @foreach($vouchers as $voucher)
        <div class="container container-accounting-admin">
            <div class="row text-center flex-column">
                <h2 class="flex-row">{{ $voucher->paymentDocument->name }}
                    - {{ $voucher->paymentDocument->titular }}</h2>
                <h3 class="flex-row">
                    {{ $voucher->number }}
                </h3>
            </div>
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                    <div class="info-row">
                        <span><b>Servicio:</b></span>
                        <span>{{ $voucher->serviceType->name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Descripción:</b></span>
                        <span>{{ $voucher->description }}</span>
                    </div>
                    @if($voucher->associate_voucher)
                    <div class="info-row">
                        <span><b>Comprobante asociado:</b></span>
                        <span>{{ $voucher->associate_voucher }}</span>
                    </div>
                    @endif
                    <div class="info-row">
                        <span><b>Cancelado / Total:</b></span>
                        <span>
                            {{ $voucher->currency->symbol }}
                            {{ number_format($voucher->amount_payed, 2, '.', "'") }} /
                            {{ $voucher->currency->symbol }}
                            {{ number_format($voucher->amount_total, 2, '.', "'") }}
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Emitido en:</b></span>
                        <span>{{ $voucher->emitted_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Cancelado en:</b></span>
                        <span>{{ $voucher->cancelled_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Expira en:</b></span>
                        <span>{{ $voucher->expired_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Usuario que registró:</b></span>
                        <span>
                        {{ $voucher->userRegistered->name }}
                            {{ $voucher->userRegistered->last_name }}
                        </span>
                    </div>
                </div>
            </div>
            <h3>Cliente</h3>
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                @switch($voucher->service_type_id)
                    @case(1)
                        @include(
                        'partials.vouchers.customer',
                        ['customer' => $voucher->accounting->company->customer]
                        )
                    @break
                    @case(2)
                        @include(
                        'partials.vouchers.customer',
                        ['customer' => $voucher->constitution->company->customer]
                        )
                    @break
                    @default
                        @isset($voucher->extra->customer)
                            @include(
                            'partials.vouchers.customer',
                            ['customer' => $voucher->extra->customer]
                            )
                        @else
                            @include(
                            'partials.vouchers.customer',
                            ['customer' => $voucher->extra->company->customer]
                            )
                        @endisset
                @endswitch
                </div>
            </div>
            @switch($voucher->service_type_id)
                @case(1)
                @include(
                'partials.vouchers.company',
                ['company' => $voucher->accounting->company]
                )
                @break
                @case(2)
                @include(
                'partials.vouchers.company',
                ['company' => $voucher->constitution->company]
                )
                @break
                @default
                @isset($voucher->extra->company->id)
                    @include(
                    'partials.vouchers.company',
                    ['company' => $voucher->extra->company]
                    )
                @endisset
            @endswitch
        </div>
    @endforeach
@endsection
