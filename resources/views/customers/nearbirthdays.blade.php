@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Próximos cumpleaños"),
        'icon' => "birthday-cake"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Doc.</th>
                    <th scope="col">Número</th>
                    <th scope="col">Cumpleaños</th>
                    <th scope="col">Registró</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customers as $customer)
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->last_name }}</td>
                        <td>{{ $customer->cellphone }}</td>
                        <td>{{ $customer->identityDocument->name }}</td>
                        <td>{{ $customer->document_number }}</td>
                        <td>{{ $customer->birthday }}</td>
                        <td>{{ $customer->user->name }}</td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('customers.admin', $customer->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Más información"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info btn-block btn-notes"
                                    data-toggle="modal"
                                    data-target="#modalNotes"
                                    href="#modalNotes"
                                    data-id="{{ $customer->id }}"
                                >
                                    <i class="fa fa-tag"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $customers->links() }}
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        //Variable globar para guardar el id del cliente seleccionad
        let customer_id_actual;
        /*
        * Función para ver las notas del cliente
        */
        $(document).on('click', '.btn-notes', function(){
            $(".modal-ajax-content").html('Cargando los datos...');

            let customer_id = $(this).attr('data-id');

            //console.log(company_search);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('notes.modalSeeForm') }}',
                data: {
                    customer_id: customer_id
                },
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        customer_id_actual = customer_id;
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });
        /*
        * Función para paginar las notase
        */
        $(document).on('click', '.notes-pagination .pagination a', function(e){
            e.preventDefault();
            console.log("Se hizo click");

            $(".modal-ajax-content").html('Cargando los datos...');

            var url = $(this).attr('href')+"&customer_id="+customer_id_actual;

            console.log(url);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: url,
                success: function (data) {
                    if(data.includes("Error")){
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        $(".modal-ajax-content").html(data);
                    }

                },error:function(){
                    console.log(data);
                }
            });
        });


        /*
        * Función para ver las notas del cliente
        */
        $(document).on('click', '.agregar-nota', function(){
            $(this).hide();
            $("#form-nota").show();

        });
    </script>
@endpush
