@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Cliente"),
        'icon' => "user"
    ])
@endsection

@section('content')
    @foreach($customers as $customer)
        <div class="container container-accounting-admin">
            <div class="row text-center flex-column">
                <h2 class="flex-row">{{ $customer->name }} {{ $customer->last_name }}</h2>
                <h3 class="flex-row">
                    {{ $customer->identityDocument->name }} - {{ $customer->document_number }}
                </h3>
            </div>
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                    <div class="info-row">
                        <span><b>Correo:</b></span>
                        <span>{{ $customer->email }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Teléfono:</b></span>
                        <span>{{ $customer->cellphone }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Cumpleaños:</b></span>
                        <span>{{ date('Y-m-d', strtotime($customer->birthday)) }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Tipo de cliente:</b></span>
                        <span>
                            @switch($customer->type)
                                @case(\App\Customer::NORMAL)
                                <span>Normal</span>
                                @break
                                @case(\App\Customer::PROBLEMATIC)
                                <span>Problemático</span>
                                @break
                                @default
                                <span></span>
                            @endswitch
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Medio de captación:</b></span>
                        <span>
                            @switch($customer->acquisition_way)
                                @case(\App\Customer::FACEBOOK)
                                <span>Facebook</span>
                                @break
                                @case(\App\Customer::INSTAGRAM)
                                <span>Instagram</span>
                                @break
                                @case(\App\Customer::GOOGLE_ADS)
                                <span>Google Ads</span>
                                @break
                                @case(\App\Customer::REFERER)
                                <span>Referidos</span>
                                @break
                                @case(\App\Customer::OTHER_ACQUISITION_WAY)
                                <span>Otro</span>
                                @break
                                @default
                                <span></span>
                            @endswitch
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Detalles adicionales a captación:</b></span>
                        <span>{{ $customer->other_acquisition_way }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Tipo de prospecto:</b></span>
                        <span>
                            @switch($customer->prospect_type)
                                @case(\App\Customer::CONSTITUTION_PROSPECT)
                                <span>Constitución</span>
                                @break
                                @case(\App\Customer::ACCOUNTING_PROSPECT)
                                <span>Contabilidad</span>
                                @break
                                @case(\App\Customer::CUSTOMER)
                                <span>Cliente</span>
                                @break
                                @default
                                <span></span>
                            @endswitch
                        </span>
                    </div>
                    <div class="info-row">
                        <span><b>Oficina:</b></span>
                        <span>{{ $customer->office->name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Usuario que registró:</b></span>
                        <span>{{ $customer->user->name }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Fecha de registro:</b></span>
                        <span>{{ $customer->created_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Fecha de actualización:</b></span>
                        <span>{{ $customer->updated_at }}</span>
                    </div>
                    <div class="info-row">
                        <span><b>Servicios:</b></span>
                        @foreach($customer->extras as $extra)
                            <a
                                href="{{ route('extras.admin',
                                ['id'=>$extra->id]) }}"
                            >{{ $extra->serviceType->name }}</a><span class="coma-servicios">,</span>
                        @endforeach
                    </div>
                </div>
            </div>
            @can('list', \App\Company::class)
            <h3>Empresas</h3>
            <div class="row mt-3 mb-3">
                <div class="info-wrapper">
                    @foreach($customer->companies as $company)
                    <div class="info-row">
                            <a
                                href="{{ route('companies.admin',
                                ['id'=>$company->id]) }}"
                            >
                                <i class="fa fa-building"></i>
                                {{ $company->name }}
                            </a><span class="coma-servicios">,</span>
                    </div>
                    @endforeach
                </div>
            </div>
            @endcan
        </div>
    @endforeach
@endsection
