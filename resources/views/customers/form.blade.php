@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Cliente"),
        'icon' => "user"
    ])
@endsection


@section('content')
<div class="container container-accounting-admin">
    <form
        method="POST"
        action="{{ ! $customer->id ? route('customers.store'): route('customers.update', ['slug'=>$customer->slug]) }}"
        novalidate
    >
        @if($customer->id)
            @method('PUT')
        @endif

        @csrf
            <div class="form-group">
                <label for="name">Nombres*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $customer->name }}"
                    required
                    autofocus
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="last_name">Apellidos*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('last_name') ? 'is-invalid': '' }}"
                    name="last_name"
                    id="last_name"
                    placeholder=""
                    value="{{ old('last_name') ?: $customer->last_name }}"
                    required
                >
                @if($errors->has('last_name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Correo electrónico*</label>
                <input
                    type="email"
                    class="form-control {{ $errors->has('email') ? 'is-invalid': '' }}"
                    name="email"
                    id="email"
                    placeholder=""
                    value="{{ old('email') ?: $customer->email }}"
                    required
                >
                @if($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="cellphone">Teléfono*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('cellphone') ? 'is-invalid': '' }}"
                    name="cellphone"
                    id="cellphone"
                    placeholder=""
                    value="{{ old('cellphone') ?: $customer->cellphone }}"
                    required
                >
                @if($errors->has('cellphone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('cellphone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="birthday">Cumpleaños</label>
                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input
                        type="text"
                        name="birthday"
                        id="birthday"
                        class="form-control datetimepicker-input {{ $errors->has('birthday') ? 'is-invalid': '' }}"
                        data-target="#datetimepicker1"
                        value="{{ old('birthday') ?: $customer->birthday }}"
                    />
                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('birthday'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('birthday') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="identity_document_id">Tipo de documento*</label>
                <select
                    class="form-control {{ $errors->has('identity_document_id') ? 'is-invalid': '' }}"
                    name="identity_document_id"
                    id="identity_document_id"
                    required
                >
                    @foreach(\App\IdentityDocument::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('identity_document_id') === $id || $customer->identity_document_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="document_number">Número de documento*</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('document_number') ? 'is-invalid': '' }}"
                    name="document_number"
                    id="document_number"
                    placeholder=""
                    value="{{ old('document_number') ?: $customer->document_number }}"
                    required
                >
                @if($errors->has('document_number'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('document_number') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="office_id">Oficina*</label>
                <select
                    class="form-control {{ $errors->has('office_id') ? 'is-invalid': '' }}"
                    name="office_id"
                    id="office_id"
                    required
                >
                    @foreach(\App\Office::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('office_id') === $id || $customer->office_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="type">Tipo de cliente*</label>
                <select
                    class="form-control {{ $errors->has('type') ? 'is-invalid': '' }}"
                    name="type"
                    id="type"
                    required
                >
                    <option
                        {{ (int) old('type') === \App\Customer::NORMAL || (int) $customer->type === \App\Customer::NORMAL ? 'selected' : '' }}
                        value="1"
                    >Normal</option>
                    <option
                        {{ (int) old('type') === \App\Customer::PROBLEMATIC || (int) $customer->type === \App\Customer::PROBLEMATIC ? 'selected' : '' }}
                        value="2"
                    >Problemático</option>
                </select>
            </div>
            <div class="form-group">
                <label for="acquisition_way">Medio de captación*</label>
                <select
                    class="form-control {{ $errors->has('acquisition_way') ? 'is-invalid': '' }}"
                    name="acquisition_way"
                    id="acquisition_way"
                    required
                >
                    <option
                        {{ (int) old('acquisition_way') === \App\Customer::FACEBOOK || (int) $customer->acquisition_way === \App\Customer::FACEBOOK ? 'selected' : '' }}
                        value="{{ \App\Customer::FACEBOOK }}"
                    >Facebook</option>
                    <option
                        {{ (int) old('acquisition_way') === \App\Customer::INSTAGRAM || (int) $customer->acquisition_way === \App\Customer::INSTAGRAM ? 'selected' : '' }}
                        value="{{ \App\Customer::INSTAGRAM }}"
                    >Instragram</option>
                    <option
                        {{ (int) old('acquisition_way') === \App\Customer::GOOGLE_ADS || (int) $customer->acquisition_way === \App\Customer::GOOGLE_ADS ? 'selected' : '' }}
                        value="{{ \App\Customer::GOOGLE_ADS }}"
                    >Google Ads</option>
                    <option
                        {{ (int) old('acquisition_way') === \App\Customer::REFERER || (int) $customer->acquisition_way === \App\Customer::REFERER ? 'selected' : '' }}
                        value="{{ \App\Customer::REFERER }}"
                    >Referidos</option>
                    <option
                        {{ (int) old('acquisition_way') === \App\Customer::OTHER_ACQUISITION_WAY || (int) $customer->acquisition_way === \App\Customer::OTHER_ACQUISITION_WAY ? 'selected' : '' }}
                        value="{{ \App\Customer::OTHER_ACQUISITION_WAY }}"
                    >Otro</option>
                </select>
            </div>
            <div class="form-group">
                <label for="other_acquisition_way">Detalles adicionales a captación</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('other_acquisition_way') ? 'is-invalid': '' }}"
                    name="other_acquisition_way"
                    id="other_acquisition_way"
                    placeholder=""
                    value="{{ old('other_acquisition_way') ?: $customer->other_acquisition_way }}"
                    required
                >
                @if($errors->has('other_acquisition_way'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('other_acquisition_way') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="prospect_type">Tipo de prospecto*</label>
                <select
                    class="form-control {{ $errors->has('prospect_type') ? 'is-invalid': '' }}"
                    name="prospect_type"
                    id="prospect_type"
                    required
                >
                    <option
                        {{ (int) old('prospect_type') === \App\Customer::CONSTITUTION_PROSPECT || (int) $customer->prospect_type === \App\Customer::CONSTITUTION_PROSPECT ? 'selected' : '' }}
                        value="{{ \App\Customer::CONSTITUTION_PROSPECT }}"
                    >Constitución</option>
                    <option
                        {{ (int) old('prospect_type') === \App\Customer::ACCOUNTING_PROSPECT || (int) $customer->prospect_type === \App\Customer::ACCOUNTING_PROSPECT ? 'selected' : '' }}
                        value="{{ \App\Customer::ACCOUNTING_PROSPECT }}"
                    >Contabilidad</option>
                    <option
                        {{ (int) old('prospect_type') === \App\Customer::CUSTOMER || (int) $customer->prospect_type === \App\Customer::CUSTOMER ? 'selected' : '' }}
                        value="{{ \App\Customer::CUSTOMER }}"
                    >Cliente</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

    </form>

</div>
@endsection
@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es'
            });
        });
    </script>
@endpush
