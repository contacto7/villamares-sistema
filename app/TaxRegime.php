<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TaxRegime
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaxRegime whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 */
class TaxRegime extends Model
{
    public $timestamps = false;

    public function companies(){
        return $this->hasMany(Company::class);
    }
}
