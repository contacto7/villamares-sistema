<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\CompanyRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function list($slug = null){
        if (!$slug) {
            $companies = Company::with([
                'customer.office',
                'taxRegime',
                'companyTurn',
                'user'
            ]);

            //POLICIES
            $canListAllUsers = auth()->user()->can('listAllUsers', Company::class );
            //END POLICIES

            if($canListAllUsers){
                $companies = $companies->orderBy('id', 'desc')->paginate(12);
            }else{
                $companies =  $companies->where('id',0)->orderBy('id', 'desc')->paginate(12);
            }

            return view('companies.list', compact('companies'));
            //dd($vouchers);
        } else {
            $companies = Company::with([
                'customer.user',
                'customer.office',
                'taxRegime',
                'companyTurn',
                'user'
            ])
                ->whereHas("customer", function ($q) use ($slug) {
                    $q->where("slug", "=", $slug);
                })
                ->orderBy('id', 'desc')
                ->paginate(12);

            //dd($vouchers);
            return view('companies.list', compact('companies'));
        }
    }

    public function user($id){
        $companies = Company::with([
            'customer.user',
            'customer.office',
            'taxRegime',
            'companyTurn',
            'user'
        ])
            ->where('user_id',$id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('companies.admin', compact('companies'));
    }

    public function admin($id){
        $companies = Company::with([
            'customer.user',
            'customer.office',
            'taxRegime',
            'companyTurn',
            'user',
            'accounting',
            'constitution',
            'extras.serviceType'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->paginate(1);

        //dd($companies);
        return view('companies.admin', compact('companies'));
    }

    public function filter(Request $request, Company $company){

        //$company = $company->newQuery();
        $company = $company::with([
            'customer.user',
            'customer.office',
            'taxRegime',
            'companyTurn',
            'user'
        ]);

        $name = $request->has('name') ? $request->input('name'): null;
        $tax_number = $request->has('tax-number') ? $request->input('tax-number'): null;

        //POLICIES
        $canListAllUsers = auth()->user()->can('listAllUsers', Company::class );
        //END POLICIES


        // Search for a user based on their name.
        if ($name) {
            $company->where('name', $name);
        }

        // Search for a user based on their name.
        if ($tax_number) {
            $company->where('tax_number', $tax_number);
        }

        if($canListAllUsers || $name || $tax_number){
            $companies = $company->orderBy('id', 'desc')->paginate(12);
        }else{
            $companies =  $company->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }

        //dd($movements);
        return view('companies.list', compact('companies'));

    }

    public function rucSearch(Request $request, Company $company){

        $tax_number = $request->has('company_search') ? $request->input('company_search'): null;
        $service_type = (int) $request->has('service_type') ? $request->input('service_type'): null;
        $without_service= (int) $request->has('without_service') ? $request->input('without_service'): null;

        $company = $company->newQuery();
        /*$company = $company::with([
                'accounting',
                'constitution',
                'extras.serviceType',
                'extras.currency'
        ]);*/

        // Search for a user based on their document number.
        if ($tax_number) {
            $company->where('tax_number', $tax_number);
        }
        // Search for a user based on their document number.
        if ($without_service) {
            switch ($without_service) {
                case 1:
                    $company->whereDoesntHave('accounting');
                    break;
                case 2:
                    $company->whereDoesntHave('constitution');
                    break;
                default:
                    break;
            }
        }

        if ($service_type){
            switch ($service_type) {
                case 1:
                    $company->with('accounting')->whereHas("accounting");
                    $company = $company->first();
                    break;
                case 2:
                    $company->with('constitution')->whereHas("constitution");
                    $company = $company->first();
                    break;
                default:
                    $company->with(['extras' => function($query) use($service_type) {
                        $query
                            ->where('service_type_id', $service_type)
                            ->with('serviceType','currency');
                    }]);
                    $company = $company->get();
                    break;
            }
        }else{
            $company = $company->first();
        }


        //dd($company);
        return $company;

    }

    public function create(){
        $company = new Company;

        $btnText = __("Crear empresa");
        return view('companies.form', compact('company','btnText'));
    }

    public function store(CompanyRequest $companyRequest){

        $canCreateOnlyFromOffice = auth()->user()->can('createOnlyFromOffice', Company::class );

        //dd($companyRequest->all());

        $companyRequest->merge(['user_id' => auth()->user()->id ]);
        $companyRequest->except(['company_name']);

        if($canCreateOnlyFromOffice){
            $companyRequest->merge(['office_id' => auth()->user()->office_id ]);
        }

        Company::create($companyRequest->input());

        return back()->with('message',['success', __("Se agregó el cliente correctamente.")]);
    }

    public function edit($id){
        $company = Company::with([
            'customer.user',
            'customer.office',
            'taxRegime',
            'companyTurn',
            'user'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar empresa");

        //dd($vouchers);
        return view('companies.form', compact('company','btnText'));
    }

    public function update(CompanyRequest $companyRequest, Company $company){

        //dd($customerRequest);
        $company->fill($companyRequest->input())->save();
        $companyRequest->except(['company_name']);

        //dd($customerRequest);
        return back()->with('message',['success', __("Se actualizó la empresa correctamente.")]);
    }








}
