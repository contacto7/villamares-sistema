<?php

namespace App\Http\Controllers;

use App\CompanyNote;
use App\Http\Requests\CompanyNoteRequest;
use Illuminate\Http\Request;

class CompanyNoteController extends Controller
{

    public function list($company_id){
        $notes = CompanyNote::with([])
            ->where('company_id',$company_id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('companynotes.list', compact('notes', 'company_id'));
    }

    public function modalSeeForm(Request $request){

        $company_id = (int) $request->has('company_id') ? $request->input('company_id'): null;

        return self::list($company_id);
    }

    public function create($accounting_id){
        $note = new Note();

        $btnText = __("Agregar nota");
        return view('partials.accountings.modalCompanyBook', compact('note','btnText'));
    }

    public function store(CompanyNoteRequest $companyNoteRequest){

        try {
            $companyNoteRequest->merge(['user_id' => auth()->user()->id ]);
            CompanyNote::create($companyNoteRequest->input());
            return back()->with('message',['success',
                __("Se agregó la nota correctamente.")]);
        } catch(\Illuminate\Database\QueryException $e){
            return back()->with('message',['danger',
                __("Hubo un error agregando la nota, 
                por favor verifique que está colocando los datos requeridos.")]);

        }

    }
}
