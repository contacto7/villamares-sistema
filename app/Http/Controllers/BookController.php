<?php

namespace App\Http\Controllers;

use App\Book;
use App\BookType;
use App\Company;
use App\Helpers\Helper;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function missing($book = 1){
        $regimenes_obligatorios = self::regimensObligatory($book);

        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Book::class );
        $user_office_id = auth()->user()->office_id;

        $companies = Company::with([
            'accounting.books.bookType'
        ])
            ->whereHas('accounting')
            ->whereDoesntHave("accounting.books", function($q) use($book){
                $q->where("book_type_id",$book);
            })
            ->whereIn("tax_regime_id", $regimenes_obligatorios);

        if($canSeeOnlyOffice){
            $companies = $companies->where('office_id', $user_office_id);
        }

        $companies = $companies->orderBy('id', 'desc')->paginate(12);

            //BOOKS
        $books = BookType::orderBy('id', 'asc')->get();

        $actualBook = BookType::where('id', $book)->first();

        //dd($vouchers);
        return view('books.missing', compact('companies', 'books', 'actualBook'));
    }

    public function filter(Request $request){

        $book = $request->has('book') ? $request->input('book'): 1;

        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Book::class );
        $user_office_id = auth()->user()->office_id;

        $regimenes_obligatorios = self::regimensObligatory($book);

        $companies = Company::with([
            'accounting.books.bookType'
        ])
            ->whereHas('accounting')
            ->whereDoesntHave("accounting.books", function($q) use($book){
                $q->where("book_type_id",$book);
            })
            ->whereIn("tax_regime_id", $regimenes_obligatorios);

        if($canSeeOnlyOffice){
            $companies = $companies->where('office_id', $user_office_id);
        }

        $companies = $companies->orderBy('id', 'desc')->paginate(12);


        //BOOKS
        $books = BookType::orderBy('id', 'asc')->get();

        $actualBook = BookType::where('id', $book)->first();

        //dd($vouchers);
        return view('books.missing', compact('companies', 'books', 'actualBook'));


    }

    public function regimensObligatory($book){
        /*
            TIPOS DE REGIMEN
            NRUS = 1;
            RER = 2;
            RMT < 300 = 3;
            RMT < 500 = 4;
            RMT < 1700 = 5;
            RG < 300 = 6;
            RG < 500 = 7;
            RG < 1700 = 8;
            RG > 1700 = 9;
        */

        $regimenes_obligatorios = array();

        switch ($book){
            case 1:
                $regimenes_obligatorios = [2,3,4,5,6,7,8,9];
                break;
            case 2:
                $regimenes_obligatorios = [2,3,4,5,6,7,8,9];
                break;
            case 3:
                $regimenes_obligatorios = [3,6];
                break;
            case 4:
                $regimenes_obligatorios = [4,5,7,8,9];
                break;
            case 5:
                $regimenes_obligatorios = [4,5,7,8,9];
                break;
            case 6:
                $regimenes_obligatorios = [5,8,9];
                break;
            case 7:
                $regimenes_obligatorios = [9];
                break;
            case 8:
                $regimenes_obligatorios = [1,2,3,4,5,6,7,8,9];
                break;
            case 9:
                $regimenes_obligatorios = [9];
                break;
            case 10:
                $regimenes_obligatorios = [9];
                break;
            case 11:
                $regimenes_obligatorios = [9];
                break;
            case 12:
                $regimenes_obligatorios = [9];
                break;
        }
        return $regimenes_obligatorios;
    }

    public function modalForm(Request $request){

        $id = (int) $request->has('book_id') ? $request->input('book_id'): null;
        $accounting_id = (int) $request->has('accounting_id') ? $request->input('accounting_id'): null;

        if($id){
            return self::edit($id,$accounting_id);
        }else{
            return self::create($accounting_id);
        }
    }

    public function create($accounting_id){
        $book = new Book();

        $btnText = __("Agregar libro");
        return view('partials.accountings.modalCompanyBook', compact('book','btnText','accounting_id'));
    }

    public function store(BookRequest $bookRequest){

        if($bookRequest->has('picture')){
            $picture = Helper::uploadFile('picture', 'books');
            $bookRequest->merge(['picture' => $picture ]);
        }

        try {
            Book::create($bookRequest->input());
            return back()->with('message',['success',
                __("Se agregó el libro correctamente.")]);
        } catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == '1062'){
                return back()->with('message',['danger',
                    __("No puede colocar un mismo libro dos veces
                     al mismo servicio contable.")]);
            }else{
                return back()->with('message',['danger',
                    __("Hubo un error agregando el libro, 
                por favor verifique que está colocando los datos requeridos.")]);
            }

        }
        /*
        Book::create($bookRequest->input());

        return back()->with('message',['success', __("Se agregó el libro correctamente.")]);
        */
    }

    public function edit($id,$accounting_id){
        $book = Book::with([
            'bookType'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar libro");

        //dd($accounting);
        return view('partials.accountings.modalCompanyBook', compact('book','btnText','accounting_id'));
    }

    public function update(BookRequest $bookRequest, Book $book){

        if($bookRequest->has('picture')){
            \Storage::delete('books/'.$book->picture);
            $picture = Helper::uploadFile('picture', 'books');
            $bookRequest->merge(['picture' => $picture ]);
        }

        try {
            $book->fill($bookRequest->input())->save();
            return back()->with('message',['success', __("Se actualizó el libro.")]);

        } catch(\Illuminate\Database\QueryException $e){

            $errorCode = $e->errorInfo[1];
            if($errorCode == '1062'){
                return back()->with('message',['danger',
                    __("No puede colocar un mismo libro dos veces
                     al mismo servicio contable.")]);
            }else{
                return back()->with('message',['danger',
                    __("Hubo un error agregando el libro, 
                por favor verifique que está colocando los datos requeridos.")]);
            }

        }

        //dd($customerRequest);
    }

}
