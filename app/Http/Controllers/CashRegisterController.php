<?php

namespace App\Http\Controllers;

use App\CashFlow;
use App\CashRegister;
use Illuminate\Http\Request;

class CashRegisterController extends Controller
{
    public function list($slug = null){

        $user = auth()->user()->id;

        if(!$slug){
            $cashflows = CashFlow::with([
                'voucher',
                'userRegistered',
                'userTransfer',
                'bankAccount.currency',
                'paymentWay',
            ])
                ->join('cash_registers','cash_flows.user_registered_id','=','cash_registers.user_id')
                ->select('cash_flows.*')
                ->whereColumn('cash_flows.done_at','>=','cash_registers.last_cash_count')
                ->where('cash_flows.user_registered_id',$user)
                ->orderBy('cash_flows.id', 'desc')
                ->get();

            $last_count= CashRegister::getLastCashCount( $user );

            $soles = 1;
            $dolares = 2;

            $total_cash_soles = CashRegister::totalCash($user,$soles);
            $total_cash_dolares = CashRegister::totalCash($user,$dolares);

            //dd(CashRegister::totalCash( $user ));
            return view('cashregister.list',
                compact('cashflows','last_count', 'total_cash_soles', 'total_cash_dolares'));
        }
    }

    public function filter(Request $request, CashFlow $cashflow){

        //$company = $company->newQuery();
        $cashflow = $cashflow::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
        ]);

        $user =  $request->has('user_id') ? $request->input('user_id') : auth()->user()->id;

        // Search for a user based on their name.
        $cashflows = CashFlow::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
        ])
            ->join('cash_registers','cash_flows.user_registered_id','=','cash_registers.user_id')
            ->select('cash_flows.*')
            ->whereColumn('cash_flows.done_at','>=','cash_registers.last_cash_count')
            ->where('cash_flows.user_registered_id',$user)
            ->orderBy('cash_flows.id', 'desc')
            ->get();


        $last_count= CashRegister::getLastCashCount( $user );

        $soles = 1;
        $dolares = 2;

        $total_cash_soles = CashRegister::totalCash($user,$soles);
        $total_cash_dolares = CashRegister::totalCash($user,$dolares);

        //dd(CashRegister::totalCash( $user ));
        return view('cashregister.list',
            compact('cashflows','last_count', 'total_cash_soles', 'total_cash_dolares'));

    }
}
