<?php

namespace App\Http\Controllers;

use App\CashFlow;
use App\CashRegister;
use App\Helpers\Helper;
use App\Http\Requests\CashFlowRequest;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CashFlowController extends Controller
{

    public function getDownload($picture_voucher){
        return response()->download(storage_path("app/public/vouchers/{$picture_voucher}"));
    }

    public function list($slug = null){

        $user = auth()->user()->id;

        if(!$slug){
            $cashflows = CashFlow::with([
                'voucher',
                'userRegistered',
                'userTransfer',
                'bankAccount.currency',
                'paymentWay',
            ])
                ->where('user_registered_id',$user)
                ->orderBy('id', 'desc')
                ->paginate(12);

            //dd(CashRegister::totalCash( $user ));
            return view('cashflows.list',
                compact('cashflows'));
        }
    }

    public function filter(Request $request, CashFlow $cashflow){

        //$company = $company->newQuery();
        $cashflow = $cashflow::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
        ]);

        $user_registered_id = $request->has('user_registered_id') ? $request->input('user_registered_id'): null;
        $operation_type = $request->has('operation_type') ? $request->input('operation_type'): null;

        // Search for a user based on their name.
        if ($user_registered_id) {
            $cashflow->where('user_registered_id', $user_registered_id);
        }


        if ($operation_type) {
            $cashflow->where('operation_type', $operation_type);
        }

        $cashflows = $cashflow->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('cashflows.list', compact('cashflows'));

    }

    public function admin($id){
        $cashflow = CashFlow::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($companies);
        return view('cashflows.admin', compact('cashflow'));
    }

    public function create(){
        $cashFlow = new CashFlow();

        $btnText = __("Crear flujo de dinero");
        return view('cashflows.form', compact('cashFlow','btnText'));
    }

    public function store(CashFlowRequest $cashFlowRequest){

        if($cashFlowRequest->has('picture_voucher')){
            $picture = Helper::uploadFile('picture_voucher', 'vouchers');
            $cashFlowRequest->merge(['picture_voucher' => $picture ]);
        }
        $cashFlowRequest->merge(['user_registered_id' => auth()->user()->id ]);
        $cashFlowRequest->except(['voucher_number']);

        $operation_type = (int) $cashFlowRequest->operation_type;

        $role = auth()->user()->role_id;

        $state = CashFlow::WAITING;

        $cashFlowRequest->merge(['state' => $state]);

        //dd($voucherRequest);

        $transfer_out = new CashFlow();
        $cash_flow_id = $transfer_out::create($cashFlowRequest->input())->id;

        if ( $operation_type === 4) {


            $description = $cashFlowRequest->description;
            $operation_type = CashFlow::INTERN_TRANSFER_IN;
            $currency_id = $cashFlowRequest->currency_id;
            $amount_movement = $cashFlowRequest->amount_movement;
            $done_at = $cashFlowRequest->done_at;
            $voucher_id = $cashFlowRequest->voucher_id;
            $user_registered_id = $cashFlowRequest->user_transfer_id;
            $user_transfer_id = $cashFlowRequest->user_registered_id;
            $bank_account_id = $cashFlowRequest->bank_account_id;
            $payment_way_id = $cashFlowRequest->payment_way_id;
            $number_voucher_associated = $cashFlowRequest->number_voucher_associated;
            $picture_voucher = $cashFlowRequest->picture_voucher;

            $transfer_in = new CashFlow();

            $transfer_in::create([
                'description'=> $description,
                'operation_type'=> '3',
                'state'=> $state,
                'currency_id'=> $currency_id,
                'amount_movement'=> $amount_movement,
                'done_at'=> $done_at,
                'voucher_id'=> $voucher_id,
                'user_registered_id'=> $user_registered_id,
                'user_transfer_id'=> $user_transfer_id,
                'bank_account_id'=> $bank_account_id,
                'payment_way_id'=> $payment_way_id,
                'number_voucher_associated'=> $number_voucher_associated,
                'picture_voucher'=> $picture_voucher,
                'cash_flow_id'=> $cash_flow_id
            ]);

        }


        return back()->with('message',['success', __("Se agregó el flujo de dinero correctamente.")]);
    }

    public function edit($id){
        $cashFlow = CashFlow::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
            'currency',
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar flujo");

        //dd($vouchers);
        return view('cashflows.form', compact('cashFlow','btnText'));
    }

    public function update(CashFlowRequest $cashFlowRequest, CashFlow $cashFlow){

        if($cashFlowRequest->has('picture_voucher')){
            \Storage::delete('vouchers/'.$cashFlow->picture_voucher);
            $picture = Helper::uploadFile('picture_voucher', 'vouchers');
            $cashFlowRequest->merge(['picture_voucher' => $picture ]);
        }else{
            $cashFlowRequest->merge(['picture_voucher' => $cashFlow->picture_voucher ]);

            if($cashFlowRequest->operation_type == 2 && empty($cashFlow->picture_voucher)){
                return back()->with('message',['danger', __("Por ser egreso, se debe agregar juna imagen.")]);
            }

        }
        //dd($customerRequest);
        $cashFlowRequest->except(['voucher_number']);

        $cashFlow->fill($cashFlowRequest->input())->save();

        //dd($customerRequest);
        return back()->with('message',['success', __("Se actualizó el flujo.")]);
    }

    public function updateState(Request $request, CashFlow $cashflow){


        $id = $request->has('id') ? $request->input('id'): null;
        $state = $request->has('state') ? $request->input('state'): null;
        $original_cash_flow = $request->has('cash_flow_id') ? $request->input('cash_flow_id'): null;

        // Search for a user based on their name.
        if ($id && $state) {
            $cashflow::whereId($id)->update(['state'=>$state]);
        }

        if($original_cash_flow){
            $cashflow::whereId($original_cash_flow)->update(['state'=>$state]);
        }

        return back();

    }

    public function arqueo(Request $request){

        $user_id = auth()->user()->id;
        $role_id = auth()->user()->role_id;

        $soles = 1;
        $dolares = 2;

        //ARQUEO SOLES A SUPERIOR, EGRESO

        if($role_id > 1){

            $cash_flow_out_soles_id = self::createCashFlowForArqueo(CashFlow::INTERN_TRANSFER_OUT, $soles, null);

            //ARQUEO SOLES A SUPERIOR, INGRESO

            $cash_flow_in_soles_id = self::createCashFlowForArqueo(CashFlow::INTERN_TRANSFER_IN, $soles, $cash_flow_out_soles_id);

            //ARQUEO DOLARES A SUPERIOR, EGRESO

            $cash_flow_out_dolares_id = self::createCashFlowForArqueo(CashFlow::INTERN_TRANSFER_OUT, $dolares, null);

            //ARQUEO DOLARES A SUPERIOR, INGRESO

            $cash_flow_in_dolares_id = self::createCashFlowForArqueo(CashFlow::INTERN_TRANSFER_IN, $dolares, $cash_flow_out_dolares_id);
        }

        CashRegister::whereUserId($user_id)->update(['last_cash_count'=>Carbon::now('America/Lima')->addSeconds(20)]);


        return back()->with('message',['success', __("Se realizó el arqueo correctamente.")]);

    }

    function createCashFlowForArqueo($operation_type, $currency_id, $cash_flow_id){

        $user_registered_id = auth()->user()->id;
        $role_id = auth()->user()->role_id;

        $total_cash = CashRegister::totalCash($user_registered_id,$currency_id);

        $user_transfer_id = 1;

        if($role_id === 2){
            $user_transfer_id = 1;
        }elseif($role_id > 2){
            $user_transfer_id = 2;
        }

        //SI SE INGRESA EL INFLOW, LOS USUARIOS SE INTERCAMBIAN

        if($operation_type === 3){
            $user_registered_id = $user_transfer_id;
            $user_transfer_id = auth()->user()->id;
        }


        $arqueo = new CashFlow();

        $cash_flow_created_id = $arqueo::create([
            'description'=> 'Arqueo de caja',
            'operation_type'=> $operation_type,
            'state'=> CashFlow::WAITING,
            'currency_id'=> $currency_id,
            'amount_movement'=> $total_cash,
            'done_at'=> Carbon::now('America/Lima'),
            'voucher_id'=> null,
            'user_registered_id'=> $user_registered_id,
            'user_transfer_id'=> $user_transfer_id,
            'bank_account_id'=> null,
            'payment_way_id'=> 1,
            'number_voucher_associated'=> null,
            'picture_voucher'=> null,
            'cash_flow_id'=> $cash_flow_id
        ])->id;

        return $cash_flow_created_id;
    }
}
