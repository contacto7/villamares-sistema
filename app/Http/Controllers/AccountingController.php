<?php

namespace App\Http\Controllers;

use App\Accounting;
use App\Customer;
use App\Helpers\Helper;
use App\Http\Requests\AccountingRequest;
use Illuminate\Http\Request;

class AccountingController extends Controller
{

    public function getDownload($delivery_charge){
        return response()->download(storage_path("app/public/accountings/{$delivery_charge}"));
    }

    public function list($slug = null){
        if(!$slug){
            $accountings = Accounting::with([
                'company.customer.office',
                'currency',
                'user',
                'paymentDocument',
                'worker',
                'supervisor'
            ]);

            //POLICIES
            $user_office_id = auth()->user()->office_id;
            $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Accounting::class );
            //END OF POLICIES

            $rate_soles_factura = clone $accountings;
            $rate_dolares_factura = clone $accountings;
            $rate_soles_recibo_caja = clone $accountings;
            $rate_dolares_recibo_caja = clone $accountings;

            $rate_soles_factura = $rate_soles_factura->where('payment_document_id',1)->where('currency_id',1)->sum('normal_rate');
            $rate_dolares_factura = $rate_dolares_factura->where('payment_document_id',1)->where('currency_id',2)->sum('normal_rate');
            $rate_soles_recibo_caja = $rate_soles_recibo_caja->where('payment_document_id',4)->where('currency_id',1)->sum('normal_rate');
            $rate_dolares_recibo_caja = $rate_dolares_recibo_caja->where('payment_document_id',4)->where('currency_id',2)->sum('normal_rate');

            $rate_comprobantes = [$rate_soles_factura, $rate_dolares_factura, $rate_soles_recibo_caja, $rate_dolares_recibo_caja];

            if($canSeeOnlyOffice){
                $accountings = $accountings->whereHas("company", function($q) use($user_office_id){
                    $q->where("office_id",$user_office_id);
                });
            }

            $accountings = $accountings->orderBy('id', 'desc')->paginate(12);

            //dd($movements);
            return view('accountings.list', compact('accountings', 'rate_comprobantes'));


        }else{
            $accountings = Accounting::with([
                'company',
                'currency'
            ])->orderBy('id', 'desc')->paginate(12);

            //dd($vouchers);
            return view('accountings.list', compact('accountings'));
        }
    }

    public function admin($id){
        $accountings = Accounting::with([
            'company',
            'currency',
            'books',
            'vouchers'=> function($query){
                $query->orderBy('id','desc')->paginate(12);   //your popular comment or other logic
            }
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($accountings);
        return view('accountings.admin', compact('accountings'));
    }

    public function filter(Request $request, Accounting $accountings){

        $name = $request->has('name') ? $request->input('name'): null;
        $tax_number = $request->has('tax-number') ? $request->input('tax-number'): null;
        $worker_user_id = $request->has('worker_user_id') ? $request->input('worker_user_id'): null;
        $supervisor_user_id = $request->has('supervisor_user_id') ? $request->input('supervisor_user_id'): null;
        $state = $request->has('state') ? $request->input('state'): null;

        //POLICIES
        $canFilterByWorker = auth()->user()->can('filterByWorker', Accounting::class );
        $canFilterByCoordinator = auth()->user()->can('filterByCoordinator', Accounting::class );
        $canFilterByState = auth()->user()->can('filterByState', Accounting::class );

        //END OF POLICIES

        //$company = $company->newQuery();
        $accountings = $accountings::with([
            'company.customer.office',
            'currency',
            'user',
            'paymentDocument',
            'worker',
            'supervisor'
        ]);

        if ($name) {
            $accountings
                ->whereHas("company", function($q) use($name){
                    $q->where("name",$name);
                });
        }

        if ($tax_number) {
            $accountings
                ->whereHas("company", function($q) use($tax_number){
                    $q->where("tax_number",$tax_number);
                });
        }

        if ($worker_user_id && $canFilterByWorker) {
            $accountings->where('worker_user_id',$worker_user_id);
        }

        if ($supervisor_user_id && $canFilterByCoordinator) {
            $accountings->where('supervisor_user_id',$supervisor_user_id);
        }

        if ($state && $canFilterByState) {
            $accountings->where('state',$state);
        }

        $rate_soles_factura = clone $accountings;
        $rate_dolares_factura = clone $accountings;
        $rate_soles_recibo_caja = clone $accountings;
        $rate_dolares_recibo_caja = clone $accountings;

        $rate_soles_factura = $rate_soles_factura->where('payment_document_id',1)->where('currency_id',1)->sum('normal_rate');
        $rate_dolares_factura = $rate_dolares_factura->where('payment_document_id',1)->where('currency_id',2)->sum('normal_rate');
        $rate_soles_recibo_caja = $rate_soles_recibo_caja->where('payment_document_id',4)->where('currency_id',1)->sum('normal_rate');
        $rate_dolares_recibo_caja = $rate_dolares_recibo_caja->where('payment_document_id',4)->where('currency_id',2)->sum('normal_rate');

        $rate_comprobantes = [$rate_soles_factura, $rate_dolares_factura, $rate_soles_recibo_caja, $rate_dolares_recibo_caja];

        //dd($rate_comprobantes);

        $accountings = $accountings->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('accountings.list', compact('accountings', 'rate_comprobantes'));

    }


    public function create(){
        $accounting = new Accounting();

        $btnText = __("Crear servicio contable");
        return view('accountings.form', compact('accounting','btnText'));
    }

    public function store(AccountingRequest $accountingFlowRequest){

        $accountingFlowRequest->merge(['user_id' => auth()->user()->id ]);

        if($accountingFlowRequest->has('delivery_charge')){
            $delivery_charge = Helper::uploadFile('delivery_charge', 'accountings');
            $accountingFlowRequest->merge(['delivery_charge' => $delivery_charge ]);
        }

        //dd($accountingFlowRequest);
        Accounting::create($accountingFlowRequest->input());

        return back()->with('message',['success', __("Se agregó el flujo de dinero correctamente.")]);
    }

    public function edit($id){
        $accounting = Accounting::with([
            'company.customer.office',
            'currency',
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar servicio contable");

        //dd($accounting);
        return view('accountings.form', compact('accounting','btnText'));
    }

    public function update(AccountingRequest $accountingFlowRequest, Accounting $accounting){

        if($accountingFlowRequest->has('delivery_charge')){
            \Storage::delete('accountings/'.$accounting->delivery_charge);
            $delivery_charge = Helper::uploadFile('delivery_charge', 'accountings');
            $accountingFlowRequest->merge(['delivery_charge' => $delivery_charge ]);
        }

        $accounting->fill($accountingFlowRequest->input())->save();

        //dd($customerRequest);
        return back()->with('message',['success', __("Se actualizó el servicio contable.")]);
    }

}
