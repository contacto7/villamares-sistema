<?php

namespace App\Http\Controllers;

use App\Extra;
use App\Http\Requests\ExtraRequest;
use Illuminate\Http\Request;

class ExtraController extends Controller
{
    public function list($slug = null){
        if(!$slug){
            $extras = Extra::
            with([
                'serviceType',
                'customer',
                'company',
            ]);

            //POLICIES
            $canListAllUsers = auth()->user()->can('listAllUsers', Extra::class );
            //$canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Extra::class );
            //$user_office_id = auth()->user()->office_id;
            //END POLICIES

            /*
            if($canSeeOnlyOffice){
                $extras = $extras->whereHas("company", function($q) use($user_office_id){
                        $q->where("office_id",$user_office_id);
                    })
                    ->orwhereHas("customer", function($q) use($user_office_id){
                        $q->where("office_id",$user_office_id);
                    });;
            }
            */
            if($canListAllUsers){
                $extras = $extras->orderBy('id', 'desc')->paginate(12);
            }else{
                $extras =  $extras->where('id',0)->orderBy('id', 'desc')->paginate(12);
            }

            //dd($constitutions);
            return view('extras.list', compact('extras'));
        }else{
            $extras = Extra::with([
                'serviceType',
                'customer',
                'company',
            ]);

            $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Extra::class );
            $user_office_id = auth()->user()->office_id;

            if($canSeeOnlyOffice){
                $extras = $extras->whereHas("company", function($q) use($user_office_id){
                    $q->where("office_id",$user_office_id);
                })
                    ->orwhereHas("customer", function($q) use($user_office_id){
                        $q->where("office_id",$user_office_id);
                    });;
            }

            $extras = $extras->orderBy('id', 'desc')->paginate(12);

            //dd($vouchers);
            return view('extras.list', compact('extras'));
        }
    }
    public function customer($id){
        $extras = Extra::with([
            'serviceType',
            'customer',
            'company',
        ])
            ->where('customer_id',$id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('extras.list', compact('extras'));
    }

    public function admin($id){
        $extras = Extra::with([
            'customer.identityDocument',
            'user',
            'company',
            'vouchers'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($constitutions);
        return view('extras.admin', compact('extras'));
    }

    public function filter(Request $request, Extra $extras){

        $tax_number = $request->has('tax-number') ? $request->input('tax-number'): null;
        $document_number = $request->has('document-number') ? $request->input('document-number'): null;
        $office_id = $request->has('office_id') ? $request->input('office_id'): null;

        //POLICIES
        $canListAllUsers = auth()->user()->can('listAllUsers', Extra::class );
        //END POLICIES

        //$company = $company->newQuery();
        $extras = $extras::with([
            'customer.identityDocument',
            'user',
            'company',
            'vouchers'
        ]);

        if ($tax_number) {
            $extras
                ->whereHas("company", function($q) use($tax_number){
                    $q->where("tax_number",$tax_number);
                });
        }

        // Search for a user based on their document number.
        if ($document_number) {
            $extras
                ->whereHas("customer", function($q) use($document_number){
                    $q->where("document_number",$document_number);
                });
        }

        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Extra::class );
        $user_office_id = auth()->user()->office_id;

        // Search for a user based on their document number.
        if ($office_id || $canSeeOnlyOffice) {

            if ($canSeeOnlyOffice){
                $office_id = $user_office_id;
            }

            $extras->where(function($q) use($office_id) {
                $q->whereHas("customer", function($q) use($office_id){
                    $q->where("office_id",$office_id);
                })
                    ->orWhereHas("company", function($q) use($office_id){
                        $q->where("office_id",$office_id);
                    });
            });

        }

        if($canListAllUsers || $tax_number || $document_number || $office_id){
            $extras = $extras->orderBy('id', 'desc')->paginate(12);
        }else{
            $extras =  $extras->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }


        //dd($movements);
        return view('extras.list', compact('extras'));

    }

    public function create(){
        $extra = new Extra();

        $btnText = __("Crear servicio extra");
        return view('extras.form', compact('extra','btnText'));
    }

    public function store(ExtraRequest $extraRequest){

        $extraRequest->merge(['user_id' => auth()->user()->id ]);

        //dd($accountingFlowRequest);
        Extra::create($extraRequest->input());

        return back()->with('message',['success', __("Se agregó el servicio correctamente.")]);
    }

    public function edit($id){
        $extra = Extra::with([
            'customer.identityDocument',
            'user',
            'company',
            'vouchers'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar servicio");

        //dd($accounting);
        return view('extras.form', compact('extra','btnText'));
    }

    public function update(ExtraRequest $extraRequest, Extra $extra){

        try {

            $extra->fill($extraRequest->input())->save();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó el servicio.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            /*return back()->with('message',['danger',
                __("Hubo un error agregando la nota, 
                por favor verifique que está colocando los datos requeridos.")]);
            */

        }
    }
}
