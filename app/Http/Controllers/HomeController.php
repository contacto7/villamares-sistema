<?php

namespace App\Http\Controllers;

use App\CashFlow;
use App\CashRegister;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /*
        $user = auth()->user()->id;
        $cashflows = CashFlow::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
        ])
            ->join('cash_registers','cash_flows.user_registered_id','=','cash_registers.user_id')
            ->select('cash_flows.*')
            ->whereColumn('cash_flows.done_at','>=','cash_registers.last_cash_count')
            ->where('cash_flows.user_registered_id',$user)
            ->orderBy('cash_flows.id', 'desc')
            ->get();

        $last_count= CashRegister::getLastCashCount( $user );

        $soles = 1;
        $dolares = 2;

        $total_cash_soles = CashRegister::totalCash($user,$soles);
        $total_cash_dolares = CashRegister::totalCash($user,$dolares);

        //dd(CashRegister::totalCash( $user ));
        return view('cashregister.list',
            compact('cashflows','last_count', 'total_cash_soles', 'total_cash_dolares'));
        //dd(CashRegister::totalCash( $user ));
        */
        return view('home');




    }
}
