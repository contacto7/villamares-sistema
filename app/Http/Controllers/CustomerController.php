<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function list($slug = null){
        if(!$slug){
            $customers = Customer::with([
                'identityDocument',
                'office',
                'user',
                'companies'
            ]);

            //POLICIES
            /*
            $canSeeOnlyAccountingCustomer = auth()->user()->can('seeOnlyAccountingCustomer', Customer::class );
            $canSeeOnlyConstitutionCustomer = auth()->user()->can('seeOnlyConstitutionCustomer', Customer::class );

            if($canSeeOnlyAccountingCustomer){
                $customers = $customers->whereIn('prospect_type', [Customer::ACCOUNTING_PROSPECT, Customer::CUSTOMER]);
            }
            if($canSeeOnlyConstitutionCustomer){
                $customers = $customers->whereIn('prospect_type', [Customer::CONSTITUTION_PROSPECT, Customer::CUSTOMER]);
            }
            //END POLICIES
            */
            $canListAllUsers = auth()->user()->can('listAllUsers', Customer::class );

            if($canListAllUsers){
                $customers = $customers->orderBy('id', 'desc')->paginate(12);
            }else{
                $customers =  $customers->where('id',0)->orderBy('id', 'desc')->paginate(12);
            }


            //dd($vouchers);
            return view('customers.list', compact('customers'));
        }else{
            $customers = Customer::with([
                'identityDocument',
                'office',
                'user',
                'companies'
            ])
                ->whereHas("user", function($q) use($slug){
                    $q->where("slug","=",$slug);
                });

            //POLICIES
            $canSeeOnlyAccountingCustomer = auth()->user()->can('seeOnlyAccountingCustomer', Customer::class );
            $canSeeOnlyConstitutionCustomer = auth()->user()->can('seeOnlyConstitutionCustomer', Customer::class );
            //END POLICIES

            if($canSeeOnlyAccountingCustomer){
                $customers = $customers->whereIn('prospect_type', [Customer::ACCOUNTING_PROSPECT, Customer::CUSTOMER]);
            }
            if($canSeeOnlyConstitutionCustomer){
                $customers = $customers->whereIn('prospect_type', [Customer::CONSTITUTION_PROSPECT, Customer::CUSTOMER]);
            }

            $customers = $customers->orderBy('id', 'desc')->paginate(12);

            //dd($vouchers);
            return view('customers.list', compact('customers'));
        }
    }
    public function nearBirthdays(){

        $proximos_dias = 15;

        $customers = Customer::with([
            'identityDocument',
            'office',
            'user',
            'companies'
        ]);

        /*
        //POLICIES
        $canSeeOnlyAccountingCustomer = auth()->user()->can('seeOnlyAccountingCustomer', Customer::class );
        $canSeeOnlyConstitutionCustomer = auth()->user()->can('seeOnlyConstitutionCustomer', Customer::class );
        //END POLICIES

        if($canSeeOnlyAccountingCustomer){
            $customers = $customers->whereIn('prospect_type', [Customer::ACCOUNTING_PROSPECT, Customer::CUSTOMER]);
        }
        if($canSeeOnlyConstitutionCustomer){
            $customers = $customers->whereIn('prospect_type', [Customer::CONSTITUTION_PROSPECT, Customer::CUSTOMER]);
        }
        */
        $customers = $customers
            ->whereRaw('DAYOFYEAR(curdate()) <= DAYOFYEAR(birthday) AND DAYOFYEAR(curdate()) + :proximos_dias >=  dayofyear(birthday)', array('proximos_dias'=>$proximos_dias))
            ->orderByRaw('DAYOFYEAR(birthday)')
            ->paginate(12);

        //dd($vouchers);
        return view('customers.nearbirthdays', compact('customers'));
    }

    public function admin($id){
        $customers = Customer::with([
            'identityDocument',
            'office',
            'user',
            'companies',
            'extras'
        ])
            ->where('id',$id);


        //POLICIES
        $canSeeOnlyAccountingCustomer = auth()->user()->can('seeOnlyAccountingCustomer', Customer::class );
        $canSeeOnlyConstitutionCustomer = auth()->user()->can('seeOnlyConstitutionCustomer', Customer::class );
        //END POLICIES

        if($canSeeOnlyAccountingCustomer){
            $customers = $customers->whereIn('prospect_type', [Customer::ACCOUNTING_PROSPECT, Customer::CUSTOMER]);
        }
        if($canSeeOnlyConstitutionCustomer){
            $customers = $customers->whereIn('prospect_type', [Customer::CONSTITUTION_PROSPECT, Customer::CUSTOMER]);
        }

        $customers = $customers->orderBy('id', 'desc')->paginate(12);

        //dd($vouchers);
        return view('customers.admin', compact('customers'));
    }

    public function filter(Request $request, Customer $customers){

        $canFilterByUser= auth()->user()->can('filterByUser', Customer::class );
        $canFilterProspectFromOffice= auth()->user()->can('filterProspectFromOffice', Customer::class );

        $name = $request->has('name') ? $request->input('name'): null;
        $last_name = $request->has('last-name') ? $request->input('last-name'): null;
        $document_number = $request->has('document-number') ? $request->input('document-number'): null;
        $prospect_type = $request->has('prospect_type') ? $request->input('prospect_type'): null;
        $user_id = $request->has('user_id') ? $request->input('user_id'): null;

        //$company = $company->newQuery();
        $customers = $customers::with([
            'identityDocument',
            'office',
            'user',
            'companies'
        ]);
        // Search for a user based on their prospect type.
        if ($prospect_type) {
            $customers->where('prospect_type', $prospect_type);

            if($canFilterProspectFromOffice){
                $customers = $customers->where('office_id', auth()->user()->office_id);
            }

            $customers = $customers->orderBy('id', 'desc')->paginate(12);

            return view('customers.list', compact('customers'));
        }

        // Search for a user based on their name.
        if ($name) {
            $customers->where('name', 'like', "%".$name."%");
        }
        // Search for a user based on their last name.
        if ($last_name) {
            $customers->where('last_name', 'like', "%".$last_name."%");
        }
        // Search for a user based on their document number.
        if ($document_number) {
            $customers->where('document_number', $document_number);
        }

        if ($user_id && $canFilterByUser) {
            $customers->where('user_id', $user_id);
        }

        //POLICIES
        $canSeeOnlyAccountingCustomer = auth()->user()->can('seeOnlyAccountingCustomer', Customer::class );
        $canSeeOnlyConstitutionCustomer = auth()->user()->can('seeOnlyConstitutionCustomer', Customer::class );
        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Customer::class );
        $canSeeOnlyUploaded = auth()->user()->can('seeOnlyUploaded', Customer::class );
        $canListAllUsers = auth()->user()->can('listAllUsers', Customer::class );
        //END POLICIES

        if($canSeeOnlyAccountingCustomer){
            $customers = $customers->whereIn('prospect_type', [Customer::ACCOUNTING_PROSPECT, Customer::CUSTOMER]);
        }
        if($canSeeOnlyConstitutionCustomer){
            $customers = $customers->whereIn('prospect_type', [Customer::CONSTITUTION_PROSPECT, Customer::CUSTOMER]);
        }
        if($canSeeOnlyOffice){
            $customers = $customers->where('office_id', auth()->user()->office_id);
        }
        if($canSeeOnlyUploaded){
            $customers = $customers->where('user_id', auth()->user()->id);
        }

        if($canListAllUsers || $name || $last_name || $document_number){
            $customers = $customers->orderBy('id', 'desc')->paginate(12);
        }else{
            $customers =  $customers->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }

        //dd($movements);
        return view('customers.list', compact('customers'));

    }

    public function dniSearch(Request $request, Customer $customer){

        /**
         *
         *
        $document_number = $request->has('customer_search') ? $request->input('customer_search'): null;
        $service_type = (int) $request->has('service_type') ? $request->input('service_type'): null;

        $customer = $customer::with(['extras' => function($query) use($service_type) {
        $query
        ->where('service_type_id', $service_type)
        ->with('serviceType','currency');
        }]);

        // Search for a user based on their document number.
        if ($document_number) {
        $customer->where('document_number', $document_number);
        }

        $customer = $customer->get();

        //dd($movements);
        return $customer;

         *
         */

        $document_number = $request->has('customer_search') ? $request->input('customer_search'): null;
        $service_type = (int) $request->has('service_type') ? $request->input('service_type'): null;

        $customer = $customer->newQuery();

        //POLICIES
        $canSeeOnlyAccountingCustomer = auth()->user()->can('seeOnlyAccountingCustomer', Customer::class );
        $canSeeOnlyConstitutionCustomer = auth()->user()->can('seeOnlyConstitutionCustomer', Customer::class );
        //END POLICIES

        if($canSeeOnlyAccountingCustomer){
            $customer = $customer->whereIn('prospect_type', [Customer::ACCOUNTING_PROSPECT, Customer::CUSTOMER]);
        }
        if($canSeeOnlyConstitutionCustomer){
            $customer = $customer->whereIn('prospect_type', [Customer::CONSTITUTION_PROSPECT, Customer::CUSTOMER]);
        }

        // Search for a user based on their document number.
        if ($document_number) {
            $customer = $customer->where('document_number', $document_number);
        }

        if($service_type){
            $customer = $customer->with(['extras' => function($query) use($service_type) {
                $query
                    ->where('service_type_id', $service_type)
                    ->with('serviceType','currency');
            }])->get();

        }else{
            $customer = $customer->first();
        }

        //dd($customer);
        return $customer;

    }

    public function create(){
        $customer = new Customer;

        $btnText = __("Crear cliente");
        return view('customers.form', compact('customer','btnText'));
    }

    public function store(CustomerRequest $customerRequest){

        //dd($customerRequest->all());
        $customerRequest->merge(['user_id' => auth()->user()->id ]);

        Customer::create($customerRequest->input());

        return back()->with('message',['success', __("Se agregó el cliente correctamente.")]);
    }

    public function edit($id){
        $customer = Customer::with([
            'identityDocument',
            'office',
            'user',
            'companies',
            'extras'
        ])
            ->where('id',$id);

        //POLICIES
        $canSeeOnlyAccountingCustomer = auth()->user()->can('seeOnlyAccountingCustomer', Customer::class );
        $canSeeOnlyConstitutionCustomer = auth()->user()->can('seeOnlyConstitutionCustomer', Customer::class );
        $canUpdate = auth()->user()->can('update', Customer::class );
        //END POLICIES

        if($canSeeOnlyAccountingCustomer){
            $customer = $customer->whereIn('prospect_type', [Customer::ACCOUNTING_PROSPECT, Customer::CUSTOMER]);
        }
        if($canSeeOnlyConstitutionCustomer){
            $customer = $customer->whereIn('prospect_type', [Customer::CONSTITUTION_PROSPECT, Customer::CUSTOMER]);
        }

        $customer = $customer->orderBy('id', 'desc')->first();

        $btnText = __("Actualizar cliente");

        //dd($vouchers);
        if ($customer && $canUpdate ) {
            return view('customers.form', compact('customer','btnText'));
        }else{
            return redirect('/')->with('message',['danger', __("No tiene permiso para editar este cliente.")]);
        }

    }

    public function update(CustomerRequest $customerRequest, Customer $customer){

        //dd($customerRequest);
        $customer->fill($customerRequest->input())->save();

        //dd($customerRequest);
        return back()->with('message',['success', __("Se actualizó el cliente correctamente.")]);
    }
}
