<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function admin(){
        $users = User::with('role','office')->paginate(12);

        //dd($users);

        return view('users.admin', compact('users'));
    }

    public function create(){
        $user = new User();

        $btnText = __("Crear usuario");
        return view('users.form', compact('user','btnText'));
    }

    public function store(UserRequest $userRequest){

        $userRequest->merge(['remember_token' => str_random(10) ]);

        //dd($accountingFlowRequest);
        User::create($userRequest->input());

        return back()->with('message',['success', __("Se agregó el usuario correctamente.")]);
    }

    public function edit($id){
        $user = User::with([
            'role',
            'office'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar usuario");

        //dd($accounting);
        return view('users.form', compact('user','btnText'));
    }

    public function update(UserRequest $userRequest, User $user){

        $user->fill($userRequest->input())->save();

        //dd($customerRequest);
        return back()->with('message',['success', __("Se actualizó el servicio.")]);
    }

}
