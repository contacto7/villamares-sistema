<?php

namespace App\Http\Controllers;

use App\Constitution;
use App\Helpers\Helper;
use App\Http\Requests\ConstitutionRequest;
use Illuminate\Http\Request;

class ConstitutionController extends Controller
{

    public function list($slug = null){
        if(!$slug){
            $constitutions = Constitution::
                with([
                    'company.customer.identityDocument'
                ]);

            //POLICIES
            $canListAllUsers = auth()->user()->can('listAllUsers', Constitution::class );
            //$canSeeOnlyOffice = auth()->user()->can('seeOnlyOffices', Constitution::class );
            //END POLICIES

            /*
            if( $canSeeOnlyOffice ){
                $user_office = auth()->user()->office_id;
                $constitutions = $constitutions
                    ->whereHas("company", function($q) use($user_office){
                    $q->where("office_id",$user_office);
                });
            }
            */

            if($canListAllUsers){
                $constitutions = $constitutions->orderBy('id', 'desc')->paginate(12);
            }else{
                $constitutions =  $constitutions->where('id',0)->orderBy('id', 'desc')->paginate(12);
            }

            //dd($constitutions);
            return view('constitutions.list', compact('constitutions'));
        }else{
            $constitutions = Constitution::with([
                'company.customer.identityDocument'
            ])->orderBy('id', 'desc')->paginate(12);

            //dd($vouchers);
            return view('constitutions.list', compact('constitutions'));
        }
    }

    public function admin($id){
        $constitutions = Constitution::with([
            'customer.identityDocument',
            'user',
            'company',
            'vouchers'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($constitutions);
        return view('constitutions.admin', compact('constitutions'));
    }

    public function filter(Request $request, Constitution $constitutions){

        $name = $request->has('name') ? $request->input('name'): null;
        $tax_number = $request->has('tax-number') ? $request->input('tax-number'): null;
        $office_id = $request->has('office_id') ? $request->input('office_id'): null;

        //POLICIES
        $canListAllUsers = auth()->user()->can('listAllUsers', Constitution::class );
        //END POLICIES

        //$company = $company->newQuery();
        $constitutions = $constitutions::with([
            'company.customer.identityDocument'
        ]);

        if ($name) {
            $constitutions
                ->whereHas("company", function($q) use($name){
                    $q->where("name",$name);
                });
        }

        if ($tax_number) {
            $constitutions
                ->whereHas("company", function($q) use($tax_number){
                    $q->where("tax_number",$tax_number);
                });
        }

        if ($office_id) {
            $constitutions
                ->whereHas("company", function($q) use($office_id){
                    $q->where("office_id",$office_id);
                });
        }


        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffices', Constitution::class );

        if( $canSeeOnlyOffice ){
            $user_office = auth()->user()->office_id;
            $constitutions = $constitutions
                ->whereHas("company", function($q) use($user_office){
                    $q->where("office_id",$user_office);
                });
        }


        if($canListAllUsers || $name || $tax_number || $office_id){
            $constitutions = $constitutions->orderBy('id', 'desc')->paginate(12);
        }else{
            $constitutions =  $constitutions->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }

        //dd($movements);
        return view('constitutions.list', compact('constitutions'));

    }

    public function create(){
        $constitution = new Constitution();

        $btnText = __("Crear constitución");
        return view('constitutions.form', compact('constitution','btnText'));
    }

    public function store(ConstitutionRequest $constitutionRequest){

        $constitutionRequest->merge(['user_id' => auth()->user()->id ]);

        if($constitutionRequest->has('registration_charge_picture')){
            $registration_charge_picture = Helper::uploadFile('registration_charge_picture', 'constitutions');
            $constitutionRequest->merge(['registration_charge_picture' => $registration_charge_picture ]);
        }
        if($constitutionRequest->has('picture_1')){
            $picture_1 = Helper::uploadFile('picture_1', 'constitutions');
            $constitutionRequest->merge(['picture_1' => $picture_1 ]);
        }
        if($constitutionRequest->has('picture_2')){
            $picture_2 = Helper::uploadFile('picture_2', 'constitutions');
            $constitutionRequest->merge(['picture_2' => $picture_2 ]);
        }
        if($constitutionRequest->has('picture_3')){
            $picture_3 = Helper::uploadFile('picture_3', 'constitutions');
            $constitutionRequest->merge(['picture_3' => $picture_3 ]);
        }
        if($constitutionRequest->has('picture_4')){
            $picture_4 = Helper::uploadFile('picture_4', 'constitutions');
            $constitutionRequest->merge(['picture_4' => $picture_4 ]);
        }

        //dd($accountingFlowRequest);
        Constitution::create($constitutionRequest->input());

        return back()->with('message',['success', __("Se agregó la constitución correctamente.")]);
    }

    public function edit($id){
        $constitution = Constitution::with([
            'customer.identityDocument',
            'user',
            'company',
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar constitución");

        //dd($accounting);
        return view('constitutions.form', compact('constitution','btnText'));
    }

    public function update(ConstitutionRequest $constitutionRequest, Constitution $constitution){


        if($constitutionRequest->has('registration_charge_picture')){
            \Storage::delete('constitutions/'.$constitution->registration_charge_picture);
            $registration_charge_picture = Helper::uploadFile('registration_charge_picture', 'constitutions');
            $constitutionRequest->merge(['registration_charge_picture' => $registration_charge_picture ]);
        }
        if($constitutionRequest->has('picture_1')){
            \Storage::delete('constitutions/'.$constitution->picture_1);
            $picture_1 = Helper::uploadFile('picture_1', 'constitutions');
            $constitutionRequest->merge(['picture_1' => $picture_1 ]);
        }
        if($constitutionRequest->has('picture_2')){
            \Storage::delete('constitutions/'.$constitution->picture_2);
            $picture_2 = Helper::uploadFile('picture_2', 'constitutions');
            $constitutionRequest->merge(['picture_2' => $picture_2 ]);
        }
        if($constitutionRequest->has('picture_3')){
            \Storage::delete('constitutions/'.$constitution->picture_3);
            $picture_3 = Helper::uploadFile('picture_3', 'constitutions');
            $constitutionRequest->merge(['picture_3' => $picture_3 ]);
        }
        if($constitutionRequest->has('picture_4')){
            \Storage::delete('constitutions/'.$constitution->picture_4);
            $picture_4 = Helper::uploadFile('picture_4', 'constitutions');
            $constitutionRequest->merge(['picture_4' => $picture_4 ]);
        }

        //Se le hace la verificación al usuario para saber si puede cambiar el monto a cobrar
        //Usando la policy de Constitution::class
        //Pasando el objeto $constitution, que estamos actualizando
        $canChangeChargedAmount = auth()->user()->can('changeChargedAmount', [ Constitution::class, $constitution ] );

        if( !$canChangeChargedAmount ){
            $constitutionRequest->merge(['charged_amount' => $constitution->charged_amount ]);
        }

        $constitution->fill($constitutionRequest->input())->save();

        //dd($customerRequest);
        return back()->with('message',['success', __("Se actualizó la constitución.")]);
    }

}
