<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoucherRequest;
use App\PaymentDocument;
use App\ServiceType;
use App\Voucher;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class VoucherController extends Controller
{
    public function getvoucher($id){
        $vouchers = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.customer',
            'constitution.company',
            'extra.customer',
            'extra.company'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('vouchers.admin', compact('vouchers'));
    }

    public function allowedServices(){
        //LISTAMOS LOS SERVICIOS PERMITIDOS
        $canCreateOnlyConstitution = auth()->user()->can('createOnlyConstitution', Voucher::class );

        $allowedServices = new ServiceType();
        $allowedServices = $allowedServices->newQuery();

        if($canCreateOnlyConstitution){ $allowedServices->where('id',2); }

        $allowedServices = $allowedServices->get();

        return $allowedServices;
    }

    public function list($slug = null){
        if(!$slug){
            $vouchers = Voucher::with([
                'currency',
                'paymentDocument',
                'userRegistered',
                'userEmitted',
                'serviceType',
                'accounting.company.customer',
                'constitution.customer',
                'constitution.company',
                'extra.customer',
                'extra.company'
            ]);

            //$vouchers = self::canSeePolicies($vouchers);
            $canListAllUsers = auth()->user()->can('listAllUsers', Voucher::class );

            if($canListAllUsers){
                $vouchers = $vouchers->orderBy('id', 'desc')->paginate(12);
            }else{
                $vouchers =  $vouchers->where('id',0)->orderBy('id', 'desc')->paginate(12);
            }

            //$vouchers = $vouchers->orderBy('id', 'desc')->paginate(12);

            //dd($vouchers);
            return view('vouchers.list', compact('vouchers'));
        }else{
            $vouchers = Voucher::with([
                'currency',
                'paymentDocument',
                'userRegistered',
                'userEmitted',
                'serviceType',
                'accounting.company.customer',
                'constitution.customer',
                'constitution.company',
                'extra.customer',
                'extra.company'
            ])
                ->whereHas("userEmitted", function($q) use($slug){
                    $q->where("slug","=",$slug);
                });

            $vouchers = self::canSeePolicies($vouchers);

            $vouchers = $vouchers->orderBy('id', 'desc')->paginate(12);

            //dd($vouchers);
            return view('vouchers.list', compact('vouchers'));
        }
    }

    public function company($id){
        $vouchers = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.company.customer',
            'extra.customer',
            'extra.company'
        ])
            ->whereHas("accounting", function($q) use($id){
                $q->where("company_id",$id);
            })
            ->orwhereHas("constitution", function($q) use($id){
                $q->where("company_id",$id);
            })
            ->orwhereHas("extra.company", function($q) use($id){
                $q->where("company_id",$id);
            })
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('vouchers.list', compact('vouchers'));
    }

    public function customer($id){
        $vouchers = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.company.customer',
            'extra.customer',
            'extra.company.customer'
        ])
            ->whereHas("accounting.company", function($q) use($id){
                $q->where("customer_id",$id);
            })
            ->orwhereHas("constitution.company", function($q) use($id){
                $q->where("customer_id",$id);
            })
            ->orwhereHas("extra.company", function($q) use($id){
                $q->where("customer_id",$id);
            })
            ->orwhereHas("extra.customer", function($q) use($id){
                $q->where("customer_id",$id);
            })
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('vouchers.list', compact('vouchers'));
    }

    public function admin($id){
        $vouchers = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.customer',
            'constitution.company.taxRegime',
            'extra.customer',
            'extra.company'
        ])
            ->where('id',$id)
            ->get();

        //dd($vouchers);
        return view('vouchers.admin', compact('vouchers'));
    }

    public function downloadPDF($id){
        $voucher = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.customer',
            'constitution.company.taxRegime',
            'extra.customer',
            'extra.company'
        ])->find($id);

        $pdf = PDF::loadView('vouchers.pdf', compact('voucher'));
        return $pdf->setPaper('a7')->download('invoice.pdf');

    }

    public function debts(){

        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Voucher::class );
        $canSeeOnlyConstitutions = auth()->user()->can('seeOnlyConstitutions', Voucher::class );
        $office_id = auth()->user()->office_id;

        $vouchers = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted.office',
            'serviceType',
            'accounting.company.customer',
            'constitution.company.customer',
            'extra.customer',
            'extra.company'
        ])
            ->where('emitted_at','<', Carbon::now('America/Lima'))
            ->whereColumn('amount_payed','<','amount_total');

        if($canSeeOnlyOffice){
            $vouchers = $vouchers->whereHas("accounting.company", function($q) use($office_id){
                $q->where("office_id",$office_id);
            })->orWhereHas("constitution.company", function($q) use($office_id){
                $q->where("office_id",$office_id);
            })->orWhereHas("extra.company", function($q) use($office_id){
                $q->where("office_id",$office_id);
            })->orWhereHas("extra.customer", function($q) use($office_id){
                $q->where("office_id",$office_id);
            });
        }elseif($canSeeOnlyConstitutions){
            $vouchers = $vouchers->whereHas("constitution.company");
        }

        $vouchers = $vouchers->orderBy('id', 'desc')->paginate(12);

        $soles = 1;
        $dolares = 2;

        $factura = 1;
        $recibo_caja = 4;

        $factura_debt_soles = Voucher::totalDebt($soles, $factura);
        $factura_debt_dolares = Voucher::totalDebt($dolares, $factura);

        $recibo_caja_debt_soles = Voucher::totalDebt($soles, $recibo_caja);
        $recibo_caja_debt_dolares = Voucher::totalDebt($dolares, $recibo_caja);

        $deuda_por_comprobante = array();

        $deuda_por_comprobante = [
          'factura_debt_soles' => $factura_debt_soles,
          'factura_debt_dolares' => $factura_debt_dolares,
          'recibo_caja_debt_soles' => $recibo_caja_debt_soles,
          'recibo_caja_debt_dolares' => $recibo_caja_debt_dolares
        ];

        //dd($deuda_por_comprobante);
        return view('vouchers.debts',
            compact('vouchers', 'deuda_por_comprobante'));

    }

    public function downloadExcel(){
        //DOCUMENTACION EN
        //https://itsolutionstuff.com/post/laravel-56-import-export-to-excel-and-csv-exampleexample.html
        $type = "xls";
        /*
         * En el select se ponen los campos que irán en el EXCEL, el AS sirve para cambiar el nombre del campo en el EXCEL
         *
         *
         * */
        $data = Voucher::select(
            'vouchers.id as Id',
            'vouchers.number AS Número',
            'vouchers.description AS Descripción',
            'vouchers.associate_voucher AS Voucher_asociado',
            'currencies.currency_code AS Moneda',
            'vouchers.amount_payed AS Monto_pagado',
            'vouchers.amount_total AS Monto_total',
            'service_types.name AS Servicio',
            'vouchers.emitted_at AS Emitido',
            'vouchers.expired_at AS Vencido',
            'payment_documents.name AS Documento',
            'payment_documents.titular AS Titular',
            /*
            'cac.name AS Empresa-constitucion',
            'cco.name AS Empresa-contabilidad',
            'cex.name AS Empresa-extras',
            */
            /*
             * El concat sirve para concatenar columnas, en este caso se concatenó el nombre de las empresas de los servicios.
             * Como cuando una columna tiene valor NULL no se puede concatenar, entonces usamos COALESCE para transformar
             * el NULL en un valor vacío.
             *
             * */
            \DB::raw("CONCAT(COALESCE(cac.name,''),COALESCE(cco.name,''),COALESCE(cex.name,'')) AS Empresa"),
            'cue.name AS Nombres',
            'cue.last_name AS Apellidos'
            )
            /*
             * El Join sirve para unir las tablas que se quieren mostrar en el Excel
             * */
            ->join('currencies', 'vouchers.currency_id','=','currencies.id')
            ->join('service_types', 'vouchers.service_type_id','=','service_types.id')
            ->join('payment_documents', 'vouchers.payment_document_id','=','payment_documents.id')
            ->leftJoin('accountings', 'vouchers.accounting_id','=','accountings.id')
            ->leftJoin('constitutions', 'vouchers.constitution_id','=','constitutions.id')
            ->leftJoin('extras', 'vouchers.extra_id','=','extras.id')
            ->leftJoin('companies as cac', 'accountings.company_id','=','cac.id')
            ->leftJoin('companies as cco', 'constitutions.company_id','=','cco.id')
            ->leftJoin('companies as cex', 'extras.company_id','=','cex.id')
            ->leftJoin('customers as cue', 'extras.company_id','=','cue.id')
             ->where('emitted_at','<', Carbon::now('America/Lima'))
            ->whereColumn('amount_payed','<','amount_total')
            ->orderBy('vouchers.id', 'desc')
            ->get()
            ->toArray();


        return Excel::create('cpc', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function accounting($id){
        $vouchers = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.customer',
            'constitution.company',
            'extra.customer',
            'extra.company'
        ])
            ->whereHas("accounting", function($q) use($id){
                $q->where("id",$id);
            })
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('vouchers.list', compact('vouchers'));
    }

    public function filter(Request $request, Voucher $vouchers){

        $tax_number = $request->has('tax-number') ? $request->input('tax-number'): null;
        $document_number = $request->has('document-number') ? $request->input('document-number'): null;
        $payment_document_id = $request->has('payment_document_id') ? $request->input('payment_document_id'): null;
        $service_type_id = $request->has('service_type_id') ? $request->input('service_type_id'): null;

        $canFilterByVoucher = auth()->user()->can('filterByVoucher', Voucher::class );
        $canFilterByServiceType = auth()->user()->can('filterByServiceType', Voucher::class );

        //$company = $company->newQuery();
        $vouchers = $vouchers::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.company.customer',
            'extra.customer',
            'extra.company'
        ]);

        if ($tax_number) {
            $vouchers
                ->where(function($q) use($tax_number) {
                    $q->whereHas("accounting.company", function($q) use($tax_number){
                            $q->where("tax_number",$tax_number);
                        })
                        ->orwhereHas("constitution.company", function($q) use($tax_number){
                            $q->where("tax_number",$tax_number);
                        })
                        ->orwhereHas("extra.company", function($q) use($tax_number){
                            $q->where("tax_number",$tax_number);
                        });
                });
        }

        // Search for a user based on their document number.
        if ($document_number) {
            $vouchers
                ->where(function($q) use($document_number) {
                    $q->whereHas("accounting.company.customer", function($q) use($document_number){
                        $q->where("document_number",$document_number);
                    })
                        ->orwhereHas("constitution.company.customer", function($q) use($document_number){
                            $q->where("document_number",$document_number);
                        })
                        ->orwhereHas("extra.company.customer", function($q) use($document_number){
                            $q->where("document_number",$document_number);
                        })
                        ->orwhereHas("extra.customer", function($q) use($document_number){
                            $q->where("document_number",$document_number);
                        });
                });
        }

        if ($payment_document_id && $canFilterByVoucher) {
            $vouchers->where('payment_document_id', $payment_document_id);
        }

        if ($service_type_id && $canFilterByServiceType) {
            $vouchers->where('service_type_id', $service_type_id);
        }

        //$vouchers = self::canSeePolicies($vouchers);


        //$vouchers = self::canSeePolicies($vouchers);

        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Voucher::class );
        $office_id = auth()->user()->office_id;
        //$canListAllUsers = auth()->user()->can('listAllUsers', Voucher::class );
        if($canSeeOnlyOffice){
            $vouchers = $vouchers->whereHas("accounting.company", function($q) use($office_id){
                $q->where("office_id",$office_id);
            })->orWhereHas("constitution.company", function($q) use($office_id){
                $q->where("office_id",$office_id);
            })->orWhereHas("extra.company", function($q) use($office_id){
                $q->where("office_id",$office_id);
            })->orWhereHas("extra.customer", function($q) use($office_id){
                $q->where("office_id",$office_id);
            });
        }

        if($tax_number || $document_number || $payment_document_id || $service_type_id){
            $vouchers = $vouchers->orderBy('id', 'desc')->paginate(12);
        }else{
            $vouchers =  $vouchers->where('id',0)->orderBy('id', 'desc')->paginate(12);
        }


        //dd($movements);
        return view('vouchers.list', compact('vouchers'));

    }

    public function numberSearch(Request $request, Voucher $voucher){

        $number = $request->has('voucher_search') ? $request->input('voucher_search'): null;

        $voucher = $voucher::with([
                'currency',
                'serviceType',
                'accounting.company',
                'constitution.company',
                'extra.company',
                'extra.customer'
        ]);

        // Search for a user based on their document number.
        if ($number) {
            $voucher->where('number', $number);
        }
        $voucher = $voucher->first();
        //dd($voucher);
        return $voucher;

    }


    public function create(){
        $voucher = new Voucher;

        $btnText = __("Crear comprobante");

        $allowedDocuments = self::allowedDocuments();

        $allowedServices = self::allowedServices();

        return view('vouchers.form', compact('voucher', 'allowedDocuments', 'allowedServices','btnText'));
    }

    public function store(VoucherRequest $voucherRequest){

        //dd($voucherRequest->all());

        $voucherRequest->merge(['user_registered_id' => auth()->user()->id ]);
        $voucherRequest->merge(['user_emitted_id' => auth()->user()->id ]);
        $voucherRequest->merge(['amount_payed' => 0 ]);
        //$voucherRequest->merge(['emitted_at' => Carbon::now() ]);
        //$voucherRequest->merge(['expired_at' => Carbon::now() ]);

        //dd($voucherRequest);
        Voucher::create($voucherRequest->input());

        return back()->with('message',['success', __("Se agregó el comprobante correctamente.")]);
    }

    public function edit($id){
        $voucher = Voucher::with([
            'currency',
            'paymentDocument',
            'userRegistered',
            'userEmitted',
            'serviceType',
            'accounting.company.customer',
            'constitution.company.customer',
            'extra.customer',
            'extra.company'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar comprobante");

        $allowedDocuments = self::allowedDocuments();

        $allowedServices = self::allowedServices();

        //dd($vouchers);
        return view('vouchers.form', compact('voucher', 'allowedDocuments', 'allowedServices','btnText'));
    }

    public function update(VoucherRequest $voucherRequest, Voucher $voucher){

        //dd($customerRequest);
        $voucher->fill($voucherRequest->input())->save();

        //dd($customerRequest);
        return back()->with('message',['success', __("Se actualizó la empresa correctamente.")]);
    }

    public function destroy(Voucher $voucher){

        try{
            if($voucher->CashFlows->isEmpty()){
                $voucher->delete();
                return back()->with('message',['success', __("Comprobante eliminado correctamente.")]);
            }else{

                return back()->with('message',['success', __("No se puede eliminar, porque tiene movimientos asociados.")]);
            }
        } catch(\Exception $exception){
            dd($exception);
            return back()->with('message',['danger', __("Error eliminando el comprobante.")]);
        }


    }

    public function canSeePolicies($vouchers){
        $constitution = 2;

        $user_office_id = auth()->user()->office_id;

        $canSeeOnlyOffice = auth()->user()->can('seeOnlyOffice', Voucher::class );
        $canSeeOnlyConstitutions = auth()->user()->can('seeOnlyConstitutions', Voucher::class );

        if($canSeeOnlyOffice){
            $vouchers= $vouchers->whereHas("accounting.company", function($q) use($user_office_id){
                $q->where("companies.office_id",$user_office_id);
            })
                ->orwhereHas("constitution.company", function($q) use($user_office_id){
                    $q->where("companies.office_id",$user_office_id);
                })
                ->orwhereHas("extra.company", function($q) use($user_office_id){
                    $q->where("companies.office_id",$user_office_id);
                })
                ->orwhereHas("extra.customer", function($q) use($user_office_id){
                    $q->where("customers.office_id",$user_office_id);
                });
        }
        if($canSeeOnlyConstitutions){ $vouchers->where('service_type_id',$constitution); }

        return $vouchers;
    }

    public function allowedDocuments(){
        //LISTAMOS LOS DOCUMENTOS PERMITIDOS
        $canCreateFactura = auth()->user()->can('createFactura', Voucher::class );
        $canCreateRRHH = auth()->user()->can('createRRHH', Voucher::class );
        $canCreateRRCC = auth()->user()->can('createRRCC', Voucher::class );

        $array_allowed = array();

        if($canCreateFactura){ $array_allowed[] = 1; }
        if($canCreateRRHH){ $array_allowed[] = 2;$array_allowed[] = 3; }
        if($canCreateRRCC){ $array_allowed[] = 4; }

        $allowedDocuments = PaymentDocument::whereIN('id',$array_allowed)->get();

        return $allowedDocuments;
    }

}
