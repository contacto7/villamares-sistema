<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Note;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function list($customer_id){
        $notes = Note::with([])
            ->where('customer_id',$customer_id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('notes.list', compact('notes', 'customer_id'));
    }

    public function modalSeeForm(Request $request){

        $customer_id = (int) $request->has('customer_id') ? $request->input('customer_id'): null;

        return self::list($customer_id);
    }

    public function modalForm(Request $request){

        $id = (int) $request->has('book_id') ? $request->input('book_id'): null;
        $accounting_id = (int) $request->has('accounting_id') ? $request->input('accounting_id'): null;

        if($id){
            return self::edit($id,$accounting_id);
        }else{
            return self::create($accounting_id);
        }
    }

    public function create($accounting_id){
        $note = new Note();

        $btnText = __("Agregar nota");
        return view('partials.accountings.modalCompanyBook', compact('note','btnText'));
    }

    public function store(NoteRequest $noteRequest){

        try {
            $noteRequest->merge(['user_id' => auth()->user()->id ]);
            Note::create($noteRequest->input());
            return back()->with('message',['success',
                __("Se agregó la nota correctamente.")]);
        } catch(\Illuminate\Database\QueryException $e){
            return back()->with('message',['danger',
                __("Hubo un error agregando el libro, 
                por favor verifique que está colocando los datos requeridos.")]);

        }

    }

    public function edit($id,$accounting_id){
        $note = Note::with([
            'bookType'
        ])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar libro");

        //dd($accounting);
        return view('partials.accountings.modalCompanyBook', compact('note','btnText','accounting_id'));
    }

    public function update(NoteRequest $noteRequest, Note $note){

        try {
            $note->fill($noteRequest->input())->save();
            return back()->with('message',['success', __("Se actualizó el libro.")]);

        } catch(\Illuminate\Database\QueryException $e){

            return back()->with('message',['danger',
                __("Hubo un error agregando la nota, 
                por favor verifique que está colocando los datos requeridos.")]);

        }

        //dd($customerRequest);
    }

}
