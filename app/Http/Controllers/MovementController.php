<?php

namespace App\Http\Controllers;

use App\BankAccount;
use App\CashFlow;
use App\Http\Requests\MovementRequest;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Maatwebsite\Excel\Facades\Excel;

class MovementController extends Controller
{
    public function list($slug = null){
        if(!$slug){
            $movements = CashFlow::with([
                'voucher',
                'bankAccount.currency',
                'paymentWay',
                'currency',
            ])
                ->whereHas('bankAccount')
                ->where('state',CashFlow::ACCEPTED)
                ->whereIn('operation_type',['1','2','5','6'])
                ->orderBy('done_at', 'desc')
                ->paginate(12);

            //BANK ACCOUNTS
            $banks = BankAccount::with('currency')->get();

            //dd($movements);
            return view('movements.list', compact('movements', 'banks'));
        }
    }

    public function filter(Request $request, CashFlow $movements){

        $movements = $movements::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
            'currency',
        ])->where('state',CashFlow::ACCEPTED);

        $type = $request->has('type') ? $request->input('type'): null;

        $bank = $request->has('bank') ? $request->input('bank'): null;

        // Search for a user based on their name.
        if ($type) {
            if((int) $type === 1){
                $movements->whereIn('operation_type',['1','5']);
            }elseif ((int) $type === 2){
                $movements->whereIn('operation_type',['2','6']);
            }
        }else{
            $movements->where(function ($query) {
                    $query->whereIn('operation_type',['1','2','5','6']);
                });
        }

        // Search for a user based on their name.
        if ($bank) {
            $movements->where('bank_account_id', $bank);
        }else{
            $movements->whereHas('bankAccount');
        }

        $movements= $movements->orderBy('done_at', 'desc')->paginate(12);

        //BANK ACCOUNTS
        $banks = BankAccount::with('currency')->get();

        //dd($movements);
        return view('movements.list', compact('movements','banks'));

    }

    public function consolidateExcel(Request $request, CashFlow $movements){

        $bank = $request->has('bank') ? $request->input('bank'): null;
        $year = $request->has('year') ? $request->input('year'): null;
        $month = $request->has('month') ? $request->input('month'): null;

        Date::setLocale('es');

        $dateObj   = Date::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F'); // March


        $movements = $movements::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
            'currency',
        ])->where('state',CashFlow::ACCEPTED);

        // Search for a user based on their name.
        if ($year && $month) {
            $movements->whereYear('done_at', $year);
            $movements->whereMonth('done_at', $month);
        }

        // Search for a user based on their name.
        if ($bank) {
            $movements->where('bank_account_id', $bank);
        }else{
            $movements->whereHas('bankAccount');
        }

        $movements= $movements->orderBy('done_at', 'desc')->get();

        $bankInfo = BankAccount::find($bank);

        $ingresos_mes = CashFlow::getAccountStatusByDate($bank,1, $year, $month );
        $egresos_mes = CashFlow::getAccountStatusByDate($bank,2, $year, $month );
        $balance_mes = $ingresos_mes - $egresos_mes;

        $ingresos_mes_formateado = number_format( $ingresos_mes, 2, '.', "'");
        $egresos_mes_formateado = number_format( $egresos_mes, 2, '.', "'");
        $balance_mes_formateado = number_format( $balance_mes, 2, '.', "'");

        $balance_general = number_format( CashFlow::getAccountStatus($bank,1 )- CashFlow::getAccountStatus($bank,2 ), 2, '.', "'");

        $pdf = PDF::loadView('movements.pdf', compact('movements', 'year','monthName', 'bankInfo', 'ingresos_mes_formateado', 'egresos_mes_formateado', 'balance_mes_formateado', 'balance_general'));
        return $pdf->setPaper('a4')->download('movements.pdf');

    }

    public function consolidate(Request $request, CashFlow $movements){

        $type = "xls";

        $bank = $request->has('bank') ? $request->input('bank'): null;
        $year = $request->has('year') ? $request->input('year'): null;
        $month = $request->has('month') ? $request->input('month'): null;

        Date::setLocale('es');

        $dateObj   = Date::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F'); // March

        /*
        $movements = $movements::with([
            'voucher',
            'userRegistered',
            'userTransfer',
            'bankAccount.currency',
            'paymentWay',
            'currency',
        ])->where('state',CashFlow::ACCEPTED);
        */

        $movements = $movements::select(
            'cash_flows.id AS Id',
            'bank_accounts.titular AS Titular',
            'bank_accounts.bank AS Banco',
            'currencies.currency_code AS Moneda',
            'cash_flows.description AS Concepto',
            'cash_flows.done_at AS Realizado',
            'cash_flows.amount_movement AS Monto',
            \DB::raw("
            CASE
                WHEN cash_flows.operation_type = 1 THEN 'Ingreso'
                WHEN cash_flows.operation_type = 2 THEN 'Egreso'
                WHEN cash_flows.operation_type = 5 THEN 'Banco a caja'
                WHEN cash_flows.operation_type = 6 THEN 'Caja a banco'
            END
            AS Operacion")
        )
            ->join('bank_accounts','cash_flows.bank_account_id','bank_accounts.id')
            ->join('currencies','cash_flows.currency_id','currencies.id')


            ->where('state',CashFlow::ACCEPTED);

        // Search for a user based on their name.
        if ($year && $month) {
            $movements->whereYear('done_at', $year);
            $movements->whereMonth('done_at', $month);
        }

        // Search for a user based on their name.
        if ($bank) {
            $movements->where('bank_account_id', $bank);
        }else{
            $movements->whereHas('bankAccount');
        }

        $movements= $movements->orderBy('done_at', 'desc')->get()->toArray();

        $bankInfo = BankAccount::find($bank);

        $ingresos_mes = CashFlow::getAccountStatusByDate($bank,1, $year, $month );
        $egresos_mes = CashFlow::getAccountStatusByDate($bank,2, $year, $month );
        $balance_mes = $ingresos_mes - $egresos_mes;

        $ingresos_mes_formateado = number_format( $ingresos_mes, 2, '.', "'");
        $egresos_mes_formateado = number_format( $egresos_mes, 2, '.', "'");
        $balance_mes_formateado = number_format( $balance_mes, 2, '.', "'");

        $balance_general = number_format( CashFlow::getAccountStatus($bank,1 )- CashFlow::getAccountStatus($bank,2 ), 2, '.', "'");

        return Excel::create('cpc', function($excel) use ($movements) {
            $excel->sheet('mySheet', function($sheet) use ($movements)
            {
                $sheet->fromArray($movements);
            });
        })->download($type);

    }

    public function create(){
        $movement = new CashFlow();

        $btnText = __("Crear movimiento bancario");
        return view('movements.form', compact('movement','btnText'));
    }

    public function store(MovementRequest $movementRequest){

        //dd($voucherRequest->all());

        $movementRequest->merge(['user_registered_id' => auth()->user()->id ]);
        //$voucherRequest->merge(['emitted_at' => Carbon::now() ]);
        //$voucherRequest->merge(['expired_at' => Carbon::now() ]);

        //dd($voucherRequest);
        CashFlow::create($movementRequest->input());

        return back()->with('message',['success', __("Se agregó el movimiento correctamente.")]);
    }
}
