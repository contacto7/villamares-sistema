<?php

namespace App\Http\Requests;

use App\CashFlow;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CashFlowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'description' => 'required|min: 3',
                    'associate_voucher' => 'nullable',
                    'operation_type' =>
                        Rule::in([
                            CashFlow::INFLOW,
                            CashFlow::OUTFLOW,
                            CashFlow::INTERN_TRANSFER_IN,
                            CashFlow::INTERN_TRANSFER_OUT,
                            CashFlow::BANK_TO_CASH,
                            CashFlow::CASH_TO_BANK
                        ]),
                    'state' =>
                        'nullable',
                    Rule::in([
                        CashFlow::WAITING,
                        CashFlow::REJECTED,
                        CashFlow::ACCEPTED
                    ]),
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'amount_movement' => 'required',
                    'done_at' => 'required|date_format:Y-m-d H:i:s',
                    'voucher_id' => [
                        'nullable',
                        Rule::exists('vouchers','id')
                    ],
                    'user_transfer_id' => [
                        'nullable',
                        'required_if:operation_type,4',
                        Rule::exists('users','id')
                    ],
                    'bank_account_id' => [
                        'nullable',
                        Rule::exists('bank_accounts','id')
                    ],
                    'payment_way_id' => [
                        'required',
                        Rule::exists('payment_ways','id')
                    ],
                    'number_voucher_associated' => 'nullable',
                    'picture_voucher' => [
                        'nullable',
                        'required_if:operation_type,2'
                    ],
                    'cash_flow_id' => [
                        'nullable',
                        Rule::exists('cash_flows','id')
                    ]
                ];
            }
            case 'PUT':{
                return [
                    'description' => 'required|min: 3',
                    'associate_voucher' => 'nullable',
                    'operation_type' =>
                        Rule::in([
                            CashFlow::INFLOW,
                            CashFlow::OUTFLOW,
                            CashFlow::INTERN_TRANSFER_IN,
                            CashFlow::INTERN_TRANSFER_OUT,
                            CashFlow::BANK_TO_CASH,
                            CashFlow::CASH_TO_BANK
                        ]),
                    'state' =>
                        'nullable',
                    Rule::in([
                        CashFlow::WAITING,
                        CashFlow::REJECTED,
                        CashFlow::ACCEPTED
                    ]),
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'amount_movement' => 'required',
                    'done_at' => 'required|date_format:Y-m-d H:i:s',
                    'deposited_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'voucher_id' => [
                        'nullable',
                        Rule::exists('vouchers','id')
                    ],
                    'user_transfer_id' => [
                        'nullable',
                        'required_if:operation_type,4',
                        Rule::exists('users','id')
                    ],
                    'bank_account_id' => [
                        'nullable',
                        Rule::exists('bank_accounts','id')
                    ],
                    'payment_way_id' => [
                        'required',
                        Rule::exists('payment_ways','id')
                    ],
                    'number_voucher_associated' => 'nullable',
                    'picture_voucher' => 'nullable',
                    'cash_flow_id' => [
                        'nullable',
                        Rule::exists('cash_flows','id')
                    ]
                ];
            }
        }
    }
}
