<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VoucherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'number' => 'required',
                    'description' => 'required|min: 3',
                    'payment_document_id' => [
                        'required',
                        Rule::exists('payment_documents','id')
                    ],
                    'user_emitted_id' => [
                        'nullable',
                        Rule::exists('users','id')
                    ],
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'amount_payed' => 'nullable',
                    'amount_total' => 'required',
                    'service_type_id' => [
                        'required',
                        Rule::exists('service_types','id')
                    ],
                    'accounting_id' => [
                        'nullable',
                        Rule::exists('accountings','id')
                    ],
                    'constitution_id' => [
                        'nullable',
                        Rule::exists('constitutions','id')
                    ],
                    'extra_id' => [
                        'nullable',
                        Rule::exists('extras','id')
                    ],
                    'emitted_at' => 'required|date_format:Y-m-d H:i:s',
                    'expired_at' => 'required|date_format:Y-m-d H:i:s'
                ];
            }
            case 'PUT':{
                return [
                    'number' => 'required',
                    'description' => 'required|min: 3',
                    'payment_document_id' => [
                        'required',
                        Rule::exists('payment_documents','id')
                    ],
                    'user_emitted_id' => [
                        'nullable',
                        Rule::exists('users','id')
                    ],
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'amount_payed' => 'nullable',
                    'amount_total' => 'required',
                    'service_type_id' => [
                        'required',
                        Rule::exists('service_types','id')
                    ],
                    'accounting_id' => [
                        'nullable',
                        Rule::exists('accountings','id')
                    ],
                    'constitution_id' => [
                        'nullable',
                        Rule::exists('constitutions','id')
                    ],
                    'extra_id' => [
                        'nullable',
                        Rule::exists('extras','id')
                    ],
                    'emitted_at' => 'required|date_format:Y-m-d H:i:s',
                    'expired_at' => 'required|date_format:Y-m-d H:i:s'
                ];
            }
        }
    }
}
