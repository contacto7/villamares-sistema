<?php

namespace App\Http\Requests;

use App\Accounting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AccountingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'supervisor_user_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'company_id' => [
                        'required',
                        Rule::exists('companies','id')
                    ],
                    'monthly_revenue' => 'required',
                    'monthly_vouchers' => 'required',
                    'staff_payroll' => 'required',
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'payment_document_id' => [
                        'required',
                        Rule::exists('payment_documents','id')
                    ],
                    'first_rate' => 'required',
                    'normal_rate' => 'required',
                    'state' =>
                        Rule::in([
                            Accounting::RUNNING,
                            Accounting::TERMINATED
                        ]),
                    'electronic_books' =>
                        Rule::in([
                            Accounting::WITHOUT_ELECTRONIC_BOOKS,
                            Accounting::WITH_ELECTRONIC_BOOKS
                        ]),
                    'electronic_books_date' => 'nullable|date_format:Y-m-d',
                    'electronic_books_details' => 'nullable',
                    'started_at' => 'required|date_format:Y-m-d',
                    'delivery_charge' => 'nullable'
                ];
            }
            case 'PUT':{
                return [
                    'supervisor_user_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'company_id' => [
                        'required',
                        Rule::exists('companies','id')
                    ],
                    'monthly_revenue' => 'required',
                    'monthly_vouchers' => 'required',
                    'staff_payroll' => 'required',
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'payment_document_id' => [
                        'required',
                        Rule::exists('payment_documents','id')
                    ],
                    'first_rate' => 'required',
                    'normal_rate' => 'required',
                    'state' =>
                        Rule::in([
                            Accounting::RUNNING,
                            Accounting::TERMINATED
                        ]),
                    'electronic_books' =>
                        Rule::in([
                            Accounting::WITHOUT_ELECTRONIC_BOOKS,
                            Accounting::WITH_ELECTRONIC_BOOKS
                        ]),
                    'electronic_books_date' => 'nullable|date_format:Y-m-d',
                    'electronic_books_details' => 'nullable',
                    'started_at' => 'required|date_format:Y-m-d',
                    'delivery_charge' => 'nullable'
                ];
            }
        }
    }
}
