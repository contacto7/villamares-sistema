<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ExtraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'user_got_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'service_type_id' => [
                        'required',
                        Rule::exists('service_types','id')
                    ],
                    'customer_id' => [
                        'nullable',
                        'required_without:company_id',
                        Rule::exists('customers','id')
                    ],
                    'company_id' => [
                        'nullable',
                        'required_without:customer_id',
                        Rule::exists('companies','id')
                    ],
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'charged_amount' => 'required',
                    'description' => 'required',
                    'folios' => 'nullable',
                    'done_at' => 'required|date_format:Y-m-d H:i:s'
                ];
            }
            case 'PUT':{
                return [
                    'user_got_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'service_type_id' => [
                        'required',
                        Rule::exists('service_types','id')
                    ],
                    'customer_id' => 'nullable',
                    'company_id' => 'nullable',
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'charged_amount' => 'required',
                    'description' => 'required',
                    'folios' => 'nullable',
                    'done_at' => 'required|date_format:Y-m-d H:i:s'
                ];
            }
        }
    }
}
