<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'folios' => 'required',
                    'notary' => 'required',
                    'legalization' => 'nullable|date_format:Y-m-d H:i:s',
                    'picture' => 'nullable',
                    'book_type_id' => [
                        'required',
                        Rule::exists('book_types','id')
                    ],
                    'accounting_id' => [
                        'required',
                        Rule::exists('accountings','id')
                    ]
                ];
            }
            case 'PUT':{
                return [
                    'folios' => 'required',
                    'notary' => 'required',
                    'legalization' => 'nullable|date_format:Y-m-d H:i:s',
                    'picture' => 'nullable',
                    'book_type_id' => [
                        'required',
                        Rule::exists('book_types','id')
                    ],
                    'accounting_id' => [
                        'required',
                        Rule::exists('accountings','id')
                    ]
                ];
            }
        }
    }
}
