<?php

namespace App\Http\Requests;

use App\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'name' => 'required|min: 3',
                    'last_name' => 'required|min: 3',
                    'email' => 'required|email',
                    'cellphone' => 'required|min: 5',
                    'birthday' => 'nullable|date_format:Y-m-d',
                    'identity_document_id' => [
                        'required',
                        Rule::exists('identity_documents','id')
                    ],
                    'document_number' => 'required|min: 5',
                    'office_id' => [
                        'required',
                        Rule::exists('offices','id')
                    ],
                    'type' =>
                        Rule::in([
                            Customer::NORMAL,
                            Customer::PROBLEMATIC
                        ]),
                    'acquisition_way' =>
                        Rule::in([
                            Customer::FACEBOOK,
                            Customer::INSTAGRAM,
                            Customer::GOOGLE_ADS,
                            Customer::REFERER,
                            Customer::OTHER_ACQUISITION_WAY
                        ]),
                    'other_acquisition_way' => 'nullable',
                    'prospect_type' =>
                        Rule::in([
                            Customer::CONSTITUTION_PROSPECT,
                            Customer::ACCOUNTING_PROSPECT,
                            Customer::CUSTOMER
                        ]),
                ];
            }
            case 'PUT':{
                return [
                    'name' => 'required|min: 3',
                    'last_name' => 'required|min: 3',
                    'email' => 'required|email',
                    'cellphone' => 'required|min: 5',
                    'birthday' => 'nullable|date_format:Y-m-d',
                    'identity_document_id' => [
                        'required',
                        Rule::exists('identity_documents','id')
                    ],
                    'document_number' => 'required|min: 5',
                    'office_id' => [
                        'required',
                        Rule::exists('offices','id')
                    ],
                    'type' =>
                        Rule::in([
                            Customer::NORMAL,
                            Customer::PROBLEMATIC
                        ]),
                    'acquisition_way' =>
                        Rule::in([
                            Customer::FACEBOOK,
                            Customer::INSTAGRAM,
                            Customer::GOOGLE_ADS,
                            Customer::REFERER,
                            Customer::OTHER_ACQUISITION_WAY
                        ]),
                    'other_acquisition_way' => 'nullable',
                    'prospect_type' =>
                        Rule::in([
                            Customer::CONSTITUTION_PROSPECT,
                            Customer::ACCOUNTING_PROSPECT,
                            Customer::CUSTOMER
                        ]),
                ];
            }


        }
    }
}
