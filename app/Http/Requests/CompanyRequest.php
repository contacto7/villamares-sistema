<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'office_id' => [
                        'required',
                        Rule::exists('offices','id')
                    ],
                    'customer_id' => [
                        'required',
                        Rule::exists('customers','id')
                    ],
                    'name' => 'required|min: 3',
                    'tax_number' => 'required|min: 5',
                    'address' => 'required|min: 3',
                    'manager_name' => 'required|min: 3',
                    'manager_last_name' => 'required|min: 3',
                    'manager_dni' => 'required|min: 5',
                    'contact_name' => 'required|min: 3',
                    'contact_last_name' => 'required|min: 3',
                    'contact_email' => 'required|email',
                    'contact_phone' => 'required|min: 5',
                    'tax_regime_id' => [
                        'required',
                        Rule::exists('tax_regimes','id')
                    ],
                    'company_turn_id' => [
                        'required',
                        Rule::exists('company_turns','id')
                    ],
                    'company_turn_other' => 'nullable',
                    'registered_at' => 'date_format:Y-m-d H:i:s'
                ];
            }
            case 'PUT':{
                return [
                    'office_id' => [
                        'required',
                        Rule::exists('offices','id')
                    ],
                    'customer_id' => [
                        'required',
                        Rule::exists('customers','id')
                    ],
                    'name' => 'required|min: 3',
                    'tax_number' => 'required|min: 5',
                    'address' => 'required|min: 3',
                    'manager_name' => 'required|min: 3',
                    'manager_last_name' => 'required|min: 3',
                    'manager_dni' => 'required|min: 5',
                    'contact_name' => 'required|min: 3',
                    'contact_last_name' => 'required|min: 3',
                    'contact_email' => 'required|email',
                    'contact_phone' => 'required|min: 5',
                    'tax_regime_id' => [
                        'required',
                        Rule::exists('tax_regimes','id')
                    ],
                    'company_turn_id' => [
                        'required',
                        Rule::exists('company_turns','id')
                    ],
                    'company_turn_other' => 'nullable'
                ];
            }
        }
    }
}
