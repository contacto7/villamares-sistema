<?php

namespace App\Http\Requests;

use App\CashFlow;
use App\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MovementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->role_id === Role::ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'description' => 'required|min: 3',
                    'operation_type' =>
                        Rule::in([
                            CashFlow::INFLOW,
                            CashFlow::OUTFLOW,
                            CashFlow::INTERN_TRANSFER_IN,
                            CashFlow::INTERN_TRANSFER_OUT
                        ]),
                    'state' =>
                        'nullable',
                    Rule::in([
                        CashFlow::WAITING,
                        CashFlow::REJECTED,
                        CashFlow::ACCEPTED
                    ]),
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'amount_movement' => 'required',
                    'done_at' => 'required|date_format:Y-m-d H:i:s',
                    'voucher_id' => [
                        'nullable',
                        Rule::exists('vouchers','id')
                    ],
                    'user_did_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'user_registered_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'user_transfer_id' => [
                        'nullable',
                        Rule::exists('users','id')
                    ],
                    'bank_account_id' => [
                        'nullable',
                        Rule::exists('bank_accounts','id')
                    ],
                    'payment_way_id' => [
                        'required',
                        Rule::exists('payment_ways','id')
                    ],
                    'number_voucher_associated' => 'nullable',
                    'picture_voucher' => 'nullable'
                ];
            }
            case 'PUT':{
                return [
                    'description' => 'required|min: 3',
                    'operation_type' =>
                        Rule::in([
                            CashFlow::INFLOW,
                            CashFlow::OUTFLOW,
                            CashFlow::INTERN_TRANSFER_IN,
                            CashFlow::INTERN_TRANSFER_OUT
                        ]),
                    'state' =>
                        'nullable',
                        Rule::in([
                            CashFlow::WAITING,
                            CashFlow::REJECTED,
                            CashFlow::ACCEPTED
                        ]),
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'amount_movement' => 'required',
                    'done_at' => 'required|date_format:Y-m-d H:i:s',
                    'voucher_id' => [
                        'nullable',
                        Rule::exists('vouchers','id')
                    ],
                    'user_did_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'user_registered_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'user_transfer_id' => [
                        'nullable',
                        Rule::exists('users','id')
                    ],
                    'bank_account_id' => [
                        'nullable',
                        Rule::exists('bank_accounts','id')
                    ],
                    'payment_way_id' => [
                        'required',
                        Rule::exists('payment_ways','id')
                    ],
                    'number_voucher_associated' => 'nullable',
                    'picture_voucher' => 'nullable'
                ];
            }
        }
    }
}
