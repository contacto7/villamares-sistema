<?php

namespace App\Http\Requests;

use App\Constitution;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ConstitutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'company_id' => [
                        'required',
                        Rule::exists('companies','id')
                    ],
                    'user_got_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'charged_amount' => 'required',
                    'capital_amount' => 'required',
                    'reservation_number' => 'nullable',
                    'reservation_service' => 'required',
                    'reservation_expiration' => 'nullable|date_format:Y-m-d H:i:s',
                    'partners_number' => 'nullable',
                    'additional_managers' => 'nullable',
                    'registration_charge_picture' => 'nullable',
                    'impression_state' =>
                        Rule::in([
                            Constitution::NOT_PRINTED,
                            Constitution::PRINTED_FOR_ONE,
                            Constitution::PRINTED_FOR_TWO,
                        ]),
                    'lawyer_fee' => 'nullable',
                    'notary_fee' => 'nullable',
                    'public_record' => 'nullable',
                    'notarial_letter' => 'nullable',
                    'directory' => 'nullable',
                    'others' => 'nullable',
                    'devolution' => 'nullable',
                    'picture_1' => 'nullable',
                    'picture_2' => 'nullable',
                    'picture_3' => 'nullable',
                    'picture_4' => 'nullable',
                    'done_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'signed_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'picked_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'delivered_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'delivered_person' => 'nullable',
                    'vehicles' => 'nullable',
                    'social_object' => 'nullable'
                ];
            }
            case 'PUT':{
                return [
                    'company_id' => [
                        'required',
                        Rule::exists('companies','id')
                    ],
                    'user_got_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'charged_amount' => 'required',
                    'capital_amount' => 'required',
                    'reservation_number' => 'nullable',
                    'reservation_service' => 'required',
                    'reservation_expiration' => 'nullable|date_format:Y-m-d H:i:s',
                    'partners_number' => 'nullable',
                    'additional_managers' => 'nullable',
                    'registration_charge_picture' => 'nullable',
                    'impression_state' =>
                        Rule::in([
                            Constitution::NOT_PRINTED,
                            Constitution::PRINTED_FOR_ONE,
                            Constitution::PRINTED_FOR_TWO,
                        ]),
                    'lawyer_fee' => 'nullable',
                    'notary_fee' => 'nullable',
                    'public_record' => 'nullable',
                    'notarial_letter' => 'nullable',
                    'directory' => 'nullable',
                    'others' => 'nullable',
                    'devolution' => 'nullable',
                    'picture_1' => 'nullable',
                    'picture_2' => 'nullable',
                    'picture_3' => 'nullable',
                    'picture_4' => 'nullable',
                    'done_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'signed_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'picked_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'delivered_at' => 'nullable|date_format:Y-m-d H:i:s',
                    'delivered_person' => 'nullable',
                    'vehicles' => 'nullable',
                    'social_object' => 'nullable'
                ];
            }
        }
    }
}
