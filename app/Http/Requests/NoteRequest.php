<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'customer_id' => [
                        'nullable',
                        'required_without:company_id',
                        Rule::exists('customers','id')
                    ],
                    'description' => 'required'
                ];
            }
            case 'PUT':{
                return [
                    'customer_id' => [
                        'nullable',
                        'required_without:company_id',
                        Rule::exists('customers','id')
                    ],
                    'description' => 'required'
                ];
            }
        }
    }
}
