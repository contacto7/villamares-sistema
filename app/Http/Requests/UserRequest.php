<?php

namespace App\Http\Requests;

use App\Role;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->role_id === Role::ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':{
                return [
                    'role_id' => [
                        'required',
                        Rule::exists('roles','id')
                    ],
                    'office_id' => [
                        'required',
                        Rule::exists('offices','id')
                    ],
                    'name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email',
                    'email_verified_at' => 'nullable',
                    'password' => 'required',
                    'picture' => 'nullable',
                    'state' =>
                        Rule::in([
                            User::ACTIVE,
                            User::INACTIVE,
                        ])
                ];
            }
            case 'PUT':{
                return [
                    'role_id' => [
                        'required',
                        Rule::exists('roles','id')
                    ],
                    'office_id' => [
                        'required',
                        Rule::exists('offices','id')
                    ],
                    'name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email',
                    'email_verified_at' => 'nullable',
                    'password' => 'required',
                    'picture' => 'nullable',
                    'state' =>
                        Rule::in([
                            User::ACTIVE,
                            User::INACTIVE,
                        ])
                ];
            }
        }
    }
}
