<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Company
 *
 * @property int $id
 * @property string $name
 * @property string $tax_number
 * @property string $manager_name
 * @property string $manager_last_name
 * @property string $contact_name
 * @property string $contact_last_name
 * @property string $contact_email
 * @property int $tax_regime_id
 * @property int $company_turn_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCompanyTurnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereManagerLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereManagerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTaxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTaxRegimeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $customer_id
 * @property string|null $registered_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereRegisteredAt($value)
 * @property int $office_id
 * @property int $user_id
 * @property string $slug
 * @property string $address
 * @property string $manager_dni
 * @property string $contact_phone
 * @property-read \App\Accounting $accounting
 * @property-read \App\CompanyTurn $companyTurn
 * @property-read \App\Constitution $constitution
 * @property-read \App\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \App\Office $office
 * @property-read \App\TaxRegime $taxRegime
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereManagerDni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereUserId($value)
 */
class Company extends Model
{

    protected $fillable = [
        'office_id', 'customer_id', 'user_id', 'name', 'slug',
        'tax_number', 'address', 'manager_name',
        'manager_last_name', 'manager_dni', 'contact_name',
        'contact_last_name', 'contact_email', 'contact_phone',
        'tax_regime_id', 'company_turn_id', 'company_turn_other',
        'registered_at'
    ];

    public static function boot(){
        parent::boot();

        static::saving(function (Company $company){
            if(! \App::runningInConsole()){
                $company->slug = str_slug($company->name, "-");
            }
        });

        static::updating(function (Company $company){
            if(! \App::runningInConsole()){
                $company->slug = str_slug($company->name, "-");
            }
        });

    }

    public function getRouteKeyName(){
        //En este caso, el nombre de la ruta será el atributo slug
        return 'slug';
    }

    public function office(){
        return $this->belongsTo(Office::class);
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function taxRegime(){
        return $this->belongsTo(TaxRegime::class);
    }

    public function companyTurn(){
        return $this->belongsTo(CompanyTurn::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function accounting(){
        return $this->hasOne(Accounting::class);
    }

    public function constitution(){
        return $this->hasOne(Constitution::class);
    }

    public function extras(){
        return $this->hasMany(Extra::class);
    }

}
