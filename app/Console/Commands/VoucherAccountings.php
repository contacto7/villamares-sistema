<?php

namespace App\Console\Commands;

use App\Accounting;
use App\Voucher;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Psr\Log\NullLogger;

class VoucherAccountings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:accountings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create monthly vouchers for accountings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accountings = Accounting::where('state',Accounting::RUNNING)->get();

        foreach ($accountings as $accounting) {
            $accountingFactura = ((int) $accounting->payment_document_id === 1);
            $accountingRRHH = ((int) $accounting->payment_document_id === 2 || (int) $accounting->payment_document_id === 3 );

            $factura = 1;
            $recibo_de_caja = 4;

            if ( $accountingFactura ){
                $payment_document_id = $factura;
            }else{
                $payment_document_id = $recibo_de_caja;
            }

            $voucherCreated = new Voucher();

            $emitted_at = Carbon::now('America/Lima');
            $expired_at = Carbon::createFromFormat('Y-m-d H:i:s', $emitted_at)->addDays( 30 );

            $voucherCreated::create([
                'number'=> 'F001-AUTO00'.$accounting->id.$emitted_at,
                'description'=> "Comprobante emitido mensualmente por el sistema, por concepto de servicio contable",
                'associate_voucher'=> null,
                'payment_document_id'=> $payment_document_id,
                'user_registered_id'=> 1,
                'user_emitted_id'=> 1,
                'currency_id'=> $accounting->currency_id,
                'amount_payed'=> 0,
                'amount_total'=> $accounting->normal_rate,
                'service_type_id'=> 1,
                'accounting_id'=> $accounting->id,
                'constitution_id'=> null,
                'extra_id'=> null,
                'emitted_at'=> $emitted_at,
                'cancelled_at'=> null,
                'expired_at'=> $expired_at,
            ]);




        }

    }
}
