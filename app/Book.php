<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Book
 *
 * @property int $id
 * @property int $folios
 * @property string|null $notary
 * @property string|null $legalization
 * @property string|null $picture
 * @property int $book_type_id
 * @property int $accounting_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereAccountingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereFolios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereLegalization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereNotary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Accounting $accounting
 * @property-read \App\BookType $bookType
 */
class Book extends Model
{
    protected $fillable = [
        'folios', 'notary', 'legalization', 'picture',
        'book_type_id', 'accounting_id'
    ];

    public function pathAttachment(){
        return "/images/books/".$this->picture;
    }

    public function bookType(){
        return $this->belongsTo(BookType::class);
    }

    public function accounting(){
        return $this->belongsTo(Accounting::class);
    }
}
