<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CompanyTurn
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyTurn whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 */
class CompanyTurn extends Model
{
    public $timestamps = false;

    public function companies(){
        return $this->hasMany(Company::class);
    }
}
