<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CashRegister
 *
 * @property int $id
 * @property string $operation_type
 * @property int $user_registered_id
 * @property int|null $payment_method_id
 * @property string $state
 * @property int|null $voucher_id
 * @property int|null $customer_id
 * @property string|null $establishment
 * @property int|null $user_transferred_id
 * @property string $done_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereDoneAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereEstablishment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereOperationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserRegisteredId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserTransferredId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereVoucherId($value)
 * @mixin \Eloquent
 * @property float $initial_cash
 * @property string $last_cash_count último arqueo de caja
 * @property int|null $user_owner_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereInitialCash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereLastCashCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserOwnerId($value)
 * @property int $user_id Dueño de la caja
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CashRegister whereUserId($value)
 */
class CashRegister extends Model
{
    protected $fillable = [
        'initial_cash', 'last_cash_count', 'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public static function getLastCashCount($user){
        return CashRegister::where('user_id',$user)
            ->select('last_cash_count')
            ->first();
    }

    public static function  totalCash($user, $currency){

        //INGRESO DE EFECTIVO

        //INGRESOS
        $cash_in = self::cashFlowSum($user, CashFlow::INFLOW, $currency);
        $bank_to_cash = self::bankCashSum($user, CashFlow::BANK_TO_CASH, $currency);
        $transfer_in = self::internTransferSum($user, CashFlow::INTERN_TRANSFER_IN, $currency);

        //EGRESOS
        $cash_out = self::cashFlowSum($user, CashFlow::OUTFLOW, $currency);
        $cash_to_bank = self::bankCashSum($user, CashFlow::CASH_TO_BANK, $currency);
        $transfer_out = self::internTransferSum($user, CashFlow::INTERN_TRANSFER_OUT, $currency);

        //dd($transfer_out);
        return ($cash_in + $transfer_in +$bank_to_cash) - ($cash_out + $transfer_out + $cash_to_bank);
    }

    public static function cashFlowSum($user, $operation_type, $currency){
        $flowSum = CashFlow::
        join('cash_registers','cash_flows.user_registered_id','=','cash_registers.user_id')
            ->whereColumn('cash_flows.done_at','>=','cash_registers.last_cash_count')
            ->where('cash_flows.currency_id',$currency)
            ->where('cash_flows.user_registered_id',$user)
            ->where('cash_flows.operation_type',$operation_type)
            ->where('cash_flows.payment_way_id',1)
            ->sum('amount_movement');
        return $flowSum;
    }

    public static function bankCashSum($user, $operation_type, $currency){
        $flowSum = CashFlow::
        join('cash_registers','cash_flows.user_registered_id','=','cash_registers.user_id')
            ->whereColumn('cash_flows.done_at','>=','cash_registers.last_cash_count')
            ->where('cash_flows.currency_id',$currency)
            ->where('cash_flows.user_registered_id',$user)
            ->where('cash_flows.operation_type',$operation_type)
            ->sum('amount_movement');
        return $flowSum;
    }

    public static function internTransferSum($user, $operation_type, $currency){
        $transferSum = CashFlow::
        join('cash_registers','cash_flows.user_registered_id','=','cash_registers.user_id')
            ->whereColumn('cash_flows.done_at','>=','cash_registers.last_cash_count')
            ->where('cash_flows.currency_id',$currency)
            ->where('cash_flows.user_registered_id',$user)
            ->where('cash_flows.operation_type',$operation_type)
            ->where('cash_flows.state',CashFlow::ACCEPTED)
            ->where('cash_flows.payment_way_id',1)
            ->sum('amount_movement');
        return $transferSum;
    }
}
