<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BookType
 *
 * @property int $id
 * @property string $name libro de compras, libro de ventas, libro de actas
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BookType whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Book[] $books
 */
class BookType extends Model
{
    public $timestamps = false;

    public function books(){
        return $this->hasMany(Book::class);
    }
}
