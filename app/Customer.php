<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Customer
 *
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string|null $birthday
 * @property string|null $email
 * @property int $identity_document_id
 * @property string $document_number
 * @property int $office_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereIdentityDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_registered_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUserRegisteredId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Constitution[] $constitutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \App\IdentityDocument $identityDocument
 * @property-read \App\Office $office
 * @property-read \App\User $user
 * @property string $slug
 * @property string $cellphone
 * @property int $user_id usuario que registró
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCellphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUserId($value)
 */
class Customer extends Model
{
    //TYPE
    const NORMAL = 1;
    const PROBLEMATIC = 2;

    //ACQUISITION WAY
    const FACEBOOK = 1;
    const INSTAGRAM = 2;
    const GOOGLE_ADS = 3;
    const REFERER = 4;
    const OTHER_ACQUISITION_WAY = 5;

    //PROSPECT TYPE
    const CONSTITUTION_PROSPECT = 1;
    const ACCOUNTING_PROSPECT = 2;
    const CUSTOMER = 3;

    protected $fillable = [
        'name', 'last_name', 'slug', 'birthday', 'email',
        'cellphone', 'identity_document_id',
        'document_number', 'office_id', 'user_id', 'type',
        'acquisition_way', 'other_acquisition_way', 'prospect_type'
    ];

    public static function boot(){
        parent::boot();

        static::saving(function (Customer $customer){
            if(! \App::runningInConsole()){
                $customer->slug = str_slug($customer->name." ".$customer->last_name, "-");

                $customer->birthday =(new Carbon($customer->birthday))->format('Y-m-d');
            }
        });

    }

    public function getRouteKeyName(){
        //En este caso, el nombre de la ruta será el atributo slug
        return 'slug';
    }

    public function identityDocument(){
        return $this->belongsTo(IdentityDocument::class);
    }

    public function office(){
        return $this->belongsTo(Office::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function companies(){
        return $this->hasMany(Company::class);
    }

    public function constitutions(){
        return $this->hasMany(Constitution::class);
    }

    public function extras(){
        return $this->hasMany(Extra::class);
    }

    public function notes(){
        return $this->hasMany(Note::class);
    }

    /*
    public function setSlugAttribute(){
        $name = $this->attributes['name'] ;
        $last_name = $this->attributes['last_name'];

        $slug = str_slug($name." ".$last_name,'-');
        $this->attributes['slug'] = $slug;
    }
    */
}
