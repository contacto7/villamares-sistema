<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use phpDocumentor\Reflection\Types\This;

/**
 * App\User
 *
 * @property int $id
 * @property int $role_id
 * @property int $office_id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $picture
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlowRegistered
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlowTransfer
 * @property-read \App\CashRegister $cashRegister
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 * @property-read \App\Office $office
 * @property-read \App\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $voucherEmitted
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $voucherRegistered
 * @property string $slug
 * @property string $state
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Accounting[] $accountings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Constitution[] $constitutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereState($value)
 */
class User extends Authenticatable
{
    const ACTIVE = 1;
    const INACTIVE = 2;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id','office_id','last_name', 'slug', 'email_verified_at',
        'picture','state','name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public static function boot(){
        parent::boot();

        static::saving(function (User $user){
            if(! \App::runningInConsole()){
                //Crear el slug
                $name = $user->name;
                $last_name = $user->last_name;

                $slug_name = str_slug($name." ".$last_name, '-');
                $user->slug = $slug_name;
            }
        });

        static::created(function (User $user){
            if(! \App::runningInConsole()){
                $cashRegister = new CashRegister();

                $user_id = $user->id;

                $cashRegister::create([
                    'initial_cash'=>0,
                    'last_cash_count'=>Carbon::now(),
                    'user_id' => $user_id
                    ]);
            }
        });

        static::updating(function (User $user){
            if(! \App::runningInConsole()){
                //Crear el slug
                $name = $user->name;
                $last_name = $user->last_name;

                $slug_name = str_slug($name." ".$last_name, '-');
                $user->slug = $slug_name;
            }
        });

    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setSlugAttribute(){

        $slug_name = str_slug($this->attributes['name']." ".$this->attributes['last_name'], '-');

        $this->attributes['slug'] = $slug_name;
    }

    public function getRouteKeyName(){
        //En este caso, el nombre de la ruta será el atributo slug
        return 'slug';
    }

    public static function navigation(){
        return auth()->check() ? auth()->user()->role->name : 'guest';
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function office(){
        return $this->belongsTo(Office::class);
    }

    public function customers(){
        return $this->hasMany(Customer::class);
    }

    public function companies(){
        return $this->hasMany(Company::class);
    }

    public function voucherRegistered(){
        return $this->hasMany(Voucher::class, 'user_registered_id');
    }

    public function voucherEmitted(){
        return $this->hasMany(Voucher::class, 'user_emitted_id');
    }

    public function cashFlowRegistered(){
        return $this->hasMany(CashFlow::class, 'user_registered_id');
    }

    public function cashFlowTransfer(){
        return $this->hasMany(CashFlow::class, 'user_transfer_id');
    }

    public function cashRegister(){
        return $this->hasOne(CashRegister::class);
    }

    public function accountings(){
        return $this->hasMany(Accounting::class);
    }

    public function accountingsWorker(){
        return $this->hasMany(Accounting::class,'worker_user_id');
    }

    public function accountingsSupervisors(){
        return $this->hasMany(Accounting::class,'supervisor_user_id');
    }

    public function constitutions(){
        return $this->hasMany(Constitution::class);
    }

    public function extras(){
        return $this->hasMany(Extra::class);
    }

    public function extrasGot(){
        return $this->hasMany(Extra::class,'user_got_id');
    }

    public function notes(){
        return $this->hasMany(Note::class);
    }

    public function constitutionsGot(){
        return $this->hasMany(Constitution::class,'user_got_id');
    }

}
