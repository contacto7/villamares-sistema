<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ServiceType
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ServiceType whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 */
class ServiceType extends Model
{
    public $timestamps = false;

    public function vouchers(){
        return $this->hasMany(Voucher::class);
    }

    public function extras(){
        return $this->hasMany(Extra::class);
    }
}
