<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Extra;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExtraPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the extra.
     *
     * @param  \App\User  $user
     * @param  \App\Extra  $extra
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::ACCOUNTINGS;
    }

    /**
     * Determine whether the user can create extras.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS;
    }

    /**
     * Determine whether the user can update the extra.
     *
     * @param  \App\User  $user
     * @param  \App\Extra  $extra
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS;
    }

    /**
     * Determine whether the user can delete the extra.
     *
     * @param  \App\User  $user
     * @param  \App\Extra  $extra
     * @return mixed
     */
    public function delete(User $user, Extra $extra)
    {
        //
    }

    /**
     * Determine whether the user can restore the extra.
     *
     * @param  \App\User  $user
     * @param  \App\Extra  $extra
     * @return mixed
     */
    public function restore(User $user, Extra $extra)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the extra.
     *
     * @param  \App\User  $user
     * @param  \App\Extra  $extra
     * @return mixed
     */
    public function forceDelete(User $user, Extra $extra)
    {
        //
    }

    public function seeOnlyOffice(User $user){
        return $user->role_id === Role::CASHIER;
    }

    public function addOnlyOffice(User $user){
        return $user->role_id === Role::CASHIER;
    }

    public function filterByOffice(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }
    public function listAllUsers(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER;
    }
}
