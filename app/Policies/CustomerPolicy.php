<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Customer;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the customer.
     *
     * @param  \App\User  $user
     * @param  \App\Customer  $customer
     * @return mixed
     */
    public function view(User $user, Customer $customer)
    {
        //
    }

    /**
     * Determine whether the user can create customers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CONSTITUTIONS;
    }

    /**
     * Determine whether the user can update the customer.
     *
     * @param  \App\User  $user
     * @param  \App\Customer  $customer
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    /**
     * Determine whether the user can delete the customer.
     *
     * @param  \App\User  $user
     * @param  \App\Customer  $customer
     * @return mixed
     */
    public function delete(User $user, Customer $customer)
    {
        //
    }

    /**
     * Determine whether the user can restore the customer.
     *
     * @param  \App\User  $user
     * @param  \App\Customer  $customer
     * @return mixed
     */
    public function restore(User $user, Customer $customer)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the customer.
     *
     * @param  \App\User  $user
     * @param  \App\Customer  $customer
     * @return mixed
     */
    public function forceDelete(User $user, Customer $customer)
    {
        //
    }

    public function list(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::ACCOUNTINGS || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER;
    }

    public function listAllUsers(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function edit(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filterByUser(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filterAccountingProspect(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER;
    }

    public function filterConstitutionProspect(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CONSTITUTIONS;
    }

    public function filterProspectFromOffice(User $user){
        return $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER;
    }

    public function seeOnlyAccountingCustomer(User $user){
        return false;
        //return $user->role_id === Role::CASHIER || $user->role_id === Role::ACCOUNTINGS;
    }

    public function seeOnlyConstitutionCustomer(User $user){
        return false;
        //return $user->role_id === Role::CONSTITUTIONS;
    }

    public function seeOnlyOffice(User $user){
        return $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER;
    }

    public function seeOnlyUploaded(User $user){
        return $user->role_id === Role::ACCOUNTINGS;
    }

    public function seeBirthday(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER;
    }

}
