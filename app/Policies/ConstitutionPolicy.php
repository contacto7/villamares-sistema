<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Constitution;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConstitutionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the constitution.
     *
     * @param  \App\User  $user
     * @param  \App\Constitution  $constitution
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::ACCOUNTINGS;
    }

    /**
     * Determine whether the user can create constitutions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the constitution.
     *
     * @param  \App\User  $user
     * @param  \App\Constitution  $constitution
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    /**
     * Determine whether the user can delete the constitution.
     *
     * @param  \App\User  $user
     * @param  \App\Constitution  $constitution
     * @return mixed
     */
    public function delete(User $user, Constitution $constitution)
    {
        //
    }

    /**
     * Determine whether the user can restore the constitution.
     *
     * @param  \App\User  $user
     * @param  \App\Constitution  $constitution
     * @return mixed
     */
    public function restore(User $user, Constitution $constitution)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the constitution.
     *
     * @param  \App\User  $user
     * @param  \App\Constitution  $constitution
     * @return mixed
     */
    public function forceDelete(User $user, Constitution $constitution)
    {
        //
    }

    /**
     * Determine whether the user can list the constitution.
     *
     * @param  \App\User  $user
     * @param  \App\Constitution  $constitution
     * @return mixed
     */

    public function changeChargedAmount(User $user, Constitution $constitution){
        return !isset($constitution->signed_at) || $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filterOffice(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function seeOnlyOffices(User $user){
        return $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER;
    }

    public function addConstitution(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS;
    }
    public function listAllUsers(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER;
    }

}
