<?php

namespace App\Policies;

use App\CashFlow;
use App\Role;
use App\User;
use App\CashRegister;
use Illuminate\Auth\Access\HandlesAuthorization;

class CashRegisterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the cash register.
     *
     * @param  \App\User  $user
     * @param  \App\CashRegister  $cashRegister
     * @return mixed
     */
    public function view(User $user, CashRegister $cashRegister)
    {
        //
    }

    /**
     * Determine whether the user can create cash registers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the cash register.
     *
     * @param  \App\User  $user
     * @param  \App\CashRegister  $cashRegister
     * @return mixed
     */
    public function update(User $user, CashRegister $cashRegister)
    {
        //
    }

    /**
     * Determine whether the user can delete the cash register.
     *
     * @param  \App\User  $user
     * @param  \App\CashRegister  $cashRegister
     * @return mixed
     */
    public function delete(User $user, CashRegister $cashRegister)
    {
        //
    }

    /**
     * Determine whether the user can restore the cash register.
     *
     * @param  \App\User  $user
     * @param  \App\CashRegister  $cashRegister
     * @return mixed
     */
    public function restore(User $user, CashRegister $cashRegister)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the cash register.
     *
     * @param  \App\User  $user
     * @param  \App\CashRegister  $cashRegister
     * @return mixed
     */
    public function forceDelete(User $user, CashRegister $cashRegister)
    {
        //
    }

    /**
     * Determine whether the user can list the constitution.
     *
     * @param  \App\User  $user
     * @param  \App\Constitution  $constitution
     * @return mixed
     */
    public function list(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CONSTITUTIONS;
    }

    public function seeAllMovement(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER;
    }

    public function filterMovement(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER;
    }

    public function validateMovement(User $user, CashFlow $cashFlow){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN  || ((int) $cashFlow->operation_type === $cashFlow::INTERN_TRANSFER_IN && $cashFlow->user_registered_id === $user->id );
    }

    public function balanceCash(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }



}
