<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Voucher;
use Illuminate\Auth\Access\HandlesAuthorization;

class VoucherPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the voucher.
     *
     * @param  \App\User  $user
     * @param  \App\Voucher  $voucher
     * @return mixed
     */
    public function view(User $user, Voucher $voucher)
    {
        //
    }

    /**
     * Determine whether the user can create vouchers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER;
    }

    /**
     * Determine whether the user can update the voucher.
     *
     * @param  \App\User  $user
     * @param  \App\Voucher  $voucher
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    /**
     * Determine whether the user can delete the voucher.
     *
     * @param  \App\User  $user
     * @param  \App\Voucher  $voucher
     * @return mixed
     */
    public function delete(User $user, Voucher $voucher)
    {
        //
    }

    /**
     * Determine whether the user can restore the voucher.
     *
     * @param  \App\User  $user
     * @param  \App\Voucher  $voucher
     * @return mixed
     */
    public function restore(User $user, Voucher $voucher)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the voucher.
     *
     * @param  \App\User  $user
     * @param  \App\Voucher  $voucher
     * @return mixed
     */
    public function forceDelete(User $user, Voucher $voucher)
    {
        //
    }

    public function seeResume(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER;
    }

    public function seeOnlyOffice(User $user){
        return $user->role_id === Role::CASHIER ||$user->role_id === Role::CONSTITUTIONS;
    }

    public function seeOnlyConstitutions(User $user){
        //SE QUITO -> VER SOLO POR CONSTITUCIÓN
        return false;
    }

    public function list(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::ACCOUNTINGS;
    }

    public function listAllUsers(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function createFactura(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CENTRAL_CASHIER;
    }

    public function createRRHH(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS;
    }

    public function createRRCC(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CONSTITUTIONS || $user->role_id === Role::CENTRAL_CASHIER;
    }

    public function createOnlyFromOffice(User $user)
    {
        return $user->role_id === Role::CASHIER;
    }

    public function createOnlyConstitution(User $user)
    {
        return $user->role_id === Role::CONSTITUTIONS;
    }

    public function filterByVoucher(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filterByServiceType(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

}
