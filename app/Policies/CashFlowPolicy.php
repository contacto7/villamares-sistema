<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\CashFlow;
use Illuminate\Auth\Access\HandlesAuthorization;

class CashFlowPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the cash flow.
     *
     * @param  \App\User  $user
     * @param  \App\CashFlow  $cashFlow
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER || $user->role_id === Role::CONSTITUTIONS;
    }

    /**
     * Determine whether the user can create cash flows.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the cash flow.
     *
     * @param  \App\User  $user
     * @param  \App\CashFlow  $cashFlow
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    /**
     * Determine whether the user can delete the cash flow.
     *
     * @param  \App\User  $user
     * @param  \App\CashFlow  $cashFlow
     * @return mixed
     */
    public function delete(User $user, CashFlow $cashFlow)
    {
        //
    }

    /**
     * Determine whether the user can restore the cash flow.
     *
     * @param  \App\User  $user
     * @param  \App\CashFlow  $cashFlow
     * @return mixed
     */
    public function restore(User $user, CashFlow $cashFlow)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the cash flow.
     *
     * @param  \App\User  $user
     * @param  \App\CashFlow  $cashFlow
     * @return mixed
     */
    public function forceDelete(User $user, CashFlow $cashFlow)
    {
        //
    }

    public function listMovements(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function seeOnlyUser(User $user){
        return $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER || $user->role_id === Role::CONSTITUTIONS;
    }

    public function filterByUser(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filterByOperationType(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filter(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }
}
