<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Company;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function view(User $user, Company $company)
    {
        //
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CONSTITUTIONS;
    }

    /**
     * Determine whether the user can update the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CONSTITUTIONS;
    }

    /**
     * Determine whether the user can delete the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function delete(User $user, Company $company)
    {
        //
    }

    /**
     * Determine whether the user can restore the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function restore(User $user, Company $company)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the company.
     *
     * @param  \App\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function forceDelete(User $user, Company $company)
    {
        //
    }

    public function list(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::ACCOUNTINGS || $user->role_id === Role::CONSTITUTIONS;
    }

    public function listAllUsers(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function createOnlyFromOffice(User $user)
    {
        return $user->role_id === Role::CASHIER || $user->role_id === Role::CENTRAL_CASHIER;
    }
}
