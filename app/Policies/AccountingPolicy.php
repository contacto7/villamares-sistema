<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Accounting;
use Illuminate\Auth\Access\HandlesAuthorization;

class AccountingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the accounting.
     *
     * @param  \App\User  $user
     * @param  \App\Accounting  $accounting
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER || $user->role_id === Role::ACCOUNTINGS || $user->role_id === Role::CONSTITUTIONS;
    }

    /**
     * Determine whether the user can create accountings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN || $user->role_id === Role::CENTRAL_CASHIER || $user->role_id === Role::CASHIER;
    }

    /**
     * Determine whether the user can update the accounting.
     *
     * @param  \App\User  $user
     * @param  \App\Accounting  $accounting
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    /**
     * Determine whether the user can delete the accounting.
     *
     * @param  \App\User  $user
     * @param  \App\Accounting  $accounting
     * @return mixed
     */
    public function delete(User $user, Accounting $accounting)
    {
        //
    }

    /**
     * Determine whether the user can restore the accounting.
     *
     * @param  \App\User  $user
     * @param  \App\Accounting  $accounting
     * @return mixed
     */
    public function restore(User $user, Accounting $accounting)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the accounting.
     *
     * @param  \App\User  $user
     * @param  \App\Accounting  $accounting
     * @return mixed
     */
    public function forceDelete(User $user, Accounting $accounting)
    {
        //
    }

    public function filterByWorker(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filterByCoordinator(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function filterByState(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function seeResume(User $user){
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUB_ADMIN;
    }

    public function seeOnlyOffice(User $user){
        return $user->role_id === Role::CASHIER || $user->role_id === Role::ACCOUNTINGS;
    }
}
