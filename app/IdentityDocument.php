<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\IdentityDocument
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IdentityDocument whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 */
class IdentityDocument extends Model
{
    public $timestamps = false;

    public function customers(){
        return $this->hasMany(Customer::class);
    }

}
