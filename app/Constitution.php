<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Constitution
 *
 * @property int $id
 * @property int $customer_id
 * @property int|null $company_id
 * @property float $capital_amount
 * @property string|null $reservation_number
 * @property int $reservation_service
 * @property int|null $partners_number
 * @property int|null $additional_managers
 * @property string|null $registration_charge_picture
 * @property string $impression_state
 * @property float|null $lawyer_fee
 * @property float|null $notary_fee
 * @property string|null $public_record
 * @property string|null $notarial_letter
 * @property string|null $directory
 * @property string|null $others
 * @property float|null $devolution
 * @property string|null $picture_1
 * @property string|null $picture_2
 * @property string|null $picture_3
 * @property string|null $picture_4
 * @property string|null $done_at
 * @property string|null $signed_at
 * @property string|null $reservation_expiration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereAdditionalManagers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCapitalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereDevolution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereDirectory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereDoneAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereImpressionState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereLawyerFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereNotarialLetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereNotaryFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereOthers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePartnersNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePicture4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution wherePublicRecord($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereRegistrationChargePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereReservationExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereReservationNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereReservationService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereSignedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property int $currency_id
 * @property float $charged_amount
 * @property-read \App\Company $company
 * @property-read \App\Currency $currency
 * @property-read \App\Customer $customer
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereChargedAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Constitution whereUserId($value)
 */
class Constitution extends Model
{
    const NOT_PRINTED = 1;
    const PRINTED_FOR_ONE = 2;
    const PRINTED_FOR_TWO = 3;

    const NOT_RESERVATION_SERVICE = 0;
    const FIRST_RESERVATION = 1;
    const SECOND_RESERVATION = 2;
    const THIRD_RESERVATION = 3;

    const NORMAL_SOCIAL_OBJECT = 1;
    const TWO_PAGES_SOCIAL_OBJECT = 2;

    protected $fillable = [
        'company_id', 'user_id',  'user_got_id', 'currency_id', 'charged_amount',
        'capital_amount', 'reservation_number', 'reservation_service', 'reservation_expiration',
        'partners_number', 'additional_managers', 'registration_charge_picture', 'impression_state',
        'lawyer_fee', 'notary_fee', 'public_record', 'notarial_letter','directory',
        'others', 'devolution', 'picture_1','picture_2', 'picture_3','picture_4',
        'done_at', 'signed_at', 'picked_at', 'delivered_at', 'delivered_person', 'vehicles', 'social_object'
    ];

    public function pathAttachment($image){
        return "/images/constitutions/".$image;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function userGot(){
        return $this->belongsTo(User::class, 'user_got_id');
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function vouchers(){
        return $this->hasMany(Voucher::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

}
