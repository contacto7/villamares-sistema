<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PaymentWay
 *
 * @property int $id
 * @property string $name Depósito, efectivo en caja, Visanet
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentWay whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 */
class PaymentWay extends Model
{
    public $timestamps = false;

    public function cashFlows(){
        return $this->hasMany(CashFlow::class);
    }
}
