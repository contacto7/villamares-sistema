<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BankAccount
 *
 * @property int $id
 * @property string $titular
 * @property string $account_number
 * @property string $cci
 * @property string $bank
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereCci($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereTitular($value)
 * @mixin \Eloquent
 * @property int $currency_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BankAccount whereCurrencyId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 * @property-read \App\Currency $currency
 */
class BankAccount extends Model
{
    public $timestamps = false;

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    public function cashFlows(){
        return $this->hasMany(CashFlow::class);
    }
}
