<?php

namespace App\Providers;

use App\Accounting;
use App\Book;
use App\CashFlow;
use App\CashRegister;
use App\Company;
use App\Constitution;
use App\Customer;
use App\Extra;
use App\Policies\AccountingPolicy;
use App\Policies\BookPolicy;
use App\Policies\CashFlowPolicy;
use App\Policies\CashRegisterPolicy;
use App\Policies\CompanyPolicy;
use App\Policies\ConstitutionPolicy;
use App\Policies\CustomerPolicy;
use App\Policies\ExtraPolicy;
use App\Policies\UserPolicy;
use App\Policies\VoucherPolicy;
use App\User;
use App\Voucher;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    /*
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];
    */
    protected $policies = [
        Constitution::class => ConstitutionPolicy::class,
        CashRegister::class => CashRegisterPolicy::class,
        Voucher::class => VoucherPolicy::class,
        Customer::class => CustomerPolicy::class,
        Company::class => CompanyPolicy::class,
        CashFlow::class => CashFlowPolicy::class,
        Book::class => BookPolicy::class,
        Accounting::class => AccountingPolicy::class,
        Extra::class => ExtraPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
