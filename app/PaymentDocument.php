<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PaymentDocument
 *
 * @property int $id
 * @property string $name Recibo por honorarios, recibo de caja, factura
 * @property string $titular Mónica, Villamares
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentDocument whereTitular($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Accounting[] $accountings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 */
class PaymentDocument extends Model
{
    public $timestamps = false;

    public function vouchers(){
        return $this->hasMany(Voucher::class);
    }

    public function accountings(){
        return $this->hasMany(Accounting::class);
    }
}
