<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Voucher
 *
 * @property int $id
 * @property int $payment_method_id
 * @property int $user_registered_id
 * @property int|null $user_cancelled_id
 * @property float $amount_payed
 * @property float $amount_total
 * @property int $service_type_id
 * @property int $constitution_id
 * @property int $accounting_id
 * @property int $extra_id
 * @property string $emitted_at
 * @property string|null $cancelled_at
 * @property string|null $expired_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereAccountingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereAmountPayed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereAmountTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereCancelledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereConstitutionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereEmittedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereExtraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereServiceTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUserCancelledId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUserRegisteredId($value)
 * @mixin \Eloquent
 * @property string $number
 * @property string|null $description
 * @property int $payment_document_id
 * @property int|null $user_emitted_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher wherePaymentDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereUserEmittedId($value)
 * @property int $currency_id
 * @property-read \App\Accounting|null $accounting
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 * @property-read \App\Constitution|null $constitution
 * @property-read \App\Currency $currency
 * @property-read \App\Extra|null $extra
 * @property-read \App\PaymentDocument $paymentDocument
 * @property-read \App\ServiceType $serviceType
 * @property-read \App\User $userEmitted
 * @property-read \App\User $userRegistered
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereCurrencyId($value)
 */
class Voucher extends Model
{
    protected $fillable = [
        'number', 'description', 'associate_voucher', 'payment_document_id', 'user_emitted_id',
        'user_registered_id', 'currency_id', 'amount_payed',
        'amount_total', 'service_type_id',
        'accounting_id', 'constitution_id', 'extra_id',
        'emitted_at', 'expired_at', 'cancelled_at'
    ];

    public static function boot(){
        parent::boot();

        static::saving(function (Voucher $voucher){
            if(! \App::runningInConsole()){
                if($voucher->amount_total <= $voucher->amount_payed){
                    $voucher->cancelled_at = Carbon::now();
                }
            }
        });

        static::saved(function (Voucher $voucher){
            /*
            if(! \App::runningInConsole()){
                $payment_document_id = (int) $voucher->payment_document_id;

                $voucher_id = $voucher->id;

                $office = $voucher->userRegistered->office_id;
                $office = str_pad($office, 2, '0', STR_PAD_LEFT);

                $user = auth()->user()->id;

                $correlative_serie = "RC".$office."-00".$user."-";

                $correlative_number = str_pad($voucher_id, 6, '0', STR_PAD_LEFT);

                if($payment_document_id === 4){
                    $new_number = $correlative_serie.$correlative_number;
                    $voucher::whereId($voucher_id)->update(['number'=>$new_number]);
                }
            }*/

            $payment_document_id = (int) $voucher->payment_document_id;

            $voucher_id = $voucher->id;

            $office = $voucher->userRegistered->office_id;
            $office = str_pad($office, 2, '0', STR_PAD_LEFT);


            if( \App::runningInConsole()){
                $user = 1;
            }else{
                $user = auth()->user()->id;
            }

            $correlative_serie = "RC".$office."-00".$user."-";

            $correlative_number = str_pad($voucher_id, 6, '0', STR_PAD_LEFT);

            if($payment_document_id === 4){
                $new_number = $correlative_serie.$correlative_number;
                $voucher::whereId($voucher_id)->update(['number'=>$new_number]);
            }
        });

        static::updating(function (Voucher $voucher){
            if(! \App::runningInConsole()){
                if($voucher->amount_total <= $voucher->amount_payed){
                    $voucher->cancelled_at = Carbon::now();
                }
            }
        });

    }

    public function paymentDocument(){
        return $this->belongsTo(PaymentDocument::class);
    }

    public function userRegistered(){
        return $this->belongsTo(User::class, 'user_registered_id');
    }

    public function userEmitted(){
        return $this->belongsTo(User::class, 'user_emitted_id');
    }

    public function serviceType(){
        return $this->belongsTo(ServiceType::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    public function accounting(){
        return $this->belongsTo(Accounting::class);
    }

    public function constitution(){
        return $this->belongsTo(Constitution::class);
    }

    public function extra(){
        return $this->belongsTo(Extra::class);
    }

    public function cashFlows(){
        return $this->hasMany(CashFlow::class);
    }

    public static function  totalDebt($currency_id,$payment_document_id){
        $total_debt = Voucher::where('emitted_at','<', Carbon::now('America/Lima'))
            ->where('currency_id',$currency_id)
            ->where('payment_document_id',$payment_document_id)
            ->whereColumn('amount_payed','<','amount_total')
            ->select(\DB::raw('sum(amount_total - amount_payed) as total_debt'))
            ->first();

        return $total_debt->total_debt;
    }

}
