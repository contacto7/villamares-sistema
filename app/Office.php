<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Office
 *
 * @property int $id
 * @property string $name nombre oficina del usuario
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Office whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 */
class Office extends Model
{
    const SJL = 1;
    const MIRAFLORES = 2;

    public $timestamps = false;

    public function users(){
        return $this->hasMany(User::class);
    }

    public function customers(){
        return $this->hasMany(Customer::class);
    }

    public function companies(){
        return $this->hasMany(Company::class);
    }

}
