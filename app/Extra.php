<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Extra
 *
 * @property int $id
 * @property int $customer_id
 * @property int|null $company_id
 * @property string $name
 * @property string $description
 * @property int $folios
 * @property string $done_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereDoneAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereFolios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $service_type_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereServiceTypeId($value)
 * @property int $user_id
 * @property int $currency_id
 * @property float $charged_amount
 * @property-read \App\Company|null $company
 * @property-read \App\Currency $currency
 * @property-read \App\Customer|null $customer
 * @property-read \App\ServiceType $serviceType
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereChargedAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extra whereUserId($value)
 */
class Extra extends Model
{
    protected $fillable = [
        'user_id', 'user_got_id', 'service_type_id', 'customer_id', 'company_id', 'currency_id',
        'charged_amount', 'description', 'folios', 'done_at'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function userGot(){
        return $this->belongsTo(User::class, 'user_got_id');
    }

    public function serviceType(){
        return $this->belongsTo(ServiceType::class);
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function vouchers(){
        return $this->hasMany(Voucher::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }
}
