<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Currency
 *
 * @property int $id
 * @property string $name
 * @property string $symbol
 * @property string $currency_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Currency whereSymbol($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Accounting[] $accountings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankAccount[] $bankAccounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CashFlow[] $cashFlows
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Constitution[] $constitutions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extra[] $extras
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 */
class Currency extends Model
{

    public $timestamps = false;

    public function cashFlows(){
        return $this->hasMany(CashFlow::class);
    }

    public function vouchers(){
        return $this->hasMany(Voucher::class);
    }

    public function bankAccounts(){
        return $this->hasMany(BankAccount::class);
    }

    public function accountings(){
        return $this->hasMany(Accounting::class);
    }

    public function constitutions(){
        return $this->hasMany(Constitution::class);
    }

    public function extras(){
        return $this->hasMany(Extra::class);
    }
}
