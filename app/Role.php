<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Role
 *
 * @property int $id
 * @property string $name nombre del rol del usuario
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 */
class Role extends Model
{
    const ADMIN = 1;
    const SUB_ADMIN = 2;
    const CENTRAL_CASHIER = 3;
    const CASHIER = 4;
    const CONSTITUTIONS = 5;
    const ACCOUNTINGS = 6;

    public $timestamps = false;

    public function users(){
        return $this->hasMany(User::class);
    }


}
