<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Accounting
 *
 * @property int $id
 * @property int $company_id
 * @property float $monthly_revenue
 * @property int $monthly_vouchers
 * @property int $staff_payroll
 * @property int $currency_id
 * @property float $first_rate
 * @property float $normal_rate
 * @property string $state
 * @property string $started_at
 * @property string $terminated_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereFirstRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereMonthlyRevenue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereMonthlyVouchers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereNormalRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereStaffPayroll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereTerminatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property int $payment_document_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Book[] $books
 * @property-read \App\Company $company
 * @property-read \App\Currency $currency
 * @property-read \App\PaymentDocument $paymentDocument
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Voucher[] $vouchers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting wherePaymentDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Accounting whereUserId($value)
 */
class Accounting extends Model
{
    const RUNNING = 1;
    const TERMINATED = 2;
    const WITHOUT_ELECTRONIC_BOOKS= 1;
    const WITH_ELECTRONIC_BOOKS  = 2;

    protected $fillable = [
        'user_id', 'worker_user_id', 'supervisor_user_id', 'company_id',
        'monthly_revenue', 'monthly_vouchers','staff_payroll',
        'currency_id', 'payment_document_id', 'first_rate',
        'normal_rate', 'state', 'started_at', 'terminated_at',
        'electronic_books', 'electronic_books_date', 'electronic_books_details',
        'delivery_charge'
    ];

    public static function boot(){
        parent::boot();

        static::saving(function (Accounting $accounting){
            if(! \App::runningInConsole()){
                $state = (int) $accounting->state;
                if($state === self::TERMINATED ){
                    $accounting->terminated_at = Carbon::now();
                }
            }
        });

        static::updating(function (Accounting $accounting){
            if(! \App::runningInConsole()){
                $state = (int) $accounting->state;
                if($state === self::TERMINATED ){
                    $accounting->terminated_at = Carbon::now();
                }
            }
        });

    }

    public function pathAttachment($image){
        return "/images/accountings/".$image;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function worker(){
        return $this->belongsTo(User::class, 'worker_user_id');
    }

    public function supervisor(){
        return $this->belongsTo(User::class, 'supervisor_user_id');
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    public function paymentDocument(){
        return $this->belongsTo(PaymentDocument::class);
    }

    public function vouchers(){
        return $this->hasMany(Voucher::class);
    }

    public function books(){
        return $this->hasMany(Book::class);
    }

}
