<?php

use Faker\Generator as Faker;

$factory->define(App\Extra::class, function (Faker $faker) {
    return [
        'user_id'=>\App\User::all()->random()->id,
        'user_got_id'=>\App\User::all()->random()->id,
        'service_type_id'=>$faker->numberBetween(3,6),
        'customer_id'=>\App\Customer::all()->random()->id,
        'company_id'=>\App\Company::all()->random()->id,
        'currency_id'=>\App\Currency::all()->random()->id,
        'charged_amount'=>$faker->randomFloat(2,850,4000),
        'description'=>$faker->sentence,
        'folios' => $faker->randomElement([25,50,100,150,200,250,300,350,400,450,500]),
        'done_at'=>$faker->dateTime('now'),
    ];
});
