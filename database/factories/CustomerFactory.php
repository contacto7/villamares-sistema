<?php

use Faker\Generator as Faker;

$factory->define(App\Customer::class, function (Faker $faker) {
    $name = $faker->name;
    $last_name =  $faker->lastName;
    $birthday = \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '-60 years', $endDate = '-25 years')->getTimeStamp()) ;

    return [
        'name' => $name,
        'last_name' => $last_name,
        'slug' => str_slug($name." ".$last_name,'-'),
        'email' => $faker->unique()->safeEmail,
        'cellphone' => $faker->phoneNumber,
        'birthday' => $birthday,
        'identity_document_id'=>\App\IdentityDocument::all()->random()->id,
        'document_number'=> $faker->bothify('??########??'),
        'office_id'=>\App\Office::all()->random()->id,
        'user_id'=>\App\User::all()->random()->id,
        'type'=>$faker->numberBetween(1,2),
        'acquisition_way'=>$faker->numberBetween(1,5),
        'other_acquisition_way'=>$faker->sentence,
        'prospect_type'=>$faker->numberBetween(1,3)
    ];
});
