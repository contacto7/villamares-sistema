<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'folios' => $faker->randomElement([25,50,100,150,200,250,300,350,400,450,500]),
        'notary' => $faker->name,
        'legalization' => $faker->dateTime('now'),
        'picture' => $faker->image(storage_path() . '/app/public/books',200,200, 'business', false),
        'book_type_id' => $faker->numberBetween(3,12),
        'accounting_id' => \App\Accounting::all()->random()->id,
    ];
});
