<?php

use Faker\Generator as Faker;

$factory->define(App\CashFlow::class, function (Faker $faker) {
        return [
        'description'=>$faker->sentence,
        'operation_type'=>$faker->numberBetween(1,6),
        'state'=>null,
        'currency_id'=>\App\Currency::all()->random()->id,
        'amount_movement'=>$faker->randomFloat(2,50,2500),
        'done_at'=>$faker->dateTime('now'),
        'deposited_at'=>$faker->dateTime('now'),
        'voucher_id'=>null,
        'user_registered_id'=>\App\User::all()->random()->id,
        'user_transfer_id'=>null,
        'bank_account_id'=>\App\BankAccount::all()->random()->id,
        'payment_way_id'=>\App\PaymentWay::all()->random()->id,
        'cash_flow_id'=>null,

        'number_voucher_associated'=>null,
        'picture_voucher'=>null,

    ];
});
