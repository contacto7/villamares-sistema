<?php

use Faker\Generator as Faker;

$factory->define(App\Voucher::class, function (Faker $faker) {
    $amount_payed =$faker->randomFloat(2,150,2000);
    $amount_total =$faker->numberBetween($amount_payed, 2000);


    $emitted_at= \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '-5 years', $endDate = '-1 week')->getTimeStamp()) ;
    $cancelled_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $emitted_at)->addDays( $faker->numberBetween( 1, 30 ) );
    $expired_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $emitted_at)->addDays( 30 );


    return [
        'number'=>$faker->numberBetween(1000000,9999999),
        'description'=>$faker->sentence,
        'associate_voucher'=>$faker->numberBetween(1000000,9999999),
        'payment_document_id'=>\App\PaymentDocument::all()->random()->id,
        'user_registered_id'=>\App\User::all()->random()->id,
        'user_emitted_id'=>\App\User::all()->random()->id,
        'currency_id'=>\App\Currency::all()->random()->id,
        'amount_payed'=>$amount_payed,
        'amount_total'=>$amount_total,

        'service_type_id'=>\App\ServiceType::all()->random()->id,

        'accounting_id'=>null,
        'constitution_id'=>null,
        'extra_id'=>null,

        'emitted_at'=>$emitted_at,
        'cancelled_at'=>$cancelled_at,
        'expired_at'=>$expired_at,

    ];
});
