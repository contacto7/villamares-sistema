<?php

use Faker\Generator as Faker;

$factory->define(App\Accounting::class, function (Faker $faker) {

    $started_at= \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '-5 years', $endDate = '-1 week')->getTimeStamp()) ;
    $terminated_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $started_at)->addMonths( $faker->numberBetween( 1, 12 ) );

    return [
        'user_id'=>\App\User::all()->random()->id,
        'worker_user_id'=>\App\User::all()->random()->id,
        'supervisor_user_id'=>\App\User::all()->random()->id,
        'company_id'=>\App\Company::all()->random()->id,
        'monthly_revenue'=>$faker->randomFloat(2,200,1000000),
        'monthly_vouchers'=>$faker->numberBetween(1,100000),
        'staff_payroll'=>$faker->numberBetween(1,1000),
        'currency_id'=>\App\Currency::all()->random()->id,
        'payment_document_id'=>\App\PaymentDocument::all()->random()->id,
        'first_rate'=>$faker->randomFloat(2,50,2500),
        'normal_rate'=>$faker->randomFloat(2,200,5000),
        'state'=>$faker->randomElement([
            \App\Accounting::RUNNING,
            \App\Accounting::TERMINATED
        ]),
        'electronic_books'=>$faker->randomElement([
            \App\Accounting::WITHOUT_ELECTRONIC_BOOKS,
            \App\Accounting::WITH_ELECTRONIC_BOOKS
        ]),
        'electronic_books_date'=>$started_at,
        'electronic_books_details'=>$faker->sentence,
        'delivery_charge' => $faker->image(storage_path() . '/app/public/accountings',200,200, 'business', false),
        'started_at'=>$started_at,
        'terminated_at'=>$terminated_at,

    ];
});
