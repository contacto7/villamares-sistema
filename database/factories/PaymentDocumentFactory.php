<?php

use Faker\Generator as Faker;

$factory->define(App\PaymentDocument::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'titular' => $faker->name
    ];
});
