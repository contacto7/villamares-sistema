<?php

use Faker\Generator as Faker;

$factory->define(App\CashRegister::class, function (Faker $faker) {
    $last_cash_count= \Carbon\Carbon::createFromTimestamp($faker->dateTimeBetween($startDate = '-7 days', $endDate = '-1 day')->getTimeStamp()) ;

    return [
        'initial_cash'=>$faker->randomFloat(2,50,2500),
        'last_cash_count'=>$last_cash_count,
        'user_id'=>\App\User::all()->random()->id,
    ];
});
