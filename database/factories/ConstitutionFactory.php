<?php

use Faker\Generator as Faker;

$factory->define(App\Constitution::class, function (Faker $faker) {
    return [
        'company_id'=>null,
        'user_id'=>\App\User::all()->random()->id,
        'user_got_id'=>\App\User::all()->random()->id,
        'currency_id'=>\App\Currency::all()->random()->id,
        'charged_amount'=>$faker->randomFloat(2,850,4000),
        'capital_amount'=>$faker->randomFloat(2,500,1000000),
        'reservation_number'=>$faker->numberBetween(1000000,9999999),
        'reservation_service'=>$faker->numberBetween(0,1),
        'reservation_expiration'=>$faker->dateTime(),
        'partners_number'=>$faker->numberBetween(1,20),
        'additional_managers'=>$faker->numberBetween(0,5),
        'registration_charge_picture' => $faker->image(storage_path() . '/app/public/constitutions',200,200, 'business', false),
        'impression_state'=>$faker->numberBetween(1,3),
        'lawyer_fee'=>$faker->randomFloat(2,150,250),
        'notary_fee'=>$faker->randomFloat(2,150,250),
        'public_record'=>$faker->numberBetween(1000000,9999999),
        'notarial_letter'=>$faker->numberBetween(1000000,9999999),
        'directory'=>$faker->numberBetween(0,20),
        'vehicles'=>$faker->numberBetween(0,10),
        'social_object'=>$faker->optional($weight = 0.8)->numberBetween(1,2),
        'others'=>$faker->text,
        'devolution'=>$faker->randomFloat(2,20,50),
        'picture_1' => $faker->image(storage_path() . '/app/public/constitutions',200,200, 'business', false),
        'picture_2' => $faker->image(storage_path() . '/app/public/constitutions',200,200, 'business', false),
        'picture_3' => $faker->image(storage_path() . '/app/public/constitutions',200,200, 'business', false),
        'picture_4' => $faker->image(storage_path() . '/app/public/constitutions',200,200, 'business', false),
        'done_at'=>$faker->dateTime('now'),
        'signed_at'=>$faker->dateTime('now'),
        'picked_at'=>$faker->dateTime('now'),
        'delivered_at'=>$faker->dateTime('now'),
        'delivered_person'=>$faker->name." ".$faker->lastName,
    ];
});
