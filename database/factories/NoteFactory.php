<?php

use Faker\Generator as Faker;

$factory->define(App\Note::class, function (Faker $faker) {
    return [
        'user_id'=>\App\User::all()->random()->id,
        'customer_id'=>\App\Customer::all()->random()->id,
        'description'=>$faker->sentence,
    ];
});
