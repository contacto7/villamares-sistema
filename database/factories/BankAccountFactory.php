<?php

use Faker\Generator as Faker;

$factory->define(App\BankAccount::class, function (Faker $faker) {
    $titular = $faker->name . " " . $faker->lastName;

    return [
        'titular' => $titular,
        'account_number' =>$faker->swiftBicNumber,
        'cci' => $faker->iban('PE'),
        'bank' => $faker->company,
        'currency_id'=>\App\Currency::all()->random()->id,
    ];
});
