<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    $name = $faker->name;

    return [
        'office_id'=>\App\Office::all()->random()->id,
        'customer_id'=>null,
        'user_id'=>\App\User::all()->random()->id,
        'name' => $name,
        'slug' => str_slug($name,'-'),
        'tax_number' => $faker->randomElement([
            $faker->numerify('20#########'),
            $faker->numerify('10#########'),
        ]),
        'address' => $faker->address,
        'manager_name' => $faker->name,
        'manager_last_name' => $faker->lastName,
        'manager_dni' => $faker->numerify('########'),
        'contact_name' => $faker->name,
        'contact_last_name' => $faker->lastName,
        'contact_email' => $faker->unique()->safeEmail,
        'contact_phone' => $faker->phoneNumber,
        'tax_regime_id'=>\App\TaxRegime::all()->random()->id,
        'company_turn_id'=>\App\CompanyTurn::all()->random()->id,
        'company_turn_other' => $faker->sentence
    ];
});
