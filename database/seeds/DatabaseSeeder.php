<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();

        Storage::deleteDirectory('users');
        Storage::deleteDirectory('books');
        Storage::deleteDirectory('accountings');
        Storage::deleteDirectory('constitutions');
        Storage::deleteDirectory('vouchers');

        Storage::makeDirectory('users');
        Storage::makeDirectory('books');
        Storage::makeDirectory('accountings');
        Storage::makeDirectory('constitutions');
        Storage::makeDirectory('vouchers');

        //////////ROLES
        factory(\App\Role::class, 1)->create([
            'name' => 'administrador'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'sub-administrador'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'cajero central'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'cajero'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'constituciones'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'contabilidad'
        ]);

        //////////OFFICES
        factory(\App\Office::class, 1)->create([
            'name' => 'San Juan de Lurigancho',
        ]);
        factory(\App\Office::class, 1)->create([
            'name' => 'Miraflores',
        ]);

        //////////USERS
        factory(\App\User::class, 1)->create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
        ])->each(function (\App\User $us) {
            factory(\App\CashRegister::class, 1)
                ->create([
                    'user_id'=>$us->id,
                ]);
        });
        factory(\App\User::class, 1)->create([
            'name' => 'Sub-admin',
            'email' => 'subadmin@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::SUB_ADMIN,
        ])->each(function (\App\User $us) {
            factory(\App\CashRegister::class, 1)
                ->create([
                    'user_id'=>$us->id,
                ]);
        });
        factory(\App\User::class, 1)->create([
            'name' => 'Cajero central',
            'email' => 'cajerocentral@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::CENTRAL_CASHIER,
        ])->each(function (\App\User $us) {
            factory(\App\CashRegister::class, 1)
                ->create([
                    'user_id'=>$us->id,
                ]);
        });
        factory(\App\User::class, 1)->create([
            'name' => 'Cajero',
            'email' => 'cajero@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::CASHIER,
        ])->each(function (\App\User $us) {
            factory(\App\CashRegister::class, 1)
                ->create([
                    'user_id'=>$us->id,
                ]);
        });
        factory(\App\User::class, 1)->create([
            'name' => 'Constituciones',
            'email' => 'constituciones@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::CONSTITUTIONS,
        ])->each(function (\App\User $us) {
            factory(\App\CashRegister::class, 1)
                ->create([
                    'user_id'=>$us->id,
                ]);
        });
        factory(\App\User::class, 1)->create([
            'name' => 'Contabilidad',
            'email' => 'contabilidad@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::ACCOUNTINGS,
        ])->each(function (\App\User $us) {
            factory(\App\CashRegister::class, 1)
                ->create([
                    'user_id'=>$us->id,
                ]);
        });

        //////////CURRENCIES
        factory(\App\Currency::class, 1)->create([
            'name' => 'Soles',
            'symbol' => 'S/',
            'currency_code' => 'PEN',
        ]);
        factory(\App\Currency::class, 1)->create([
            'name' => 'Dólares',
            'symbol' => '$',
            'currency_code' => 'USD',
        ]);

        //////////BANK ACCOUNT

        //CUENTAS VILLAMARES
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Villamares & Asociados S.A.C.',
            'account_number' => '194-36938850-0-67',
            'cci' => '002-194-136938850067-98',
            'bank' => 'BCP',
            'currency_id' => '1',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Villamares & Asociados S.A.C.',
            'account_number' => '194-37261009-1-82',
            'cci' => '002-194-137261009182-91',
            'bank' => 'BCP',
            'currency_id' => '2',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Villamares & Asociados S.A.C.',
            'account_number' => '0011-0117-0200797597',
            'cci' => '011-117-000200797597-98',
            'bank' => 'BBVA',
            'currency_id' => '1',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Villamares & Asociados S.A.C.',
            'account_number' => '0011-0117-0200797600',
            'cci' => '011-117-000200797600-90',
            'bank' => 'BBVA',
            'currency_id' => '2',
        ]);

        //CUENTAS MÓNICA
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Mónica Sanchez Villamares',
            'account_number' => '193-12171482-0-25',
            'cci' => '002-193-112171482025-18',
            'bank' => 'BCP',
            'currency_id' => '1',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Mónica Sanchez Villamares',
            'account_number' => '194-38943599-1-66',
            'cci' => '002-194-138943599166-92',
            'bank' => 'BCP',
            'currency_id' => '2',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Mónica Sanchez Villamares',
            'account_number' => '0011-0147-0200442199',
            'cci' => '011-147-000200442199-62',
            'bank' => 'BBVA',
            'currency_id' => '1',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Mónica Sanchez Villamares',
            'account_number' => '0011-0147-0200442202',
            'cci' => '011-147-000200442202-64',
            'bank' => 'BBVA',
            'currency_id' => '2',
        ]);

        //CUENTAS MARLENE
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Marlene Sanchez Villamares',
            'account_number' => '193-32256328-0-48',
            'cci' => '002-193-132256328048-13',
            'bank' => 'BCP',
            'currency_id' => '1',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Marlene Sanchez Villamares',
            'account_number' => '1073090907224',
            'cci' => '003-107-013090907224-06',
            'bank' => 'Interbank',
            'currency_id' => '1',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Marlene Sanchez Villamares',
            'account_number' => '0011-0117-0200983633',
            'cci' => '011-117-000200983633-95',
            'bank' => 'BBVA',
            'currency_id' => '1',
        ]);
        factory(\App\BankAccount::class, 1)->create([
            'titular' => 'Marlene Sanchez Villamares',
            'account_number' => '127-0312364',
            'cci' => '009-040-201270312364-79',
            'bank' => 'Scotiabank',
            'currency_id' => '1',
        ]);

        ////////PAYMENT DOCUMENTS

        factory(\App\PaymentDocument::class, 1)->create([
            'name' => 'Factura',
            'titular' => 'Villamares & Asociados S.A.C.',
        ]);

        factory(\App\PaymentDocument::class, 1)->create([
            'name' => 'Recibo por honorarios',
            'titular' => 'Mónica Sanchez Villamares',
        ]);

        factory(\App\PaymentDocument::class, 1)->create([
            'name' => 'Recibo por honorarios',
            'titular' => 'Marlene Sanchez Villamares',
        ]);

        factory(\App\PaymentDocument::class, 1)->create([
            'name' => 'Recibo de caja',
            'titular' => 'Villamares & Asociados S.A.C.',
        ]);

        ////////PAYMENT WAYS

        factory(\App\PaymentWay::class, 1)->create([
            'name' => 'Efectivo',
            'description' => 'Pago en efectivo',
        ]);

        factory(\App\PaymentWay::class, 1)->create([
            'name' => 'Depósito/ Retiro',
            'description' => 'Depósito en cuenta bancaria',
        ]);

        factory(\App\PaymentWay::class, 1)->create([
            'name' => 'Transferencia',
            'description' => 'Transferencia a cuenta bancaria',
        ]);

        factory(\App\PaymentWay::class, 1)->create([
            'name' => 'Visanet POS',
            'description' => 'Pago a través de Visanet POS',
        ]);

        factory(\App\PaymentWay::class, 1)->create([
            'name' => 'Pasarela de pago',
            'description' => 'Pago en línea a través de pasarela de pago',
        ]);

        ////////IDENTITY DOCUMENTS

        factory(\App\IdentityDocument::class, 1)->create([
            'name' => 'DNI',
            'description' => 'Documento nacional de identidad',
        ]);

        factory(\App\IdentityDocument::class, 1)->create([
            'name' => 'Pasaporte',
            'description' => 'Pasaporte',
        ]);

        factory(\App\IdentityDocument::class, 1)->create([
            'name' => 'PTP',
            'description' => 'Permiso temporal de permanencia',
        ]);

        factory(\App\IdentityDocument::class, 1)->create([
            'name' => 'CE',
            'description' => 'Carné de extranjería',
        ]);

        //CUSTOMERS

        factory(\App\Customer::class, 30)->create();

        //COMPANY TURNS

        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE CEREALES (EXCEPTO ARROZ), LEGUMBRES Y SEMILLAS OLEAGINOSAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE ARROZ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE HORTALIZAS Y MELONES, RAÍCES Y TUBÉRCULOS.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE CAÑA DE AZÚCAR',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE TABACO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE PLANTAS DE FIBRA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE OTRAS PLANTAS NO PERENNES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE UVA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE FRUTAS TROPICALES Y SUBTROPICALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE  CÍTRICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE FRUTAS DE PEPITA Y DE HUESO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE OTROS FRUTOS Y NUECES DE ÁRBOLES Y ARBUSTOS ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE FRUTOS OLEAGINOSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE PLANTAS CON LAS QUE SE PREPARAN BEBIDAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE ESPECIAS Y DE PLANTAS AROMÁTICAS, MEDICINALES Y FARMACEUTICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE OTRAS PLANTAS NO PERENNES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PROPAGACIÓN DE PLANTAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CRÍA DE GANADO BOVINO Y BÚFALOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CRÍA DE CABALLOS Y OTROS EQUINOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CRÍA DE CAMELLOS Y OTROS CAMÉLIDOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CRÍA DE OVEJAS Y CABRAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CRÍA DE CERDOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CRÍA DE AVES DE CORRAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CRÍA DE OTROS ANIMALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CULTIVO DE PRODUCTOS AGRÌCOLAS EN COMBINACIÒN CON LA CRÌA DE ANIMALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE APOYO A LA AGRICULTURA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE APOYO A LA GANADERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES POSCOSECHA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRATAMIENTO DE SEMILLAS PARA PROPAGACIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CAZA ORDINARIA Y MEDIANTE TRAMPAS Y ACTIVIDADES DE SERVICIOS CONEXAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'SILVICULTURA Y OTRAS ACTIVIDADES FORESTALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE MADERA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'RECOLECCIÓN DE PRODUCTOS FORESTALES DISTINTOS DE LA MADERA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'SERVICIOS DE APOYO A LA SILVICULTURA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PESCA MARÍTIMA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PESCA DE AGUA DULCE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACUICULTURA MARÍTIMA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACUICULTURA DE AGUA DULCE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE CARBÓN DE PIEDRA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE LIGNITO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE PETRÓLEO CRUDO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE GAS NATURAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE MINERALES DE HIERRO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE MINERALES DE URANIO Y TORIO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE OTROS MINERALES METALÍFEROS NO FERROSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE PIEDRA, ARENA Y ARCILLA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE MINERALES PARA LA FABRICACIÓN DE ABONOS Y PRODUCTOS QUÍMICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE TURBA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXTRACCIÓN DE SAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EXPLOTACIÓN DE OTRAS MINAS Y CANTERAS N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE APOYO PARA LA EXTRACCIÓN DE PETRÓLEO Y GAS NATURAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE APOYO PARA OTRAS ACTIVIDADES DE EXPLOTACIÓN DE MINAS Y CANTERAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÒN Y CONSERVACIÓN DE CARNE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÒN Y CONSERVACIÓN DE PESCADOS, CRUSTÁCEOS Y MOLUSCOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÒN Y CONSERVACIÓN DE FRUTAS,LEGUMBRES Y HORTALIZAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE ACEITES Y GRASAS DE ORIGEN VEGETAL Y ANIMAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE PRODUCTOS LÁCTEOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE PRODUCTOS DE MOLINERÍA.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE ALMIDONES Y PRODUCTOS DERIVADOS DEL ALMIDÓN.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE PRODUCTOS DE PANADERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE AZÚCAR',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE CACAO Y CHOCOLATE Y DE PRODUCTOS DE CONFITERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE MACARRONES, FIDEOS, ALCUZCUS Y PRODUCTOS FARINÁCEOS SIMILARES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE COMIDAS Y PLATOS PREPARADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE OTROS PRODUCTOS ALIMENTICIOS N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE PIENSOS PREPARADOS PARA ANIMALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'DESTILACIÓN, RECTIFICACIÓN Y MEZCLA DE BEBIDAS ALCOHÓLICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE VINOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE BEBIDAS MALTEADAS Y DE MALTA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE BEBIDAS NO ALCOHÓLICAS; PRODUCCIÓN DE AGUAS MINERALES Y OTRAS AGUAS EMBOTELLADAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ELABORACIÓN DE PRODUCTOS DE TABACO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PREPARACIÓN E HILATURA DE FIBRAS TEXTILES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TEJEDURA DE PRODUCTOS TEXTILES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACABADO DE PRODUCTOS TEXTILES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE TEJIDOS DE PUNTO Y GANCHILLO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ARTÍCULOS CONFECCIONADOS DE MATERIALES TEXTILES, EXCEPTO PRENDAS DE VESTIR',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE TAPICES Y ALFOMBRAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE CUERDAS, CORDELES, BRAMANTES Y REDES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS PRODUCTOS TEXTILES N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRENDAS DE VESTIR, EXCEPTO PRENDAS DE PIEL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ARTÍCULOS DE PIEL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ARTICULOS DE PUNTO Y GANCHILLO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CURTIDO Y ADOBO DE CUEROS; ADOBO Y TEÑIDO DE PIELES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MALETAS, BOLSOS DE MANO, Y ARTÍCULOS SIMILARES,Y DE ARTICULOS DE TALABARTERÍA Y GUARNICIONERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE CALZADO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ASERRADOS Y ACEPILLADURA DE MADERA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE HOJAS DE MADERA PARA ENCHAPADO Y TABLEROS A BASE DE MADERA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PARTES Y PIEZAS DE CARPINTERÍA PARA EDIFICIOS Y CONSTRUCCIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE RECIPIENTES DE MADERA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS PRODUCTOS DE MADERA; FABRICACIÓN DE ARTÍCULOS DE CORCHO, PAJA Y MATERIALES TRENZABLES.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PASTA DE MADERA, PAPEL Y CARTÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DEL PAPEL Y CARTÓN ONDULADO Y DE ENVASES DE PAPEL Y CARTÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS ARTÍCULOS DEL PAPEL Y CARTÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'IMPRESIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SERVICIOS RELACIONADAS CON LA IMPRESIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPRODUCCIÓN DE GRABACIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRODUCTOS DE HORNOS DE COQUE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRODUCTOS DE LA REFINACIÓN DEL PETRÓLEO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE SUSTANCIAS QUÍMICAS BÁSICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN ABONOS Y COMPUESTOS DE NITRÓGENO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PLÁSTICOS Y DE CAUCHO SINTÉTICO EN FORMAS PRIMARIAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PLAGUICIDAS Y OTROS PRODUCTOS QUÍMICOS DE USO AGROPECUARIO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PINTURAS, BARNICES Y PRODUCTOS DE REVESTIMIENTO SIMILARES, TINTAS DE IMPRENTA Y MASILLAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE JABONES Y DETERGENTES, PREPARADOS PARA LIMPIAR Y PULIR, PERFUMES Y PREPARADOS DE TOCADOR.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS PRODUCTOS QUÍMICOS N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE FIBRAS ARTIFICIALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRODUCTOS FARMACÉUTICOS, SUSTANCIAS QUÍMICAS MEDICINALES Y PRODUCTOS BOTÁNICOS DE USO FARMACÉUTICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE CUBIERTAS Y CÁMARAS DE CAUCHO; RECAUCHUTADO Y RENOVACIÓN DE CUBIERTAS DE CAUCHO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS PRODUCTOS DE CAUCHO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRODUCTOS DE PLÁSTICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE VIDRIO Y DE PRODUCTOS DE VIDRIO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRODUCTOS REFRACTARIOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MATERIALES DE CONSTRUCCIÓN DE ARCILLA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS PRODUCTOS DE PORCELANA Y DE CERÁMICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE CEMENTO, CAL Y YESO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ARTÍCULOS DE HORMIGÓN, DE CEMENTO Y DE YESO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CORTE, TALLA Y ACABADO DE LA PIEDRA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS PRODUCTOS MINERALES NO METÁLICOS N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'INDUSTRIAS BÁSICAS DE HIERRO Y ACERO ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRODUCTOS PRIMARIOS DE METALES PRECIOSOS Y OTROS METALES NO FERROSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FUNDICIÓN DE HIERRO Y ACERO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FUNDICIÓN DE METALES NO FERROSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PRODUCTOS METÁLICOS PARA USO ESTRUCTURAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE TANQUES, DEPÓSITOS Y RECIPIENTES DE METAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE LOS GENERADORES DEL VAPOR, EXCEPTO CALDERAS DE AGUA CALIENTE PARA CALEFACCIÓN CENTRAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ARMAS Y MUNICIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FORJA, PRENSADO, ESTAMPADO Y LAMINADO DE METALES; PULVIMETALURGIA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRATAMIENTO Y REVESTIMIENTO DE METALES; MAQUINADO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ARTÍCULOS DE CUCHILLERÍA, HERRAMIENTAS DE MANO Y ARTÍCULOS DE FERRETERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS PRODUCTOS ELABORADOS DE METAL N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE COMPONENTES Y TABLEROS ELECTRÓNICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ORDENADORES Y EQUIPO PERIFÉRICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE EQUIPOS DE COMUNICACIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE APARATOS ELECTRÓNICOS DE CONSUMO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE EQUIPO DE MEDICIÓN, PRUEBA, NAVEGACIÓN Y CONTROL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE RELOJES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE EQUIPO DE IRRADIACIÓN Y EQUIPO ELECTRÓNICO DE USO MÉDICO Y TERAPÉUTICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE INSTRUMENTOS ÓPTICOS Y EQUIPO FOTOGRÁFICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE SOPORTES MAGNÉTICOS Y ÓPTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MOTORES, GENERADORES Y TRANSFORMADORES ELÉCTRICOS Y APARATOS DE DISTRIBUCIÓN Y CONTROL DE LA ENERGÍA ELÉCTRICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PILAS, BATERÍAS Y ACUMULADORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE CABLES DE FIBRA ÓPTICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS HILOS Y CABLES ELÉCTRICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE DISPOSITIVOS DE CABLEADO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE EQUIPO ELÉCTRICO DE ILUMINACIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE APARATOS DE USO DOMÉSTICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS TIPOS DE EQUIPO ELÉCTRICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MOTORES Y TURBINAS, EXCEPTO MOTORES PARA AERONAVES, VEHÍCULOS AUTOMOTORES Y MOTOCICLETAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE EQUIPO DE PROPULSIÓN DE FLUIDOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE BOMBAS, COMPRESORES, GRIFOS Y VÁLVULAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE COJINETES, ENGRANAJES, TRENES DE ENGRANAJES Y PIEZAS DE TRANSMISIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE HORNOS, HOGARES Y QUEMADORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE EQUIPO DE ELEVACIÓN Y MANIPULACIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MAQUINARIA Y EQUIPO DE OFICINA (EXCEPTO ORDENADORES Y EQUIPO PERIFÉRICO)',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE HERRAMIENTAS DE MANO MOTORIZADAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS TIPOS DE MAQUINARIA DE USO GENERAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MAQUINARIA AGROPECUARIA Y FORESTAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MAQUINARIA PARA LA CONFORMACIÓN DE METALES Y DE MÁQUINAS HERRAMIENTA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MAQUINARIA METALÚRGICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MAQUINARIA PARA EXPLOTACIÓN DE MINAS Y CANTERAS Y PARA OBRAS DE CONSTRUCCIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MAQUINARIA PARA LA ELABORACIÓN DE ALIMENTOS, BEBIDAS Y TABACO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MAQUINARIA PARA LA ELABORACIÓN DE PRODUCTOS TEXTILES, PRENDAS DE VESTIR Y CUEROS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS TIPOS DE MAQUINARIA DE USO ESPECIAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE VEHÍCULOS AUTOMOTORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE CARROCERÍAS PARA VEHÍCULOS AUTOMOTORES; FABRICACIÓN DE REMOLQUES Y SEMIRREMOLQUES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE PARTES, PIEZAS Y ACCESORIOS PARA VEHÍCULOS DE AUTOMOTORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CONSTRUCCIÓN DE BUQUES Y ESTRUCTURAS FLOTANTES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CONSTRUCCIÓN DE EMBARCACIONES DE RECREO Y DEPORTE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE LOCOMOTORAS Y DE MATERIAL RODANTE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE AERONAVES Y  NAVES ESPACIALES Y MAQUINARIA CONEXA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE VEHÍCULOS MILITARES DE COMBATE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MOTOCICLETAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE BICICLETAS Y DE SILLONES DE RUEDAS PARA INVÁLIDOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE OTROS TIPOS DE EQUIPO DE TRANSPORTE N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE MUEBLES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE JOYAS Y ARTÍCULOS CONEXOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE BISUTERÍA Y ARTÍCULOS CONEXOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE INSTRUMENTOS DE MÚSICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE ARTÍCULOS DE DEPORTE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE JUEGOS Y JUGUETES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DE INSTRUMENTOS Y MATERIALES MÉDICOS Y ODONTOLÓGICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS INDUSTRIAS MANUFACTURERAS N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE PRODUCTOS ELABORADOS DE METAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE MAQUINARIA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE EQUIPO ELECTRÓNICO Y ÓPTICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE EQUIPO ELÉCTRICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE EQUIPO DE TRANSPORTE, EXCEPTO VEHÍCULOS AUTOMOTORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE OTROS TIPOS DE EQUIPO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'INSTALACIÓN DE MAQUINARIA Y EQUIPO INDUSTRIALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'GENERACIÓN, TRANSMISIÓN Y DISTRIBUCIÓN DE ENERGÍA ELÉCTRICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FABRICACIÓN DEL GAS; DISTRIBUCIÓN DE COMBUSTIBLES GASEOSOS POR TUBERÍAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'SUMINISTRO DE VAPOR Y AIRE ACONDICIONADO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CAPTACIÓN, TRATAMIENTO Y DISTRIBUCIÓN DE AGUA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EVACUACIÓN DE AGUAS RESIDUALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'RECOGIDA DE DESECHOS NO PELIGROSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'RECOGIDA DE DESECHOS PELIGROSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRATAMIENTO Y ELIMINACIÓN DE DESECHOS NO PELIGROSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRATAMIENTO Y ELIMINACIÓN DE DESECHOS PELIGROSOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'RECUPERACIÓN DE MATERIALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE DESCONTAMINACIÓN Y OTROS SERVICIOS DE GESTIÓN DE DESECHOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CONSTRUCCIÓN DE EDIFICIOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CONSTRUCCIÓN DE CARRETERAS Y LÍNEAS DE FERROCARRIL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CONSTRUCCIÓN DE PROYECTOS DE SERVICIO PÚBLICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CONSTRUCCIÓN DE OTRAS OBRAS DE INGENIERÍA CIVIL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'DEMOLICIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PREPARACIÓN DEL TERRENO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'INSTALACIONES ELÉCTRICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'INSTALACIONES DE FONTANERÍA, CALEFACCIÓN Y AIRE ACONDICIONADO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS INSTALACIONES PARA OBRAS DE CONSTRUCCIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TERMINACIÓN Y ACABADO DE EDIFICIOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES ESPECIALIZADAS DE LA CONSTRUCCIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA DE VEHÍCULOS AUTOMOTORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'MANTENIMIENTO Y REPARACIÓN DE VEHÍCULOS AUTOMOTORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTAS DE PARTES, PIEZAS Y ACCESORIOS PARA VEHÍCULOS AUTOMOTORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA, MANTENIMIENTO Y REPARACIÓN DE MOTOCICLETAS Y DE SUS PARTES, PIEZAS Y ACCESORIOS.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR A CAMBIO DE UNA RETRIBUCIÓN O POR CONTRATA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE MATERIAS PRIMAS AGROPECUARIAS Y ANIMALES VIVOS.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE ALIMENTOS, BEBIDAS Y TABACO.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE PRODUCTOS TEXTILES, PRENDAS DE VESTIR Y CALZADO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE OTROS ENSERES DOMÉSTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE ORDENADORES, EQUIPO PERIFÉRICO Y PROGRAMAS DE INFORMÁTICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE EQUIPO, PARTES Y PIEZAS ELECTRÓNICOS Y DE TELECOMUNICACIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE MAQUINARIA, EQUIPO Y MATERIALES AGROPECUARIOS ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE OTROS TIPOS DE MAQUINARIA Y EQUIPO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE COMBUSTIBLES SÓLIDOS, LÍQUIDOS Y GASEOSOS Y  PRODUCTOS CONEXOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE METALES Y MINERALES METALÍFEROS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE MATERIALES DE CONSTRUCCIÓN, ARTÍCULOS DE FERRETERÍA Y EQUIPO Y MATERIALES DE FONTANERÍA Y CALEFACCIÓN.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR DE DESPERDICIOS, DESECHOS, CHATARRA Y OTROS PRODUCTOS N.C.P ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MAYOR NO ESPECIALIZADA ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR EN COMERCIOS NO ESPECIALIZADOS CON PREDOMINIO DE LA VENTA DE ALIMENTOS, BEBIDAS O TABACO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE VENTA AL POR MENOR EN COMERCIOS NO ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE ALIMENTOS EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE BEBIDAS EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE PRODUCTOS DE TABACO EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE COMBUSTIBLES PARA VEHÍCULOS AUTOMOTORES EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE ORDENADORES, EQUIPO PERIFÉRICO, PROGRAMAS INFORMÁTICOS Y EQUIPO DE TELECOMUNICACIONES EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE EQUIPO DE SONIDO Y DE VÍDEO EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE PRODUCTOS TEXTILES EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE ARTÍCULOS DE FERRETERÍA, PINTURAS Y PRODUCTOS DE VIDRIO EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE TAPICES, ALFOMBRAS Y CUBRIMIENTOS PARA PAREDES Y PISOS EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE APARATOS ELÉCTRICOS DE USO DOMÉSTICO,  MUEBLES, EQUIPO DE ILUMINACIÓN Y OTROS ENSERES DOMÉSTICOS EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE LIBROS, PERIÓDICOS Y ARTÍCULOS DE PAPELERÍA EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE GRABACIONES DE MÚSICA Y DE VÍDEO EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE EQUIPO DE DEPORTE EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE JUEGOS Y  JUGUETES EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE PRENDAS DE VESTIR, CALZADO Y ARTÍCULOS DE CUERO EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE PRODUCTOS FARMACÉUTICOS Y MEDICINALES, COSMÉTICOS Y ARTÍCULOS DE TOCADOR EN COMERCIOS ESPECIALIZADOS   ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE OTROS PRODUCTOS NUEVOS EN COMERCIOS ESPECIALIZADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE ARTÍCULOS DE SEGUNDA MANO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE ALIMENTOS, BEBIDAS Y TABACO EN PUESTOS DE VENTA Y MERCADOS ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE PRODUCTOS TEXTILES, PRENDAS DE VESTIR Y CALZADO EN PUESTOS DE VENTA Y MERCADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR DE OTROS PRODUCTOS EN PUESTOS DE VENTA Y MERCADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'VENTA AL POR MENOR POR CORREO Y POR INTERNET',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE VENTA AL POR MENOR NO REALIZADAS EN COMERCIOS, PUESTOS DE VENTA O MERCADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE INTERURBANO DE PASAJEROS POR FERROCARRIL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE CARGA POR FERROCARRIL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE URBANO Y SUBURBANO DE PASAJEROS POR VÍA TERRESTRE ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE TRANSPORTE POR VÍA TERRESTRE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE CARGA POR CARRETERA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE POR TUBERÍAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE PASAJEROS MARÍTIMO Y DE CABOTAJE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE CARGA MARÍTIMO Y DE CABOTAJE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE PASAJEROS POR VÍAS DE NAVEGACIÓN INTERIORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE CARGA, POR VÍAS DE NAVEGACIÓN INTERIORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE PASAJEROS POR VÍA AÉREA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSPORTE DE CARGA POR VÍA AÉREA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ALMACENAMIENTO Y DEPÓSITO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SERVICIOS VINCULADAS AL TRANSPORTE TERRESTRE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SERVICIOS VINCULADAS AL TRANSPORTE ACUÁTICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SERVICIOS VINCULADAS AL TRANSPORTE AÉREO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'MANIPULACIÓN DE CARGA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE APOYO AL TRANSPORTE',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES POSTALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE MENSAJERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ALOJAMIENTO PARA ESTANCIAS CORTAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE CAMPAMENTOS, PARQUES DE VEHÍCULOS RECREATIVOS Y PARQUES DE CARAVANAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE ALOJAMIENTO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE RESTAURANTES Y DE SERVICIO MÓVIL DE COMIDAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'SUMINISTRO DE COMIDAS POR ENCARGO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE SERVICIO DE COMIDAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SERVICIO DE BEBIDAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EDICIÓN DE LIBROS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EDICIÓN DE DIRECTORIOS Y LISTAS DE CORREO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EDICIÓN DE PERIÓDICOS, REVISTAS Y OTRAS PUBLICACIONES PERIÓDICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE EDICIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EDICIÓN DE PROGRAMAS DE INFORMÁTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE PRODUCCIÓN DE PELÍCULAS CINEMATOGRÁFICAS, VÍDEOS Y PROGRAMAS DE TELEVISIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE POSTPRODUCCIÓN DE PELÍCULAS CINEMATOGRÁFICAS, VÍDEOS Y PROGRAMAS DE TELEVISIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE DISTRIBUCIÓN DE PELÍCULAS CINEMATOGRÁFICAS, VÍDEOS Y PROGRAMAS DE TELEVISIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE EXHIBICIÓN DE PELÍCULAS CINEMATOGRÁFICAS Y CINTAS DE VÍDEO ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE GRABACIÓN DE SONIDO Y EDICIÓN DE MÚSICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'TRANSMISIONES DE RADIO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PROGRAMACIÓN Y TRANSMISIONES DE TELEVISIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE TELECOMUNICACIONES ALÁMBRICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE TELECOMUNICACIONES INALÁMBRICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE TELECOMUNICACIONES POR SATÉLITE.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE TELECOMUNICACIÓN.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PROGRAMACIÓN INFORMÁTICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CONSULTORÍA DE INFORMÁTICA Y DE GESTIÓN DE INSTALACIONES INFORMÁTICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE TECNOLOGÍA DE LA INFORMACIÓN Y DE SERVICIOS INFORMÁTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PROCESAMIENTO DE DATOS, HOSPEDAJE Y ACTIVIDADES CONEXAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PORTALES WEB',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE AGENCIAS DE NOTICIAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE SERVICIOS DE INFORMACIÓN N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'BANCA CENTRAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTROS TIPOS DE INTERMEDIACIÓN MONETARIA.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SOCIEDADES DE CARTERA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FONDOS Y SOCIEDADES DE INVERSIÓN Y ENTIDADES FINANCIERAS SIMILARES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ARRENDAMIENTO FINANCIERO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE CONCESIÓN DE CRÉDITO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE SERVICIOS FINANCIEROS, EXCEPTO LAS DE SEGUROS Y FONDOS DE PENSIONES, N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'SEGUROS DE VIDA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'SEGUROS GENERALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REASEGUROS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FONDOS DE PENSIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ADMINISTRACIÓN DE MERCADOS FINANCIEROS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'CORRETAJE DE VALORES Y DE CONTRATOS DE PRODUCTOS BÁSICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES AUXILIARES DE LAS ACTIVIDADES DE SERVICIOS FINANCIEROS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EVALUACIÓN DE RIESGOS Y DAÑOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE AGENTES Y CORREDORES DE SEGUROS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES AUXILIARES DE LAS ACTIVIDADES DE SEGUROS Y FONDOS DE PENSIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE GESTIÓN DE FONDOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES INMOBILIARIAS REALIZADAS CON BIENES PROPIOS O ARRENDADOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES INMOBILIARIAS REALIZADAS A CAMBIO DE UNA RETRIBUCIÓN O POR CONTRATA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES JURÍDICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE CONTABILIDAD, TENEDURÍA DE LIBROS Y AUDITORÍA; CONSULTORÍA FISCAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE OFICINAS CENTRALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE CONSULTORÍA DE GESTIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ARQUITECTURA E INGENIERÍA Y ACTIVIDADES CONEXAS DE CONSULTORÍA TÉCNICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ENSAYOS Y ANÁLISIS TÉCNICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'INVESTIGACIÓN Y DESARROLLO EXPERIMENTAL EN EL CAMPO DE LAS CIENCIAS NATURALES Y LA INGENIERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'INVESTIGACIÓN Y DESARROLLO EXPERIMENTAL EN EL CAMPO DE LAS CIENCIAS SOCIALES Y LAS HUMANIDADES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PUBLICIDAD',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ESTUDIOS DE MERCADO Y ENCUESTAS DE OPINIÓN PÚBLICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES ESPECIALIZADAS DE DISEÑO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE FOTOGRAFÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES PROFESIONALES, CIENTÍFICAS Y TÉCNICAS N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES VETERINARIAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ALQUILER Y ARRENDAMIENTO DE VEHÍCULOS AUTOMOTORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ALQUILER Y ARRENDAMIENTO DE EQUIPO RECREATIVO Y DEPORTIVO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ALQUILER DE CINTAS DE VÍDEO Y DISCOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ALQUILER Y ARRENDAMIENTO DE OTROS EFECTOS PERSONALES Y ENSERES DOMÉSTICOS ',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ALQUILER Y ARRENDAMIENTO DE OTROS TIPOS DE MAQUINARIA, EQUIPO Y BIENES TANGIBLES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ARRENDAMIENTO DE PROPIEDAD INTELECTUAL Y PRODUCTOS SIMILARES, EXCEPTO OBRAS PROTEGIDAS  POR DERECHOS DE AUTOR',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE AGENCIAS DE EMPLEO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE AGENCIAS DE EMPLEO TEMPORAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE DOTACIÓN DE RECURSOS HUMANOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE AGENCIAS DE VIAJES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE OPERADORES TURÍSTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTROS SERVICIOS DE RESERVAS Y ACTIVIDADES CONEXAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SEGURIDAD PRIVADA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SERVICIO DE SISTEMAS DE SEGURIDAD',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE INVESTIGACIÓN',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES COMBINADAS DE APOYO A INSTALACIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'LIMPIEZA GENERAL DE EDIFICIOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE LIMPIEZA DE EDIFICIOS E INSTALACIONES INDUSTRIALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE PAISAJISMO Y SERVICIOS DE MANTENIMIENTO CONEXOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES COMBINADAS DE SERVICIOS ADMINISTRATIVOS DE OFICINA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'FOTOCOPIADO, PREPARACIÓN DE DOCUMENTOS Y OTRAS ACTIVIDADES ESPECIALIZADAS DE APOYO DE OFICINA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE CENTROS DE LLAMADAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ORGANIZACIÓN DE CONVENCIONES Y EXPOSICIONES COMERCIALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE AGENCIAS DE COBRO Y AGENCIAS DE CALIFICACIÓN CREDITICIA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ENVASADO Y EMPAQUETADO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPRESAS N.C.P',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE LA ADMINISTRACIÓN PÚBLICA EN GENERAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REGULACIÓN DE LAS ACTIVIDADES DE ORGANISMOS QUE PRESTAN SERVICIOS SANITARIOS, EDUCATIVOS, CULTURALES Y OTROS SERVICIOS SOCIALES, EXCEPTO SERVICIOS DE SEGURIDAD SOCIAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REGULACIÓN Y FACILITACIÓN DE LA ACTIVIDAD ECONÓMICA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'RELACIONES EXTERIORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE DEFENSA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE MANTENIMIENTO DEL ORDEN PÚBLICO Y DE SEGURIDAD',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE PLANES DE SEGURIDAD SOCIAL DE AFILIACIÓN OBLIGATORIA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ENSEÑANZA PREESCOLAR Y PRIMARIA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ENSEÑANZA SECUNDARIA DE FORMACIÓN GENERAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ENSEÑANZA SECUNDARIA DE FORMACIÓN TÉCNICA Y PROFESIONAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ENSEÑANZA SUPERIOR',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'EDUCACIÓN DEPORTIVA Y RECREATIVA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ENSEÑANZA CULTURAL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTROS TIPOS DE ENSEÑANZA N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE APOYO A LA ENSEÑANZA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE HOSPITALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE MÉDICOS Y ODONTÓLOGOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE ATENCIÓN DE LA SALUD HUMANA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ATENCIÓN DE ENFERMERÍA EN INSTITUCIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ATENCIÓN EN INSTITUCIONES PARA PERSONAS CON RETRASO MENTAL, ENFERMOS MENTALES Y TOXICÓMANOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ATENCIÓN EN INSTITUCIONES PARA PERSONAS DE EDAD PERSONAS CON DISCAPACIDAD',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE ATENCIÓN EN INSTITUCIONES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ASISTENCIA SOCIAL SIN ALOJAMIENTO PARA PERSONAS DE EDAD Y PERSONAS CON DISCAPACIDAD',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE ASISTENCIA SOCIAL SIN ALOJAMIENTO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES CREATIVAS, ARTÍSTICAS Y DE ENTRETENIMIENTO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE MUSEOS Y GESTIÓN DE LUGARES Y EDIFICIOS HISTÓRICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE JARDINES BOTÁNICOS Y ZOOLÓGICOS Y RESERVAS NATURALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE JUEGOS DE AZAR Y APUESTAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'GESTIÓN DE INSTALACIONES DEPORTIVAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE CLUBES DEPORTIVOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DEPORTIVAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE PARQUES DE ATRACCIONES Y PARQUES TEMÁTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE ESPARCIMIENTO Y RECREATIVAS N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ASOCIACIONES EMPRESARIALES Y DE EMPLEADORES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ASOCIACIONES PROFESIONALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE SINDICATOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ORGANIZACIONES POLÍTICAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE OTRAS ASOCIACIONES N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE ORDENADORES Y EQUIPO PERIFÉRICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE EQUIPOS COMUNICACIONALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE APARATOS ELECTRÓNICOS DE CONSUMO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE APARATOS DE USO DOMÉSTICO Y EQUIPO DOMÉSTICO Y DE JARDINERÍA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE CALZADO Y ARTÍCULOS DE CUERO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE MUEBLES Y ACCESORIOS DOMÉSTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'REPARACIÓN DE OTROS EFECTOS PERSONALES Y ENSERES DOMÉSTICOS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'LAVADO Y LIMPIEZA, INCLUIDA LA LIMPIEZA EN SECO, DE PRODUCTOS TEXTILES Y DE PIEL',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'PELUQUERÍA Y OTROS TRATAMIENTOS DE BELLEZA',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'POMPAS FÚNEBRES Y ACTIVIDADES CONEXAS',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRAS ACTIVIDADES DE SERVICIOS PERSONALES N.C.P.',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE LOS HOGARES COMO EMPLEADORES DE PERSONAL DOMÉSTICO',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'ACTIVIDADES DE ORGANIZACIONES Y ÓRGANOS EXTRATERRITORIALES',]);
        factory(\App\CompanyTurn::class, 1)->create(['name' => 'OTRO',]);

        ////////TAX REGIMES

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'NRUS',
            'description' => 'Nuevo Régimen Único Simplificado',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RER',
            'description' => 'Régimen Especial de Impuesto a la Renta',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RMT < 300',
            'description' => 'Régimen MYPE Tributario, ingresos menores a 300 UIT',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RMT < 500',
            'description' => 'Régimen MYPE Tributario, ingresos menores a 500 UIT',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RMT < 1700',
            'description' => 'Régimen MYPE Tributario, ingresos menores a 1700 UIT',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RG < 300',
            'description' => 'Régimen General, ingresos menores a 300 UIT',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RG < 500',
            'description' => 'Régimen General, ingresos menores a 500 UIT',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RG < 1700',
            'description' => 'Régimen General, ingresos menores a 1700 UIT',
        ]);

        factory(\App\TaxRegime::class, 1)->create([
            'name' => 'RG > 1700',
            'description' => 'Régimen General, ingresos mayores a 1700 UIT',
        ]);

        ////////BOOK TYPES

        factory(\App\BookType::class, 1)->create([
            'name' => 'Registro de Compras',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Registro de Ventas',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Libro Diario Simplificado',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Libro Diario',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Libro Mayor',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Libro de Inventario y Balances',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Libro de Retenciones',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Registro de Activos Fijos',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Registro de Costos',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Registro de Inventario Permanente en Unidades Físicas',
        ]);

        factory(\App\BookType::class, 1)->create([
            'name' => 'Registro de Inventario Permanente Valorizado',
        ]);

        ////////CUSTOMERS & COMPANIES & ACCOUNTING & BOOKS

        //COMPANIES WITH ACCOUNTINGS RUNNING, BOOKS CREATED FOR EACH ACCOUTING
        factory(\App\Customer::class, 20)
            ->create()
            ->each(function (\App\Customer $c){
                factory(\App\Company::class, 1)
                    ->create(['customer_id' => $c->id])
                    ->each(function (\App\Company $co){
                        factory(\App\Accounting::class, 1)
                            ->create([
                                'company_id' => $co->id,
                                'state' => 1,
                                'terminated_at' => null,
                            ])
                            ->each(function (\App\Accounting $ac) {
                                factory(\App\Book::class, 1)
                                    ->create(['book_type_id' => 1,'accounting_id' => $ac->id,]);

                                factory(\App\Book::class, 1)
                                    ->create(['book_type_id' => 2,'accounting_id' => $ac->id,]);
                            });
                    });
                });

        //COMPANIES WITH ACCOUNTINGS ENDED, BOOKS CREATED FOR EACH ACCOUTING
        factory(\App\Customer::class, 5)
            ->create()
            ->each(function (\App\Customer $c){
                factory(\App\Company::class, 1)
                    ->create(['customer_id' => $c->id])
                    ->each(function (\App\Company $co){
                        factory(\App\Accounting::class, 1)
                            ->create([
                                'company_id' => $co->id,
                                'state' => 2,
                            ])
                            ->each(function (\App\Accounting $ac) {
                                factory(\App\Book::class, 1)->create(['book_type_id' => 1,'accounting_id' => $ac->id,]);
                                factory(\App\Book::class, 1)->create(['book_type_id' => 2,'accounting_id' => $ac->id,]);
                                });
                    });
                });

        ////////BOOKS

        //CREATING BOOKS FOR ACCOUNTINGS, TAKING CARE OF UNIQUE VALUES
        for ($i = 1; $i <= 10; $i++) {
            try {
                factory(\App\Book::class, 1)->create([]);
            } catch (Exception $e) {

            }
        }

        ////////SERVICE TYPES

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Servicio Contable',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Constitución de Empresas',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Asesoria Tributaria',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Asesoría al Inversionista Extranjero',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Reconstrucción de la información contable',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Servicio de capacitación',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Cambio de socio',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Cambio de gerente',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Cambio de socio y gerente',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Aumento de capital',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Aumento de capital, socio y gerente',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Legalización de libros',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Transformación de empresa',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Ampliación del objeto social',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Testimonio original',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Legalización de documentos',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Registro OSCE',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Registro REMYPE',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Registro INDECOPI',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Actualización contable',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Asesoría tributaria',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Asesoría en fiscalizaciones SUNAT',
        ]);

        factory(\App\ServiceType::class, 1)->create([
            'name' => 'Admiinstración de planilla',
        ]);

        ////////EXTRAS

        factory(\App\Extra::class, 10)->create([]);

        ////////CONSTITUTIONS
        for ($i = 1; $i <= 20; $i++) {
            factory(\App\Constitution::class, 1)
                ->create([
                    'company_id' => $i
                ]);
        }

        ////////VOUCHERS


        //VOUCHERS W CASHFLOW

        for ($i = 1; $i <= 20; $i++) {
            $service_type = \App\ServiceType::all()->random()->id;

            $amount_payed =$faker->randomFloat(2,150,2000);
            $amount_total =$faker->numberBetween($amount_payed, 2000);

            switch ($service_type) {
                case 1:
                    factory(\App\Voucher::class, 1)
                    ->create([
                        'service_type_id' => $service_type,
                        'accounting_id' => \App\Accounting::all()->random()->id,
                        'amount_payed'=>$amount_payed,
                        'amount_total'=>$amount_total,
                        'payment_document_id' => 1,
                    ])
                    ->each(function (\App\Voucher $vo) {
                        factory(\App\CashFlow::class, 1)
                            ->create([
                                'operation_type'=>1,
                                'amount_movement'=>$vo->amount_payed,
                                'voucher_id' => $vo->id,
                             ]);
                        });
                    break;
                case 2:
                    factory(\App\Voucher::class, 1)
                    ->create([
                        'service_type_id' => $service_type,
                        'constitution_id' => \App\Constitution::all()->random()->id,
                        'amount_payed'=>$amount_payed,
                        'amount_total'=>$amount_total,
                        'payment_document_id' => 2,
                    ])
                    ->each(function (\App\Voucher $vo) {
                        factory(\App\CashFlow::class, 1)
                            ->create([
                                'operation_type'=>1,
                                'amount_movement'=>$vo->amount_payed,
                                'voucher_id' => $vo->id,
                            ]);
                        });
                    break;
                default:
                    factory(\App\Voucher::class, 1)
                    ->create([
                        'service_type_id' => $service_type,
                        'extra_id' => \App\Extra::all()->random()->id,
                        'amount_payed'=>$amount_payed,
                        'amount_total'=>$amount_total,
                    ])
                        ->each(function (\App\Voucher $vo) {
                            factory(\App\CashFlow::class, 1)
                                ->create([
                                    'operation_type'=>1,
                                    'amount_movement'=>$vo->amount_payed,
                                    'voucher_id' => $vo->id,
                                ]);
                        });
            }

        }

        //////CASH FLOW

        //   OUTFLOW

        factory(\App\CashFlow::class, 10)->create([
            'operation_type'=>2,
            'number_voucher_associated'=>$faker->numberBetween(1000000,9999999),
            'picture_voucher' => $faker->image(storage_path() . '/app/public/vouchers',200,200, 'business', false),

        ]);

        //  INTERN TRANSFER OUT

        factory(\App\CashFlow::class, 5)
            ->create([
                'operation_type'=>3,
                'state'=>3,
                'user_transfer_id'=>\App\User::all()->random()->id,
            ])->each(function (\App\CashFlow $ca) {
                factory(\App\CashFlow::class, 1)
                    ->create([
                        'operation_type'=>4,
                        'amount_movement'=>$ca->amount_movement,
                        'user_transfer_id'=>$ca->user_transfer_id,
                        'state'=>3,
                    ]);
            });

        factory(\App\CashFlow::class, 4)->create([
            'operation_type'=>3,
            'state'=>2,
            'user_transfer_id'=>\App\User::all()->random()->id,
        ])->each(function (\App\CashFlow $ca) {
            factory(\App\CashFlow::class, 1)
                ->create([
                    'operation_type'=>4,
                    'amount_movement'=>$ca->amount_movement,
                    'user_transfer_id'=>$ca->user_transfer_id,
                    'state'=>2,
                ]);
        });

        factory(\App\CashFlow::class, 2)->create([
            'operation_type'=>3,
            'state'=>1,
            'user_transfer_id'=>\App\User::all()->random()->id,
        ])->each(function (\App\CashFlow $ca) {
            factory(\App\CashFlow::class, 1)
                ->create([
                    'operation_type'=>4,
                    'amount_movement'=>$ca->amount_movement,
                    'user_transfer_id'=>$ca->user_transfer_id,
                    'state'=>1,
                ]);
        });

        ////////NOTES

        factory(\App\Note::class, 50)->create([]);


    }
}
