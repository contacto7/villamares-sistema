<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('slug');
            $table->string('email')->nullable();
            $table->string('cellphone');
            $table->dateTime('birthday')->nullable();
            $table->unsignedInteger('identity_document_id');
            $table->foreign('identity_document_id')->references('id')->on('identity_documents');
            $table->string('document_number')->unique();
            $table->unsignedInteger('office_id')->default(\App\Office::MIRAFLORES);
            $table->foreign('office_id')->references('id')->on('offices');
            $table->unsignedInteger('user_id')->comment('usuario que registró');
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('type', [
                \App\Customer::NORMAL,
                \App\Customer::PROBLEMATIC
            ])->default(\App\Customer::NORMAL);
            $table->enum('acquisition_way', [
                \App\Customer::FACEBOOK,
                \App\Customer::INSTAGRAM,
                \App\Customer::GOOGLE_ADS,
                \App\Customer::REFERER,
                \App\Customer::OTHER_ACQUISITION_WAY,
            ])->default(\App\Customer::OTHER_ACQUISITION_WAY);
            $table->string('other_acquisition_way')->nullable();
            $table->enum('prospect_type', [
                \App\Customer::CONSTITUTION_PROSPECT,
                \App\Customer::ACCOUNTING_PROSPECT,
                \App\Customer::CUSTOMER,
            ])->default(\App\Customer::CUSTOMER);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
