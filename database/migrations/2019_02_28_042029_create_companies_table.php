<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('office_id')->default(\App\Office::MIRAFLORES);
            $table->foreign('office_id')->references('id')->on('offices');
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('name');
            $table->string('slug');
            $table->string('tax_number')->unique();
            $table->string('address');
            $table->string('manager_name');
            $table->string('manager_last_name');
            $table->string('manager_dni');
            $table->string('contact_name');
            $table->string('contact_last_name');
            $table->string('contact_email');
            $table->string('contact_phone');
            $table->unsignedInteger('tax_regime_id');
            $table->foreign('tax_regime_id')->references('id')->on('tax_regimes');
            $table->unsignedInteger('company_turn_id');
            $table->foreign('company_turn_id')->references('id')->on('company_turns');
            $table->string('company_turn_other');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
