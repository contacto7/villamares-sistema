<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_flows', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->enum('operation_type', [
                \App\CashFlow::INFLOW,
                \App\CashFlow::OUTFLOW,
                \App\CashFlow::INTERN_TRANSFER_IN,
                \App\CashFlow::INTERN_TRANSFER_OUT,
                \App\CashFlow::BANK_TO_CASH,
                \App\CashFlow::CASH_TO_BANK
            ]);
            $table->enum('state', [
                \App\CashFlow::WAITING,
                \App\CashFlow::REJECTED,
                \App\CashFlow::ACCEPTED,
            ])->nullable()->default(\App\CashFlow::WAITING);
            $table->unsignedInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->float('amount_movement',10,2);
            $table->dateTime('done_at');
            $table->dateTime('deposited_at')->nullable();
            $table->unsignedInteger('voucher_id')->nullable();
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->unsignedInteger('user_registered_id');
            $table->foreign('user_registered_id')->references('id')->on('users');
            $table->unsignedInteger('user_transfer_id')->nullable();
            $table->foreign('user_transfer_id')->references('id')->on('users');
            $table->unsignedInteger('bank_account_id')->nullable();
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->unsignedInteger('payment_way_id');
            $table->foreign('payment_way_id')->references('id')->on('payment_ways');
            $table->unsignedInteger('cash_flow_id')->nullable();

            //SI ES QUE ES EGRESO
            $table->string('number_voucher_associated')
                ->nullable()
                ->comment('Ingresar número de voucher de EGRESO');
            $table->string('picture_voucher')
                ->nullable()
                ->comment('Foto del voucher de EGRESO');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_flows');
    }
}
