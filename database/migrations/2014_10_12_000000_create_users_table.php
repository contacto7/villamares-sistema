<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table){
            $table->increments('id');
            $table->string('name')->comment('nombre del rol del usuario');
            $table->text('description');
        });
        Schema::create('offices', function (Blueprint $table){
            $table->increments('id');
            $table->string('name')->comment('nombre oficina del usuario');
            $table->text('description');
        });
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('role_id')->default(\App\Role::CASHIER);
            $table->foreign('role_id')->references('id')->on('roles');
            $table->unsignedInteger('office_id')->default(\App\Office::MIRAFLORES);
            $table->foreign('office_id')->references('id')->on('offices');
            $table->string('name');
            $table->string('last_name');
            $table->string('slug');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('picture')->nullable()->default('user.png');
            $table->enum('state', [
                \App\User::ACTIVE,
                \App\User::INACTIVE
            ])->default(\App\User::ACTIVE);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('offices');
    }
}
