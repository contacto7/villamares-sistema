<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folios');
            $table->string('notary')->nullable();
            $table->dateTime('legalization')->nullable();
            $table->string('picture')->nullable();
            $table->unsignedInteger('book_type_id');
            $table->foreign('book_type_id')->references('id')->on('book_types');
            $table->unsignedInteger('accounting_id');
            $table->foreign('accounting_id')->references('id')->on('accountings');

            $table->timestamps();

            $table->unique(['book_type_id', 'accounting_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
