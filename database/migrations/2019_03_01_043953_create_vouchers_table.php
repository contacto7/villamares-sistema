<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->unique();
            $table->text('description')->nullable();
            $table->text('associate_voucher')->nullable();
            $table->unsignedInteger('payment_document_id');
            $table->foreign('payment_document_id')->references('id')->on('payment_documents');
            $table->unsignedInteger('user_registered_id');
            $table->foreign('user_registered_id')->references('id')->on('users');
            $table->unsignedInteger('user_emitted_id');
            $table->foreign('user_emitted_id')->references('id')->on('users');
            $table->unsignedInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->float('amount_payed',10,2);
            $table->float('amount_total',10,2);
            $table->unsignedInteger('service_type_id');
            $table->foreign('service_type_id')->references('id')->on('service_types');
            $table->unsignedInteger('accounting_id')->nullable();
            $table->foreign('accounting_id')->references('id')->on('accountings');
            $table->unsignedInteger('constitution_id')->nullable();
            $table->foreign('constitution_id')->references('id')->on('constitutions');
            $table->unsignedInteger('extra_id')->nullable();
            $table->foreign('extra_id')->references('id')->on('extras');
            $table->dateTime('emitted_at');
            $table->dateTime('cancelled_at')->nullable();
            $table->dateTime('expired_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
