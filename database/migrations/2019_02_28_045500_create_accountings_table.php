<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('worker_user_id');
            $table->foreign('worker_user_id')->references('id')->on('users');
            $table->unsignedInteger('supervisor_user_id');
            $table->foreign('supervisor_user_id')->references('id')->on('users');
            $table->unsignedInteger('company_id')->unique();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->float('monthly_revenue',10,2);
            $table->integer('monthly_vouchers');
            $table->integer('staff_payroll');
            $table->unsignedInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->unsignedInteger('payment_document_id');
            $table->foreign('payment_document_id')->references('id')->on('payment_documents');
            $table->float('first_rate',10,2);
            $table->float('normal_rate',10,2);
            $table->enum('state', [
                \App\Accounting::RUNNING,
                \App\Accounting::TERMINATED
            ])->default(\App\Accounting::RUNNING);
            $table->enum('electronic_books', [
                \App\Accounting::WITHOUT_ELECTRONIC_BOOKS,
                \App\Accounting::WITH_ELECTRONIC_BOOKS
            ])->default(\App\Accounting::WITHOUT_ELECTRONIC_BOOKS);
            $table->dateTime('electronic_books_date')->nullable();
            $table->text('electronic_books_details')->nullable();
            $table->string('delivery_charge')->nullable();
            $table->dateTime('started_at');
            $table->dateTime('terminated_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accountings');
    }
}
