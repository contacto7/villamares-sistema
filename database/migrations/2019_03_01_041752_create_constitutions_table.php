<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constitutions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id')->unique();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('user_got_id');
            $table->foreign('user_got_id')->references('id')->on('users');
            $table->unsignedInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->float('charged_amount',10,2);
            $table->float('capital_amount',10,2);
            $table->string('reservation_number')->nullable();
            $table->boolean('reservation_service');
            $table->dateTime('reservation_expiration')->nullable();
            $table->integer('partners_number')->nullable();
            $table->integer('additional_managers')->nullable();
            $table->string('registration_charge_picture')->nullable();
            $table->enum('impression_state', [
                \App\Constitution::NOT_PRINTED,
                \App\Constitution::PRINTED_FOR_ONE,
                \App\Constitution::PRINTED_FOR_TWO,
            ])->default(\App\Constitution::NOT_PRINTED);
            $table->float('lawyer_fee',10,2)->nullable();
            $table->float('notary_fee',10,2)->nullable();
            $table->string('public_record')->nullable();
            $table->string('notarial_letter')->nullable();
            $table->integer('directory')->nullable();
            $table->integer('vehicles')->nullable();
            $table->enum('social_object', [
                \App\Constitution::NORMAL_SOCIAL_OBJECT,
                \App\Constitution::TWO_PAGES_SOCIAL_OBJECT
            ])->nullable();
            $table->text('others')->nullable();
            $table->float('devolution',10,2)->nullable();
            $table->string('picture_1')->nullable();
            $table->string('picture_2')->nullable();
            $table->string('picture_3')->nullable();
            $table->string('picture_4')->nullable();
            $table->dateTime('done_at')->nullable();
            $table->dateTime('signed_at')->nullable();
            $table->dateTime('picked_at')->nullable();
            $table->dateTime('delivered_at')->nullable();
            $table->string('delivered_person')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constitutions');
    }
}
