<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function (){

    Route::group(['prefix' => 'users', "middleware" => 'can:view,App\User'], function (){
        Route::get('/admin','UserController@admin')
            ->name('users.admin');
        Route::get('/create','UserController@create')
            ->name('users.create');
        Route::post('/store','UserController@store')
            ->name('users.store');
        Route::get('/edit/{id}','UserController@edit')
            ->name('users.edit');
        Route::put('/update/{user}','UserController@update')
            ->name('users.update');
    });
    Route::group(['prefix' => 'customers'], function (){

        Route::group(["middleware" => 'can:list,App\Customer'], function (){
            Route::get('/list/{slug?}','CustomerController@list')
                ->name('customers.list');
            Route::get('/admin/{slug?}','CustomerController@admin')
                ->name('customers.admin');
            Route::get('/search','CustomerController@filter')
                ->name('customers.search');
        });
        Route::group(["middleware" => 'can:create,App\Customer'], function (){
            Route::get('/create','CustomerController@create')
                ->name('customers.create');
            Route::post('/store','CustomerController@store')
                ->name('customers.store');
            Route::get('/edit/{id}','CustomerController@edit')
                ->name('customers.edit');
            Route::put('/update/{customer}','CustomerController@update')
                ->name('customers.update');
        });

        Route::group(["middleware" => 'can:seeBirthday,App\Customer'], function (){
            Route::get('/near-birthdays','CustomerController@nearBirthdays')
                ->name('customers.nearBirthdays');
        });
        Route::get('/dni-search','CustomerController@dniSearch')
            ->name('customers.dniSearch');
    });
    Route::group(['prefix' => 'vouchers', "middleware" => 'can:list,App\Voucher'], function (){

        Route::group(["middleware" => ['can:create,App\Voucher']], function (){
            Route::get('/create','VoucherController@create')
                ->name('vouchers.create');
            Route::post('/store','VoucherController@store')
                ->name('vouchers.store');
            Route::get('/edit/{id}','VoucherController@edit')
                ->name('vouchers.edit');
            Route::put('/update/{voucher}','VoucherController@update')
                ->name('vouchers.update');
        });

        Route::get('/list/{slug?}','VoucherController@list')
            ->name('vouchers.list');
        Route::get('/admin/{id}','VoucherController@admin')
            ->name('vouchers.admin');
        Route::get('/downloadPDF/{id}','VoucherController@downloadPDF')
            ->name('vouchers.downloadPDF');
        Route::get('/debts','VoucherController@debts')
            ->name('vouchers.debts');
        Route::get('/accounting/{id}','VoucherController@accounting')
            ->name('vouchers.accounting');
        Route::get('/company/{id}','VoucherController@company')
            ->name('vouchers.company');
        Route::get('/customer/{id}','VoucherController@customer')
            ->name('vouchers.customer');
        Route::get('/search','VoucherController@filter')
            ->name('vouchers.search');
        Route::get('/number-search','VoucherController@numberSearch')
            ->name('vouchers.numberSearch');
        Route::delete('/destroy/{voucher}','VoucherController@destroy')
            ->name('vouchers.destroy');

        /*
         * DESCARGAR EN EXCEL LAS DEUDAS
         *
         * */
        Route::get('importExport', 'VoucherController@importExport')
            ->name('vouchers.importExport');
        Route::get('downloadExcel', 'VoucherController@downloadExcel')
            ->name('vouchers.downloadExcel');
        Route::post('importExcel', 'VoucherController@importExcel')
            ->name('vouchers.importExcel');
    });
    Route::group(['prefix' => 'companies'], function (){
        Route::group(["middleware" => ['can:list,App\Company']], function (){
            Route::get('/search','CompanyController@filter')
                ->name('companies.search');
            Route::get('/list/{slug?}','CompanyController@list')
                ->name('companies.list');
            Route::get('/admin/{id}','CompanyController@admin')
                ->name('companies.admin');
            Route::get('/user/{id}','CompanyController@user')
                ->name('companies.user');
            Route::get('/create','CompanyController@create')
                ->name('companies.create');
            Route::post('/store','CompanyController@store')
                ->name('companies.store');
            Route::get('/edit/{id}','CompanyController@edit')
                ->name('companies.edit');
            Route::put('/update/{company}','CompanyController@update')
                ->name('companies.update');
        });
        Route::get('/ruc-search','CompanyController@rucSearch')
            ->name('companies.rucSearch');
    });
    Route::group(['prefix' => 'movements', "middleware" => 'can:view,App\CashFlow'], function (){
        Route::get('/search','MovementController@filter')
            ->name('movements.search');
        Route::get('/list/{slug?}','MovementController@list')
            ->name('movements.list');
        Route::get('/consolidate','MovementController@consolidate')
            ->name('movements.consolidate');
    });
    Route::group(['prefix' => 'cash-flow', "middleware" => 'can:view,App\CashFlow'], function (){
        Route::get('/list/{slug?}','CashFlowController@list')
            ->name('cashFlows.list');
        Route::get('/search','CashFlowController@filter')
            ->name('cashFlows.search')->middleware('can:filter,App\CashFlow');
        Route::get('/admin/{id}','CashFlowController@admin')
            ->name('cashFlows.admin');
        Route::get('/download/{picture_voucher}','CashFlowController@getDownload')
            ->name('cashFlows.download');
        Route::get('/create','CashFlowController@create')
            ->name('cashFlows.create');
        Route::post('/store','CashFlowController@store')
            ->name('cashFlows.store');
        Route::get('/edit/{id}','CashFlowController@edit')
            ->name('cashFlows.edit');
        Route::put('/update/{cashFlow}','CashFlowController@update')
            ->name('cashFlows.update');
        Route::put('/updateState/{cashFlow}','CashFlowController@updateState')
            ->name('cashFlows.updateState');
        Route::put('/arqueo','CashFlowController@arqueo')
            ->name('cashFlows.arqueo');
    });
    Route::group(['prefix' => 'cash-register', "middleware" => 'can:list,App\CashRegister'], function (){
        Route::get('/list/search','CashRegisterController@filter')
            ->name('cashRegister.search');
        Route::get('/list/{slug?}','CashRegisterController@list')
            ->name('cashRegister.list');
        Route::get('/search','CashRegisterController@filter')
            ->name('cashRegister.search')->middleware('can:seeAllMovement,App\CashRegister');
    });
    Route::group(['prefix' => 'accountings', "middleware" => 'can:view,App\Accounting'], function (){
        Route::get('/list/{slug?}','AccountingController@list')
            ->name('accountings.list');
        Route::get('/admin/{id}','AccountingController@admin')
            ->name('accountings.admin');
        Route::get('/search','AccountingController@filter')
            ->name('accountings.search');
        Route::get('/download/{delivery_charge}','AccountingController@getDownload')
            ->name('accountings.download');
        Route::get('/create','AccountingController@create')
            ->name('accountings.create');
        Route::post('/store','AccountingController@store')
            ->name('accountings.store');
        Route::get('/edit/{id}','AccountingController@edit')
            ->name('accountings.edit');
        Route::put('/update/{accounting}','AccountingController@update')
            ->name('accountings.update');
    });
    Route::group(['prefix' => 'constitutions', "middleware" => 'can:view,App\Constitution'], function (){
        Route::get('/list/{slug?}','ConstitutionController@list')
            ->name('constitutions.list');
        Route::get('/admin/{id}','ConstitutionController@admin')
            ->name('constitutions.admin');
        Route::get('/search','ConstitutionController@filter')
            ->name('constitutions.search');
        Route::get('/create','ConstitutionController@create')
            ->name('constitutions.create')->middleware('can:addConstitution,App\Constitution');
        Route::post('/store','ConstitutionController@store')
            ->name('constitutions.store')->middleware('can:addConstitution,App\Constitution');
        Route::get('/edit/{id}','ConstitutionController@edit')
            ->name('constitutions.edit')->middleware('can:addConstitution,App\Constitution');
        Route::put('/update/{constitution}','ConstitutionController@update')
            ->name('constitutions.update')->middleware('can:addConstitution,App\Constitution');
    });
    Route::group(['prefix' => 'extras', "middleware" => 'can:view,App\Extra'], function (){
        Route::get('/list/{slug?}','ExtraController@list')
            ->name('extras.list');
        Route::get('/admin/{id}','ExtraController@admin')
            ->name('extras.admin');
        Route::get('/customer/{id}','ExtraController@customer')
            ->name('extras.customer');
        Route::get('/search','ExtraController@filter')
            ->name('extras.search');
        Route::get('/create','ExtraController@create')
            ->name('extras.create');
        Route::post('/store','ExtraController@store')
            ->name('extras.store');
        Route::get('/edit/{id}','ExtraController@edit')
            ->name('extras.edit');
        Route::put('/update/{extra}','ExtraController@update')
            ->name('extras.update');
    });
    Route::group(['prefix' => 'notes'], function (){
        Route::get('/list/{slug?}','NoteController@list')
            ->name('notes.list');
        Route::get('/admin/{id}','NoteController@admin')
            ->name('notes.admin');
        Route::get('/customer/{id}','NoteController@customer')
            ->name('notes.customer');
        Route::get('/search','NoteController@filter')
            ->name('notes.search');
        Route::get('/create','NoteController@create')
            ->name('notes.create');
        Route::post('/store','NoteController@store')
            ->name('notes.store');
        Route::get('/edit/{id}','NoteController@edit')
            ->name('notes.edit');
        Route::put('/update/{note}','NoteController@update')
            ->name('notes.update');
        Route::get('/modalSeeForm','NoteController@modalSeeForm')
            ->name('notes.modalSeeForm');
    });
    Route::group(['prefix' => 'company-notes'], function (){
        Route::get('/list/{slug?}','CompanyNoteController@list')
            ->name('companyNotes.list');
        Route::get('/admin/{id}','CompanyNoteController@admin')
            ->name('companyNotes.admin');
        Route::get('/customer/{id}','CompanyNoteController@customer')
            ->name('companyNotes.customer');
        Route::get('/search','CompanyNoteController@filter')
            ->name('companyNotes.search');
        Route::get('/create','CompanyNoteController@create')
            ->name('companyNotes.create');
        Route::post('/store','CompanyNoteController@store')
            ->name('companyNotes.store');
        Route::get('/modalSeeForm','CompanyNoteController@modalSeeForm')
            ->name('companyNotes.modalSeeForm');
    });
    Route::group(['prefix' => 'books'], function (){
        Route::get('/missing','BookController@missing')
            ->name('books.missing');
        Route::get('/search','BookController@filter')
            ->name('books.filter');
        Route::get('/modalForm','BookController@modalForm')
            ->name('books.modalForm');
        Route::get('/create','BookController@create')
            ->name('books.create');
        Route::post('/store','BookController@store')
            ->name('books.store');
        Route::get('/edit/{id}','BookController@edit')
            ->name('books.edit');
        Route::put('/update/{book}','BookController@update')
            ->name('books.update');
    });

});

Route::get('images/{path}/{attachment}', function ($path, $attachment){
    $file = sprintf('storage/%s/%s', $path, $attachment);

    if(File::exists($file)){
        return \Intervention\Image\Facades\Image::make($file)->response();
    }else{
        return \Intervention\Image\Facades\Image::make('image/not-found.png')->response();
    }
});

